# Book settings
# Learn more at https://jupyterbook.org/customize/config.html

title: Soft matter physics
author: Timon Idema
logo: content/images/TUDelft.svg
description: Soft matter physics. The book was developed for a course in the Nanobiology and Applied Physics programmes of TU Delft. We recap continuum mechanics (elasticity and fluid dynamics, with summaries of thermodynamics and statistical physics), and cover polymers, solutions, viscoelastic materials, membranes and liquid crystals. We also discuss dynamics in soft matter systems.
copyright: Delft University of Technology, CC BY 4.0

# Only build files in the ToC to avoid building README, etc.
only_build_toc_files: true

# Force re-execution of notebooks on each build.
# See https://jupyterbook.org/content/execute.html
execute:
  execute_notebooks: force

# Define the name of the latex output file for PDF builds
latex:
  latex_documents:
    targetname: SoftMatterBook.tex

# Add a bibtex file so that we can create citations
bibtex_bibfiles:
  - physics.bib

# Sphinx, for html formatting. Needs checking version.
# Mathjax config allows us to define tex macros. Note that we need to escape the \ in a latex math command with an extra \.
# I have no idea what the html_js_files link does (TODO!). See https://cdnjs.com/libraries/require.js and  https://cdnjs.cloudflare.com
# Unfortunately, the mathjax3_config loader options break (!) mathjax for markdown files that have a jupytext yaml block. But not loading prevents us from using the physics or boldmatch packages. Clearly this is a bug, needs reporting.
# The bibtex_reference_style is 'Use numeric reference labels, citations are sorted by order of appearance.' Alternatives include alpha, plain, and unsrtalpha. See https://jupyterbook.org/en/stable/content/citations.html
# We don't use this option as we go for local references instead, adding them to individual 'chapter' pages.
# The sphinx_proof extension requires installing the sphinx-proof package, see https://jupyterbook.org/en/stable/content/proof.html
# Likewise, the sphinx_exercise requires installing the sphinx-exercise package.
# The local extension applet.py allows us to include applets (developed by Julia van de Kris and Abel de Bruijn for the linear algebra open textbook; source https://gist.github.com/yustarandomname/2be0f45758d0fe3d2d765da4e621a547). See https://jupyterbook.org/en/stable/advanced/sphinx.html#local-sphinx-extensions about the local extensions.
sphinx:
  config:
    # html_js_files necessary for interactive plot (plotly) in ch. 2, but breaks applet in ch. 3. cf. https://jupyterbook.org/en/stable/interactive/interactive.html?highlight=html_js_files#plotly
    language: en
    html_js_files:
    - https://cdnjs.cloudflare.com/ajax/libs/require.js/2.3.6/require.min.js
    mathjax3_config:
      #loader: {load: ['[tex]/ams', '[tex]/physics', '[tex]/boldsymbol']}
      loader: {load: ['[tex]/autoload', '[tex]/ams'], '[tex]/physics'}
      tex:
        packages: {'[+]': ['autoload', 'ams', 'physics', 'boldsymbol']}
        tags: 'ams'
        macros:
          "kB" : "k_\\mathrm{B}"
          "Rn" : "\\mathrm{Re}"
          #"bm" : "\\boldsymbol"
          #"bm": "\\vb*"
          "bm": "\\mathbf"
          "inprod" : "\\innerproduct"
          "diff" : "\\pdv"
          "me": "m_\\mathrm{e}"
    bibtex_reference_style: author_year # or unsrt, alpha, ...
#  local_extensions: # For the applet inclusion.
#    applet: _ext/
    # Light and dark version of logo. NB: including this removes the repo button - no clue yet how to fix that.
#    html_theme_options:
#        logo:
#            image_light: content/images/TUDelft.svg
#            image_dark: content/images/TUDelft-dark.svg
  extra_extensions:
    - sphinx_proof
    - sphinx_exercise
    - sphinx_togglebutton
#    - sphinx_tojupyter


# Parse, for processing LaTeX-style math. See https://jupyterbook.org/en/stable/content/math.html#latex-style-math and https://myst-parser.readthedocs.io/en/latest/syntax/optional.html#syntax-amsmath
parse:
  myst_enable_extensions:
    # don't forget to list any other extensions you want enabled,
    # including those that are enabled by default!
    # meaning that we have to explicitly include dollarmath!
    # Colon-fence is to allow for ::: instead of ``` blocks.
    - amsmath
    - dollarmath

# Information about where the book exists on the web
repository:
  url: https://gitlab.tudelft.nl/opentextbooks/soft-matter-physics/  # Online location of your book
  # path_to_book: docs  # Optional path to your book, relative to the repository root
  branch: master  # Which branch of the repository should be used when creating links (optional)

# HTML specific settings - see https://jupyterbook.org/en/stable/customize/config.html
# Add GitHub / GitLab buttons to your book
# See https://jupyterbook.org/customize/config.html#add-a-link-to-your-repository
html:
  favicon: "content/images/favicon.ico"
  #use_issues_button: true
  use_edit_page_button: true
  use_repository_button: true
  extra_navbar: "" # Will be displayed underneath the left navbar. This setting overrides the default 'powered by Jupyter Book'
  comments:
    hypothesis: false # Hypothesis for comments
