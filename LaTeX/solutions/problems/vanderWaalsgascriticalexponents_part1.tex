% Problemtitle: Critial exponents in the Van der Waals gas
% Source: Callen examples pages 240-241 and pages 268-270
% Note: shortened significantly for 2023 edition, made second part optional.
The equation of state of a Van der Waals gas is given by
\begin{equation}
\label{vanderWaalsgas}
p = \frac{N \kB T}{V-Nb} - \frac{a}{(V/N)^2},
\end{equation}
where $a$ and $b$ are constants. In contrast to the ideal gas, the Van der Waals gas allows for the coexistence of two states with different values of the number density $n = N/V$. In this problem, we'll derive the conditions for these phases to coexist, and find the critical exponents associated with the coexistence line in the phase diagram.
\begin{enumerate}[(a)]
\ifproblemset\item Give a physical interpretation of the parameters $a$ and $b$. \textit{Hint}: first determine their dimensions and compare equation~(\ref{vanderWaalsgas}) with the ideal gas law.\fi
%\item Expand this equation of state in a virial expansion, and express the virial coefficients in terms of $a$ and $b$. % Callen 13.3-2.
\item Find the critical temperature, density and pressure for a Van der Waals gas. We'll denote these as $T^*$, $n^*$ and $p^*$, respectively.
\item Re-write the equation of state in terms of rescaled parameters $\tilde{n} = n/n^*$, $\tilde{p} = p/p^*$ and $\tilde{T} = T/T^*$.
\item Expand the equation of state around $\hat{n} = (n-n^*)/n^* = \tilde{n} - 1$ (i.e., expand $\tilde{n}$, but about $1$, not about $0$) to find the critical exponent~$\delta$, i.e., the scaling of $\hat{n}$ with $\hat{p} = \tilde{p} - 1$.
\item Determine the critical exponent~$\gamma$ of the susceptibility~$\chi$, here given by
\[ \chi^{-1} = \left[ \frac{1}{\hat{n}+1} \left( \frac{\dd \hat{p}}{\dd \hat{n}} \right)_T \right]_{\hat{n} \to 0}. \]
(Note that from the relation between $p(n)$ and $f(n)$ you found in problem~\ref{pb:virialexpansionphasediagram}, you can easily check that this susceptibility is the same as $(\dd^2 f / \dd n^2)^{-1}$, which was the way we calculated it in class).
\end{enumerate}

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\ifproblemset\item From the denominator in the first term, $V - Nb$, we can read off that compared to an ideal gas, the total available volume shrinks with $Nb$. We can thus interpret $b$ as the volume per particle.

The second term reduces the pressure compared to an ideal gas. The pressure in an ideal gas originates from collisions of the particles with the walls of the box that contains the gas. There are no other interactions. In a real gas, the particles also attract each other. For a particle in the bulk, these attractive forces from other particles cancel, but for a particle near the wall, there is a net attraction to the bulk, which reduces the net pressure on the walls. The scaling is because the reduction depends on the number of particle pairs.\fi
%\item The virial expansion is in the density $n$, so we first need to re-write the given equation of state, then expand it as a series in $n$:
%\[ p = \frac{(N/V) \kB T}{1 - (N/V) b} - a (N/V)^2 = \frac{n}{1-nb}\kB T - a n^2 = n \kB T + (b \kB T - a) n^2 + b^2 \kB T n^3 + \mathcal{O}\left(n^4\right).\]
%We can thus read off that $B_2(T) = b - a/\kB T$ and $B_3(T) = b^2$.
\item At the critical point, the first and second derivatives of the pressure as a function of the density (or, equivalently, the volume per particle~$v$) vanish. We have
\begin{align*}
\frac{\partial p}{\partial n} &= \frac{\kB T}{1-n b} \left(1+\frac{nb}{1-nb} \right) - 2 a n, \\
\frac{\partial^2 p}{\partial n^2} &= \frac{2 b \kB T}{(1-nb)^2}\left(1+\frac{nb}{1-nb} \right) - 2a.
\end{align*}
Equating the two derivatives to zero, we can easily find the critical density and temperature:
\[ n^* = \frac{1}{3b}, \quad \kB T^* = \frac{8 a}{27 b}. \]
Substituting back, we find for the critical pressure $p^* = a/(27b^2)$.
\item We define
\begin{align*}
\tilde{n} &= \frac{n}{n^*} = 3 b n, \\
\tilde{p} &= \frac{p}{p^*} = \frac{27 b^2}{a} p, \\
\tilde{T} &= \frac{T}{T^*} = \frac{27 b}{8a} \kB T.
\end{align*}
From the equation of state, we find
\[ \tilde{p} = \frac{27 b^2}{a} p = \frac{27 b^2}{a} \left[ \frac{\tilde{n}/3b}{1-\tilde{n}/3} \frac{8 a}{27 b} \tilde{T} - \frac{a}{9 b^2} \tilde{n}^2 \right] = \frac{8\tilde{n}}{3-\tilde{n}} \tilde{T} - 3\tilde{n}^2. \]
\item To expand around the critical point, we now introduce $\hat{n} = \tilde{n} - 1$, which gives
\[ \tilde{p} = \frac{8(\hat{n}+1)}{2-\hat{n}} \tilde{T} - 3(1+\hat{n})^2. \]
For the isotherm $\tilde{T} = 1$. Expanding around $\hat{n} = 0$, we find
\[ \tilde{p} = 1 + \frac32 \hat{n}^3 + \mathcal{O}\left(n^4\right), \]
so $\hat{n} \sim \hat{p}^{1/3}$, where $\hat{p} = \tilde{p} - 1$, and $\delta = 3$.
\item For the susceptibility $\chi$ we find
\begin{align*}
\chi^{-1} &= \left[ \frac{1}{\hat{n}+1} \left( \frac{\dd \hat{p}}{\dd \hat{n}} \right)_T \right]_{\hat{n} \to 0} \\
&= \left[ \frac{1}{1+\hat{n}} \left( \frac{8(2-\hat{n})+8(\hat{n}+1)}{(2-\hat{n})^2} (\hat{T}+1) - 6 (1+\hat{n}) \right) \right]_{\hat{n} \to 0} \\
&= \left[ \frac{24}{(1+\hat{n})(2-\hat{n})^2} (\hat{T}+1) - 6 \right]_{\hat{n} \to 0} = 6 \hat{T},
\end{align*}
so $\chi \propto \hat{T}^{-1}$, and $\gamma = 1$.

(To relate the susceptibility from the notes (inverse of the second derivative of $f(n)$), simply substitute $p(n) = -f(n) + n \dd f / \dd n$ in $\chi^{-1} = (1/n)(\dd p / \dd n)$.)
\end{enumerate}
\fi
