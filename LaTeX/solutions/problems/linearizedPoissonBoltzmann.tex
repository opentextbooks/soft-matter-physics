% Problemtitle The linearized Poisson-Boltzmann equation
% Reference: see https://m.tau.ac.il/~andelman/reprints/121_NATO_Edinburgh_elec_2005.pdf;  inspired by the calculation in Strobl, section 3.3, pages 90-92.
Solve the linearized Poisson-Boltzmann equation \ifproblemset
\begin{equation}
\label{PoissonBoltzmanneq}
\nabla^2 \psi(\bvec{r}) = \frac{1}{\xi_\mathrm{D}^2} \psi(\bvec{r}),
\end{equation}
where
\begin{equation}
\label{Debyelength}
\xi_\mathrm{D} = \sqrt{\frac{\varepsilon \kB T}{2 c_0 e^2}}
\end{equation}
is the Debye length, for
\else (\bookref{PoissonBoltzmanneq}) for\fi
\begin{enumerate}[(a)]
\item a central charge with spherical symmetry (for which $\nabla^2 = r^{-2} \frac{\dd}{\dd r} \left( r^2 \frac{\dd}{\dd r}\right)$),
\item a central charge in the form of an infinite cylinder with radius~$r_0$ and charge density~$\lambda$.
\ifproblemset\item a charged surface (exposed to a half-space with an ionic solution, see figure~\ref{fig:chargedsurfacePB} below) with charge density~$\sigma$.\fi
\end{enumerate}
\ifproblemset
In all three cases, first find the general solution, then use the boundary conditions (at infinity or at the surface) to derive the expressions given in section~\bookref{sec:ionicsolutions} of the notes.

\begin{figure}[h]
\centering
\includegraphics[scale=1]{solutions/problems/chargedsurfacePB.pdf}
\caption{Setting for problem~\ref{pb:PoissonBoltzmann}c: A charged surface with surface charge density~$\sigma$ (gray), exposed to an ionic solution (white).}
\label{fig:chargedsurfacePB}
\end{figure}
\else
In both cases, first find the general solution, then use the boundary conditions (at infinity or at the surface) to derive the expressions given in section~\bookref{sec:ionicsolutions}.
\fi


\ifincludesolutions
\Solution
Note that the boundary conditions are on the field $\bvec{E} = - \bvec{\nabla} \psi$, so we have boundary conditions on the derivative of $\psi$ at the surface.
\begin{enumerate}[(a)]
\item Re-writing the equation, we find
\[ \frac{\dd^2\psi}{\dd r^2} + \frac{2}{r} \frac{\dd\psi}{\dd r} - \frac{1}{\xi_\mathrm{D}^2} \psi = 0, \]
or
\[ r^2 \frac{\dd^2\psi}{\dd r^2} + 2 r \frac{\dd\psi}{\dd r} - \frac{r^2}{\xi_\mathrm{D}^2} \psi = 0. \]
We can easily solve the equation from an Ansatz $\psi(r) = r^{-n} \exp(-\lambda r)$, which gives
\[ \psi'(r) = -\left(\lambda + \frac{n}{r} \right) \psi(r), \quad \mbox{and} \quad \psi''(r) = \left( \lambda^2 + \frac{2\lambda n}{r} + \frac{n(n+1)}{r^2} \right) \psi(r). \]
Substituting back into the differential equation, we get
\[ n(n+1) + 2\lambda n r + \lambda^2 r^2 - 2 \lambda r - 2 n - \frac{r^2}{\xi_\mathrm{D}^2} = 0, \]
which only holds if all terms in $r$ are zero. The zeroth order term gives $n^2 + n - 2n = n(n-1) = 0$, so $n=0$ or $n=1$. For $n=0$, the first-order term can only vanish if $\lambda = 0$, but then the second-order term doesn't vanish, so we need $n=1$. For $n=1$, the first-order term vanishes, and the second-order term gives $\lambda = \pm \xi_\mathrm{D}$. The general solution thus reads:
\[ \psi(r) = \frac{A}{r} e^{-r/\xi_\mathrm{D}} + \frac{B}{r} e^{r/\xi_\mathrm{D}}. \]
The second term in the general solution blows up as $r \to \infty$, so it is unphysical. The charge~$Q$ at the origin generates a potential $Q / 4 \pi \varepsilon r$ (with $4 \pi$ from the solid angle), so we get
\[ \psi(r) = \frac{Q}{4 \pi \varepsilon r} e^{-r/\xi_\mathrm{D}}. \]
\item Re-writing the equation, we find
\[ r^2 \frac{\dd^2\psi}{\dd r^2} + r \frac{\dd\psi}{\dd r} - \frac{r^2}{\xi_\mathrm{D}^2} \psi = 0, \]
which we (hopefully) recognize as a (modified) Bessel equation (modified because the last term has a minus sign), of which the general solution is
\[ \psi(r) = A K_0(r/\xi_\mathrm{D}) + B I_0(r/\xi_\mathrm{D}). \]
The second term again blows up, so we set it to zero; for a line charge density $\lambda$ at $r=r_0$ we get a potential $- \lambda / 2 \pi \varepsilon r_0$ (see problem 1a), so we get
\[ \left.\frac{\dd \psi}{\dd r}\right|_{r=r_0} = - \frac{A}{\xi_\mathrm{D}} K_1\left(\frac{r_0}{\xi_\mathrm{D}}\right) = - \frac{\lambda}{2\pi\varepsilon r_0} \quad \Rightarrow \quad A = \frac{\lambda \xi_\mathrm{D}}{2 \pi \varepsilon r_0 K_1(r_0/\xi_\mathrm{D})}, \]
so
\[ \psi(r) = \frac{\lambda \xi_\mathrm{D}}{2 \pi \varepsilon r_0 K_1(r_0/\xi_\mathrm{D})} K_0(r/\xi_\mathrm{D}). \]
\item This case is actually the easiest, as (taking $x$ to be the direction perpendicular to the plates), we now get \[ \frac{\dd^2\psi}{\dd x^2} - \frac{1}{\xi_\mathrm{D}^2} \psi = 0, \]
of which we can immediately identify the solutions as $\exp(\pm x / \xi_\mathrm{D})$. As the solution with the positive exponent blows up again, we're left with the negative one. The boundary condition is $\dd\psi/\ddx = -\sigma/\varepsilon$, which gives 
\[ \psi(x) = \frac{\sigma \xi_\mathrm{D}}{\varepsilon} e^{-x/\xi_\mathrm{D}}. \]
\end{enumerate}
\fi