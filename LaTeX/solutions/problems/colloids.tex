% Problemtitle Colloid phases
% Midterm 2019
Colloids are small particles that are typically between~$100\;\mathrm{nm}$ and~$100\;\mu\mathrm{m}$ in size. They are often used as a model system for molecules, with the huge added benefit that they can be observed directly in a light microscope. Colloids in solution can exist in various phases; an ongoing debate in soft matter is whether excluded-volume interactions alone could be sufficient to allow colloids to crystallize, reaching a solid state. In this problem we'll consider two models for the fluid and solid / crystalline state of simple spherical colloids.

The Carnahan-Starling expression for the free energy per unit volume of a fluid of hard spheres with diameter~$a$ is given by:
\begin{equation}
\label{CSfreeenergy}
f(\phi) = \frac{\kB T}{V} \left[ \phi \log\left(\lambda^3 \phi\right) - \phi + \frac{4 \phi^2 - 3\phi^3}{(1-\phi)^2} \right],
\end{equation}
where $\phi = \frac43 \pi a^3 N / V$ is the packing fraction of the spheres, and $\lambda$ is a model parameter. See figure~\ref{fig:colloidcurves} (blue line).
\begin{enumerate}[(a)]
\item From equation~(\ref{CSfreeenergy}), find the chemical potential $\mu(\phi)$ and the pressure $p(\phi)$ of the hard-sphere fluid.
\item Will the Carnahan-Starling free energy allow for the coexistence of a liquid and a gas phase?
\ifproblemset\setcounter{problemcounter}{\value{enumi}}\else\setcounter{problemcounter}{\value{enumii}}\fi
\end{enumerate}
The Carnahan-Starling free energy is often combined with a cell model for a hard-sphere solid, of which the free energy is given by
\begin{equation}
\label{colloidsolidfreeenergy}
f(\phi) = \frac{\kB T}{V} \phi \log \left[ \lambda^3 \phi\left(1 - \sqrt[3]{\frac{\phi}{\phi_\mathrm{cp}}}\right)^{-3}\right],
\end{equation}
where $\phi_\mathrm{cp}$ is the density of the colloids at `close packing' (when they all touch). See figure~\ref{fig:colloidcurves} (orange line).
\begin{enumerate}[(a)]
\ifproblemset\setcounter{enumi}{\value{problemcounter}}\else\setcounter{enumii}{\value{problemcounter}}\fi
\item From figure~\ref{fig:colloidcurves}, find the packing fractions at which the fluid and solid phases coexist. %Explain how you found your answer and what the physics behind that approach is. The figure is printed separately, you should hand in the sheet with your answers, don't forget to put your name on it.
\end{enumerate}

\begin{figure}[ht]
\centering
\includegraphics[scale=1]{solutions/problems/colloidcurves.pdf}
\caption[Free energy of the fluid and solid phases of colloid solutions.]{Plots of the free energies per unit volume (and per $\kB T$) of the fluid phase (equation~\ref{CSfreeenergy}) and the solid phase (equation~\ref{colloidsolidfreeenergy}) as a function of packing fraction of hard spheres.}
\label{fig:colloidcurves}
\end{figure}

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item The chemical potential is simply the derivative of the free energy per unit volume \score{0.5 pt}:
\begin{align}
\label{CSchempot}
\mu(\phi) = \diff{f}{\phi} &= \frac{\kB T}{V} \left[ \log\left(\lambda^3 \phi\right) + 1 - 1 + \frac{8 \phi - 9\phi^2}{(1-\phi)^2} + \frac{2(4\phi^2-3\phi^3)}{(1-\phi)^3} \right] \nonumber\\
&= \frac{\kB T}{V} \left[ \log\left(\lambda^3 \phi\right) + \frac{\phi(3\phi^2 - 9\phi + 8)}{(1-\phi)^3}\right]
\end{align}
\score{1 pt}. For the (osmotic) pressure, we need the difference between the free energy and $\phi$ times its derivative \score{0.5 pt}:
\begin{align}
\label{CSpressure}
p(\phi) &= -f(\phi) + \phi \diff{f}{\phi} \nonumber\\
&= \frac{\kB T}{V} \left[ -\phi \log\left(\lambda^3 \phi\right) + \phi - \frac{4 \phi^2 + 3\phi^3}{(1-\phi)^2} + \phi \log\left(\lambda^3 \phi\right) + \phi \frac{8 \phi - 9\phi^2}{(1-\phi)^2} - \phi \frac{2(4\phi^2-3\phi^3)}{(1-\phi)^3}\right] \nonumber \\
&= -\frac{\kB T}{V} \phi \frac{1 - \phi - \phi^2 + \phi^3}{(1-\phi)^3}.
\end{align}
\score{1 pt}.
\item As we can see from figure~\ref{fig:colloidcurves}, at least for part of the parameter range, the free energy only has a single minimum. For this choice of parameters, there is therefore only a single phase in equilibrium. For a second phase to emerge, there needs to be a critical point, at which the second derivative of the free energy vanishes. We should find that point as we vary the parameter~$\lambda$. However, only the first term of $f(\phi)$ depends on $\lambda$, and its second derivative is independent of $\lambda$:
\begin{equation}
\frac{\partial^2 \left[ \phi \log(\lambda^3 \phi) \right]}{\partial \phi^2} = \frac{1}{\phi}.
\end{equation}
Therefore, we cannot change the sign of the second derivative of $\phi$ by changing $\lambda$. As we know that for a given value of~$\lambda$ there is only one phase, this must be true for all values. The Carnahan-Starling free energy therefore does not allow for the coexistence of a liquid and a gas phase \score{1 pt}.
\item Two phases can coexist if their temperatures, pressures, and chemical potentials are equal \score{0.5 pt}. As the plots in figure~\ref{fig:colloidcurves} are per unit $\kB T$, the temperature is no longer a parameter in the free energy. The chemical potential is the derivative of the free energy, so for coexisting phases, we need two points with the same derivative; the pressure is given by $p(\phi) = -f(\phi) + \phi \partial f / \partial \phi$, which means that the coexistence points must lie on each other's tangent lines. We can thus find the coexistence points by finding points on the $f(\phi)$ curves with a common tangent line (as then both conditions are met) \score{0.5 pt}. From the figure, we can easily approximate such a line (with a ruler) which gives coexistence values of $\phi_\mathrm{fluid} = 0.609$ and $\phi_\mathrm{solid} = 0.661$ \score{1 pt; any values from a correct construction to be counted as correct}.
\end{enumerate}

\begin{figure}[ht]
\centering
\includegraphics[scale=1]{solutions/problems/colloidcurvessolution.pdf}
\caption{Same plot as figure~\ref{fig:colloidcurves} with the common-tangent construction to find the packing fractions at which the liquid and solid phases coexist.}
\label{fig:colloidcurvessolution}
\end{figure}
\fi