\begin{enumerate}[\thechapter .1]
%\item The Gibbs free energy $G(T, p, N_i)$ is a function of temperature, pressure and particle numbers, defined as $G = E - TS + pV$. Let $N = \sum_i N_i$ be the total number of particles, then $g = G/N$ is the Gibbs energy per particle. For a closed system, $N$ is fixed, which gives a constraint on the variables $N_i$; if there are $C$ particle species, $g$ is then a function of $C+1$ variables ($T$, $p$, and $x_i = N_i/N$ for $C-1$ values of $i$). Suppose that we know that $g$ is a convex function of $x_1, x_2, \ldots, x_{C-1}$. Show that $g$ is then also a convex function after a change of variables (using the given constraint) to $x_2, x_3, \ldots, x_C$ (i.e., you have to show that the property of $g$ being convex is independent of which of the $x_i$ you eliminate as the `redundant' one). % Callen 9.6-2.
\item \input{solutions/problems/VanderWaalsgas}
\item Measurements of the second virial coefficient of some simple gases (such as helium, hydrogen and nitrogen) suggest that they can be described quite reasonably by an equation of the form $B_2 = B_0 - B_1/T$. We assume all higher-order coefficients to vanish. Based on this virial expansion, calculate both $C_\mathrm{V}(T,V)$ and $C_\mathrm{p}(T,V)$ for monatomic and diatomic gases. % Based on Callen 13.3-4.
\item \label{pb:virialexpansionphasediagram} \input{solutions/problems/virialexpansionphasediagram}
\newpage
\item \label{pb:Maxwellequalareaconstruction} Given an equation of state, there are several ways to find the coexistence line in a ($p$, $T$) phase diagram. We could try to find the full equation for the free energy as in problem~\ref{pb:virialexpansionphasediagram}, but we'll use a different technique instead, that also applies when solving the differential equation in problem~\ref{pb:virialexpansionphasediagram} proves difficult. It is known as the \index{Maxwell equal area construction}\emph{Maxwell equal area construction}, where the `equal area' refers to area under the $p, v$ curve, with $v = V/N = 1/n$. An example $p(v)$ curve for two coexisting phases is shown below.
\begin{center}
\includegraphics[scale=1]{solutions/equalareaconstructionplot.pdf}
\end{center}
As you know, phase coexistence requires equal temperatures, pressures, and chemical potentials. The equal-area construction follows from the latter.

To calculate the chemical potential, we'll use the Gibbs-Duhem relation (see appendix~\bookref{sec:GibbsDuhem}) between the intrinsic variables:
\begin{equation}
0 = S \dd T - V \dd p + N \dd \mu.
\end{equation}
\begin{enumerate}[(a)]
\item From the Gibbs-Duhem relation, show that the difference in the chemical potential between two states at equal temperature is given by
\[ \mu_2 - \mu_1 = \int_1^2 v(p) \dd p, \]
where $v = V/N = 1/n$.
\item For coexisting phases, the chemical potentials are equal, so the integral in~(a) evaluates to zero. Convince yourself that that condition is equal to the condition that the areas I and II in the $p(v)$ plot are equal (note that in the integral, we have $v(p)$, not $p(v)$).
\item Express the equal-area condition in terms of an integral over $p(v)$.
\item In the integral, change variables from $v$ to $n$, then substitute the pressure $p(n)$ as a function of the number density $n=N/V$ of the Van der Waals gas, as given by equation~(\ref{vanderWaalsgas}), and find the condition on $n_1$ and $n_2$ for coexistence. (You could also first integrate and then change variables; you'll get the same result).
\end{enumerate}

\item \label{pb:vanderWaalscritexp} % Callen examples pages 240-241 and pages 268-270.
\begin{enumerate}[(a)]
\item Find the critical temperature and pressure for a Van der Waals gas, with equation of state given by~(\ref{vanderWaalsgas}).
\item Re-write the equation of state in terms of $\tilde{n} = n/n^*$ and similar re-scaled parameters for $p$ and $T$, then expand around $\hat{n} = (n-n^*)/n^* = \tilde{n} - 1$ (i.e., expand $\tilde{n}$, but about $1$, not about $0$) to find the critical exponent~$\delta$, i.e., the scaling of $\hat{n}$ with $\hat{p}$.
\item Determine the critical exponent~$\gamma$ of the susceptibility~$\chi$, here given by
\[ \chi^{-1} = \left[ \frac{1}{\hat{n}+1} \left( \frac{\dd \hat{p}}{\dd \hat{n}} \right)_T \right]_{\hat{n} \to 0}. \]
(Note that from the relation between $p(n)$ and $f(n)$ you found in problem~\ref{pb:virialexpansionphasediagram}, you can easily check that this susceptibility is the same as $(\dd^2 f / \dd n^2)^{-1}$, which was the way we calculated it in class).
\item Finally, use the two conditions for coexistence to find the dependence of $\hat{n} = \tilde{n}-1$ on $\hat{T} = \tilde{T}-1$ to lowest order, and thus the value of the critical exponent~$\beta$ (you may find the result of problem~\ref{pb:Maxwellequalareaconstruction} useful here).
%\item Calculate the critical exponents $\beta$ (order parameter, here the density~$n$), $\gamma$ (susceptibility, here $-V (\dd p / \dd V)_T$ and $\delta$ (for the critical isotherm) for a Van der Waals gas.
%\newcounter{problemcounter}
%\setcounter{problemcounter}{\value{enumii}}
\end{enumerate}

\ifincludesolutions\else\newpage\fi
\item \label{pb:vanderWaalsidealfluid} % From Callen sect. 3.5
The Van der Waals equation of state~(\ref{vanderWaalsgas}) alone does not determine the heat capacity. We need to supplement it with a thermal equation of state to fully define the system. In general, this equation has to be of the form
\begin{equation}
\frac{1}{T} = q(u, v),
\end{equation}
with $u = E/N$ and $v = V/N$ the (number) densities of the internal energy and the volume. If we know $q$, we can integrate the first law of thermodynamics in differential form (slightly rearranged)
\begin{equation}
\label{firstlawrearranged}
\dd s = \frac{1}{T} \dd u + \frac{p}{T} \dd v.
\end{equation}
For $\dd s$ to be a perfect differential (so that we can integrate it), we need its second order partial derivatives to be equal:
\begin{equation}
\label{totaldifferentialcondition}
\frac{\partial^2 s}{\partial u \partial v} = \frac{\partial^2 s}{\partial v \partial u}.
\end{equation}
\begin{enumerate}[(a)]
%\setcounter{enumii}{\value{problemcounter}}
\item Re-write the condition~(\ref{totaldifferentialcondition}) on $s$ to a condition on $T$ (using~\ref{firstlawrearranged}), and show that the condition is not satisfied for the ideal gas law, but it is satisfied if $1/T$ depends only on the sum $1/v + u/a$, and for the case that
\begin{equation}
\label{vanderWaalsidealfluid}
\frac{1}{T} = \frac{c \kB T}{u + a/v}.
\end{equation}
We will refer to the combined system of~(\ref{vanderWaalsgas}) and~(\ref{vanderWaalsidealfluid}) as an \index{Van der Waals ideal fluid}`ideal Van der Waals fluid'~\cite[Section 3.5]{Callen1985}.
\item Show that for an ideal Van der Waals fluid, we have
\begin{equation}
S = N s_0 + N \kB \log\left[ (v-b)\left(u + \frac{a}{v}\right)^c \right],
\end{equation}
where $s_0$ is a constant.
\item Find the heat capacity of an ideal Van der Waals fluid, and its critical exponent~$\alpha$.
\end{enumerate}

\item \label{pb:Isingcooperativechain} \input{solutions/problems/Isingcooperativechain} % Similar to midterm 2020, also in Nelson, in Chaikin & Lubensky ch. 6.5, and Thijssen ch. 12.6.

\item \label{pb:colloids} \input{solutions/problems/colloids} % Midterm 2019

\item \label{pb:Landaufirstorder} \input{solutions/problems/Landaufirstorder} % Midterm 2020

%\ifincludesolutions\else\newpage\fi
\item \label{pb:FHosmoticpressure} \input{solutions/problems/FHosmoticpressure}

\item \label{pb:FHpolymer} \input{solutions/problems/FHpolymers}

\item \label{pb:depletionforcemagnitude} Estimate the magnitude of the depletion force between two colloids with radius $1\;\mu\mathrm{m}$ in a solution containing a collection of small particles of radius~$r=1\;\mathrm{nm}$ at a concentration of~$1\;\mathrm{mM}$. % For r=2nm, Phillips has F = 15pN (pag. 558).

\item \label{pb:twospheresoverlapvolume} \input{solutions/problems/depletionforcesspheres} % Inspired by Phillips et al., section 14.2.3, pages 556-557.
%In this problem, we'll derive the expression~(\bookref{twosphereexcludedvolume}) given in section~\bookref{sec:depletion} for the excluded volume of two spheres of radius~$R$. The spheres are a distance~$D$ apart and immersed in a solution containing a large number of small spheres of radius~$r$, see the sketch below.
%\begin{enumerate}[(a)]
%\item The overlap region consists of two spherical caps, that sit on top of a cone with opening angle~$\theta$. Express~$\theta$ in terms of $D$, $R$ and $r$. % \cos \theta = \frac12 D / (R+r).
%\item Calculate the volume of the `spherical cone' (cone with spherical cap) with opening angle~$\theta$. % \frac23 \pi (R+r)^3 (1-\cos\theta).
%\item Find the volume of the `regular cone' (with flat base) with opening angle~$\theta$. % \frac{\pi}{3} \frac{D}{2} \left[ (R+r)^2 - (D/2)^2 \right].
%\item Combine parts (a)-(c) to find an expression for the overlap area and verify equation~(\bookref{twosphereexcludedvolume}).
%\end{enumerate}

\item \label{pb:truestressstrain} Derive the expression $\gamma_\mathrm{T} = \log(\lambda)$ for the true strain. %TO DO; CF https://en.wikipedia.org/wiki/Deformation_(engineering)

\item \label{pb:Bjerrumlength} \input{solutions/problems/Bjerrumlength} % Inspired by the calculation in Strobl, section 3.3, pages 90-92.

\item \label{pb:PoissonBoltzmann} \input{solutions/problems/linearizedPoissonBoltzmann} % see https://m.tau.ac.il/~andelman/reprints/121_NATO_Edinburgh_elec_2005.pdf

\item \label{pb:Donnanpotential} \input{solutions/problems/Donnanpotential}

\item \label{pb:electricdoublelayer} \input{solutions/problems/electricdoublelayer}

\item \label{pb:membranepotentials} \input{solutions/problems/membranepotentials}

\end{enumerate}