%Problemtitle: Flory-Huggins model for polymers
%See e.g. https://chem.libretexts.org/Bookshelves/Biological_Chemistry/Concepts_in_Biophysical_Chemistry_(Tokmakoff)/02%3A_Macromolecules/08%3A_Polymer_Lattice_Models/8.04%3A_FloryHuggins_Model_of_Polymer_Solutions
%Midterm 2023.
In this problem, we'll derive and study the Flory-Huggins model for polymers. The setting is similar to the model for small solute particles, except that our polymers will occupy multiple sites, which are connected, \ifproblemset see figure~\ref{fig:FHpolymer}. \else see figure~\bookref{fig:FHpolymerplots}(a). \fi Each site has $z$ neighbors. Let the total number of sites be $N$, the number of polymers $N_\mathrm{p}$, and each polymer have $m$ segments; there are also $N_\mathrm{s}$ solvent particles. Naturally, we have
\begin{equation}
N = m N_\mathrm{p} + N_\mathrm{s}.
\end{equation}
We can also define a polymer volume fraction $\phi$ as $\phi = m N_\mathrm{p} / N$; the volume fraction of the solvent particles is then $1-\phi = N_\mathrm{s} / N$.

\ifproblemset
\begin{figure}[ht]
\centering
\includegraphics[scale=1]{FHpolymer}
\caption{Flory-Huggins model for the free energy of polymer solutions. We take the specific volume of a segment of the polymer and of a solvent particle to be equal. Each lattice site is occupied by either a polymer segment (blue) or a solvent particle (white). Polymer segments are connected into chains of length $m$.}
\label{fig:FHpolymer}
\end{figure}
\fi

The first order of business is to determine the number of possible configurations. We'll do so by constructing the polymers segment by segment.
\begin{enumerate}[(a)]
\item Show that the number of ways we can place the first segments of the $N_\mathrm{p}$ polymers is given by
\begin{equation}
W^{(1)} = \frac{N!}{(N-N_\mathrm{p})!}.
\end{equation}
\item What is the probability that a given site next to one of our placed first segments is empty? We'll call this probability $p_0$. (To avoid confusion: this is not the probability that there is at least one empty neighboring site, but the probability that, for an already selected neighboring site, it is empty).
\ifproblemset\setcounter{problemcounter}{\value{enumi}}\else\setcounter{problemcounter}{\value{enumii}}\fi
\end{enumerate}
As there are $z$ neighboring sites to each segment, we can extend the first segment in $p_0 z$ ways. Re-calculating $p_0$ for each of the already placed segments, we extend all segments. The number of possibilities that this can be done is then
\begin{equation}
\label{FHpolymersW2}
W^{(2)} = \left(\frac{z}{N}\right)^{N_\mathrm{p}} \frac{(N-N_\mathrm{p})!}{(N-2N_\mathrm{p})!}.
\end{equation}
For the third segment, the argument goes similar, except that the second segment already has one neighboring site that's occupied (namely by the first segment). Continuing, we find that for the $k$th segment (with $k > 2$), we have
\begin{equation}
\label{FHpolymersWk}
W^{(k)} = \left(\frac{z-1}{N}\right)^{N_\mathrm{p}} \frac{(N-(k-1)N_\mathrm{p})!}{(N-k N_\mathrm{p})!}
\end{equation}
possibilities.
To get the total number of possibilites, we now multiply the possibilities for each segment, then divide by $N_\mathrm{p}!$ (the number of ways to swap the polymers around, as they're identical), which gives
\begin{align}
\label{FHpolymersW}
W &= \frac{1}{N_\mathrm{p}!} \Pi_{k=1}^m W^{(k)} = \left(\frac{z}{N}\right)^{N_\mathrm{p}} \left(\frac{z-1}{N} \right)^{(m-2)N_\mathrm{p}} \frac{N!}{(N-mN_\mathrm{p})! N_\mathrm{p}!} \\
\label{FHpolymersWapprox}
& \approx \left(\frac{z}{N} \right)^{(m-1) N_\mathrm{p}} \frac{N!}{N_\mathrm{s}! N_\mathrm{p}!}.
\end{align}
(Note that all we did in the approximation is to set $z = z-1$ for one term; doing so will simplify our final answer without significantly changing it. Any other change will have a significant effect).

\begin{enumerate}[(a)]
\ifproblemset\setcounter{enumi}{\value{problemcounter}}\else\setcounter{enumii}{\value{problemcounter}}\fi
\item Evaluate the product in equation~(\ref{FHpolymersW}) to get the given answer.
\ifproblemset\setcounter{problemcounter}{\value{enumi}}\else\setcounter{problemcounter}{\value{enumii}}\fi
\end{enumerate}

The net mixing entropy of the polymer solution now has three contributions: the actual entropy of mixing, minus the configurational entropy of the polymers and that of the solvent particles in the absence of mixing:
\begin{equation}
\Delta S_\mathrm{mix} = S_\mathrm{mix} - S^0_\mathrm{polymers} - S^0_\mathrm{solvent}.
\end{equation}
We did not have to account for this difference before, as in the absence of polymers, the solvent particles simply take up all sites, so there is only one possible configuration, and therefore
$$ S^0_\mathrm{solvent} = \kB \log(1) = 0.$$
The polymers however can have many configurations in the absence of solvent molecules; they number is given by equation~(\ref{FHpolymersW}) with $N = m N_\mathrm{p}$ (we'll call this number $W_0$).
\begin{enumerate}[(a)]
\ifproblemset\setcounter{enumi}{\value{problemcounter}}\else\setcounter{enumii}{\value{problemcounter}}\fi
\item Evaluate the fraction $W / W_0$, using the approximate expression of equation~(\ref{FHpolymersWapprox}).
\item Using that $S = \kB \log(W)$ and the Stirling approximation ($\log(N!) \approx N \log(N) - N$), show that we get
\begin{equation}
\Delta S_\mathrm{mix} = - N \kB \left[\frac{\phi}{m} \log(\phi) + (1-\phi) \log(1-\phi) \right].
\end{equation}
\ifproblemset\setcounter{problemcounter}{\value{enumi}}\else\setcounter{problemcounter}{\value{enumii}}\fi
\end{enumerate}

With the usual Flory-Huggins mean-field interaction energy, the free energy per unit volume of the polymer solution becomes
\begin{equation}
\label{FHfreenergypolymer}
f(\phi) = \kB T \left[\frac{\phi}{m} \log(\phi) + (1-\phi) \log(1-\phi) + \chi \phi (1-\phi) \right].
\end{equation}
\begin{enumerate}[(a)]
\ifproblemset\setcounter{enumi}{\value{problemcounter}}\else\setcounter{enumii}{\value{problemcounter}}\fi
\item Find the osmotic pressure in the polymer solution.
\item For $m=4$, find the critical value of the Flory-Huggins parameter $\chi$ above which our polymer solution will phase-separate.
\ifproblemset\setcounter{problemcounter}{\value{enumi}}\else\setcounter{problemcounter}{\value{enumii}}\fi
\end{enumerate}


\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item We have $N$ choices for the first (one-segment) polymer, $N-1$ for the second, and so on, till $N-(N_\mathrm{p}-1)$ for the last. The total number of ways we can place these segments is therefore
\begin{align*}
W^{(1)} &= N \cdot (N-1) \cdot \ldots \cdot (N-N_\mathrm{p}+1) = \frac{N!}{(N-N_\mathrm{p})!}
\end{align*}
\score{2 pt}.
\item There are $N-N_\mathrm{p}$ free sites left, out of a total of $N$, so the probability of finding an empty site is $(N-N_\mathrm{p})/N$ \score{1 pt}.
\item We have
\begin{align*}
W &= \frac{1}{N_\mathrm{p}!} \Pi_{k=1}^m W^{(k)} \\
&= \frac{1}{N_\mathrm{p}!} \frac{N!}{(N-N_\mathrm{p})!} \cdot \left(\frac{z}{N}\right)^{N_\mathrm{p}} \frac{(N-N_\mathrm{p})!}{(N-2N_\mathrm{p})!} \cdot \left(\frac{z-1}{N}\right)^{N_\mathrm{p}} \frac{(N-2N_\mathrm{p})!}{(N-3 N_\mathrm{p})!} \cdot \ldots \cdot \left(\frac{z-1}{N}\right)^{N_\mathrm{p}} \frac{(N-(m-1)N_\mathrm{p})!}{(N-m N_\mathrm{p})!} \\
&= \frac{1}{N_\mathrm{p}!} N! \cdot \left(\frac{z}{N}\right)^{N_\mathrm{p}} \cdot \left(\frac{z-1}{N}\right)^{N_\mathrm{p}} \cdot \ldots \cdot \left(\frac{z-1}{N}\right)^{N_\mathrm{p}} \cdot \frac{1}{(N-m N_\mathrm{p})!} \\
&= \left(\frac{z}{N}\right)^{N_\mathrm{p}} \left(\frac{z-1}{N} \right)^{(m-2)N_\mathrm{p}} \frac{N!}{(N-mN_\mathrm{p})! N_\mathrm{p}!}
\end{align*}
as the factorials cancel out \score{2 pt}.
\item We have
\begin{align*}
\frac{W}{W_0} &= \left(\frac{z}{N} \right)^{(m-1) N_\mathrm{p}} \frac{N!}{N_\mathrm{s}! N_\mathrm{p}!} \left(\frac{m N_\mathrm{p}}{z} \right)^{(m-1) N_\mathrm{p}} \frac{N_\mathrm{p}!}{(n N_\mathrm{p})!} = \left(\frac{m N_\mathrm{p}}{N} \right)^{(m-1) N_\mathrm{p}} \frac{N!}{N_\mathrm{s}! (m N_\mathrm{p})!}
\end{align*}
\score{2 pt}
\item We have
\begin{align*}
\Delta S_\mathrm{mix} &= S_\mathrm{mix} - S^0_\mathrm{polymers} = \kB \log(W) - \kB \log(W_0) = \kB \log\left(\frac{W}{W_0}\right) \\
&= \kB \left[ (m-1) N_\mathrm{p} \log\left(\frac{m N_\mathrm{p}}{N} \right) + N \log(N) - N_\mathrm{s} \log(N_\mathrm{s}) - (m N_\mathrm{p}) \log(m N_\mathrm{p}) \right] \\
&= - N \kB \left[ -(m-1) N_\mathrm{p} \log\left(\frac{m N_\mathrm{p}}{N} \right) + \frac{N_\mathrm{s}}{N} \log\left(\frac{N_\mathrm{s}}{N}\right) + m N_\mathrm{p} \log\left( \frac{m N_\mathrm{p}}{N} \right) \right] \\
&= - N \kB \left[ \frac{N_\mathrm{s}}{N} \log\left(\frac{N_\mathrm{s}}{N}\right) + N_\mathrm{p} \log\left( \frac{m N_\mathrm{p}}{N} \right) \right] \\
&= - N \kB \left[ \frac{\phi}{m} \log(\phi) + (1-\phi) \log(1-\phi) \right].
\end{align*}
where we used the Stirling approximation ($\log(N!) = N \log(N) - N$) and the identity $N = N_\mathrm{s} + m N_\mathrm{p}$ \score{3 pt}.
\item The osmotic pressure is given by (leaving out the reference $f(0)$ as it blows up):
\begin{align*}
\Pi(\phi) &= - f(\phi) + \phi \frac{\partial f}{\partial \phi} \\
&= \kB T \left[ -\frac{\phi}{m} \log(\phi) - (1-\phi) \log(1-\phi) - \chi \phi (1-\phi) \right. \\
&\qquad \qquad \left. + \frac{\phi}{m} \log(\phi) + \frac{\phi}{m} - \phi\log(1-\phi) - \phi + \chi\phi(1-2\phi) \right] \\
&= \kB T \left[ (2\phi-1) \log(1-\phi) - \chi \phi^2 + \frac{\phi}{m} - \phi \right]
\end{align*}
\score{2 pt}.
\item To find the critical value of $\chi$, we need the point at which the second derivative first changes sign (i.e., the point where the curve of $f$ develops a concave region). That's when both the second and third derivative of $f$ vanish \score{1 pt}. We have
\begin{align*}
\frac{1}{\kB T}\frac{\partial f}{\partial \phi} &= \frac{1}{m} \log(\phi) + \frac{1}{m} - \log(1-\phi) - 1 + \chi(1-2\phi) \\
\frac{1}{\kB T}\frac{\partial^2 f}{\partial \phi^2} &= \frac{1}{m\phi} + \frac{1}{1-\phi} - 2 \chi \\
\frac{1}{\kB T}\frac{\partial^3 f}{\partial \phi^3} &= - \frac{1}{m \phi^2} + \frac{1}{(1-\phi)^2}
\end{align*}
Equating the third derivative to zero, we get
\begin{align*}
0 &= -m \phi^2 + (1-\phi)^2 = (1-m) \phi^2 - 2\phi + 1 \\
\phi &= \frac{-1}{m-1} \pm \frac{\sqrt{m}}{m-1},
\end{align*}
of which the positive root is $\phi_\mathrm{c} = \frac13$ for $m=4$ \score{1 pt}. Substituting in the expression for the second derivative and equating to zero gives us $\chi_\mathrm{c}$:
\begin{align*}
0 &= \frac{1}{4 \cdot \frac13} + \frac{1}{1-\frac13} - \frac23 \chi_\mathrm{c} \quad \Rightarrow \quad \chi_\mathrm{c}  = \frac{27}{8}
\end{align*}
\score{1 pt}.
\end{enumerate}
\fi

