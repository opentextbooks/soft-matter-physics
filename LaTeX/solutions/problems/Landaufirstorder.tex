% Problemtitle First order phase transitions in Landau theory
% Midterm 2020
In our discussion on Landau theory, we ignored all odd-powered terms in the free energy. There are examples of systems (such as the magnet) where symmetry excludes such odd-powered terms in the absence of an external field; there are also examples where odd-powered terms are not excluded. As a general rule of thumb, when something is not excluded, nature will use it, so we should study what happens in a Landau model where we include odd powers of the order parameter~$\phi$. We'll find that we get first-order phase transitions (at which the order parameter changes discontinuously).

As we've seen, linear powers in the order parameter are due to couplings to an external field; for simplicity, we'll consider the case where that field is set such that the linear term vanishes. Therefore, our free energy per unit volume reads:
\begin{equation}
\label{Landauwithcubic}
F(\phi) = \frac{a}{2} \phi^2 - \frac{b}{3} \phi^3 + \frac{c}{4} \phi^4,
\end{equation}
where as before we require $c > 0$ for the system to be globally stable. The sign of $b$ will not change the global stability, just the sign of the order parameter in the second phase; we set $b>0$. The free energy in equation~(\ref{Landauwithcubic}) is the total free energy of a single-component system,  and $\phi$ an arbitrary order parameter, like we had in section~\bookref{sec:Landautheory}; therefore, we will be looking for minima of this function to find (possibly coexisting) phases.

\begin{figure}
\centering
\includegraphics[scale=1]{solutions/problems/Landaufirstordertransitionsgraph.pdf}
\caption[Landau free energy for a first order phase transition.]{Plot of $\tilde{F}(\tilde{\phi})$ (equation~\ref{Landaucubicrescaled}) for four values of $\tilde{a}$. All curves (trivially) have a minimum at $\tilde{\phi} = 0$. The red curve has only a single minimum. The green curve has a minimum and an inflection point. The orange curve has two minima, both of which touch the $\tilde{\phi}$ axis. The blue curve has two minima at unequal depth.}
\label{fig:Landaufirstordertransitionsgraph}
\end{figure}

\begin{enumerate}[(a)]
\item Equation~(\ref{Landauwithcubic}) contains three parameters, $a$, $b$, and $c$. Fortunately, we can eliminate two by re-scaling both the free energy and the order parameter. Defining $\tilde{\phi} = c \phi / b$, show that you can re-write equation~(\ref{Landauwithcubic}) as
\begin{equation}
\label{Landaucubicrescaled}
\tilde{F}(\tilde{\phi}) = \frac{\tilde{a}}{2}\tilde{\phi}^2 + \frac13 \tilde{\phi}^3 + \frac14 \tilde{\phi}^4,
\end{equation}
and express $\tilde{F}$ and $\tilde{a}$ in $F$, $a$, $b$, and $c$.
\ifproblemset\setcounter{problemcounter}{\value{enumi}}\else\setcounter{problemcounter}{\value{enumii}}\fi
\end{enumerate}
Figure~\ref{fig:Landaufirstordertransitionsgraph} shows plots of equation~(\ref{Landaucubicrescaled}) for four values of $\tilde{a}$. Some curves have only one minimum, others have two (with a maximum in between); the green curve has an inflection (or `saddle') point (and a minimum at $\phi = 0$), the red curve has a `super straight' section where the derivative has a saddle point. %Find the value of $\tilde{a}$ for which we get the green curve. \textit{Hint}: combine your equations in a clever way and the amount of maths required will be minimal.
\begin{enumerate}[(a)]
\ifproblemset\setcounter{enumi}{\value{problemcounter}}\else\setcounter{enumii}{\value{problemcounter}}\fi
\item Figure~\ref{fig:Landaufirstordertransitionsgraph} shows plots of equation~(\ref{Landaucubicrescaled}) for four values of $\tilde{a}$. Some curves have only one minimum, others have two (with a maximum in between); the green curve has an inflection (or `saddle') point (and a minimum at $\phi = 0$). Find the value of $\tilde{a}$ for which we get the green curve. \textit{Hint}: combine your equations in a clever way and the amount of maths required will be minimal.
\item For a second-order phase transition, the value for $\tilde{a}$ you found in (b) would be the critical value~$\tilde{a}_\mathrm{c}$ of $\tilde{a}$. While the free energy density has two minima for $\tilde{a} < \tilde{a}_\mathrm{c}$, the state at $\tilde{\phi} = 0$ remains the global minimum, until the second minimum also touches the $\tilde{\phi}$ axis (orange curve in figure~\ref{fig:Landaufirstordertransitionsgraph}), at which we have a discontinuous first-order phase transition. Find the value of $\tilde{a}$ (we'll call it $\tilde{a}_*$) for which this happens.
\item Find the value $\tilde{\phi}_*$ of the phase coexisting with $\tilde{\phi} = 0$ at the first-order phase transition.
\item Sketch the plot of the values of $\tilde{\phi}$ at which $\tilde{F}(\tilde{\phi})$ has a minimum, as a function of~$\tilde{a}$; indicate the positions of $\tilde{a}_\mathrm{c}$ and $\tilde{a}_*$.
%\item The orange curve in figure~\ref{fig:Landaufirstordertransitionsgraph} allows for two coexisting phases, at $\tilde{\phi} = 0$ and $\tilde{\phi} = \tilde{\phi}_*$. Could there also be coexisting phases
%\begin{enumerate}[(I)]
%\item between the orange and blue curves,
%\item between the orange and green curves,
%\item between the green and red curves?
%\end{enumerate}
%NB: Don't just state the answer, also explain it.
%% NB: This question is misleading; this is a one-component system, which does not have a degree of freedom to tune, so two-phase coexistence (at a fixed temperature) can only occur at a fixed value of $\phi$.
\item The Landau free energy is a mean-field approximation. In real materials, fluctuations cause the value of the order parameter to change with distance. Consequently, the materials have a \emph{correlation length}~$\xi$, similar to the persistence length in the Worm-Like Chain model for polymers. The correlation length is related to the curvature (i.e., second derivative) of the free energy at the equilibrium value (i.e., at the global minimum of $\tilde{F}$):
\begin{equation}
\label{correlationlength}
\frac{\tilde{K}}{\tilde{\xi}^2} = \left. \pdv[2]{\tilde{F}}{\tilde{\phi}} \right|_{\tilde{\phi} = \tilde{\phi}_\mathrm{eq}},
\end{equation}
where $\tilde{K}$ is a (rescaled) material parameter\footnote{Equation~(\ref{correlationlength}) follows from an extended version of the Landau free energy that includes a term with the gradient of $\phi$, penalizing differences in the order parameter; such a term can be used to explain the emergence of surface tension between coexisting phases and the coagulation of blobs of material in the same phase, a process known as coarsening.}. Plot $\tilde{\xi}$ as a function of $\tilde{a}$. NB: Make sure to distinguish values below and above $\tilde{a}_*$. Note that, unlike for a continuous phase transition, the correlation length does not diverge at $\tilde{a}_*$ for this discontinuous phase transition.
\end{enumerate}

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item Substituting $\phi = b\tilde{\phi}/c$, we find
\begin{align*}
F(\phi) &= \frac{a}{2} \frac{b^2 \tilde{\phi}^2}{c^2} - \frac{b}{3} \frac{b^3 \tilde{\phi}^3}{c^3} + \frac{c}{4} \frac{b^4 \tilde{\phi}^4}{c^4} = \frac12 \frac{a b^2 c}{c^3} \tilde{\phi}^2 - \frac13 \frac{b^4}{c^3} \tilde{\phi}^3 + \frac14 \frac{b^4}{c^3} \tilde{\phi}^4 \\
\frac{c^3}{b^4} F(\phi) &= \frac12 \frac{a c}{b^2} \tilde{\phi}^2 - \frac13 \tilde{\phi}^3 + \frac14 \tilde{\phi}^4 \\
\tilde{F}(\tilde{\phi}) &= \frac12 \tilde{a} \tilde{\phi}^2 - \frac13 \tilde{\phi}^3 + \frac14 \tilde{\phi}^4,
\end{align*}
where
\[ \tilde{a} = \frac{a c}{b^2} \qquad \mbox{and} \qquad \tilde{F} = \frac{c^3}{b^4} f \]
\score{2 pt}.
\item For the inflection point, we need both the first and second derivative to equal zero. We have:
\begin{align*}
\tilde{F}'(\tilde{\phi}) &= \tilde{a} \tilde{\phi} - \tilde{\phi}^2 + \tilde{\phi}^3, \\
\tilde{F}''(\tilde{\phi}) &= \tilde{a} - 2\tilde{\phi} + 3\tilde{\phi}^2.
\end{align*}
\score{1 pt}. If both are zero, so is any linear combination, so we get
\[ 0 = 3\tilde{F}'(\tilde{\phi}) - \tilde{\phi} \tilde{F}''(\tilde{\phi}) = 2\tilde{a} \tilde{\phi} - \tilde{\phi}^2 = \tilde{\phi} (2\tilde{a} - \tilde{\phi})\]
So either $\tilde{\phi} = 0$ (the minimum) or $\tilde{\phi} = 2 \tilde{a}$ \score{1 pt}. Substituting that into $\tilde{F}''(\tilde{\phi}) = 0$ we find
\[ 0 = \tilde{a} + -4\tilde{a} + 12\tilde{a}^2 = 3\tilde{a} (4 \tilde{a} - 1), \]
which gives either $\tilde{a} = 0$ or $\tilde{a} = \frac14$. As for $\tilde{a} = 0$ we get once again that $\tilde{\phi} = 0$, this `solution' doesn't give us a second point where the derivatives vanish, so we conclude that we must have $\tilde{a} = \frac14$ \score{1 pt} (and $\tilde{\phi} = \frac12$).
\item We are asked to find the point at which $\tilde{F}(\tilde{\phi})$ has two minima of value $0$, so we need to solve:
\begin{align*}
0 &= \tilde{F}(\tilde{\phi}) = \tilde{\phi}^2 \left(\frac{\tilde{a}}{2} - \frac13 \tilde{\phi} + \frac14 \tilde{\phi}^2 \right)\\
0 &= \tilde{F}'(\tilde{\phi}) = \tilde{\phi} \left( \tilde{a} - \tilde{\phi} + \tilde{\phi}^2\right).
\end{align*}
\score{1 pt}. Obviously $\tilde{\phi} = 0$ is a solution. For the other solution, we can divide out the factors $\tilde{\phi}$, and again make a linear combination of the remaining terms (in brackets), which gives:
\[ 0 = \frac{4}{\tilde{\phi}^2} \tilde{F}(\tilde{\phi}) - \frac{1}{\tilde{\phi}} \tilde{F}'(\tilde{\phi}) = \tilde{a} - \frac13 \tilde{\phi}, \]
so $\tilde{\phi} = 3 \tilde{a}$ \score{1 pt}. Substituting again in the second equation, we get
\[ 0 = \tilde{a} - 3\tilde{a} + 9 \tilde{a}^2 = \tilde{a} \left(9 \tilde{a} - 2\right), \]
which gives $\tilde{a} = 0$ (trivial solution with one minimum) or $\tilde{a} = \frac29 = \tilde{a}_*$ \score{1 pt}.
\item From (c), we can now read off that $\tilde{\phi}_* =  3 \tilde{a}_* = \frac23$ \score{1 pt}. 
\item See figure~\ref{fig:Landaufirstordertransitionphasediagram} \score{4 pt - subtract 1 for each missing feature}.
%\item In general, two phases can coexist if their temperatures, pressures, and chemical potentials are equal. Assuming equal temperature here at equal values of $\tilde{a}$, we have equal pressures and chemical potentials if two points on the free energy density curve have a common tangent \score{1 pt}. That is the case between (I) the blue and orange (II) the orange and green curves \score{1 pt}, and between (III) the green and red curves, even though above the green curve there is just one minimum \score{1 pt}.
\item At equilibrium, the first derivative of $\tilde{F}$ vanishes:
\[ 0 = \tilde{F}'(\tilde{\phi}) = \tilde{\phi} \left( \tilde{a} - \tilde{\phi} + \tilde{\phi}^2 \right) \qquad \Rightarrow \quad \tilde{\phi} = 0 \qquad \mbox{or} \qquad \tilde{\phi} = \frac12 \pm \frac12 \sqrt{1-4\tilde{a}}, \]
where $\tilde{\phi} = 0$ is the minimum for $\tilde{a} > \tilde{a}_*$, and the other two solutions correspond to the extra minimum and maximum for $\tilde{a} < \tilde{a}_*$; the minimum is at $\tilde{\phi}_\mathrm{eq} = \frac12 + \frac12 \sqrt{1-4\tilde{a}}$ \score{1 pt}. We need the second derivative at this value, which is given by 
\[ \tilde{F}''(\tilde{\phi}_\mathrm{eq}) = \tilde{a} - 2\tilde{\phi}_\mathrm{eq} + 3\tilde{\phi}_\mathrm{eq}^2 = 3 \left( \tilde{a} - \tilde{\phi}_\mathrm{eq} + \tilde{\phi}_\mathrm{eq}^2\right) - 2 \tilde{a} + \tilde{\phi}_\mathrm{eq} = - 2 \tilde{a} + \frac12 + \frac12 \sqrt{1-4\tilde{a}},\]
where we exploited the fact that $\tilde{a} - \tilde{\phi}_\mathrm{eq} + \tilde{\phi}_\mathrm{eq}^2 = 0$ by the first equation \score{1 pt}. For the correlation length we get for $\tilde{a} < \tilde{a}_*$:
\[ \tilde{\xi} = \sqrt{\frac{2\tilde{K}}{1-4\tilde{a}+\sqrt{1-4\tilde{a}}}}, \]
while for $\tilde{a} > \tilde{a}_*$ we have $\tilde{f}''(\tilde{\phi}_\mathrm{eq}) = \tilde{f}''(0) = \tilde{a}$, and thus $\tilde{\xi} = \sqrt{\tilde{K}/\tilde{a}}$ \score{1 pt}. Note that while the expression for the correlation length at $\tilde{a} < \tilde{a}_*$ diverges for $\tilde{a} = \tilde{a}_\mathrm{c} = \frac14$, we never reach that point, as we already transition to the other branch at $\tilde{a} = \tilde{a}_* = \frac29$. The correlation length for a first-order phase transition thus remains finite; its maximum value is $\tilde{\xi} = \sqrt{\tilde{K}/\tilde{a}_*} = \sqrt{9\tilde{K}/2}$ \score{1 pt - also give if correctly indicated in the plot}. The plot of $\tilde{\xi}$ (in units of $\sqrt{\tilde{K}}$) as a function of $\tilde{a}$ is shown in figure~\ref{fig:Landaufirstordertransitioncorrelationlength} \score{1 pt}.
\end{enumerate}

\begin{figure}
\centering
\includegraphics[scale=1]{solutions/problems/Landaufirstorderphasediagram.pdf}
\caption{Phase diagram of $\tilde{\phi}$ as a function of $\tilde{a}$, including the values of $\tilde{a}$ at the critical point ($\tilde{a}_\mathrm{c}$, blue dot) and at the point where the low-$\tilde{\phi}$ phase becomes globally unstable ($\tilde{a}_\mathrm{*}$, orange dot). The continuous lines represent the binodal / coexisting phases, dashed lines unstable phases, and the dash-dotted line the spinodal.}
\label{fig:Landaufirstordertransitionphasediagram}
\end{figure}

\begin{figure}
\centering
\includegraphics[scale=1]{solutions/problems/Landaufirstordertransitioncorrelationlength.pdf}
\caption{Correlation length $\tilde{\xi}$ (in units of $\sqrt{\tilde{K}}$) as a function of $\tilde{a}$. The (finite) peak is at the phase-transition value $\tilde{a}_*$, well before the divergence at $\tilde{a}_\mathrm{c}$.}
\label{fig:Landaufirstordertransitioncorrelationlength}
\end{figure}
\fi