% Problemtitle Critical exponents in the cooperative chain / one dimensional Ising model
% Similar to midterm 2020, also in Nelson, in Chaikin & Lubensky ch. 6.5, and Thijssen ch. 12.6.
In chapter~\bookref{ch:polymers}, we discussed the Freely-Jointed Chain (FJC) and the Worm-Like Chain (WLC) model for polymers. In the FJC, the polymer is a random walk of infinitely stiff segments of length~$b$, with each configuration carrying equal weight. In the WLC, the polymer is a curve in space, with each configuration carrying a weight corresponding to the elastic energy of the curve. In this problem, we'll discuss a third model, the cooperative chain (CC). In the CC model, like in the FJC model, the polymer consists of infinite stiff segments. However, we now include a potential energy that depends on the relative orientation of segments $i$ and $i+1$. For mathematical simplicity, we'll restrict ourselves to the one-dimensional case in this problem. Steps along the chain then go up or down by $b$; we'll write $\bvec{b}_i = b \sigma_i$, where $\sigma_i = \pm 1$. Our potential energy will favor segments pointing in the same direction; therefore, if $\sigma_{i+1} = - \sigma_i$, we add an extra contribution $2 \gamma \kB T$ to the potential energy, relative to when they point in parallel. In practice, we implement this by adding the term $- \gamma \kB T \sigma_i \sigma_{i+1}$ to the energy for every junction, so our potential energy (or configurational energy) becomes:
\begin{equation}
\label{CCmodelpotenergy}
U_\mathrm{conf} = - \gamma \kB T \sum_{i=1}^{N-1} \sigma_i \sigma_{i+1},
\end{equation}
where $N$ is the number of segments. We call $\gamma$ the \emph{cooperativity parameter}.

As always, the way to test the model is to compare to experimental data. For polymers, a measurable quantity is the force-extension curve, which we can find from the partition function. Let $f$ be the force that we're pulling with, and $r$ the end-to-end distance at this force.
\begin{enumerate}[(a)]
\item Find the total number of possible configurations of our one-dimensional chain.
\item For the situation sketched above (CC model with applied force), show / argue that the partition function reads:
\begin{equation}
\label{CCmodelpartitionfunction}
Z(f) = \sum_\mathrm{configurations} \exp\left[ \frac{f b}{\kB T} \sum_{i=1}^N \sigma_i + \gamma \sum_{i=1}^{N-1} \sigma_i \sigma_{i+1} \right]. 
\end{equation}
\item Show that the mean end-to-end distance can be computed from the partition function as
\begin{equation}
\label{evendtoendlength}
\ev{r} = \kB T \dv{f} \log(Z(f)),
\end{equation}
where (as usual) the logarithm has base $e$.
\ifproblemset\setcounter{problemcounter}{\value{enumi}}\else\setcounter{problemcounter}{\value{enumii}}\fi
\end{enumerate}
To evaluate the partition function in equation~(\ref{CCmodelpartitionfunction}), we'll use transfer matrices to go from one link to the next, a trick you may have encountered elsewhere. If so, you likely recognized equation~(\ref{CCmodelpotenergy}) as being mathematically identical to the \index{Ising model} one dimensional \emph{Ising model}. The Ising model describes the total magnetization of a system consisting of an array of many small magenetic dipoles, each of which have a value of $\pm 1$, representing atomic `spins' that are either pointing up ($\sigma_i = +1$) or down ($\sigma_i = -1$). The interaction term in equation~(\ref{CCmodelpotenergy}) is then the nearest-neighbor interaction between spins, which favors alignment. The interaction term carries an energy $-J$ per pair. The analog of the external force in the Ising model is an external magenetic field of magnitude~$h$. The energy of the Ising model is then written as
\begin{equation}
\label{IsingPotenergy}
U_\mathrm{Ising} = -J \sum_{i=1}^{N-1} \sigma_i \sigma_{i+1} - h \sum_{i=1}^N \sigma_i,
\end{equation}
and the partition function reads
\begin{align}
\label{Isingpartitionfunction}
Z_\mathrm{Ising} &= \sum_\mathrm{configurations} \exp\left[ \frac{J}{\kB T} \sum_{i=1}^{N-1} \sigma_i \sigma_{i+1} + \frac{h}{\kB T} \sum_{i=1}^N \sigma_i\right] \nonumber \\
&= \sum_\mathrm{configurations} \exp\left[ K \sum_{i=1}^{N-1} \sigma_i \sigma_{i+1} + L \sum_{i=1}^N \sigma_i \right],
\end{align}
where $K = J/\kB T$ and $L = h / \kB T$ scale inversely with temperature. While (as we'll prove below) the one-dimensional Ising model does not have a phase transition as we vary temperature, it has a critical point at $T=0$ under changes in the external field, and so we can calculate its critical exponents. The two-dimensional Ising model does have a phase transition as a function of temperature, as famously shown by Onsager~\cite{Onsager1944}.

To introduce transfer matrices, we'll first consider the case that our chain consists of only two segments, or our metal of only two spins. There are then four possible configurations, corresponding to the $2 \times 2$ possible options for $\sigma_1$ and $\sigma_2$. From now on, we'll work with the $K$ and $L$ coefficients as defined in equation~(\ref{Isingpartitionfunction}). We'll denote the partition function for this system by $Z_2$.
\begin{enumerate}[(a)]
\ifproblemset\setcounter{enumi}{\value{problemcounter}}\else\setcounter{enumii}{\value{problemcounter}}\fi
\item Show that $Z_2$ can be written as a matrix product
\begin{equation}
\label{twosegmentpartitionfunctiontransfermatrix}
Z_2  = \begin{pmatrix}1, 1\end{pmatrix} \begin{pmatrix} e^{K+L} & e^-K \\ e^{-K} & e^{K-L}\end{pmatrix} \begin{pmatrix} 1 \\ 1 \end{pmatrix} \equiv \bvec{v}^\mathrm{T} \bvec{T} \bvec{v}.
\end{equation}
The matrix $\bvec{T}$ is known as the \index{transfer matrix}\emph{transfer matrix}.
\item Taking inspiration from quantum mechanics, we identify $\sigma_i = +1$ and $\sigma_i = -1$ with the basis vectors as
\begin{equation}
\label{spinors}
\sigma_i = +1 \Leftrightarrow \begin{pmatrix} 1 \\ 0 \end{pmatrix}, \qquad \mbox{and} \qquad \sigma_i = -1 \Leftrightarrow \begin{pmatrix} 0 \\ 1 \end{pmatrix}.
\end{equation}
Show that in terms of these basis vectors, the sum over the four possible configurations in $Z_2$ can be written as
\begin{equation}
\label{twosegmentpartitionfunctionbasis}
Z_2 = \begin{pmatrix}1, 0\end{pmatrix} \bvec{T} \begin{pmatrix} 1 \\ 0 \end{pmatrix} + \begin{pmatrix}1, 0\end{pmatrix} \bvec{T} \begin{pmatrix} 0 \\ 1 \end{pmatrix} + \begin{pmatrix} 0, 1 \end{pmatrix} \bvec{T} \begin{pmatrix} 1 \\ 0 \end{pmatrix} + \begin{pmatrix} 0, 1 \end{pmatrix} \bvec{T} \begin{pmatrix} 0 \\ 1 \end{pmatrix}.
\end{equation}
If you studied quantum mechanics, you may recognize the four terms in equation~(\ref{twosegmentpartitionfunctionbasis}) as the matrix elements in the spin basis, $\matrixel{\sigma_i}{\bvec{T}}{\sigma_{i+1}}$.
\item We now write $Z_2$ again as the sum over configurations
\begin{equation}
Z_2 = \sum_{\{\sigma_i = \pm 1\}} \sigma_i \bvec{T} \sigma_{i+1},
\end{equation}
where we've made the vector identification as given in equation~(\ref{spinors}). Note that the transfer matrix $\bvec{T}$ is independent of the values of the~$\sigma_i$. Therefore, the transfer matrix from segment $1$ to segment $2$ is the same as the one from segment $2$ to segment $3$, and so on. For a chain with three segments, we then get
\begin{subequations}
\label{twosegmentpartitionfunction}
\begin{align}
\label{twosegmentpartitionfunctionA}
Z_3 &= \sum_{\{\sigma_i = \pm 1\}} \left(\sigma_1 \bvec{T} \sigma_2\right) \left(\sigma_2 \bvec{T} \sigma_3\right) \\
\label{twosegmentpartitionfunctionB}
&= \sum_{\{\sigma_1, \sigma_3 = \pm 1\}} \sigma_1 \bvec{T}^2 \sigma_3.
\end{align}
\end{subequations}
Prove equation~(\ref{twosegmentpartitionfunctionB}) (if you've done quantum mechanics before, feel free to use Dirac notation, you might recognize the trick).
\item It is now trivial to extend to a chain of $N$ segments. The math will come out nicer if we make this chain periodic, introducing also a link between the $N$th and first segment. The partition function then reads
\begin{equation}
\label{CCperiodicpartitionfunction}
Z_N = \sum_{\{\sigma_1 = \pm 1\}} \sigma_1 \bvec{T}^N \sigma_1 = \Tr(\bvec{T}^N) = \lambda_{+}^N + \lambda_{-}^N,
\end{equation}
where $\lambda_\pm$ are the eigenvalues of the transfer matrix~$\bvec{T}$. Because $\bvec{T}$ is real and symmetric, we know these eigenvalues are also real. Find their values. \textit{Hint}: you can simplify your answers using the hyperbolic sine and cosine, defined as
\[ \sinh(x) = \frac{e^x - e^{-x}}{2}, \qquad \cosh(x) = \frac{e^x + e^{-x}}{2}. \]
\item As $\lambda_{+} > \lambda_{-}$, if $N$ is very large, the first term in~(\ref{CCperiodicpartitionfunction}) dominates, so we can approximate the partition function by simply $Z_N = \lambda_{+}^N$. Use this approximation and equation~(\ref{evendtoendlength}) to find the force-extension relation for the cooperative chain model.
\item At low force, the cooperative chain acts like a linear spring. Find the associated spring constant.
\ifproblemset\setcounter{problemcounter}{\value{enumi}}\else\setcounter{problemcounter}{\value{enumii}}\fi
\end{enumerate}
For the remainder of this problem, we'll focus on the Ising model of magnetic dipoles, using the quantum mechanical notation. We'll also encounter the Pauli spin matrices, defined as
\begin{equation}
\sigma_x = \begin{pmatrix} 0 & 1 \\ 1 & 0 \end{pmatrix}, \quad \sigma_y = \begin{pmatrix} 0 & -i \\ i & 0 \end{pmatrix}, \quad \sigma_z = \begin{pmatrix} 1 & 0 \\ 0 & -1 \end{pmatrix}.
\end{equation}

\begin{enumerate}[(a)]
\ifproblemset\setcounter{enumi}{\value{problemcounter}}\else\setcounter{enumii}{\value{problemcounter}}\fi
\item First, we calculate the average of the spin at site~$i$, i.e. $\ev{\sigma_i}$. Show that it is given by
\begin{equation}
\label{Isingaveragespin}
\ev{\sigma_i} = \frac{1}{Z} \sum_{\{\sigma_i = \pm 1\}} \sigma_i \prod_{j=1}^N \exp\left(K \sigma_j \sigma_{j+1} + L \sigma_j\right) = \frac{\Tr(\bvec{T}^{i-1} \sigma_z \bvec{T}^{N-i+1})}{Z} = \frac{\Tr(\bvec{T}^N \sigma_z)}{\Tr(\bvec{T}^N)},
\end{equation}
where $\sigma_z$ is the third Pauli matrix, and in the last step we used the permutation rule for the trace ($\Tr(ABC) = \Tr(CAB)$) and equation~(\ref{CCperiodicpartitionfunction}) for the partition function.
\item To evaluate the trace in equation~(\ref{Isingaveragespin}), we can take two different approaches. One is to once again work in the large $N$ limit, where the largest eigenvalue $\lambda_{+}$ dominates. Show that in that case
\begin{equation}
\ev{\sigma_i} = \matrixel{\bvec{v}}{\sigma_i}{\bvec{v}},
\end{equation}
where $\bvec{v}$ is the normalized eigenvector corresponding to the eigenvalue $\lambda_{+}$.
\item At zero field ($L=0$), we can evaluate equation~(\ref{Isingaveragespin}) exactly. To do so, we make a transformation to the basis of eigenvectors of $\bvec{T}$, in which $\bvec{T}$ is diagonal. Show that the unitary matrix
\begin{equation}
U = \frac{1}{\sqrt{2}} \begin{pmatrix} 1 & 1 \\ 1 & -1 \end{pmatrix}
\end{equation}
diagonalizes $\bvec{T}$, and use this knowledge to evaluate $\ev{\sigma_i}$. The answer should not surprise you.


\item We will now look for the critical exponents of the Ising model (treating $K$ and $L$ to be inversely proportional to temperature, or $J$ and $h$ to be constant). First, we need the free energy per spin, given by
\begin{equation}
\label{freeenergyperspin}
\frac{f}{\kB T} = \lim_{N \to \infty} \frac{1}{N} \left(- \log Z_N \right).
\end{equation}
(Note that $f$ in equation~(\ref{freeenergyperspin}) is the free energy, not the force). Working again in the approximation that $Z_N \approx \lambda_{+}^N$, find $f$ in terms of $K$ and $L$.
\item Now re-introduce $J$ and $h$, and expand the free energy per spin in the limit that both the temperature and the external field are small.
\item Find the susceptibility at low temperature, i.e.
\begin{equation}
\chi = - \pdv[2]{f}{h}.
\end{equation}

\end{enumerate}