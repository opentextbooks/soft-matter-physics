%Problemtitle Liquid-gas coexistence in the virial expansion
In section~\bookref{sec:idealgasvirialexpansion} \ifproblemset of the notes, \fi we extend the ideal gas model to account for particle-particle interactions, and be able to explain the emergence of the liquid-gas phase transition below a critical temperature~$T_\mathrm{c}$. The virial expansion gives us the pressure as a function of the (number) density $n = N/V$ and the temperature $T$, equation~(\bookref{virialexpeqstate})\ifproblemset
\begin{equation}
\label{virialexpeqstate}
\frac{p}{\kB T} = n + \left(\frac{2\pi}{3} d^3 - \frac{a}{\kB T} \right) n^2 + d^6 n^3.
\end{equation}
In equation~(\ref{virialexpeqstate}), $d$ is the typical size of the particle, and $a$ a parameter that sets the strength of the interaction between two particles. In figure~5.3 of the notes $p$ is plotted as a function of $n$ for various values of $T$; I've taken $a=d=1$ in those plots\fi .

The binodal in a phase diagram is the line separating the region (outside the binodal) in which phases are globally stable from the region in which they can phase-separate. After phase-separation, the two coexisting phases end up on the binodal and are connected by a tie line, see figure~\bookref{fig:liquidgascoexistence}. The spinodal separates the linearly stable from the linearly unstable region within the binodal. %In this problem, we'll work with the equation of state given by the virial expansion in equation~(\bookref{virialexpeqstate}).
\begin{enumerate}[(a)]
\item Find the value of the critical temperature, density and pressure as a function of $a$ and $d$.
\ifproblemset\setcounter{problemcounter}{\value{enumi}}\else\setcounter{problemcounter}{\value{enumii}}\fi
\end{enumerate}
To find pairs of points on the binodal (or equivalently, densities of the coexisting liquid and gas phases at a given value of the temperature below $T^*$), we need to equate both the pressure and the chemical potential. The chemical potential is the derivative of the free energy to the density. Unfortunately, we don't have an expression for the free energy, but we have one for the pressure, which will turn out to be just as good.
\begin{enumerate}[(a)]
\ifproblemset\setcounter{enumi}{\value{problemcounter}}\else\setcounter{enumii}{\value{problemcounter}}\fi
\item \ifproblemset In the spirit of section~\bookref{sec:mixturefreeenergy} of the notes (equations~\bookref{solutionHelmholz}-\bookref{solutionfreeenergydifferential})\else In the spirit of section~\bookref{sec:mixturefreeenergy}\fi, write the free energy of the system as $F(T, V, N) = V f(T, n)$ (with $n = N/V$), then calculate $\mathrm{d}F$ in two ways to find expressions for both $f(T,n)$ and $\mathrm{d}f$.
\item Show from your expression for $\mathrm{d}f$ that
\[ \mu = \left( \diff{f}{n} \right)_T, \]
and use that fact to rewrite your expression for $f(n,T)$ as a differential equation for $f$ (containing the known function $p(n,T)$).
\item As $p(n,T)$ is a polynomial of third order in $n$, our first guess for $f$ might also be a third order polynomial, so a first try could be $f(n,T) = c_0 + c_1 n + c_2 n^2 + c_3 n^3$. You'll find that some of these terms can indeed be used to account for terms in $p$, but one term from $p$ remains. We'll therefore use the Ansatz $f(n,T) = c_0 + c_1 n + c_2 n^2 + c_3 n^3 + g(n, T)$. Find values of the $c_i$'s and the (simpler) differential equation for $g$, then solve for $g$.
\item Now show that, up to an arbitrary constant, you get
\[ \mu = \left[ \frac32 d^6 n^2 + 2 n \left( \frac{2\pi}{3} d^3 - \frac{a}{\kB T} \right) + \log(n) \right] \kB T. \]
\ifproblemset\setcounter{problemcounter}{\value{enumi}}
\end{enumerate}
The conditions that determine the points on the binodal are now
\begin{align*}
\mu(n_1, T) &= \mu(n_2, T),\\
p(n_1, T) &= p(n_2, T).
\end{align*}
We can't solve these analytically as the chemical potential has a log term. We can solve them numerically though; this is how I got the bionodal in figure~\bookref{fig:liquidgascoexistence} of the notes (also shown as figure~\ref{fig:virialexpplotproblem} below), for $a=d=1$. We can however analytically find the \emph{spinodal} (see section~\bookref{sec:binodalsandspinodals} of the notes), which is the line at which our system becomes linearly unstable.
\begin{figure}[ht]
\centering
\includegraphics[scale=1]{solutions/problems/Virialexpansionplotwithspinodal.pdf}
\caption{Plot of the pressure in the virial expansion with critical point (red), binodal (dashed gray) and spinodal (dotted black). Like in the notes, $a=d=1$.}
\label{fig:virialexpplotproblem}
\end{figure}
\begin{enumerate}[(a)]
\setcounter{enumi}{\value{problemcounter}}
\item The spinodal is given by the condition that the second derivative of the free energy (per unit volume) vanishes; use this condition to find an equation for the spinodal (dotted black line in figure~\ref{fig:virialexpplotproblem}). Your answer should be an expression for $p(n)$ that is independent of the temperature.
\item Finally, find the critical point again, from the condition that at this point both the second and the third derivative of the free energy vanish.
\else
\item Finally, also find the spinodal of this system (that you can do analytically) and plot it in the same figure.
\item Verify that the critical point is on the spinodal, and also indicate that point in your graph.
\fi
\end{enumerate}

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item The critical point occurs when both the first and second derivative of $p$ vanish.
\begin{align*}
0 &= \diff{p}{n} = 1 + 2 \left( \frac{2\pi}{3}d^3 - \frac{a}{\kB T} \right) n + 3 d^6 n^2 \\
0 &= \frac{\partial^2 p}{\partial n^2} = 2 \left( \frac{2\pi}{3}d^3 - \frac{a}{\kB T} \right) + 6 d^6 n
\end{align*}
Multiplying the second equation by $n$ and subtracting it from the first gives $1-3 d^6 n^2 = 0$, or $n^* = 1/(d^3 \sqrt{3})$. Substituting this value back in either equation (second is easiest) gives
\[ \kB T^* = \frac{3a}{d^3} \frac{2\pi - 3\sqrt{3}}{4 \pi^2 - 27}. \]
We find the critical pressure simply by putting the critical density and temperature in the given expression for the pressure:
\begin{align*}
p^* &= \frac{a}{3d^6} \frac{2\pi\sqrt{3} - 9}{4 \pi^2 - 27}
\end{align*}
\item We have
\begin{align*}
\mathrm{d}F &= \mathrm{d} (Vf) = V \mathrm{d} f + f \mathrm{d} V \\
&= - S \mathrm{d} T - p \mathrm{d} V + \mu \mathrm{d} (nV) \\
&= V (-s \mathrm{d}T + \mu \mathrm{d} n) + (-p + \mu n) \mathrm{d} V,
\end{align*}
where $s = S/V$. We can now read off that
\begin{align*}
f &= -p + \mu n, \\
\mathrm{d}f &= -s \mathrm{d}T + \mathrm{d}n.
\end{align*}
\item From the expression from $\mathrm{d}f$, we get both
\[ s = - \left( \diff{f}{T} \right)_n, \quad \mbox{and} \quad \mu = \left( \diff{f}{n} \right)_T. \]
Substituting the expression for $\mu$ in the expression for $f$ we find (dropping explicit dependencies on $T$):
\[ f(n) = -p(n) + n \diff{f}{n}. \]
\item We substitute the given Ansatz (putting all terms with $f$ on the left):
\[ c_0 + c_1 n + c_2 n^2 + c_3 n^3 + g(n) - \left[ c_1 n + 2 c_2 n^2 + 3 c_3 n^3 + n \diff{g}{n} \right] = - \left[ n + p_2 n^2 + d^6 n^3 \right],
\]
where $p_2 = \frac{2\pi}{3} d^3 - \frac{a}{\kB T}$. We can read off that
\begin{align*}
c_2 &= p_2 = \frac{2\pi}{3} d^3 - \frac{a}{\kB T},\\
c_3 &= \frac12 d^6,\\
c_0 + g(n) - n \diff{g}{n} &= - n
\end{align*}
To solve the remaining equation, we first look at its homogeneous form:
\[ - n \diff{g}{n} + g(n) = 0, \]
where we can apply separation of variables to get
\[ \frac{1}{g} \mathrm{d}g = \frac{1}{n} \mathrm{d}n,\]
so $\log g = \log(n) + \tilde{C}$, or $g_\mathrm{hom}(n) = C n$ (with $C$ an integration constant). A particular solution can only be found by inspection (perhaps motivated by the entropic term in Flory-Huggins): $g_\mathrm{part}(n) = -c_0 + \kB T n \log n$. Adding them and re-combining with the other parts of $f$, we finally get:
\[ f(n) = \kB T \left[ C n + n \log n + \left( \frac{2\pi}{3} d^3 - \frac{a}{\kB T} \right) n^2 + \frac12 d^6 n^3 \right]. \]
\item Finding $\mu$ is now a simple matter of taking the derivative of $f(n)$, which indeed gives
\[ \mu = \left[ C + \frac32 d^6 n^2 + 2 n \left( \frac{2\pi}{3} d^3 - \frac{a}{\kB T} \right) + \log(n) \right] \kB T. \]
Note that the constant~$C$ will be irrelevant, as we will be comparing values of $\mu$ at different values of $n$.
%\item See dashed gray line in figure~\ref{fig:virialexpplot}.
\item We find the spinodal from the condition that the second derivative of $f$ vanishes, which gives:
\[ \frac{1}{n} + 3 d^6 n + 2 \left( \frac{2\pi}{3} d^3 - \frac{a}{\kB T} \right) = 0. \]
Solving for $\kB T$ gives
\[ \kB T = \frac{6 a n}{3 + 4 \pi d^3 n + 9 d^6 n^2}, \]
which we can substitute for $\kB T$ in the expression~(\ref{virialexpeqstate}) for $p$ to get the spinodal (dotted black line in figure~\ref{fig:virialexpplotproblem}):
\[
p(n) = \frac{6 a n^2}{3 + 4 \pi d^3 n + 9 d^6 n^2} \left[1 + \left(\frac{2\pi}{3} d^3 - \frac{a}{\kB T} \right) n + d^6 n^2 \right].
\]
\item In addition to the condition of (f), we also need the third derivative of $f$ to vanish, which gives:
\[ -\frac{1}{n^2} + 3 d^6 = 0, \]
so the critical value of the density is $n^* = 1/(d^3 \sqrt{3})$. Substituting back in the condition from (f) gives us the critical values of the pressure and temperature:
\begin{align*}
\kB T^* &= \frac{6 a n^*}{3 + 4 \pi d^3 n^* + 9 d^6 (n^*)^2} = \frac{6 a / (d^3 \sqrt{3})}{3 + 4 \pi / \sqrt{3} + 3} = \frac{6a}{d^3} \frac{1}{4 \pi + 6 \sqrt{3}}, \\
p^* &= n^* \kB T^* + \frac{2\pi}{3} d^3 \kB T ^* (n^*)^2 - a (n^*)^2 + d^6 (n^*)^3 = \frac{a}{d^3}\frac{1}{9 + 2 \pi \sqrt{3}}.
\end{align*}
\end{enumerate}

%\begin{figure}[ht]
%\centering
%\includegraphics[scale=1]{solutions/problems/Virialexpansionplotwithspinodal.pdf}
%\caption{Plot of the pressure in the virial expansion with critical point (red), binodal (dashed gray) and spinodal (dotted black). Like in the notes, $a=d=1$.}
%\label{fig:virialexpplot}
%\end{figure}
\fi