% Problemtitle: Maxwell equal area construction for phase coexistence
% Source: Callen examples pages 240-241 and pages 268-270
% Note: second part of original problem 2 of set 7, now bonus.
To find the critical exponent~$\beta$ (the scaling of the order parameter with the reduced temperature along the coexistence line in the phase diagram) for the van der Waals gas, we first need to find the coexistence line. We could try to find the full equation for the free energy as in problem~1, but we'll use a different technique instead, that also applies when solving the differential equation in problem~1 proves difficult. It is known as the \emph{Maxwell equal area construction}, where the `equal area' refers to area under the $p, v$ curve, with $v = V/N = 1/n$. An example $p(v)$ curve for two coexisting phases is shown in figure~\ref{fig:vdWequalareaconstrution} below.

\begin{figure}[h]
\centering
\includegraphics[scale=1]{solutions/problems/equalareaconstructionplot.pdf}
\caption{Equal-area construction in the $(p, v)$ diagram of a Van der Waals gas.}
\label{fig:vdWequalareaconstrution}
\end{figure}

\noindent As you know, phase coexistence requires equal temperatures, pressures, and chemical potentials. The equal-area construction follows from the latter.

To calculate the chemical potential, we'll use the Gibbs-Duhem relation \ifproblemset(see appendix~\bookref{sec:GibbsDuhem} of the notes) \else(see appendix~\bookref{sec:GibbsDuhem}) \fi between the intrinsic variables:
\begin{equation}
\label{GibbsDuhem}
0 = S \,\dd T - V \,\dd p + N \,\dd \mu.
\end{equation}
\begin{enumerate}[(a)]
%\ifproblemset\setcounter{enumi}{\value{problemcounter}}\else\setcounter{enumii}{\value{problemcounter}}\fi
\item From the Gibbs-Duhem relation, show that the difference in the chemical potential between two states at equal temperature is given by
\[ \mu_2 - \mu_1 = \int_1^2 v(p) \,\dd p, \]
where $v = V/N = 1/n$.
\item For coexisting phases, the chemical potentials are equal, so the integral in~(f) evaluates to zero. Convince yourself (and us) that that condition is equal to the condition that the areas I and II in the $p(v)$ plot are equal (note that in the integral, we have $v(p)$!).
\item Express the equal-area condition in terms of an integral over $p(v)$.
\item In the integral, change variables from $v$ to $n$, then re-arrange terms so you can write the condition for coexistence as $\Theta(n_1) = \Theta(n_2)$, with $\Theta(n)$ a to-be-determined function (it will contain two terms, one of which is an integral over $n$).
% then substitute $\tilde{p}(\tilde{n})$ and find the condition on $n_1$ and $n_2$ for coexistence. (You could also first integrate and then change variables; you'll get the same result).
\item Finally, use the two conditions for coexistence to find the dependence of $\hat{n} = \tilde{n}-1$ on $\hat{T} = \tilde{T}-1$ to lowest order, and thus the value of the critical exponent~$\beta$. NB: To do this, you will first need to make a coordinate transformation of $n$ to $\hat{n}$, then expand in $\hat{n}$.
\end{enumerate} 


\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item For constant temperature, $\dd T = 0$, so the Gibbs-Duhem relation gives
\[ \mu = \int \dd \mu = \int v(p) \dd p + C(T), \]
where $v = V/N$ and $C(T)$ an unknown function of the temperature. The difference in chemical potential between states 1 and 2 is thus given by
\[ \mu_2 - \mu_1 = \int_1^2 v(p) \dd p. \]
\item In the figure, we can identify coexisting phase 1 with point A, and phase 2 with point E. We can chop the integral from A to E in four pieces, from A to B, B to C, C to D, and D to E. If the phases coexist, the chemical potentials must be equal, so the sum of these four integrals must vanish:
\[ 0 = \int_A^B v(p) \dd p + \int_B^C v(p) \dd p + \int_C^D v(p) \dd p + \int_D^E v(p) \dd p, \]
or, rearranging
\[ \int_B^C v(p) \dd p - \int_B^A v(p) \dd p = \int_E^D v(p) \dd p - \int_C^D v(p) \dd p. \]
Note that all integrals now give us areas between the curve and the $p$-axis (not the $v$-axis!). The term on the left is exactly area I, and the term on the right area II, so the condition is that they must be equal.
\item If area I equals area II, the integral over $p(v)$ from phase 1 (point A) to phase 2 (point E) simply equals the pressure at coexistence times the difference in reduced volume, or
\[ \int_1^2 p(v) \dd v = p(v_1) (v_2-v_1). \]
(or, equivalently, $p(v_2)$, as $p(v_1) = p(v_2)$ at coexistence).
\item As $v = 1/n$, we can easily change variables in the integral:
\[ \int p(v) \dd v = \int p(n) \frac{\partial v}{\partial n} \dd n = - \int \frac{p(n)}{n^2} \dd n. \]
The condition from (h) then reads
\begin{align*}
- \int_{n_1}^{n_2} \frac{p(n)}{n^2} \dd{n} &= p(n_1) \left(\frac{1}{n_1} - \frac{1}{n_2}\right) = \frac{p(n_2)}{n_2} - \frac{p(n_1)}{n_1},
\end{align*}
where in the last step we again used that $p(n_1) = p(n_2)$. We can split the left integral in two parts, one from $n_1$ to some intermediary reference point $n_\mathrm{C}$, and the second from $n_\mathrm{C}$ to $n_2$:
\begin{align*}
- \int_{n_1}^{n_2} \frac{p(n)}{n^2} \dd{n} &= - \int_{n_1}^{n_\mathrm{C}} \frac{p(n)}{n^2} \dd{n} -  \int_{n_\mathrm{C}}^{n_2} \frac{p(n)}{n^2} \dd{n}.
\end{align*}
We can now bring all terms with $n_1$ to the left, and $n_2$ to the right to get
\begin{align*}
\int_{n_\mathrm{C}}^{n_1} \frac{p(n)}{n^2} \dd{n} + \frac{p_1}{n_1} &= \int_{n_\mathrm{C}}^{n_2} \frac{p(n)}{n^2} \dd{n} + \frac{p_2}{n_2}
\end{align*}
(note that we used the minus sign to swap the integration bounds on the left integral). Defining the new function
%In re-scaled coordinates, we have
%\[ - \int_1^2 \left( \frac{8 \tilde{T}}{(3-\tilde{n})\tilde{n}} - 3 \right) \dd \tilde{n} = \left[ 3 \tilde{n} + \frac83 \tilde{T} \log \left(\frac{3}{\tilde{n}} - 1 \right) \right]_1^2 = \tilde{p}(\tilde{n}_1) \left(\frac{1}{\tilde{n}_2} - \frac{1}{\tilde{n}_1}\right).\]
%Defining the new function
\[ \Theta(n) = - \int^n \frac{p(n')}{n'^2} \dd n' - \frac{p(n)}{n}, \]
the condition can then be written as $\Theta(n_1) = \Theta(n_2)$.
\item We expand both $\Theta(n)$ and $p(n)$ about $\hat{n} = \tilde{n} - 1$:
\begin{align*}
\Theta(\hat{n}) &= 2 - 4 \hat{T} + \frac83 (1+\hat{T}) \log(2) - 6 \hat{T} \hat{n} - \frac32 (1+\hat{T}) \hat{n}^3 + \frac38 (1+\hat{T}) \hat{n}^4 + \mathcal{O}\left(\hat{n}^4\right),\\
p(\hat{n}) &= 1 + 4 \tilde{T} + 6\hat{T} \hat{n} + 3 \hat{T} \hat{n}^2 + \frac32 (1+\hat{T}) \hat{n}^3 + \frac34 (1+\hat{T}) \hat{n}^4 + \mathcal{O}\left(\hat{n}^4\right).
\end{align*}
From the equilibrium conditions, we then find at coexistence (to third order, which turns out to be enough):
\begin{align*}
4 \hat{T} \hat{n}_1 + (1+\hat{T})\hat{n}_1^3 &= 4 \hat{T} \hat{n}_2 + (1+\hat{T})\hat{n}_2^3, \\
4\hat{T} \hat{n}_1 + 2 \hat{T} \hat{n}_1^2 + (1+\hat{T}) \hat{n}_1^3 &= 4\hat{T} \hat{n}_2 + 2 \hat{T} \hat{n}_2^2 + (1+\hat{T}) \hat{n}_2^3.
\end{align*}
By using the first equation to simplify the second, we get
\[ \hat{n}_1 = \pm \hat{n}_2.\]
Naturally, the plus sign means that we're in the same phase, so we need the minus sign (distances to the equilibrium point can be negative). Substituting $\hat{n}_1 = - \hat{n}_2$ back into either equation for equilibrium gives:
\[ 2 \left( 4 \hat{T} \hat{n} + (1+\hat{T})\hat{n}^3 \right) = 0 \]
The solution $\hat{n} = 0$ corresponds to the single-state ($\hat{T} > 0$) or the local maximum; what we want is the other two solutions (for which $\hat{T} < 0$), which have
\[ \hat{n}^2 = - \frac{4 \hat{T}}{1+\hat{T}} = - 4 \hat{T} + \mathcal{O}\left(\hat{T}^2\right), \]
and thus to lowest order, $\hat{n} \propto |\hat{T}|^{1/2}$, so the critical exponent $\beta$ is again $1/2$.
\end{enumerate}
\fi
