% Problemtitle Electric double layer in the Donnan potential
In our discussion of the Donnan equilibrium in section~\bookref{sec:GibbsDonnan}, we introduced the Donnan potential difference~$\Delta \psi$ across the membrane, or between a piece of polyelectrolyte gel and its surrounding fluid. You may wonder what the origin of this potential difference is, as we strictly imposed the charge neutrality condition, and therefore no macroscopic region in our system has net charge. The answer is that even though the net charge is zero everywhere, the local density of charges is not constant, especially near the boundary, which gives us a nonzero electric dipole across the boundary and hence a nonzero electric potential.

In our discussion of the Donnan effect, we assumed that the free ions behave as a free ionic gas, so their densities are constant in each compartment, but they can jump at the boundary. We can relax this condition to the more realistic case that the density changes continuously, as long as we keep the clear (by approximation sharp) difference between the densities of the bound charges, cf. figure~\ref{fig:electricdoublelayer}b. Assuming as before that the ions are all monovalent, we can then write for the charge density as a function of a coordinate~$z$ perpendicular to the boundary
\begin{equation}
\label{electricdoublelayerchargedensity}
\rho_\mathrm{e}(z) = e \left[n_+(z) - n_-(z) - n_\mathrm{b}(z) \right].
\end{equation}
If we have a sharp change in $n_\mathrm{b}$ at the boundary, there will also be a sharp change in $\rho_\mathrm{e}$, as sketched in figure~\ref{fig:electricdoublelayer}c. The charge density gets two peaks, representing an electric double layer, of positive and negative charges at the two sides of the boundary. The charge neutrality condition implies that the integral over $\rho_\mathrm{e}$ must vanish:
\begin{equation}
\int_{-\infty}^\infty \rho_\mathrm{e}(z) \dd{z} = 0.
\end{equation}
The dipole moment (which is simply the first moment of $\rho_\mathrm{e}$) of our electric double layer however is not zero
\begin{equation}
\label{electricdoublelayerdipole}
P_\mathrm{e} = \int_{-\infty}^\infty z \rho_\mathrm{e}(z) \dd{z}.
\end{equation}
In this case, the charge density is built up across the boundary, not screened, so we can get the potential from the charge density through the Poission equation, which in our one-dimensional case is given by
\begin{equation}
\varepsilon \dv[2]{\psi}{z} = \rho_\mathrm{e}(z).
\end{equation}
Use the Poisson equation and equation~(\ref{electricdoublelayerdipole}) to prove that the potential difference across the membrane is given by
\begin{equation}
\label{electricdoublelayerdipole2}
\Delta \psi = \frac{P_\mathrm{e}}{\varepsilon}.
\end{equation}

\begin{figure}[ht]
\centering
\includegraphics[scale=1]{solutions/electricdoublelayer.pdf}
\caption[Electric double layer near an interface.]{Electric double layer near the boundary between a polyelectrolyte and a membrane or ambient solution. (a) Equilibrium between a simple salt solution and an ionic gel. (b) Number density of ions and bound charges. (c) Charge density.}
\label{fig:electricdoublelayer}
\end{figure}

\ifincludesolutions
\Solution
We simply substitute the Poisson equation in the expression~(\ref{electricdoublelayerdipole}) for the dipole moment, and integrate by parts to get
\begin{align*}
P_\mathrm{e} &= \int_{-\infty}^\infty z \rho_\mathrm{e}(z) \dd{z} = - \varepsilon \int_{-\infty}^\infty z \dv[2]{\psi}{z} \dd{z} = \varepsilon \int_{-\infty}^\infty \dv{z}{z} \dv{\psi}{z} \dd{z} = \varepsilon \int_{-\infty}^\infty \dv{\psi}{z} \dd{z} = \varepsilon \int_{-\infty}^\infty \dd{\psi} = \varepsilon \Delta \psi,
\end{align*}
which is equation~(\ref{electricdoublelayerdipole2}).
\fi