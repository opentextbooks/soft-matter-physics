% Problemtitle Bjerrum length
% Source: Inspired by the calculation in Strobl, section 3.3, pages 90-92.
Consider a single rod-like chain with radius $r_0$, with a linear charge density $\lambda$.
\begin{enumerate}[(a)]
\item In the limit that the rod is infinitely long, show that its electric potential is given by % Cf. Griffiths E&M (3rd ed) problems 2.13, 2.22 and 3.23.
\begin{equation}
\label{rodelectricpotential}
\psi(r) = - \frac{\lambda}{2 \pi \varepsilon} \log\left(\frac{r}{r_0}\right).
\end{equation}
Hint: You can either find the potential from the Poisson equation in cylindrical coordinates (for which the Laplacian reads $\nabla^2 = r^{-1} \partial_r \left( r \partial_r \right) + r^{-2} \partial_\phi^2 + \partial_z^2$) or by first finding the electric field from Gauss's law and integrating.
\ifproblemset\setcounter{problemcounter}{\value{enumi}}\else\setcounter{problemcounter}{\value{enumii}}\fi
\end{enumerate}
We now consider the situation that a positively charged chain is in a solution with a low concentration of (negatively charged) monovalent anions. We can then ask how closely the charges on the chain must be packed, i.e. what the charge density on the chain must be, for the anions to get effectively bound to the chain (or, such that the attractive energy trumps the anion's entropy). To answer this question, we first note that (ignoring interactions between the anions) the probability of finding an anion at a distance $r$ from the center of the chain can be found from its potential energy, through
\begin{equation}
\label{chargedlineanionprobabilitydistribution}
P(\mbox{anion at distance }r) = P(\mbox{anion energy is } U(r)) = \frac{1}{Z} \exp\left(-\frac{U(r)}{\kB T}\right),
\end{equation}
where $Z$ is the partition function, and the potential energy of the anion is given by $U(r) = -e \psi(r)$.
\begin{enumerate}[(a)]
\ifproblemset\setcounter{enumi}{\value{problemcounter}}\else\setcounter{enumii}{\value{problemcounter}}\fi
\item Argue why we can write the partition function in equation~(\ref{chargedlineanionprobabilitydistribution}) as
\begin{equation}
\label{chargedlineanionpartitionfunction}
Z = h \int_{r_0}^\infty 2 \pi r \exp\left(\frac{e\psi(r)}{\kB T}\right) \dd r,
\end{equation}
where $h$ is the thickness of a small slice of space that only contains a single anion.
\item Evaluate the integral in the partition function~(\ref{chargedlineanionpartitionfunction}). Use $\alpha = \lambda e / (2 \pi \varepsilon \kB T)$.
\item From (c), find the critical value of $\alpha$ (for which the integral diverges), and thus the critical value of the charge density $\lambda$ of the chain.
\item The critical charge density correspond to a given distance between charges on the rod, known as the \index{Bjerrum length}\emph{Bjerrum length} $\xi_\mathrm{B}$, given by
\begin{equation}
\label{Bjerrumlengthinproblem}
\xi_\mathrm{B} = \frac{e^2}{4\pi\varepsilon \kB T}.
\end{equation}
Verify the expression~(\ref{Bjerrumlengthinproblem}) for the Bjerrum length, and its interpretation as given \ifproblemset in section~\bookref{sec:chargedpolymersinsaltsolution} of the notes.\else in section~\bookref{sec:chargedpolymersinsaltsolution}.\fi
\end{enumerate}

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item Outside the wire, there's no charge, and the Poisson equation reduces to the Laplace equation, with a boundary condition at $r=r_0$. By symmetry, the potential has to be independent of both $\phi$ and $z$, so we are left with an easy differential equation in $r$:
\[ \nabla^2 \psi(r) = \frac{1}{r} \frac{\dd}{\dd r} \left( r \frac{\dd V_\mathrm{e}(r)}{\dd r} \right) = 0. \]
We can easily find the general solution by integrating twice, which gives
\[ \psi(r) = A \log(r) + C, \]
where $A$ and $C$ are integration constants. We can't set boundary conditions at $r=0$ or $r \to \infty$ as we would usually do, but we can set conditions at $r = r_0$. There, we pick the zero of the potential ($V_\mathrm{e}(r_0) = 0$), so $C = - A \log(r_0)$. The derivative of the potential should equal the electric field at $r=r_0$, which is given by $\lambda / 2 \pi r_0 \varepsilon$. We find $A = \lambda / 2\pi \varepsilon$, so the solution is given by
\[ \psi(r) = -\frac{\lambda}{2 \pi \varepsilon} \log\left(\frac{r}{r_0}\right). \]
\item The partition function simply is the normalization factor of the probability distribution. Here, we have to account for all possible positions of the anion, i.e., we have to integrate over the slice of thickness~$h$, starting from $r=r_0$. In cylindrical coordinates, independent of the angle, we then arrive at the given integral.
\item Evaluating the integral is straightforward:
\[ Z = h \int_{r_0}^\infty 2 \pi r \exp\left[ \alpha \log \left( \frac{r_0}{r} \right) \right] \dd r = 2 \pi h \int_{r_0}^\infty r_0^\alpha r^{1-\alpha} \dd r = \frac{2 \pi h r_0^\alpha}{2-\alpha} \left[ r^{2-\alpha} \right]_{r_0}^\infty. \]
\item Obviously our expression blows up for $\alpha > 2$, so the critical value of $\alpha$ equals $2$, which gives a critical density
\[ \lambda_\mathrm{c} = \frac{4 \pi \varepsilon \kB T}{e}. \]
\item The Bjerrum length is now simply the distance between charges of magnitude~$e$ that give a charge density equal to $\lambda_\mathrm{c}$, so we find
\[ \xi_\mathrm{B} = \frac{e}{\lambda} = \frac{e^2}{4 \pi \varepsilon \kB T}. \]
\end{enumerate}
\fi