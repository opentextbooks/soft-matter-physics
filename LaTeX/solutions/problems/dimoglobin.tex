% Problemtitle Dimoglobin
% Source Phillips 7.3
\begin{figure}[h!]
\centering
\includegraphics[scale=1]{solutions/problems/dimoglobin.pdf}
\caption{Dimoglobin molecule}
\label{fig:dimoglobin}
\end{figure}
As a toy model for hemoglobin (the oxygen-binding protein in red blood cells), we consider `dimoglobin', a dimer with two oxygen-binding sites. We wish to find the probabilities of the various states of binding (zero, one or two bound oxygen molecules per dimoglobin). In a fashion similar to the Flory-Huggins model, we consider a box with $N$ $O_2$ molecules that can be distributed among $\Omega$ sites, as shown in figure~\ref{fig:dimoglobin}.

The partition function will consist of three terms, which are functions of $\varepsilon_\mathrm{s}$ (the energy of an unbound oxygen molecule), $\varepsilon_\mathrm{B}$ (the binding energy of an oxygen molecule) and $\varepsilon_2$ (the additional binding energy of a second oxygen molecule if one is already bound, i.e., the energy cost of binding the second molecule is $\varepsilon_\mathrm{B}+\varepsilon_2$).

\begin{enumerate}[(a)]
\item Write down the partition function for the oxygen-dimoglobin model.
\item From the partition function, calculate the expectation value $\ev{N_\mathrm{bound}}$ of the number of bound molecules.
\item Assuming $\Omega \gg N \gg 1$, simplify your expression at~(b), then plot it (as a function of $N/\Omega$) for $\Delta \varepsilon = \varepsilon_\mathrm{B} - \varepsilon_\mathrm{s} = - 5.0 \kB T$ and three values of $\varepsilon_2$, namely $\varepsilon_2 = 0 \kB T$ (no cooperative binding), $\varepsilon_2 = -2.5 \kB T$, and $\varepsilon_2 = -5.0 \kB T$.
\item Find and plot the probabilities $p_0$, $p_1$, and $p_2$ corresponding to occupancy $0$, $1$, and $2$, respectively. For your plot, use $\Delta \varepsilon = - 5.0 \kB T$, $\varepsilon_2 = - 2.5 \kB T$, and a logarithmic scale for the horizontal axis (you'll have to go to low values of $N/\Omega$ to see the shapes of the curves).
\end{enumerate}

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item The partition function contains three terms, accounting for the possibilities that all oxygen molecules are unbound, one oxygen molecule is bound to the dimoglobin, and two oxygen molecules are bound to the dimoglobin. If all $N$ oxygen molecules are unbound, they can be distributed over the $\Omega$ sites, so the corresponding term is
\[ Z_0 = \binom{\Omega}{N} e^{-\beta \varepsilon_\mathrm{s}} = \frac{\Omega!}{N! (\Omega-N)!} e^{-\beta N \varepsilon_\mathrm{s}}. \]
With one bound oxygen molecule, we have $N-1$ free molecules left, and the partition function becomes (note that we need to account for the two binding sites):
\[Z_1 = 2 \binom{\Omega}{N-1} e^{-\beta (N-1) \varepsilon_\mathrm{s}} e^{-\beta \varepsilon_\mathrm{B}}. \]
Finally, for two bound oxygen molecules, we have $N-2$ free molecules, and an additional energy for binding the second oxygen:
\[ Z_2 = \binom{\Omega}{N-2} e^{-\beta (N-2) \varepsilon_\mathrm{s}} e^{-\beta (2\varepsilon_\mathrm{B} + \varepsilon_2}). \]
The total partition function is simply the sum of these tree contributions, $Z = Z_0 + Z_1 + Z_2$.
\item We have (note the factor $2$ in front of $Z_2$ as it corresponds to a state with two bound oxygen molecules):
\[ \ev{N_\mathrm{bound}} = \frac{Z_1 + 2 Z_2}{Z}.\]
\item To apply the limits, we first note that we can re-write the answer at (b) as
\[ \ev{N_\mathrm{bound}} = \frac{\frac{Z_1}{Z_0} + 2 \frac{Z_2}{Z_0}}{1 + \frac{Z_1}{Z_0} + \frac{Z_2}{Z_0}}.\]
Now evaluating the two fractions and taking the limits, we get:
\begin{align*}
\frac{Z_1}{Z_0} &= 2 \frac{\Omega! N! (\Omega - N)!}{\Omega! (N-1)! (\Omega-N+1)!} e^{-\beta (\varepsilon_\mathrm{B} - \varepsilon_\mathrm{s})} = 2 \frac{N}{\Omega - N + 1} e^{-\beta \Delta \varepsilon} \to 2 \frac{N}{\Omega} e^{-\beta \Delta \varepsilon}, \\
\frac{Z_2}{Z_0} &= 2 \frac{\Omega! N! (\Omega - N)!}{\Omega! (N-2)! (\Omega-N+2)!} e^{-\beta (2\varepsilon_\mathrm{B} - 2 \varepsilon_\mathrm{s} + \varepsilon_2)} = \frac{N (N-1)}{(\Omega - N + 2)(\Omega - N + 1)} e^{-\beta (2\Delta \varepsilon + \varepsilon_2)} \to \frac{N^2}{\Omega^2}e^{-\beta (2\Delta \varepsilon + \varepsilon_2)},
\end{align*}
where $\Delta \varepsilon = \varepsilon_\mathrm{B} - \varepsilon_\mathrm{s}$. Substituting these expressions in the expectation value of the number of bound molecules, we obtain
\begin{align*}
\ev{N_\mathrm{bound}} &= 2\frac{\frac{N}{\Omega} e^{-\beta \Delta \varepsilon} + 2\frac{N^2}{\Omega^2}e^{-\beta (2\Delta \varepsilon + \varepsilon_2)}}{1 + 2 \frac{N}{\Omega} e^{-\beta \Delta \varepsilon} + \frac{N^2}{\Omega^2}e^{-\beta (2\Delta \varepsilon + \varepsilon_2)}}.
\end{align*}
The corresponding plots for the specified values of the energies are shown in figure~\ref{fig:dimoglobinbindingexpectation}. Note that for $\varepsilon_2 = 0$, we get the simple inverted exponential $\ev{N_\mathrm{bound}} = 2 / (1 + (N/\Omega) e^{-\beta \Delta \varepsilon})$.
\item The probabilities of the different occupancies can be calculated directly from the partition function:
\begin{align*}
p_0 &= \frac{Z_0}{Z} = \frac{1}{1 + 2 \frac{N}{\Omega} e^{-\beta \Delta \varepsilon} + \frac{N^2}{\Omega^2}e^{-\beta (2\Delta \varepsilon + \varepsilon_2)}},\\
p_1 &= \frac{Z_1}{Z} = \frac{2 \frac{N}{\Omega} e^{-\beta \Delta \varepsilon}}{1 + 2 \frac{N}{\Omega} e^{-\beta \Delta \varepsilon} + \frac{N^2}{\Omega^2}e^{-\beta (2\Delta \varepsilon + \varepsilon_2)}},\\
p_2 &= \frac{Z_2}{Z} = \frac{\frac{N^2}{\Omega^2}e^{-\beta (2\Delta \varepsilon + \varepsilon_2)}}{1 + 2 \frac{N}{\Omega} e^{-\beta \Delta \varepsilon} + \frac{N^2}{\Omega^2}e^{-\beta (2\Delta \varepsilon + \varepsilon_2)}}.
\end{align*}
The corresponding plots are shown in figure~\ref{fig:dimoglobinoccupancyprobability}.
\end{enumerate}

\begin{figure}
\centering
\includegraphics[scale=0.8]{solutions/problems/dimoglobinbindingexpectation.pdf}
\caption{The expected number of bound oxygen molecules as a function of the molecule concentration.}
\label{fig:dimoglobinbindingexpectation}
\end{figure}

\begin{figure}
\centering
\includegraphics[scale=0.8]{solutions/problems/dimoglobinoccupancyprobability.pdf}
\caption{The expected number of bound oxygen molecules as a function of the molecule concentration.}
\label{fig:dimoglobinoccupancyprobability}
\end{figure}


%See Phillips problem 7.3 (attached) and the figure below.
%\begin{center}
%\includegraphics[scale=0.5]{../Soft Matter 2020/Assignments/Phillips_figure_07_18.jpg}
%\end{center}
\fi