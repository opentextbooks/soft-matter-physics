% Problemtitle Membrane potentials
% Source Final 2020-2021, Inspired by Nelson - Biological physics Ch. 4.6
Usually, both the inside and the outside of a cell (or a cellular organelle) are ionic solutions (cells are very unhappy if you place them in pure water). Because the solutions are typically not the same, there will be an osmotic pressure difference across the membrane. Moreover, due to differences in charge on both sides of the membrane, there may be an electric potential difference as well. As you probably know, cells have active ion pumps to build up or maintain these potentials, and channels that can open quickly to relax them. In this problem, we'll look at a simplified case of a flat membrane in a chamber with an electric field magnitude $E$, resulting in a potential difference $\Delta \psi = E \cdot L$ across the chamber (where $L$ is the width of the chamber).

\begin{figure}[ht]
\centering
\includegraphics[scale=1]{solutions/problems/chamberwithelectrolyte.pdf}
\caption[Membrane potentials.]{Sketch of the setting of problem~\ref{pb:membranepotentials}. We apply a potential difference~$\Delta \psi$ across a chamber of width~$L$, filled with an ionic solution.}
\label{fig:chamberwithelectrolyte}
\end{figure}

\begin{enumerate}[(a)]
\item Argue why, in the absence of a membrane in the chamber, we expect a small particle with charge~$q$ and viscous drag coefficient~$\zeta$ to move with speed
\begin{equation}
\label{electrolytedrift}
v_\mathrm{drift} = \frac{q E}{\zeta}.
\end{equation}
\item Suppose the chamber is filled with ions at number density~$c$. Argue that, due to the presence of the electric field, the flux~$J$ of ions (i.e., the number of particles passing through per unit time) through a cross-sectional area~$A$ of the chamber is given by
\begin{equation}
\label{electrophoreticflux}
J = \frac{q E c}{\zeta}.
\end{equation}
We call the flux in~(\ref{electrophoreticflux}) the \emph{electrophoretic flux}.
\ifproblemset\setcounter{problemcounter}{\value{enumi}}\else\setcounter{problemcounter}{\value{enumii}}\fi
\end{enumerate}
If the density of ions in the solution is not uniform, they will exhibit net motion due to diffusion. The diffusive flux is given by Fick's (first) law, which in one dimension is given by
\begin{equation}
\label{Fickflux}
J(x) = - D \dv{c}{x},
\end{equation}
where $D$ is the diffusion constant. Combining the two fluxes, we have 
\begin{equation}
\label{NPflux}
J(x) = \frac{q E}{\zeta} c(x) - D \dv{c}{x} = -D \left[ \dv{c}{x} + \frac{q}{\kB T} E c(x) \right],
\end{equation}
where in the second equality we used the famous Stokes-Einstein relation $D = \kB T / \zeta$ relating the diffusion and drag coefficients. Equation~(\ref{NPflux}) is known as the \emph{Nernst-Planck formula}.
\begin{enumerate}[(a)]
\ifproblemset\setcounter{enumi}{\value{problemcounter}}\else\setcounter{enumii}{\value{problemcounter}}\fi
\item \label{pb:NPeq} From the Nernst-Planck formula, find the electric field (in terms of the function $c(x)$ describing the number density, and derivatives of $c(x)$) for which we get zero flux.
\item From your answer at (\ref{pb:NPeq}), derive the \emph{Nernst relation} for the electrostatic potential in equilibrium:
\begin{equation}
\label{Nernstrelation}
\Delta (\log(c)) = - \frac{q}{\kB T} \Delta \psi_\mathrm{eq},
\end{equation}
where $\Delta (\log(c)) = \log(c_\mathrm{right}) - \log(c_\mathrm{left})$ and $\Delta \psi_\mathrm{eq} = \psi_\mathrm{right} - \psi_\mathrm{left} = - E L$ represent the jumps in concentration and potential across the chamber, respectively.
\item Suppose we wish to maintain a concentration difference of a factor 10 (i.e., $c_\mathrm{right} / c_\mathrm{left} = 10$) for a monovalent ion. Find the necessary potential difference. You may use that at room temperature, $\kB T / e = (1/40)\;\mathrm{volt}$. (Actual cells indeed do maintain concentration differences of this scale, and carry a corresponding Nernst potential on their membranes; the story is of course much more complicated due to the active processes involved in the ion pumps).
\item \ifproblemset (Bonus problem).\fi Finally, let's consider a different case, where instead of applying an electric field across a room, we stick two electrodes in the ionic solution. At the electrodes, the ions donate or receive an electron, becoming neutral atoms. Therefore, in this case we don't reach a steady-state distribution, but a steady-state flux, which by equation~(\ref{NPflux}) is related to the applied electric field~$E$ through
\[ E = \frac{\kB T}{D q c} J. \]
Show that for this case, the potential difference and electric current are related by  Ohm's law, $\Delta V = I R$, and find an expression for the electrical resistance~$R$ of the cell. \textit{Hint}: Remember that the current~$I$ is the charge per unit time, and that it has the `wrong' sign (going from anode to cathode).
\end{enumerate}

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item The electric force on the charged particle is given by $F_\mathrm{el} = qE$, and the drag force by $F_\mathrm{drag} = \zeta v$ \score{1 pt}. The drift velocity~(\ref{electrolytedrift}) is simply the velocity at which the two forces balance \score{1 pt}.
\item The flux is defined as the number of particles crossing the area per unit time. For a volume $V$, the total number of particles equals~$c V$. In a short time $\dd{t}$, a volume $A v \dd{t}$ crosses through the area $A$, so the flux is given by
\[ J = \frac{c A v_\mathrm{drift}}{A} = \frac{c qE}{\zeta} \]
\score{2 pt}.
\item We simply equate the given expression to zero, which yields
\[ E = \frac{\kB T}{q} \frac{1}{c} \dv{c}{x} \]
\score{1 pt}.
\item Separating variables, we can re-write the answer of (\ref{pb:NPeq}) as
\[ \frac{1}{c} \dd{c} = \frac{q}{\kB T} E \dd{x}. \]
Integrating on both sides gives
\begin{align*}
\int_{c_\mathrm{left}}^{c_\mathrm{right}} \frac{1}{c} \dd{c} &= \frac{q}{\kB T} E \int_{x_\mathrm{left}}^{x_\mathrm{right}} \dd{x} \\
\log(c_\mathrm{right}) - \log(c_\mathrm{left}) &= \frac{q}{\kB T} E L = \frac{q}{\kB T} (-\Delta \psi_\mathrm{eq})\\
\Delta(\log(c)) &= - \frac{q}{\kB T} \Delta \psi_\mathrm{eq}
\end{align*}
\score{2 pt}.
\item We can simply substitute numbers in equation~(\ref{Nernstrelation}), using that the difference between two logarithms is the logarithm of their ratio. Therefore:
\[ \Delta \psi_\mathrm{eq} = - \frac{\kB T}{e} \log(10) = -58\;\mathrm{mV}\]
\score{1 pt; also give point if opposite sign used in answer as sign of charge was never specified}.
\item The current $I$ equals the amount of charge per unit time, which is the flux times the area times the charge per particle: $I = - q A J$. Therefore:
\[ \Delta V = - E L = - \frac{\kB T}{D q c} J L = \frac{\kB T L}{D q^2 A c} I = R I, \]
which is indeed Ohm's law, with
\[ R = \frac{\kB T}{D q^2 c} \frac{L}{A} \]
\score{2 pt}.
\end{enumerate}
\fi