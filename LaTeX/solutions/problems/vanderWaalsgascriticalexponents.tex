% Problemtitle Critial exponents in the Van der Waals gas
The equation of state of a Van der Waals gas is given by
\begin{equation}
\label{vanderWaalsgas}
p = \frac{N \kB T}{V-Nb} - \frac{a}{(V/N)^2},
\end{equation}
where $a$ and $b$ are constants. In contrast to the ideal gas, the Van der Waals gas allows for the coexistence of two states with different values of the number density $n = N/V$. In this problem, we'll derive the conditions for these phases to coexist, and find the critical exponents associated with the coexistence line in the phase diagram.
\begin{enumerate}[(a)]
\ifproblemset\item Give a physical interpretation of the parameters $a$ and $b$. \textit{Hint}: first determine their dimensions and compare equation~(\ref{vanderWaalsgas}) with the ideal gas law.\fi
%\item Expand this equation of state in a virial expansion, and express the virial coefficients in terms of $a$ and $b$. % Callen 13.3-2.
\item Find the critical temperature, density and pressure for a Van der Waals gas. We'll denote these as $T^*$, $n^*$ and $p^*$, respectively.
\item Re-write the equation of state in terms of rescaled parameters $\tilde{n} = n/n^*$, $\tilde{p} = p/p^*$ and $\tilde{T} = T/T^*$.
\item Expand the equation of state around $\hat{n} = (n-n^*)/n^* = \tilde{n} - 1$ (i.e., expand $\tilde{n}$, but about $1$, not about $0$) to find the critical exponent~$\delta$, i.e., the scaling of $\hat{n}$ with $\hat{p} = \tilde{p} - 1$.
\item (bonus) Determine the critical exponent~$\gamma$ of the susceptibility~$\chi$, here given by
\[ \chi^{-1} = \left[ \frac{1}{\hat{n}+1} \left( \frac{\dd \hat{p}}{\dd \hat{n}} \right)_T \right]_{\hat{n} \to 0}. \]
(Note that from the relation between $p(n)$ and $f(n)$ you found in problem~\ref{pb:virialexpansionphasediagram}, you can easily check that this susceptibility is the same as $(\dd^2 f / \dd n^2)^{-1}$, which was the way we calculated it in class).
\ifproblemset\setcounter{problemcounter}{\value{enumi}}\else\setcounter{problemcounter}{\value{enumii}}\fi
\end{enumerate} 
To find the critical exponent~$\beta$ (the scaling of the order parameter with the reduced temperature along the coexistence line in the phase diagram), we first need to find the coexistence line. We could try to find the full equation for the free energy as in problem~1, but we'll use a different technique instead, that also applies when solving the differential equation in problem~1 proves difficult. It is known as the \emph{Maxwell equal area construction}, where the `equal area' refers to area under the $p, v$ curve, with $v = V/N = 1/n$. An example $p(v)$ curve for two coexisting phases is shown in figure~\ref{fig:vdWequalareaconstrution} below.
\begin{figure}[h]
\centering
\includegraphics[scale=1]{solutions/problems/equalareaconstructionplot.pdf}
\caption{Equal-area construction in the $(p, v)$ diagram of a Van der Waals gas.}
\label{fig:vdWequalareaconstrution}
\end{figure}


\noindent As you know, phase coexistence requires equal temperatures, pressures, and chemical potentials. The equal-area construction follows from the latter.

To calculate the chemical potential, we'll use the Gibbs-Duhem relation \ifproblemset(see appendix~\bookref{sec:GibbsDuhem} of the notes) \else(see appendix~\bookref{sec:GibbsDuhem}) \fi between the intrinsic variables:
\begin{equation}
\label{GibbsDuhem}
0 = S \dd T - V \dd p + N \dd \mu.
\end{equation}
\begin{enumerate}[(a)]
\ifproblemset\setcounter{enumi}{\value{problemcounter}}\else\setcounter{enumii}{\value{problemcounter}}\fi
\item From the Gibbs-Duhem relation, show that the difference in the chemical potential between two states at equal temperature is given by
\[ \mu_2 - \mu_1 = \int_1^2 v(p) \dd p, \]
where $v = V/N = 1/n$.
\item For coexisting phases, the chemical potentials are equal, so the integral in~(f) evaluates to zero. Convince yourself (and us) that that condition is equal to the condition that the areas I and II in the $p(v)$ plot are equal (note that in the integral, we have $v(p)$!).
\item Express the equal-area condition in terms of an integral over $p(v)$.
\item In the integral, change variables from $v$ to $n$, then re-arrange terms so you can write the condition for coexistence as $\Theta(n_1) = \Theta(n_2)$, with $\Theta(n)$ a to-be-determined function (it will contain two terms, one of which is an integral over $n$).
% then substitute $\tilde{p}(\tilde{n})$ and find the condition on $n_1$ and $n_2$ for coexistence. (You could also first integrate and then change variables; you'll get the same result).
\item Finally, use the two conditions for coexistence to find the dependence of $\hat{n} = \tilde{n}-1$ on $\hat{T} = \tilde{T}-1$ to lowest order, and thus the value of the critical exponent~$\beta$. NB: To do this, you will first need to make a coordinate transformation of $n$ to $\hat{n}$, then expand in $\hat{n}$.
\end{enumerate} 


\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\ifproblemset\item From the denominator in the first term, $V - Nb$, we can read off that compared to an ideal gas, the total available volume shrinks with $Nb$. We can thus interpret $b$ as the volume per particle.

The second term reduces the pressure compared to an ideal gas. The pressure in an ideal gas originates from collisions of the particles with the walls of the box that contains the gas. There are no other interactions. In a real gas, the particles also attract each other. For a particle in the bulk, these attractive forces from other particles cancel, but for a particle near the wall, there is a net attraction to the bulk, which reduces the net pressure on the walls. The scaling is because the reduction depends on the number of particle pairs.\fi
%\item The virial expansion is in the density $n$, so we first need to re-write the given equation of state, then expand it as a series in $n$:
%\[ p = \frac{(N/V) \kB T}{1 - (N/V) b} - a (N/V)^2 = \frac{n}{1-nb}\kB T - a n^2 = n \kB T + (b \kB T - a) n^2 + b^2 \kB T n^3 + \mathcal{O}\left(n^4\right).\]
%We can thus read off that $B_2(T) = b - a/\kB T$ and $B_3(T) = b^2$.
\item At the critical point, the first and second derivatives of the pressure as a function of the density (or, equivalently, the volume per particle~$v$) vanish. We have
\begin{align*}
\frac{\partial p}{\partial n} &= \frac{\kB T}{1-n b} \left(1+\frac{nb}{1-nb} \right) - 2 a n, \\
\frac{\partial^2 p}{\partial n^2} &= \frac{2 b \kB T}{(1-nb)^2}\left(1+\frac{nb}{1-nb} \right) - 2a.
\end{align*}
Equating the two derivatives to zero, we can easily find the critical density and temperature:
\[ n^* = \frac{1}{3b}, \quad \kB T^* = \frac{8 a}{27 b}. \]
Substituting back, we find for the critical pressure $p^* = a/(27b^2)$.
\item We define
\begin{align*}
\tilde{n} &= \frac{n}{n^*} = 3 b n, \\
\tilde{p} &= \frac{p}{p^*} = \frac{27 b^2}{a} p, \\
\tilde{T} &= \frac{T}{T^*} = \frac{27 b}{8a} \kB T.
\end{align*}
From the equation of state, we find
\[ \tilde{p} = \frac{27 b^2}{a} p = \frac{27 b^2}{a} \left[ \frac{\tilde{n}/3b}{1-\tilde{n}/3} \frac{8 a}{27 b} \tilde{T} - \frac{a}{9 b^2} \tilde{n}^2 \right] = \frac{8\tilde{n}}{3-\tilde{n}} \tilde{T} - 3\tilde{n}^2. \]
\item To expand around the critical point, we now introduce $\hat{n} = \tilde{n} - 1$, which gives
\[ \tilde{p} = \frac{8(\hat{n}+1)}{2-\hat{n}} \tilde{T} - 3(1+\hat{n})^2. \]
For the isotherm $\tilde{T} = 1$. Expanding around $\hat{n} = 0$, we find
\[ \tilde{p} = 1 + \frac32 \hat{n}^3 + \mathcal{O}\left(n^4\right), \]
so $\hat{n} \sim \hat{p}^{1/3}$, where $\hat{p} = \tilde{p} - 1$, and $\delta = 3$.
\item For the susceptibility $\chi$ we find
\begin{align*}
\chi^{-1} &= \left[ \frac{1}{\hat{n}+1} \left( \frac{\dd \hat{p}}{\dd \hat{n}} \right)_T \right]_{\hat{n} \to 0} \\
&= \left[ \frac{1}{1+\hat{n}} \left( \frac{8(2-\hat{n})+8(\hat{n}+1)}{(2-\hat{n})^2} (\hat{T}+1) - 6 (1+\hat{n}) \right) \right]_{\hat{n} \to 0} \\
&= \left[ \frac{24}{(1+\hat{n})(2-\hat{n})^2} (\hat{T}+1) - 6 \right]_{\hat{n} \to 0} = 6 \hat{T},
\end{align*}
so $\chi \propto \hat{T}^{-1}$, and $\gamma = 1$.

(To relate the susceptibility from the notes (inverse of the second derivative of $f(n)$), simply substitute $p(n) = -f(n) + n \dd f / \dd n$ in $\chi^{-1} = (1/n)(\dd p / \dd n)$.)
\item For constant temperature, $\dd T = 0$, so the Gibbs-Duhem relation gives
\[ \mu = \int \dd \mu = \int v(p) \dd p + C(T), \]
where $v = V/N$ and $C(T)$ an unknown function of the temperature. The difference in chemical potential between states 1 and 2 is thus given by
\[ \mu_2 - \mu_1 = \int_1^2 v(p) \dd p. \]
\item In the figure, we can identify coexisting phase 1 with point A, and phase 2 with point E. We can chop the integral from A to E in four pieces, from A to B, B to C, C to D, and D to E. If the phases coexist, the chemical potentials must be equal, so the sum of these four integrals must vanish:
\[ 0 = \int_A^B v(p) \dd p + \int_B^C v(p) \dd p + \int_C^D v(p) \dd p + \int_D^E v(p) \dd p, \]
or, rearranging
\[ \int_B^C v(p) \dd p - \int_B^A v(p) \dd p = \int_E^D v(p) \dd p - \int_C^D v(p) \dd p. \]
Note that all integrals now give us areas between the curve and the $p$-axis (not the $v$-axis!). The term on the left is exactly area I, and the term on the right area II, so the condition is that they must be equal.
\item If area I equals area II, the integral over $p(v)$ from phase 1 (point A) to phase 2 (point E) simply equals the pressure at coexistence times the difference in reduced volume, or
\[ \int_1^2 p(v) \dd v = p(v_1) (v_2-v_1). \]
(or, equivalently, $p(v_2)$, as $p(v_1) = p(v_2)$ at coexistence).
\item As $v = 1/n$, we can easily change variables in the integral:
\[ \int p(v) \dd v = \int p(n) \frac{\partial v}{\partial n} \dd n = - \int \frac{p(n)}{n^2} \dd n. \]
The condition from (h) then reads
\begin{align*}
- \int_{n_1}^{n_2} \frac{p(n)}{n^2} \dd{n} &= p(n_1) \left(\frac{1}{n_1} - \frac{1}{n_2}\right) = \frac{p(n_2)}{n_2} - \frac{p(n_1)}{n_1},
\end{align*}
where in the last step we again used that $p(n_1) = p(n_2)$. We can split the left integral in two parts, one from $n_1$ to some intermediary reference point $n_\mathrm{C}$, and the second from $n_\mathrm{C}$ to $n_2$:
\begin{align*}
- \int_{n_1}^{n_2} \frac{p(n)}{n^2} \dd{n} &= - \int_{n_1}^{n_\mathrm{C}} \frac{p(n)}{n^2} \dd{n} -  \int_{n_\mathrm{C}}^{n_2} \frac{p(n)}{n^2} \dd{n}.
\end{align*}
We can now bring all terms with $n_1$ to the left, and $n_2$ to the right to get
\begin{align*}
\int_{n_\mathrm{C}}^{n_1} \frac{p(n)}{n^2} \dd{n} + \frac{p_1}{n_1} &= \int_{n_\mathrm{C}}^{n_2} \frac{p(n)}{n^2} \dd{n} + \frac{p_2}{n_2}
\end{align*}
(note that we used the minus sign to swap the integration bounds on the left integral). Defining the new function
%In re-scaled coordinates, we have
%\[ - \int_1^2 \left( \frac{8 \tilde{T}}{(3-\tilde{n})\tilde{n}} - 3 \right) \dd \tilde{n} = \left[ 3 \tilde{n} + \frac83 \tilde{T} \log \left(\frac{3}{\tilde{n}} - 1 \right) \right]_1^2 = \tilde{p}(\tilde{n}_1) \left(\frac{1}{\tilde{n}_2} - \frac{1}{\tilde{n}_1}\right).\]
%Defining the new function
\[ \Theta(n) = - \int^n \frac{p(n')}{n'^2} \dd n' - \frac{p(n)}{n}, \]
the condition can then be written as $\Theta(n_1) = \Theta(n_2)$.
\item We expand both $\Theta(n)$ and $p(n)$ about $\hat{n} = \tilde{n} - 1$:
\begin{align*}
\Theta(\hat{n}) &= 2 - 4 \hat{T} + \frac83 (1+\hat{T}) \log(2) - 6 \hat{T} \hat{n} - \frac32 (1+\hat{T}) \hat{n}^3 + \frac38 (1+\hat{T}) \hat{n}^4 + \mathcal{O}\left(\hat{n}^4\right),\\
p(\hat{n}) &= 1 + 4 \tilde{T} + 6\hat{T} \hat{n} + 3 \hat{T} \hat{n}^2 + \frac32 (1+\hat{T}) \hat{n}^3 + \frac34 (1+\hat{T}) \hat{n}^4 + \mathcal{O}\left(\hat{n}^4\right).
\end{align*}
From the equilibrium conditions, we then find at coexistence (to third order, which turns out to be enough):
\begin{align*}
4 \hat{T} \hat{n}_1 + (1+\hat{T})\hat{n}_1^3 &= 4 \hat{T} \hat{n}_2 + (1+\hat{T})\hat{n}_2^3, \\
4\hat{T} \hat{n}_1 + 2 \hat{T} \hat{n}_1^2 + (1+\hat{T}) \hat{n}_1^3 &= 4\hat{T} \hat{n}_2 + 2 \hat{T} \hat{n}_2^2 + (1+\hat{T}) \hat{n}_2^3.
\end{align*}
By using the first equation to simplify the second, we get
\[ \hat{n}_1 = \pm \hat{n}_2.\]
Naturally, the plus sign means that we're in the same phase, so we need the minus sign (distances to the equilibrium point can be negative). Substituting $\hat{n}_1 = - \hat{n}_2$ back into either equation for equilibrium gives:
\[ 2 \left( 4 \hat{T} \hat{n} + (1+\hat{T})\hat{n}^3 \right) = 0 \]
The solution $\hat{n} = 0$ corresponds to the single-state ($\hat{T} > 0$) or the local maximum; what we want is the other two solutions (for which $\hat{T} < 0$), which have
\[ \hat{n}^2 = - \frac{4 \hat{T}}{1+\hat{T}} = - 4 \hat{T} + \mathcal{O}\left(\hat{T}^2\right), \]
and thus to lowest order, $\hat{n} \propto |\hat{T}|^{1/2}$, so the critical exponent $\beta$ is again $1/2$.

\end{enumerate}
\fi
