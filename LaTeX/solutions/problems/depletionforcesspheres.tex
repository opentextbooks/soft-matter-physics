% Problemtitle Depletion forces
% Source: Inspired by Phillips et al., section 14.2.3, pages 556-557; also cf. Phillips 14.4 and 14.5
\ifproblemset
In this problem, we'll derive the expression given in section~\bookref{sec:depletion} of the notes for the excluded volume of two spheres of radius~$R$:
\begin{equation}
\label{twosphereexcludedvolume}
V_\mathrm{ex} = \frac83 \pi (R+r)^3 - \frac23 \pi \left(R + r - \frac{D}{2}\right)^2 \left(2R + 2r + \frac12 D\right),
\end{equation}
where $2R < D < 2(R+r)$ is the center-to-center distance between the two large spheres, and the spheres are immersed in a solution containing a large number of small spheres of radius~$r$.
\else
In this problem, we'll derive the expression~(\bookref{twosphereexcludedvolume}) given in section~\bookref{sec:depletion} for the excluded volume of two spheres of radius~$R$. The spheres are a distance~$D$ apart and immersed in a solution containing a large number of small spheres of radius~$r$, see the sketch below.
\fi
\begin{figure}[!ht]
\centering
\includegraphics[scale=1]{solutions/problems/depletion.pdf}
\caption[Depletion]{Depletion. a) Solution of small green particles of radius~$r$ and large blue particles of radius~$R$. A large and a small particle can't get closer than a distance $R+r$, creating a region outside every large particle where no small particle center can be found. If two large particles get relatively close together ($D < 2(R+r)$), their excluded regions overlap, creating more space (and hence more possible configurations) for the small particles. b) Calculation of the size of the overlap region; it is the spherical cap that sits on top of a cone with opening angle~$\theta$, height~$D/2$ and side~$R+r$.}
\label{fig:depletioncalculation}
\end{figure}
\begin{enumerate}[(a)]
\item The overlap region consists of two spherical caps, that sit on top of a cone with opening angle~$\theta$. Express~$\theta$ in terms of $D$, $R$ and $r$. % \cos \theta = \frac12 D / (R+r).
\item Calculate the volume of the `spherical cone' (cone with spherical cap) with opening angle~$\theta$. % \frac23 \pi (R+r)^3 (1-\cos\theta).
\item Find the volume of the `regular cone' (with flat base) with opening angle~$\theta$. % \frac{\pi}{3} \frac{D}{2} \left[ (R+r)^2 - (D/2)^2 \right].
\item Combine parts (a)-(c) to find an expression for the overlap area and verify equation~(\ifproblemset\ref{twosphereexcludedvolume}\else\bookref{twosphereexcludedvolume}\fi).
%\ifproblemset
\item Estimate the magnitude of the depletion force between two colloids with radius $1\;\mu\mathrm{m}$ in a solution containing a collection of small particles of radius~$r=1\;\mathrm{nm}$ at a concentration of~$1\;\mathrm{mM}$. % For r=2nm, Phillips has F = 15pN (pag. 558).
\item What would the net effect of depletion forces be on two large rod-shaped objects?
%\fi
\end{enumerate}

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item \[ \cos \theta = \frac{\frac12 D}{R+r}. \]
\item \[ \frac23 \pi (R+r)^3 (1-\cos\theta). \]
\item \[ \frac{\pi}{3} \frac{D}{2} \left[ (R+r)^2 - (D/2)^2 \right]. \]
\item We get
\begin{equation}
\label{twosphereexcludedvolumesln}
V_\mathrm{ex} = \frac83 \pi (R+r)^3 - \frac23 \pi \left(R + r - \frac{D}{2}\right)^2 \left(2R + 2r + \frac12 D\right),
\end{equation}
\item The depletion force is given by equation~(\bookref{depletionforce}) in the notes:
\begin{equation}
\label{depletionforce}
F_\mathrm{depletion} = - \frac{\partial F_\mathrm{ex}}{\partial D} = - \frac{N \kB T}{V} \pi \left[ (R+r)^2 - \left(\frac{D}{2}\right)^2 \right].
\end{equation}
As this equation is only valid for $2R < D < 2(R+r)$, to estimate the magnitude of the force, we'll expand around $D=2R+r$, in which case we get (to lowest order) $Rr$ for the term in the parentheses. The numerical values we have here are $R = 1\;\mu\mathrm{m}$, $r = 1\;\mathrm{nm}$ and $N/V = 1\;\mathrm{mM} = 1 \cdot 10^{-3} \cdot 6 \cdot 10^{23} \cdot 10^3 = 6 \cdot 10^{23}\;\mathrm{m}^{-3}$ (where the last $10^3$ is because molar concentrations are per liter), so
\[ F_\mathrm{depletion} \approx 6 \cdot 10^{23} \cdot 1.38 \cdot 10^{-23} \cdot 300 \cdot \pi \cdot \left(10^{-6}\cdot 10^{-9}\right) \approx 8\;\mathrm{pN}. \]
\item Rod-shape objects would both attract and align to minimize the excluded volume.
\end{enumerate}
\fi