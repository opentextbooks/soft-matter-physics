The fluid flow through a cylindrical pipe due to a pressure gradient is known as \index{Poiseuille flow}\emph{Poiseuille flow}.
\begin{enumerate}[(a)]
\item Find and solve the equation of motion for the flow of a viscous fluid through a section of horizontal cylindrical pipe of radius $R$ and length $L$, due to a pressure gradient $\Delta p$ between the entrance and exit of the pipe. Take the long axis of the pipe to be in the $z$ direction.
\item Calculate the discharge~$Q$ through the pipe (defined as the mass of fluid passing per unit time through any cross section of the pipe). 
\end{enumerate}

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item By the setup of the problem, there can only be flow in the $z$ direction, and the flow velocity can only depend on the radial coordinate, therefore $\bvec{v}(\bvec{r}) = v_z(r) \unitvec{z}$. It is a incompressible fluid, so $\rho=$constant. Like for the plane Couette flow, the continuity equation is trivially satisfied:
\begin{equation}
\cancelto{0}{\frac{\partial \rho}{\partial t}} + \frac{1}{r}\cancelto{0}{\frac{\partial (\rho rv_r)}{\partial r}}+\frac{1}{r}\cancelto{0}{\frac{\partial (\rho v_{\theta})}{\partial \theta}} + \cancelto{0}{\frac{\partial (\rho v_z)}{\partial z}} = 0.
\end{equation}

From the Navier-Stokes equations:
\begin{equation}
\begin{split}
\rho\left[
\cancelto{0}{\frac{\partial v_r}{\partial t}} + \cancelto{0}{v_r\frac{\partial v_r}{\partial r}} + \frac{\cancelto{0}{v_{\theta}}}{r}\frac{\partial v_r}{\partial \theta} - \cancelto{0}{\frac{v_{\theta}^2}{r}} + v_z \cancelto{0}{\frac{\partial v_r}{\partial z}}
\right] \\
&\hspace{-4cm} = -\frac{\partial p}{\partial r} + \eta\left[ \frac{\partial}{\partial r}\left(\frac{1}{r}\cancelto{0}{\frac{\partial (rv_r)}{\partial r}}\right) + \frac{1}{r^2}\cancelto{0}{\frac{\partial^2 v_r}{\partial \theta^2}}-\frac{2}{r^2}\cancelto{0}{\frac{\partial v_{\theta}}{\partial \theta}}+\cancelto{0}{\frac{\partial^2v_r}{\partial z^2}} \right]\\
&\hspace{-4cm} \Rightarrow \frac{\partial p}{\partial r} = 0
\end{split}
\end{equation}


\begin{equation}
\begin{split}
\rho\left[
\cancelto{0}{\frac{\partial v_{\theta}}{\partial t}} + \cancelto{0}{v_r\frac{\partial v_{\theta}}{\partial r}}+\frac{\cancelto{0}{v_{\theta}}}{r}\frac{\partial v_{\theta}}{\partial \theta} -\cancelto{0}{\frac{v_rv_{\theta}}{r}}+v_z\cancelto{0}{\frac{\partial v_{\theta}}{\partial z}}
\right]\\
& \hspace{-4cm} = -\frac{1}{r}\frac{\partial p}{\partial \theta}+
\eta\left(
\frac{\partial}{\partial r}\left(\frac{1}{r}\cancelto{0}{\frac{\partial (rv_{\theta})}{\partial r}}\right) + \frac{1}{r^2}\cancelto{0}{\frac{\partial^2 v_{\theta}}{\partial \theta^2}}-\frac{2}{r^2}\cancelto{0}{\frac{\partial v_r}{\partial \theta}}+\cancelto{0}{\frac{\partial^2v_{\theta}}{\partial z^2}}
\right)\\
&\hspace{-4cm} \Rightarrow \frac{\partial p}{\partial \theta}=0
\end{split}
\end{equation}
\begin{equation}
\begin{split}
\rho\left[ \cancelto{0}{\frac{\partial v_z}{\partial t}} + \cancelto{0}{v_r}\hspace{0.3cm}\frac{\partial v_z}{\partial r} + \frac{\cancelto{0}{v_{\theta}}}{r}\frac{\partial v_z}{\partial \theta} + v_z\cancelto{0}{\frac{\partial v_z}{\partial z}} \right]  \\
&\hspace{-4cm} = -\frac{\partial p}{\partial z} + \eta\left(\frac{1}{r} \frac{\partial}{\partial r}\left(r\frac{\partial v_z}{\partial r}\right) + \frac{1}{r^2}\cancelto{0}{\frac{\partial^2 v_z}{\partial \theta^2}}+\cancelto{0}{\frac{\partial^2v_z}{\partial z^2}} \right)\\
\end{split}
\end{equation}

So 
\begin{alignat}{5}
\frac{\partial p}{\partial z} &= \eta\frac{1}{r}\frac{\partial}{\partial r}\left(r\frac{\partial v_z}{\partial r}\right) &\quad \Rightarrow &\quad r\frac{\partial v_z}{\partial r} = \frac{r^2}{2\eta}\frac{\partial p}{\partial z} + C,\\
\frac{\partial v_z}{\partial r} &= \frac{r}{2\eta}\frac{\partial p}{\partial z} + \frac{C}{r} &\quad \Rightarrow &\quad v_z = \frac{r^2}{4\eta}\frac{\partial p}{\partial z} + C\ln(r) + D.
\end{alignat}

The boundary conditions are $v_z(r=R) = 0$ (no slip at the outer surface) and $v_z(r=0)$ should be finite. The second condition gives $C=0$. From the second condition we get

%We know that at $v_z(r=0)$ should be finite\\
%$v_z(0)=C_1\ln(0)+C_2\rightarrow C_1=0$\\
%$v_z(R) = 0$ because there is  no slip
\begin{equation}
\frac{R^2}{4\eta}\frac{\partial p}{\partial z} + D = 0 \quad \Rightarrow \quad D = -\frac{R^2}{4\eta}\frac{\partial p}{\partial z}
\end{equation}
and for the velocity we thus find
\begin{equation}
v_z(r) = \frac{1}{4\eta}\frac{\partial p}{\partial z}\left(r^2-R^2\right)
\end{equation}
We know that the pressure difference between the entrance and the exit is $\Delta p$ so that $-\frac{\partial p}{\partial z} = \frac{\Delta p}{L}$, which gives us the full solution:
\begin{equation}
\label{Poiseuilleflowsolution}
v_z(r) = \frac{1}{4\eta}\frac{\Delta p}{L}\left(R^2-r^2\right)
\end{equation}
\item The area cross section for the pipe is $2\pi r \dd{r}$ and the fluid with density $\rho$ passes through the pipe with speed $v_z$. The discharge is therefore given by:
\begin{align}
Q &= \rho\int_{0}^{R}2\pi r v_z \dd{r} \nonumber\\
&= 2\pi\rho\int_{0}^{R}\frac{1}{4\eta}\frac{\Delta p}{L}(R^2-r^2) r \dd{r} \nonumber\\
&= \frac{\pi\rho\Delta P}{8\eta L}R^4
\end{align}
\end{enumerate}

\fi