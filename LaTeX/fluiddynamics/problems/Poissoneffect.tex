In this problem, we'll analyze the Poisson effect (see figure~\bookref{fig:Poissonratio}) for the extension of a homogeneous and isotropic elastic body which originally has the shape of a cube with sides of length~$L$. We choose coordinates such that the origin is at the center of the cube, and the axes are aligned with its sides. We apply a tensile stress along the $x$ axis, causing the cube to deform into a rectangular block, with the dimension along the $x$ axis now $L + 2 \Delta L$, and along the $y$ and $z$ axes now $L - 2\Delta L'$. 
\begin{enumerate}[(a)]
\item Consider a point inside the cube at position $\bvec{r} = (x, y, z)$. After the deformation, the point has moved to $\bvec{r}' = \bvec{r} + \dd{\bvec{r}}$. As the origin stays fixed and the sides of the cube move, the deformation is not constant throughout the block. Argue why the local strain is given by
\begin{equation}
\label{cubestretchingstrain}
\dd{\gamma_x} = \frac{\dd{x}}{x}, \qquad \dd{\gamma_y} = -\frac{\dd{y}}{y}, \qquad \dd{\gamma_z} = -\frac{\dd{z}}{z}.
\end{equation}
NB the strains $\dd{\gamma_x}$, $\dd{\gamma_y}$ and $\dd{\gamma_z}$ are local strains, not the components of the strain tensor. Also, we've taken the deformations $\dd{x}$, $\dd{y}$ and $\dd{z}$ to be all positive.
\item By definition of the Poisson ratio~$\nu$ (and symmetry in $y$ and $z$), we have $-\nu \dd{\gamma_x} = \dd{\gamma_y} = \dd{\gamma_z}$. As our material is homogeneous, we can integrate the resulting differential equation, to get
\begin{equation}
-\nu \int_L^{L + \Delta L} \frac{\dd{x}}{x} = - \int_{L - \Delta L'}^L \frac{\dd{y}}{y} = \int_L^{L - \Delta L'} \frac{\dd{y}}{y}.
\end{equation}
(We're leaving out the integral over $z$ as it is identical to the one over $y$). Carry out the integration, to find an equation containing $\nu$ and the ratios $\Delta L / L$ and $\Delta L'/L$.
\item Using the expansion $(1 + x)^n = 1 + nx + \order{x^2}$ (valid for small $x$, and even for $n$ not an integer and/or negative), simplify the relation you found in (b) to get the approximate expression for the Poisson ratio we use mostly in practice.
\item To find the maximal value $\nu$ can take, we calculate the change in the volume of the cube due to the deformation. Find $\Delta V / V$, i.e., the ratio of the volume change to the original volume, and express it in terms of $\Delta L / L$ and $\Delta L'/L$.
\item Use your result from (b) to eliminate $\Delta L'/L$ from your answer in (d).
\item Using the same first-order approximation as in (c), simplify your expression in (e) for the case that $\Delta L / L$ and $\Delta L'/L$ are small.
\item Finally, find the minimum and maximum value of $\nu$ for our three dimensional cube (you can use your answer in either (e) or (f), the result will be the same).
\end{enumerate}



\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item The deformation is zero at the origin, and respectively $\Delta L$ and $-\Delta L'$ at the extending and contracting surfaces. As the body is homogeneous and isotropic, the deformation will be linear in the distance to the origin, and therefore the strain in each of the three cardinal directions is the ratio of the deformation to the distance to the origin (with appropriate signs), as given by equation~(\ref{cubestretchingstrain}).
\item The integrals are easy, giving us logarithms. Leaving out the $z$ part (identical to the $y$ one), we have
\begin{align*}
-\nu \log \left(\frac{L + \Delta L}{L} \right) &= \log\left( \frac{L-\Delta L'}{L} \right) \\
\log \left[ \left(1 + \frac{\Delta L}{L} \right)^{-\nu} \right] &= \log \left( 1 - \frac{\Delta L'}{L} \right) \\
\left(1 + \frac{\Delta L}{L} \right)^{-\nu} &= 1 - \frac{\Delta L'}{L}.
\end{align*}
\item We have
\begin{align*}
\left(1 - \frac{\Delta L}{L} \right)^{-\nu} \approx 1 - \nu \frac{\Delta L}{L} = 1 - \frac{\Delta L'}{L} \\
\nu \approx \frac{\Delta L'}{\Delta L}.
\end{align*}
\item The original volume $V$ is $L^3$, the new volume is given by $(L + \Delta L) (L-\Delta L')^2$. The ratio we are looking for is therefore:
\begin{align*}
\frac{\Delta V}{V} = \frac{(L + \Delta L) (L-\Delta L')^2 - L^3}{L^3} = \left(1 + \frac{\Delta L}{L} \right) \left(1 - \frac{\Delta L'}{L}\right)^2 - 1.
\end{align*}
\item Simply substitute the answer of (b) for $(1-\Delta L'/L)$ in (e) to get
\begin{align*}
\frac{\Delta V}{V} = \left(1 + \frac{\Delta L}{L} \right)^{1-2\nu} - 1.
\end{align*}
\item Using our expansion again, we have
\begin{align*}
\frac{\Delta V}{V} \approx (1 - 2\nu) \frac{\Delta L}{L}.
\end{align*}
\item The minimal value of $\nu$ in this setting is zero; in that case, $\Delta L' = 0$ and the cube only extends in the $x$ direction. The maximum value is $\frac12$; for $\nu = \frac12$ we find (with either (e) or (f)) that $\Delta V = 0$, i.e., the volume is conserved. A larger value of $\nu$ would mean that the volume decreases under extension.
\end{enumerate}
\fi