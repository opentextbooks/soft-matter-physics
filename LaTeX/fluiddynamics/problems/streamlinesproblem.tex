The two-dimensional steady flow of a fluid with density $\rho$ is given by
\[ \bvec{v}(x,y) = K \frac{-y \unitvec{x} + x \unitvec{y}}{x^2 + y^2}, \]
where $K$ is a constant.
\begin{enumerate}[(a)]
\item Can this flow correspond to the flow of an incompressible fluid?
\item \label{pb:streamlinessketch} \ifproblemset Determine and sketch the streamlines of this flow (a streamline is a line that is everywhere tangent to the local flow direction, see section~\bookref{sec:idealfluids} / eqs.~\bookref{streamlinesparametrized} and~\bookref{streamlines} of the notes).\else Determine and sketch the streamlines of this flow. \fi
\item Determine and sketch (in the same figure as \ref{pb:streamlinessketch})) the acceleration field of this flow.
\item Determine the pressure difference between two points, a distance $r_1$ and $r_2 > r_1$ away from the origin. (Hint: Start by taking two points on the positive $x$-axis and then generalize using a symmetry argument.) What happens at the origin?
\end{enumerate}

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item Yes, it can because, $\bvec{\nabla} \cdot \bvec{v}(x,y) = K \left( \pdv{x} \frac{-y}{x^2+y^2} + \pdv{y} \frac{x}{x^2+y^2} \right) = K \left( \frac{2 x y}{(x^2+y^2)^2} + \frac{-2 x y}{(x^2+y^2)^2} \right) = 0$.
\item The streamlines are given by
\begin{align*}
  \frac{\dd x}{v_x} &= \frac{\dd y}{v_y}\\
  \frac{\dd x}{\text{d} y} &= - \frac{x}{y} \\
  \int x \dd x + \int y \dd y &= 0 \\
  x^2 + y^2 &= R^2,
\end{align*}
where $R$ is an integration constant. The streamlines are circles. The sketch should show circular streamlines with the flow counterclockwise, see figure~\ref{fig:streamlinesandaccelerationfield}. {[\color{blue} 1 pt; 0.5 pt for sketch, 0.5 pt for final equation]}
\item The acceleration is
\begin{align*}
  \bvec{a} &= \frac{\text{D}}{\text{D}t} \bvec{v}(x,y) \\
  &= \frac{\dd}{\ddt} \bvec{v}(x,y) + (\bvec{v} \cdot \bvec{\nabla}) \bvec{v} \\
  &= \left( v_x \frac{\partial}{\partial x} + v_y \frac{\partial}{\partial y}\right) \bvec{v} \\
  &= \frac{K^2}{x^2+y^2} \left( -y \frac{\partial}{\partial x} \frac{-y \unitvec{x} + x \unitvec{y}}{x^2 + y^2} + x \frac{\partial}{\partial y} \frac{-y \unitvec{x} + x \unitvec{y}}{x^2 + y^2} \right) \\
  &= \frac{K^2}{(x^2+y^2)^2} \left( - x \unitvec{x} - y \unitvec{y} \right)
\end{align*}
So the acceleration field is radial, pointing towards the origin. The length of the arrow should decrease with the distance from the origin, see figure~\ref{fig:streamlinesandaccelerationfield}.
\item \textit{Method 1}: Using Bernoulli's law: 
\begin{align*}
  p_2 - p_1 &= \frac{1}{2} \rho \left( v_1^2 - v_2^2 \right) \\
  &= \frac{K^2 \rho}{2} \left( \frac{(r_1 \hat{k})^2}{r_1^4} - \frac{(r_2 \hat{k})^2}{r_2^4} \right) \\
  &= \frac{K^2 \rho}{2} \left( \frac{1}{r_1^2} - \frac{1}{r_2^2} \right)
\end{align*}
The pressure diverges at the origin.

\noindent \textit{Method 2}: Use $-\bvec{\nabla} p = \rho \frac{\dd}{\ddt} \bvec{v}(x,y)$. Let us first consider $y=0$, then we find
\begin{align*}
  - \frac{\partial p}{\partial x} &= - \rho \frac{K^2}{x^3} & - \frac{\partial p}{\partial y} &= 0 \\
  p(x,y) &= -\rho \frac{K^2}{2 x^2} + C_1(y) & p(x,y) &= C_2(x)
\end{align*}
The problem is rotationally symmetric, so without loss of generality we can replace $x$ with $r$ and find
$$p_2(r_2) - p_1(r_1) = \frac{K^2 \rho}{2} \left( \frac{1}{r_1^2} - \frac{1}{r_2^2}\right). $$ 
\end{enumerate}
\begin{figure}
\centering
\includegraphics[scale=1]{fluiddynamics/problems/streamlinesandaccelerationfield.pdf}
\caption{Streamlines and acceleration field for problem~\ref{pb:streamlines}.}
\label{fig:streamlinesandaccelerationfield}
\end{figure}


\fi
