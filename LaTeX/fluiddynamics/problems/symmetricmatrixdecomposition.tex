%Problemtitle: Symmetric 2-tensors
In this problem, we consider the most general symmetric 2-tensor~$\bvec{A}$. The tensor can be written as the sum of two components, $\bvec{A} = \alpha \bvec{I} + \beta \bvec{\Phi}$, with $\mathrm{Tr}(\bvec{\Phi}) = 0$ and (without loss of generality) $\det(\bvec{\Phi})= -1$. Writing the decomposition in components, we have:
\begin{equation*}
A_{ij} = \alpha \delta_{ij} +\beta \Phi_{ij}
\end{equation*}
In this problem, we'll work in two dimensions, in which case the $2$-tensor can be represented as a $2 \times 2$ matrix (note that it would be an $n \times n$ matrix in $n$ dimensions):
\begin{equation*}
\begin{pmatrix} a & b \\ b & c\end{pmatrix} = \alpha \begin{pmatrix} 1 & 0 \\ 0 & 1 \end{pmatrix} + \beta \Phi
\end{equation*}
\begin{enumerate}[(a)]
\item Find $\alpha$ and $\beta$ in terms of $a$, $b$ and $c$.
\item  What are the components of $\bvec{\Phi}$ in terms of $a$, $b$ and $c$? Hint: use that $\cos^2(\theta)+\sin^2(\theta) = 1$	\item Suppose now that $\bvec{A}$ is a strain tensor. We can distinguish a few specific cases:
\begin{itemize}
\item $a = c$, $b = 0$
\item $a = -c$, $b = 0$
\item $a = -c$, $b \neq 0$
\end{itemize} 
For each of these cases, discuss what kind(s) of deformations take place. Illustrate these deformations with a drawing. 
\end{enumerate}

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item We're given that 
\begin{align*}
\Tr(\Phi) &= \Phi_{11} + \Phi_{22} = 0 \quad \Rightarrow \quad  \Phi_{11} = -\Phi_{22}.
\end{align*}
And by the decomposition, we have (noting that $\bvec{\Phi}$ must be symmetric):
\begin{align*}
a &= \alpha + \beta\Phi_{11}\\
c &= \alpha + \beta\Phi_{22}\\
b &= \beta \Phi_{12} = \beta \Phi_{21}
\end{align*}
Therefore
\begin{align*}
\left.\begin{array}{r@{}l}
a &= \alpha + \beta\Phi_{11}\\
c &= \alpha - \beta\Phi_{11}\\
\end{array}	\right\rbrace \quad \Rightarrow \quad &\alpha =\frac{a+c}{2}, \\
\beta = \frac{b}{\Phi_{12}}.
\end{align*}
We can also read of that $\Phi_{11} = (a-c)/2\beta$ and $\Phi_{12} = b/\beta$. Now using that we're also given that
\begin{align*}
-1 &= \det(\Phi) = \Phi_{11}\Phi_{22} - \Phi_{12}\Phi_{21} = - \Phi_{11}^2 - \Phi_{12}^2 \\
&= -\frac{1}{4\beta^2} \left[(a-c)^2 + 4 b^2 \right]
\end{align*}
so
\begin{align*}
\beta &= \frac12 \sqrt{(a-c)^2 + 4 b^2}.
\end{align*}
\item We use our result from (a) that $\Phi_{11}^2+\Phi_{12}^2 = 1$ to write $\Phi_{11} = \cos(\theta)$ and $\Phi_{12} = \sin(\theta)$, so we get
\begin{align*}
\begin{pmatrix} a & b \\ b & c\end{pmatrix} &= \alpha \begin{pmatrix} 1 & 0 \\ 0 & 1 \end{pmatrix} + \beta \begin{pmatrix} \cos(\theta) & \sin(\theta) \\ \sin(\theta) & -\cos(\theta) \end{pmatrix} \\
&= \frac{a+c}{2} \begin{pmatrix} 1 & 0 \\ 0 & 1 \end{pmatrix} + \frac12 \sqrt{(a-c)^2 + 4 b^2} \begin{pmatrix} \cos(\theta) & \sin(\theta) \\ \sin(\theta) & -\cos(\theta) \end{pmatrix}.
\end{align*}
The square root may look daunting, but can be easily squared away if we look specifically at the $11$ and $12$ components (the others are no longer independent):
\begin{align*}
a - \frac{a+c}{2} &= \frac{a-c}{2} &= \frac12 \sqrt{(a-c)^2 + 4 b^2} \cos(\theta), \\
b &= \frac12 \sqrt{(a-c)^2 + 4 b^2} \sin(\theta),
\end{align*}
so squaring gives
\begin{align*}
(a-c)^2 &= \left[(a-c)^2 + 4 b^2\right] \cos^2(\theta), \\
4b^2 &= \left[(a-c)^2 + 4 b^2\right] \sin^2(\theta),
\end{align*}
and if we divide these two equations by each other, we get an expression for $\tan(\theta)$, from which we easily get both the sine and cosine:
\begin{align*}
\tan^2(\theta) &= \frac{4b^2}{(a-c)^2} \\
\frac{1}{\cos^2(\theta)} &= 1 + \tan^2(\theta) = \frac{(a-c)^2 + 4b^2}{(a-c)^2} \\
\cos(\theta) &= \frac{a-c}{\sqrt{(a-c)^2 + 4b^2}} \\
\sin(\theta) &= \tan(\theta)\cos(\theta) = \frac{2b}{\sqrt{(a-c)^2 + 4b^2}}
\end{align*}
\item Specific cases:
\begin{itemize}
\item $a = c$, $b = 0$:\\
$\alpha = a = c, \beta = 0 \;\Rightarrow\; A = \begin{pmatrix} a & 0 \\ 0 & a \end{pmatrix}$\\
This is an isotropic expansion. 
%		\includegraphics*[width = 0.1\textwidth]{isotropicexpansion}
\item $a = -c$, $b=0$: \\
$\alpha = 0, \beta = a, \Phi = \begin{pmatrix} 1 & 0 \\ 0 & -1 \end{pmatrix}$\\
This deformation is a pure shear along the horizontal:\\
%		\includegraphics*[width=0.1\textwidth]{shear}
\item $a=-c$, $b \neq 0$:\\
$\alpha = 0, \beta = \sqrt{a^2+b^2}, \tan(\theta) = \frac{b}{a}$
This deformation is a pure shear along a line with angle~$\theta$.
\end{itemize}
\end{enumerate}
\fi