In this problem, we'll derive the equation for the Stokes drag: the drag force on a sphere of radius~$R$ moving through a fluid with viscosity~$\eta$ at low Reynolds number. We denote the flow field in the fluid by~$\bvec{v}$ and the velocity of the sphere by $\bvec{u}$. By symmetry, this problem is exactly the same as that of finding the force on a fixed sphere by a fluid that moves at velocity $\bvec{u}$ at infinity, which is the easier route. The problem then is to find the solution of the combination of the Stokes equation,
\begin{equation}
\label{Stokeseq}
- \bvec{\nabla} p + \eta \Delta \bvec{v} = 0,
\end{equation}
with the continuity equation for an incompressible fluid
\begin{equation}
\label{incompcont}
\bvec{\nabla} \cdot \bvec{v} = 0,
\end{equation}
under the boundary condition that $\bvec{v} = \bvec{u}$ at infinity. We write $\bvec{v} = \bvec{v}' + \bvec{u}$, so that $\bvec{v}'$ is zero at infinity. Since $\bvec{\nabla} \cdot \bvec{v} = \bvec{\nabla} \cdot \bvec{v}' = 0$, $\bvec{v}'$ can be written as the curl of some vector~$\bvec{A}$, and we have $\bvec{v} = \bvec{\nabla} \times \bvec{A} + \bvec{u}$. Since the velocity is an ordinary vector, $\bvec{A}$ must be a pseudovector\footnote{A pseudovector (or sometimes `axial vector') in three dimensions is identical to a regular (or `ordinary', or `polar') vector in most aspects, except for its behavior under reflections. Where ordinary vectors behave as expected under reflections (no change parallel to the plane of reflection, sign inversion in the component perpendicular to that plane), the pseudovector does not. The best-known example is the cross product. If $\bvec{a}$ lies along the $\unitvec{x}$ axis and $\bvec{b}$ along the $\unitvec{y}$ axis, then $\bvec{a} \times \bvec{b}$ lies along the $\unitvec{z}$ axis. Reflection in the $yz$ plane leaves $\bvec{b}$ unaffected (parallel to reflection plane) and reflects $\bvec{a}$ (perpendicular to reflection plane). Therefore $\bvec{a} \to -\bvec{a}$, and we'd expect $\bvec{a} \times \bvec{b} \to -\bvec{a} \times \bvec{b}$, but as the cross product of $\bvec{a}$ and $\bvec{b}$ lies along the $\unitvec{z}$ axis, it actually remains unchanged under this reflection. A similar thing happens when reflecting in the $xy$ plane: now neither $\bvec{a}$ nor $\bvec{b}$ changes, but $\bvec{a} \times \bvec{b}$ gets reflected.}. Now $\bvec{v}$, and therefore $\bvec{A}$, depends only on the position vector $\bvec{r}$ (we take the origin at the center of the sphere) and on the value of $\bvec{u}$; both these vectors are polar. Furthermore, $\bvec{A}$ must evidently be a linear function of $\bvec{u}$, because both the equation of motion and its boundary conditions are linear. The only such axial vector which can be constructed for a completely symmetrical body (the sphere) from two polar vectors is the cross product $\bvec{r} \times \bvec{u}$. Hence $\bvec{A}$ must be of the form $f'(r)\unitvec{r} \times \bvec{u}$, where $f(r)$ is a scalar function of $r = |\bvec{r}|$, and $\unitvec{r}$ is the unit vector in the radial direction. The product $f'(r)\unitvec{r}$ can be written as the gradient, $\bvec{\nabla}f(r)$, of some function $f(r)$, so that the general form of $\bvec{A}$ is $(\bvec{\nabla} f) \times \bvec{u}$. Hence we can write the velocity $\bvec{v}'$ as
\[ \bvec{v} = \bvec{u} + \bvec{\nabla} \times \left[ (\bvec{\nabla} f) \times \bvec{u} \right].\]
Because $\bvec{u}$ is a constant, we can rewrite this expression to
\begin{equation}
\label{velocityAnsatz}
\bvec{v} = \bvec{u} + \bvec{\nabla} \times \left[ \bvec{\nabla} \times \left( f \bvec{u} \right) \right].
\end{equation}

\begin{enumerate}[(a)]
\item Take the curl of the Stokes equation to get an equation with $\bvec{v}$ alone.
\item Also take the curl of equation~(\ref{velocityAnsatz}) to relate the curl of $\bvec{v}$ to the combination $f\bvec{u}$ alone.
\item By combining your answers from (a) and (b), show that you get the following (fifth-order!) equation for $f$:
\begin{equation}
\label{fDE5}
\Delta^2 (\bvec{\nabla}f) = 0.
\end{equation}
You may use that $\bvec{\nabla} \times \bvec{\nabla} \times \bvec{w} = \bvec{\nabla} \left(\bvec{\nabla} \cdot \bvec{w}\right) - \Delta \bvec{w}$ for an arbitrary vector field~$\bvec{w}$.
\item A first integration of equation~(\ref{fDE5}) gives $\Delta^2 f = \mbox{constant}$. Argue from a not yet explicitly stated boundary condition at infinity why that constant must be zero.
\item Writing out the double-Laplacian in spherical coordinates (and using the fact that $f$ depends on $r$ alone), we get
\begin{equation}
\label{fDE4}
\Delta^2 f = \frac{1}{r^2} \frac{\dd}{\dd r} \left( r^2 \frac{\dd}{\dd r}\right) \Delta f = 0.
\end{equation}
Solve equation~(\ref{fDE4}) by first solving for $\Delta f$, arguing why the integration constant must be zero, and then solving for $f(r)$. Your solution should have two remaining integration constants; we'll call these $a$ and $b$. 
\item Substitute your solution for $f(r)$ in (\ref{velocityAnsatz}) to find $\bvec{v}$.
\item At the surface of the sphere, we have a no-slip boundary condition, i.e. $\bvec{v}(r = R) = 0$. Use this condition to find the constants $a$ and $b$. (In general, you'd have a third integration constant in $f(r)$, but a constant term in $f(r)$ is irrelevant, as $\bvec{v}$ only depends on derivatives of $f$, so we might as well set that one to zero straight away).
\item Show that in cylindrical coordinates, with the $z$-axis along the direction of $\bvec{u}$, you get
\begin{align}
v_r &= u \cos(\theta) \left[ 1 - \frac{3R}{2r} + \frac{R^3}{2r^3} \right], \\
v_\theta &= - u \sin(\theta) \left[ 1 - \frac{3R}{4r} - \frac{R^3}{4 r^3}\right].
\end{align}
\item Finally we need the pressure. To find it, substitute (\ref{velocityAnsatz}) into the Stokes equation~(\ref{Stokeseq}), and use what you know about $\Delta^2 f$.
\item In cylindrical coordinates, we have
\[ \sigma'_{rr} = 2 \eta \frac{\partial v_r}{\partial r}, \qquad \sigma'_{r\theta} = \eta \left( \frac{1}{r} \diff{v_r}{\theta} + \diff{v_\theta}{r} - \frac{v_\theta}{r} \right). \]
Find the values of $\sigma'_{rr}$ and $\sigma'_{r\theta}$ at the surface of the sphere.
\item For the force, we get in cylindrical coordinates
\[ F = \oint \left( - p \cos(\theta) + \sigma'_{rr} \cos(\theta) - \sigma'_{r\theta} \sin(\theta) \right) \dd A, \]
where the integral is over the whole surface of the sphere. Use this integral to calculate the force (the answer should of course be the Stokes drag, $F = 6 \pi \eta R u$).
\end{enumerate}

\ifincludesolutions
\begin{enumerate}[(a)]
\item As the curl of the gradient of any scalar function vanishes, we get
\begin{align*}
\Delta \left( \bvec{\nabla} \times \bvec{v} \right) &= 0.
\end{align*}
\item As the curl of $\bvec{u}$ vanishes, we have
\begin{align*}
\bvec{\nabla} \times \bvec{v} &= \bvec{\nabla} \times \bvec{\nabla} \times \left[ \bvec{\nabla} \times \left( f \bvec{u} \right) \right].
\end{align*}
\item Using the provided mathematical relation, we re-write the first two curls in (b). We then realize that the first term of the answer contains the divergence of yet another curl, which is always zero. Therefore, we get
\begin{align*}
\bvec{\nabla} \times \bvec{v} &=  - \Delta \left[ \bvec{\nabla} \times \left( f \bvec{u} \right) \right].
\end{align*}
Substituting this expression in the answer of (a) we find the given fifth order equation $\Delta^2 (\bvec{\nabla}f) = 0$.
\item The boundary condition is not just that $\bvec{v} = \bvec{u}$ at infinity, but also that all derivatives of $\bvec{v} - \bvec{u}$ vanish at infinity, which is why derivatives of $f$ cannot contain any constants.
\item Integrating, dividing by $r^2$, and integrating again, we get
\begin{align*}
\Delta f &= 2 \frac{a}{r} + c,
\end{align*}
where $a$ and $c$ are integration constants. We already argued that derivatives of $f$ cannot contain constants, so $c$ must be zero. We now multiply by $r^2$, integrate, divide by $r^2$, and integrate again, which gets us
\begin{align*}
f(r) &= a r + \frac{b}{r},
\end{align*}
where we left out another integration constant (this one doesn't matter, as we only get derivatives of $f(r)$ in the velocity anyway).
\item We 
\end{enumerate}
\fi