% Problemtitle Derivation of Stokes' law from the Oseen tensor
Deriving Stokes' law~(\bookref{FStokes}) for the drag force on a sphere at low Reynolds number is a nontrivial exercise if you try to solve the whole problem in one go. However, because the Stokes equation is linear, there is an alternative way, as we can use the superposition principle to write the drag force as a sum over point forces on the surface of the sphere. As we know the solution of Stokes' equation for a point force (given by the Oseen tensor~\bookref{Oseentensor}), we can then sum up all flows to get the total velocity for a given applied force, or inversely, the force needed to sustain a given velocity. Because our sphere is a continuous object, the point forces will be distributed continuously over the surface, turning our sum into an integral.

To set the stage, suppose we have a sphere of radius $R$ with its center at the origin, embedded in an incompressible fluid with viscosity~$\eta$. We exert a force $\bvec{F} = F \unitvec{z}$ on the sphere, causing it to move with (currently unknown) velocity $\bvec{v}$ (of course we'll find that $\bvec{v}$ will also be in the $z$ direction). We make sure that our force and resulting velocity are low enough that we are in the low Reynolds number regime, allowing us to use Stokes law.

When the sphere is moving at constant velocity, it exerts a net force $\bvec{F}$ on the surrounding fluid; by Newton's third law, the surrounding fluid exerts an equal but opposite net drag force on the sphere. These sphere-fluid forces act at the surface of the sphere, resulting in an effective force per unit area of the sphere of\footnote{To see that this is true, simply integrate $F \unitvec{z} / 4 \pi R^2$ over the area of the sphere, and we retrieve the net force $\bvec{F}$.} $F\unitvec{z}/4\pi R^2$. For the velocity we then find
\begin{equation}
\label{Stokesletintegrationsphere}
\bvec{v} = \oint_{\partial V} \dd{S} \bvec{J}(\bvec{r}) \cdot \frac{F \unitvec{z}}{4 \pi R^2}.
\end{equation}
The integral in~(\ref{Stokesletintegrationsphere}) is taken over the surface of the sphere.

\begin{enumerate}[(a)]
\item Substitute the expression for the Oseen tensor~(\bookref{Oseentensor2}) in~(\ref{Stokesletintegrationsphere}), simplifying as much as possible.
\item Write the metric $\dd{S}$ and the position vector $\bvec{r}$ in spherical coordinates.
\item Show that the integrals over the $x$ and $y$ components are zero (you can do this without actually calculating an integral, but evaluating the integral can also be done).
\item Evaluate the integral over the $z$ component to get
\begin{equation}
\label{Stokesspherevelocity}
\bvec{v} = \frac{F}{6 \pi \eta R} \unitvec{z}.
\end{equation}
\item Argue why Stokes law~(\bookref{FStokes}) follows from~(\ref{Stokesspherevelocity}).
\end{enumerate}

A slightly more general form of~(\ref{Stokesletintegrationsphere}) allows for arbitrary surface shape and center position:
\begin{equation}
\label{Stokesletintegration}
\bvec{v} = \oint_{\partial V} \dd{S} \bvec{J}(\bvec{r} - \bvec{r}') \cdot \bvec{f}(\bvec{r}),
\end{equation}
where $\bvec{f}$ is still a force per unit (surface) area, and the object is centered at $\bvec{r}'$. Equation~(\ref{Stokesletintegration}) can be used to calculate the drag force on an ellipsoid or on a cylinder (as a limit of a long sequence of spheres)~\cite[Section 3.4]{Dhont2006}.

\ifincludesolutions
\ifproblemset\newpage\fi
\Solution
\begin{enumerate}[(a)]
\item We have (noting that we're at the surface of the sphere, so $|\bvec{r}| = R$)
\begin{align*}
\bvec{v} &= \oint_{\partial V} \dd{S} \bvec{J}(\bvec{r}) \cdot \frac{F \unitvec{z}}{4 \pi R^2} = \oint_{\partial V} \dd{S} \frac{1}{8 \pi \eta R} \left[\bvec{I} + \frac{\bvec{r} \bvec{r}}{R^2} \right] \cdot \frac{F \unitvec{z}}{4 \pi R^2} \\
&= \frac{1}{8 \pi \eta R}  \frac{F}{4 \pi R^2} \oint_{\partial V} \dd{S} \left[ \unitvec{z} + \frac{\bvec{r} \cdot \bvec{z}}{R^2} \bvec{r} \right].
\end{align*}
\item Using
\begin{align*}
\dd{S} &= R^2 \dd{\Omega} = R^2 \sin(\theta) \dd{\theta} \dd{\phi}, \\
\bvec{r} &= R \left[\cos(\phi)\sin(\theta) \unitvec{x} + \sin(\phi)\sin(\theta)\unitvec{y} + \cos(\theta) \unitvec{z}\right]
\end{align*}
we get
\begin{align*}
\bvec{v} &= \frac{1}{8 \pi \eta R}  \frac{F}{4 \pi} \int_0^\pi \sin(\theta) \dd{\theta} \int_0^{2\pi} \dd{\phi} \left[\cos(\phi)\sin(\theta) \unitvec{x} + \sin(\phi)\sin(\theta)\unitvec{y} + \left(1 + \cos^2(\theta) \right) \unitvec{z}\right].
\end{align*}
\item The $\phi$ integrals over the $\unitvec{x}$ and $\unitvec{y}$ components are both a full-period integral over a sine/cosine, and thus evaluate to zero.
\item As the $z$ component is independent of $\phi$, the $\phi$ integral evaluates to $2\pi$. For the remaining $\theta$ integral we get
\begin{align*}
\bvec{v} &= \frac{1}{8 \pi \eta R}  \frac{F \unitvec{z}}{2} \int_0^\pi \sin(\theta) \dd{\theta} \left[1 + \cos^2(\theta) \right] = \frac{1}{8 \pi \eta R}  \frac{F \unitvec{z}}{2} \left[ 2 + \frac23 \right] = \frac{F}{6 \pi \eta R} \unitvec{z}.
\end{align*}
\item By Newton's third law, the drag force on the sphere is minus the force of the sphere on the fluid, so Stokes law follows:
\begin{equation}
\bvec{F}^\mathrm{drag} = 6 \pi \eta R \bvec{v}.
\end{equation}
\end{enumerate}
\fi
