A layer of incompressible liquid of thickness~$h$ is flowing down a mountain slope. For simplicity, we approximate the side of the mountain as an inclined plane at slope~$\theta$, where we assume that the plane is large enough that we can treat it as infinite. We have a no-slip boundary condition where the liquid touches the solid surface of the slope, and a free surface where the liquid is in contact with the ambient air at pressure $p_0$. The flow is driven only by gravity.

\begin{center}
\includegraphics[scale=0.75]{fluiddynamics/problems/flowdownaslope.pdf}
\end{center}

\begin{enumerate}[(a)]
\item Choosing coordinates such that $x$ points down along the slope, and $z$ up perpendicular to the slope, write down the Navier-Stokes equations in the $x$ and $z$ directions.
\item Translate the boundary conditions to conditions on the pressure and components of the velocity field. There should be three: one from the no-slip condition, one for the pressure at the free surface (or the $zz$ component of the stress tensor), and one for the shear stress $\sigma_{xz}$.
\item Solve for the velocity and pressure fields in the liquid layer.
\item Find the discharge per unit length in the direction perpendicular to the flow (the $y$ direction), i.e. the amount of mass per unit length that flows down the surface per unit time.
\end{enumerate}

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item By symmetry, we have $\bvec{v} = v_x(z) \unitvec{x}$ and $p = p(z)$. In particular, both $\bvec{v}$ and $p$ are time-independent. The external force (per unit volume) due to gravity is $\rho \bvec{g}$; in our coordinate system, we have $\rho g \sin(\theta)$ in the $x$ direction and $- \rho g \cos(\theta)$ in the $z$ direction. Substituting these expressions into the Navier-Stokes equations, we get
\begin{align*}
\rho \left[ \pdv{v_x}{t} + \bvec{v} \cdot \bvec{\nabla} v_x \right] = 0 &= \eta \pdv[2]{v_x}{z} + \rho g \sin(\theta), \\
\rho \left[ \pdv{v_z}{t} + \bvec{v} \cdot \bvec{\nabla} v_z \right] = 0 &= - \pdv{p}{z} - \rho g \cos(\theta).
\end{align*}
\item At the solid-liquid interface, we have a no-slip boundary condition, i.e. $v_x(z=0) = 0$. At the free surface, the pressures must balance, so $p(z=h) = p_0$ (we get the same from balancing $\sigma_{zz}$ components). A free surface between two liquids also means no shear, so $\sigma_{xz}(z=h) = 0$.
\item From the Navier-Stokes equation in the $z$ direction, combined with the second boundary condition, we can solve for the pressure:
\begin{align*}
p(z) &= \int \rho g \cos(\theta) \dd{z} = \rho g \cos(\theta) z + A,
\end{align*}
where $A$ is an integration constant, set by the condition that $p(z=h) = p_0$, which gives
\begin{align*}
p(z) &= p_0 + \rho g \cos(\theta) (z-h).
\end{align*}
From the Navier-Stokes equation in the $x$ direction, we can integrate twice to find the velocity field:
\begin{align*}
v_x(z) &= -\frac12 \frac{\rho g}{\eta} \sin(\theta) z^2 + B z + C,
\end{align*}
where $B$ and $C$ are integration constants. The condition that $v_z(0) = 0$ gives $C=0$. The condition that $\sigma_{xz}(z=h) = 0$ gives
\begin{align*}
0 &= \sigma_{xz}(z=h) = \eta \left.\pdv{v_x}{z} \right|_{z=h} = - \rho g \sin(\theta) h + \frac{B}{\eta},
\end{align*}
so $B = h + (\rho g /\eta) \sin(\theta)$, and the velocity field is given by
\begin{align*}
v_x &= \frac{\rho g}{\eta} \sin(\theta) \left[hz - \frac12 z^2\right].
\end{align*}
\item For the discharge per unit length we have
\begin{align*}
Q &= \rho \int_0^h v_x \dd{z} = \frac{\rho^2 g}{3 \eta} h^3 \sin(\theta).
\end{align*}
\end{enumerate}
\fi