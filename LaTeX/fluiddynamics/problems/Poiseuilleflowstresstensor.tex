% Problemtitle Viscous stress tensor in Poisseuille flow
% Source https://www.syvum.com/cgi/online/serve.cgi/eng/fluid/fluid_2b.html
In problem~\bookref{ch:fluiddynamics}.\bookref{pb:Poiseuilleflow}, we solved the Navier-Stokes equations for fluid in a cylindrical pipe. To do so, we used the version of the equations in which we already knew what the viscous stress tensor is. We can also find an equation for the stress tensor itself, which will be valid also for non-Newtonian fluids (as we'll discuss in chapter~\bookref{ch:complexfluids}). This derivation will also illustrate how the Navier-Stokes equations are an expression of conservation of momentum.

We again consider the flow through a cylindrical pipe of radius~$R$ and length~$L$, ignoring any effects at the end of the pipe. We will genearize the problem to also allow for gravitationally driven flow, by tilting the cylinder such that its axis makes an angle~$\theta$ with the horizontal.
\begin{enumerate}[(a)]
\item Identify the nonzero components of the velocity, pressure, and stress tensor, and on which of the coordinates they can depend.
\item Verify that there can be no convective flow in the cylinder.
\ifproblemset\setcounter{problemcounter}{\value{enumi}}\else\setcounter{problemcounter}{\value{enumii}}\fi
\end{enumerate}
\begin{figure}[ht]
\centering
\includegraphics[scale=1]{fluiddynamics/problems/cylinderwithinnertube.pdf}
\caption[Fluid layers in a cylindrical pipe.]{Cylindrical pipe of radius~$R$ with an inner cylindrical tube of inner radius~$r$ and thickness~$\Delta r$.}
\label{fig:cylinderwithinnertube}
\end{figure}
We now consider a thin ring-shaped cylinder inside the pipe (see figure~\ref{fig:cylinderwithinnertube}). The ring has inner radius~$r$, thickness~$\Delta r$, and is as long as the pipe itself. Because the velocity can depend on the radial coordinate, there is a difference in the momentum of the fluid at the inner and outer surface of our thin cylinder. Because there is no convective term, and the fluid is moving faster inside than outside (which we know because the fluid is stationary at $r=R$ due to the no-slip boundary condition), momentum must effectively be generated inside our thin cylinder. The `generation' of momentum is due to the net force acting on the fluid (due to the pressure difference and gravitational force). By Newton's second law, the force equals the rate of momentum generation.
\begin{enumerate}[(a)]
\ifproblemset\setcounter{enumi}{\value{problemcounter}}\else\setcounter{enumii}{\value{problemcounter}}\fi
\item Express the rate of momentum flow across the inner surface of the cylinder in terms of the cylinder's properties and the viscous stress tensor of the fluid (remember that the stress tensor represents a force per unit area; what we need is the effective force on the total inner surface, as that equals the momentum rate across that surface).
\item Also write down the rate of momentum flow across the outer surface of the cylinder (doing so should be a trivial extension of your answer at (c)).
\item Write down the total amount of momentum generated in our thin cylinder by the external forces (pressure difference and gravity).
\item In steady state, there should be no net momentum change in the cylinder; we therefore get (momentum flow out) - (momentum flow in) = (momentum generated). Substitute your expressions from (c-e) to get an equation for the stress tensor.
\item Now take the limit that $\Delta r \to 0$, and show that we get
\begin{equation}
\label{Poiseuilleflowviscousstress}
\frac{1}{r} \frac{\dd}{\dd r}\left( r \sigma_{rz} \right) = \frac{\Delta p}{L} + \rho g \sin(\theta).
\end{equation}
\item Show that if we substitute the viscous stress tensor~(\bookref{viscousstresstensor}) for an incompressible flow in cylindrical coordinates, equation~(\ref{Poiseuilleflowviscousstress}) predicts the same flow as you found in problem~\bookref{ch:fluiddynamics}.\bookref{pb:Poiseuilleflow}.
\end{enumerate}

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item By the setup of the problem, there can only be flow in the $z$ direction, and the flow velocity can only depend on the radial coordinate, therefore $\bvec{v}(\bvec{r}) = v_z(r) \unitvec{z}$. Likewise, the viscous components of the stress tensor can only depend on the radial coordinate, while the pressure is a function of the $z$ coordinate alone. There are two contributions: a diagonal term which is essentially the pressure, and a viscous non-diagonal term $\sigma_{rz}(r)$; by symmetry $\sigma_{r\theta} = \sigma_{\theta z} =0$, as there can be no (shear) forces in the angular direction.
\item There can be no convective term, because there is only a $z$ component of the velocity, which only depends on the radial coordinate, and so $(\bvec{v} \cdot \bvec{\nabla}) \bvec{v} = 0$.
\item The force on the inner surface due to the fluid flow is given by the viscous stress tensor $\sigma_{rz}$. The stress is the force per unit area; the total force on the inner surface is thus $2 \pi r L \sigma_{rz}$, evaluated at $r=r$.
\item The force on the outer surface is the same expression as in (c), evaluated at $r = r + \Delta r$.
\item The pressure difference generates a total force $\Delta p \cdot 2\pi r \cdot \Delta r$ on our thin cylinder. For the gravitational force we get $\rho g \sin(\theta) \cdot 2 \pi r L \Delta r$.
\item Putting everything together we have
\begin{align*}
\left. 2 \pi r L \sigma_{rz}\right|_{r+\Delta r} - \left. 2 \pi r L \sigma_{rz}\right|_r &= \Delta p \, 2 \pi r \, \Delta r + \rho g \sin(\theta) 2 \pi r L \, \Delta r \\
\frac{\left. r \sigma_{rz}\right|_{r+\Delta r} - \left. r \sigma_{rz}\right|_r}{\Delta r} &= \frac{\Delta p}{L} r + \rho g \sin(\theta) r.
\end{align*}
\item Taking the limit $\Delta r \to 0$, the term on the left in (f) becomes the $r$ derivative of $r \sigma_{rz}$; dividing through by~$r$ then gives equation~(\ref{Poiseuilleflowviscousstress}).
\item If the viscous stress tensor is simply the viscosity times the strain rate tensor, then, by equation~(\bookref{straincylindricalcoords}), $\sigma_{rz} = \eta \partial_r v_z$, and therefore equation~(\ref{Poiseuilleflowviscousstress}) becomes
\begin{align*}
\frac{1}{r} \frac{\dd}{\dd r}\left( r \sigma_{rz} \right) &= \frac{\eta}{r} \frac{\dd}{\dd r} \left( r \pdv{v_z}{r} \right) = \eta \left( \pdv[2]{v_z}{r} + \frac{1}{r} \pdv{v_z}{r} \right) = \frac{\Delta p}{L} \\
\end{align*}
Solving the homogeneous equation gives $v_\mathrm{h}(r) = \frac12 A r^2 + B$, and a particular solution is easily spotted for $v_\mathrm{p} = \frac12 C r^2$, which gives $C = \Delta p / 2 \eta L$. The condition that $v(r=R) = 0$ then sets $A = - C R^2$ and $B = 0$, so the solution indeed becomes equation~(\ref{Poiseuilleflowsolution}).
\end{enumerate}
\fi