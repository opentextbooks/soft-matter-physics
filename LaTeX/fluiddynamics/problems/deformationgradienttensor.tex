% Problemtitle: Deformation gradient and strain tensors
% Source: based in part on Doi section 3.1 and problem 3.3
The \emph{deformation gradient tensor}~$\bvec{E}$ maps points~$\bvec{x}$ in an undeformed material to their positions $\bvec{x}'$ after the deformation, i.e., $x'_i = E_{ij} x_j$. The components of the tensor are given by (equation~\bookref{deformationgradienttensor}) $E_{ij} = \diff{x'_i}{x_j}$. The deformation gradient tensor is related to the strain tensor through $\bvec{E}^\mathrm{T}\cdot\bvec{E} = \bvec{I} + \bvec{\gamma}$, or $E_{ji} E_{jk} = \delta_{ik} + \gamma_{ik}$.
\begin{enumerate}[(a)]
\item Find the deformation gradient tensor for a simple shear deformation of magnitude $\gamma = \Delta x / h$.
\item Find the deformation gradient tensor for a uniform incompressible material that is stretched by a factor~$\lambda$ in the $z$-direction (and thus shrinks by a factor $1/\sqrt{\lambda}$ in both the $x$ and $y$ directions).
\item Also find the strain tensor for both deformations, and verify that the relation between the deformation gradient tensor and the strain tensor is satisfied. Note that for the stretching deformation in (b), the strain is given by $\gamma = \lambda - 1$; express the strain tensor in this strain, not in $\lambda$. \textit{NB}: you should get the full strain tensor (equation~\bookref{straintensorgeneral}), not the linearized one (equation~\bookref{straintensor}).
\item Calculate the Poisson ratio from the linearized version of the strain tensor of the stretching deformation (because we assumed that the material is incompressible and thus that the volume is conserved, you know that your answer should be $\frac12$).
\ifproblemset \setcounter{problemcounter}{\value{enumi}} \else \setcounter{problemcounter}{\value{enumii}} \fi
\end{enumerate}
With the strain tensor, we showed that every deformation can be written as the sum of a pure compression and a pure shear. With the deformation gradient tensor, we can show that we can also write every deformation as the product of an orthogonal stretching and a rotation. To that end, we first define the (right) Cauchi-Green tensor $\bvec{C} = \bvec{E}^\mathrm{T}\cdot\bvec{E}$. Because $\bvec{C}$ is symmetric, it has three real eigenvalues, and can be diagonalized as $\bvec{C} = \bvec{Q}^\mathrm{T} \cdot \bvec{L}^2 \cdot \bvec{Q}$, where $\bvec{L}$ is a diagonal tensor and $\bvec{Q}$ is an orthogonal one (meaning that its transpose equals its inverse, or $\bvec{Q} \cdot \bvec{Q}^\mathrm{T} = \bvec{I}$). We thus find that we can write $\bvec{E} = \bvec{L} \cdot \bvec{Q}$. Let $\lambda_1$, $\lambda_2$ and $\lambda_3$ be the three values on the diagonal of $\bvec{L}$.
\begin{enumerate}[(a)]
\ifproblemset \setcounter{enumi}{\value{problemcounter}} \else \setcounter{enumii}{\value{problemcounter}} \fi
\item Show that $\Tr(\bvec{C}) = \lambda_1^2 + \lambda_2^2 + \lambda_3^2$ and $\det(\bvec{C}) = \lambda_1^2 \lambda_2^2 \lambda_3^2$.
\item Verify for a two-dimensional deformation that $\bvec{Q}$ represents a rotation.
\end{enumerate}

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item If the shear is in the positive $x$ direction, and the separation between the plates is in the $y$ direction (with the bottom plate at $y=0$ stationary), then for each point $\bvec{r} = (x,y)$ in the material, we have 
\[ \bvec{r}' = \bvec{r} + (\Delta x) \frac{y}{h} \unitvec{x} = \bvec{r} + \gamma y \unitvec{x}, \]
where the unprimed variables are in the undeformed situation. Therefore:
\begin{alignat*}{3}
E_{11} &= \diff{x'}{x} = \diff{x + \gamma y}{x} = 1, &\qquad & E_{12} = \diff{x'}{y} = \diff{x + \gamma y}{y} = \gamma,\\
E_{21} &= \diff{y'}{x} = \diff{y}{x} = 0, &\qquad & E_{22} = \diff{y'}{y} = \diff{y}{y} = \gamma,
\end{alignat*}
and thus
\begin{align*}
\bvec{E} = \begin{pmatrix} 1 & \gamma \\ 0 & 1 \end{pmatrix}.
\end{align*}
\item First off, to see that if the material is incompressible, an extension by a factor~$\lambda$ in the $z$ direction gives a compression by a factor $1/\sqrt{\lambda}$ in the other two, we write $L_z' = \lambda L_z$, and from $V = L_x L_y L_z = L_x' L_y' L_z' = L_x' L_y' \lambda L_z$ we get $L_x' = L_x / \sqrt{\lambda}$ and similar for $L_y$ (where the behavior for $L_x$ and $L_y$ must be identical because the material is uniform). Fixing the origin at the center of the material, for a point $\bvec{r} = (x, y, z)$ inside the material, we then get a displaced position after stretching $\bvec{r}' = (x', y', z') = (x/\sqrt{\lambda}, y/\sqrt{\lambda}, \lambda z)$, and the deformation gradient tensor readily follows as
\begin{align*}
\bvec{E} &= \begin{pmatrix} \frac{1}{\sqrt{\lambda}} & 0 & 0 \\ 0 & \frac{1}{\sqrt{\lambda}} & 0 \\ 0 & 0 & \lambda \end{pmatrix}.
\end{align*}
\item For the simple shear deformation of part (a), we get
\begin{align*}
\bvec{\gamma} = \bvec{E}^\mathrm{T} \cdot \bvec{E} - \bvec{I} = \begin{pmatrix} 0 & \gamma \\ \gamma & \gamma^2 \end{pmatrix} \approx \begin{pmatrix} 0 & \gamma \\ \gamma & 0 \end{pmatrix}
\end{align*}
for the full strain tensor; the linearized version just has $\gamma$ on the off-diagonal elements and zero trace, as it should be.\\
For the stretch deformation of part (b), we have (using $\gamma = \lambda-1$):
\begin{align*}
\bvec{\gamma} = \bvec{E}^\mathrm{T} \cdot \bvec{E} - \bvec{I} = \begin{pmatrix} \frac{1}{\lambda} - 1 & 0 & 0 \\ 0 & \frac{1}{\lambda} - 1 & 0 \\ 0 & 0 & \lambda^2 - 1 \end{pmatrix} = \begin{pmatrix} - \frac{\gamma}{\gamma + 1} & 0 & 0 \\ 0 & -\frac{\gamma}{\gamma + 1} & 0 \\ 0 & 0 & \gamma^2 + 2 \gamma \end{pmatrix} \approx \begin{pmatrix} -\gamma & 0 & 0 \\ 0 & -\gamma & 0 \\ 0 & 0 & 2 \gamma \end{pmatrix}
\end{align*}
for the full and linearized strain tensors.
\item We have $\nu = -\gamma_{xx}/\gamma_{zz} = - (-\gamma) / 2 \gamma = \frac12$.
\item \textit{Method 1}: Using linear algebra identities:\\
As all matrices involved are square, for the trace we can simply invoke the rule that $\Tr(\bvec{A} \bvec{B}) = \Tr(\bvec{B} \bvec{A})$ to get 
\begin{align*}
\Tr(\bvec{C}) = \Tr(\bvec{Q} \cdot \bvec{L}^2 \cdot \bvec{Q}^\mathrm{T}) = \Tr(\bvec{L}^2 \cdot \bvec{Q} \cdot \bvec{Q}^\mathrm{T}) = \Tr(\bvec{L}^2 \cdot \bvec{I}) = \Tr(\bvec{L}^2) = \lambda_1^2 + \lambda_2^2 + \lambda_3^2.
\end{align*}
For the determinant, we use the multiplicative property:
\begin{align*}
\det(\bvec{C}) = \det(\bvec{Q} \cdot \bvec{L}^2 \cdot \bvec{Q}^\mathrm{T}) = \det(\bvec{Q}) \det(\bvec{L}^2) \det(\bvec{Q}^\mathrm{T}) = \det(\bvec{L}^2) \det(\bvec{I}) = \lambda_1^2 \lambda_2^2 \lambda_3^2.
\end{align*}
\textit{Method 2}: Using the characteristic polynomial:\\
We're given that $\lambda_1$, $\lambda_2$ and $\lambda_3$ are the three values on the diagonal of $\bvec{L}$. By $\bvec{C} = \bvec{Q}^\mathrm{T} \cdot \bvec{L}^2 \cdot \bvec{Q}$, with $Q$ an orthogonal tensor, we have that the eigenvalues of $\bvec{C}$ are $\lambda_1^2$, $\lambda_2^2$, and $\lambda_3^2$. These eigenvalues are also given by the characteristic polynomial of $\bvec{C}$, which reads (using that $\bvec{C}$ is symmetric):
\begin{align*}
0 &= \det(\bvec{C} - \lambda \bvec{I}) = \begin{vmatrix} C_{11} - \lambda & C_{12} & C_{13} \\ C_{12} & C_{22}-\lambda & C_{23} \\ C_{13} & C_{23} & C_{33}-\lambda \end{vmatrix} \\
&=\lambda^3 - (C_{11} + C_{22} + C_{33}) \lambda^2 + (C_{11}C_{22} + C_{22}C_{33} + C_{33}C_{11} - C_{12}^2 - C_{13}^2 - C_{23}^2 ) \lambda + \det(\bvec{C}) \\
&= (\lambda - \lambda_1^2)(\lambda - \lambda_2^2)(\lambda - \lambda_3^2),
\end{align*}
from which we can read off that $\Tr(\bvec{C}) = \lambda_1^2 + \lambda_2^2 + \lambda_3^2$ and $\det(\bvec{C}) = \lambda_1^2 \lambda_2^2 \lambda_3^2$.
\item For an arbitrary two-dimensional orthogonal matrix
\[ \bvec{Q} = \begin{pmatrix}
a & b \\ c & d
\end{pmatrix}
\]
we have
\begin{align*}
\bvec{I} &= \bvec{Q} \cdot \bvec{Q}^\mathrm{T} = \begin{pmatrix}
a & b \\ c & d
\end{pmatrix} \begin{pmatrix}
a & c \\ b & d
\end{pmatrix} = \begin{pmatrix}
a^2 + b^2  & ac + bd \\ ac + bd & c^2 + d^2
\end{pmatrix}
\end{align*}
so
\begin{alignat*}{3}
1 &= a^2 + b^2 & \Rightarrow & a = \cos(\phi), b = \sin(\phi), \\
1 &= c^2 + d^2 & \Rightarrow & d = \cos(\psi), c = \sin(\psi),  \\
0 &= ac + bd & \Rightarrow & \cos(\phi)\sin(\psi) + \sin(\phi)\cos(\psi) = \cos(\phi - \psi),
\end{alignat*}
where we chose without loss of generality to write $a$ as $\cos(\phi)$ for an arbitrary angle $\phi$, and similarly $d = \cos(\psi)$ for an arbitrary angle $\psi$; the third line gives that $\psi = -\phi$, and thus our matrix $\bvec{Q}$ is given by
\begin{align*}
\bvec{Q} = \begin{pmatrix}
\cos(\phi) & \sin(\phi) \\ -\sin(\phi) & \cos(\phi)
\end{pmatrix},
\end{align*}
representing a rotation over $\phi$.
\end{enumerate}
\fi