In this problem (and all that follow), we will use the summation convention (repeated indices are presumed to be summed over). Here $\bvec{A}$ and $\bvec{B}$ are two-tensors (which can be represented by matrices), and $\bvec{x}$, $\bvec{v}$ and $\bvec{w}$ are vectors. The Levi-Civita symbol (not a tensor) is defined as:
\begin{equation}
\label{LeviCivita2}
\varepsilon_{ijk} = \left\{ \begin{array}{cl}
+1 & \mbox{if } ijk \mbox{ is an even permutation of 123,} \\
-1 & \mbox{if } ijk \mbox{ is an odd permutation of 123,} \\
0 & \mbox{otherwise.}
\end{array}
\right.
\end{equation}
Finally, the Pauli spin matrices are given by:
\begin{equation}
\sigma_x = \begin{pmatrix} 0 & 1 \\ 1 & 0 \end{pmatrix}, \quad \sigma_y = \begin{pmatrix} 0 & -i \\ i & 0 \end{pmatrix}, \quad \sigma_z = \begin{pmatrix} 1 & 0 \\ 0 & -1 \end{pmatrix}.
\end{equation}
\begin{enumerate}[(a)]
	\item Write $\bvec{A}^\mathrm{T} \bvec{B}$ in index notation.
	\item Write the eigenvector equation $\bvec{A}\bvec{v} = \lambda \bvec{v}$ in index notation.
	\item Show that the Pauli spin matrices satisfy the product rule
	\begin{equation}
	\label{spinmatricesproductrule}
	\sigma_j \sigma_k = \delta_{jk} + i \varepsilon_{jkl} \sigma_l.
	\end{equation}
	Note that the indices here can take values $1=x$, $2=y$, and $3=z$, whereas the $i$ in equation~(\ref{spinmatricesproductrule}) is the complex number $\sqrt{-1}$.
	\item Calculate
	\[ \frac{\partial x_i}{\partial x_j} \qquad \mbox{ and } \qquad \left( \delta_{ij} - \frac{x_i x_j}{x^2} \right) \delta_{ij}, \]
	where $x$ is the length of the vector $\bvec{x}$ with $n$ independent components $x_i$.
	\item Express $\bvec{v} \times \bvec{w}$ and $\det(\bvec{A})$ in index notation using the Levi-Civita symbol; for the determinant you only need to do the two-dimensional case (so $\bvec{A}$ can be represented by a $2 \times 2$ matrix).
	\item Write $\partial_k A_{ik}$ and $v_k \partial_k v_i$ in index-free notation.
\end{enumerate}

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
	\item \begin{align*}
\left.
\begin{array}{r@{}l}
\textbf{AB} & {}: \rightarrow A_{ik}B_{kj} \\
\textbf{A}^{T} & {}: \rightarrow A_{ij}^{T} = A_{ji}
\end{array}
\right\rbrace \textbf{A}^{T}\textbf{B} \rightarrow A_{ki}B_{kj}
\end{align*}
\item  \textbf{A}$\vec{\mathrm{v}} = \lambda \vec{\mathrm{v}} \rightarrow A_{ij}\mathrm{v}_j = \lambda\mathrm{v}_i$

\item Pauli spin matrices
\begin{align*}
	\sigma_i\sigma_i &= \sigma_x\sigma_x = \begin{pmatrix} 0 & 1 \\ 1 & 0 \end{pmatrix}\begin{pmatrix} 0 & 1 \\ 1 & 0 \end{pmatrix}  \\
	& = \sigma_y\sigma_y = \begin{pmatrix} 0 & -i \\ i & 0 \end{pmatrix}\begin{pmatrix} 0 & -i \\ i & 0 \end{pmatrix}  \\
	& = \sigma_z\sigma_z = \begin{pmatrix} 1 & 0 \\ 0 & -1 \end{pmatrix}\begin{pmatrix} 1 & 0 \\ 0 & -1 \end{pmatrix} = \begin{pmatrix} 1 & 0 \\ 0 & 1 \end{pmatrix}
	 = \delta_{ii} + i\cancelto{0}{\epsilon_{iil}} \sigma_l
\end{align*}

\begin{align*}
\sigma_x\sigma_y &= \begin{pmatrix} 0 & 1 \\ 1 & 0 \end{pmatrix}\begin{pmatrix} 0 & -i \\ i & 0 \end{pmatrix} = \begin{pmatrix}i & 0\\ 0 & -i\end{pmatrix} = \cancelto{0}{\delta_{xy}} + i\epsilon_{xyz}\sigma_{z} \\
\sigma_y\sigma_x &= \begin{pmatrix} 0 & -i \\ i & 0 \end{pmatrix}\begin{pmatrix} 0 & 1 \\ 1 & 0 \end{pmatrix} = \begin{pmatrix} -i & 0\\ 0 & i\end{pmatrix} = \cancelto{0}{\delta_{yx}} + i\epsilon_{yxz}\sigma_{z} \\
\sigma_x\sigma_z &= \begin{pmatrix} 0 & 1 \\ 1 & 0 \end{pmatrix}\begin{pmatrix} 1 & 0 \\ 0 & -1 \end{pmatrix} = \begin{pmatrix}0 & -1\\ 1 & 0\end{pmatrix} = \cancelto{0}{\delta_{xz}} + i\epsilon_{xzy}\sigma_{y} \\
\sigma_z\sigma_x &= \begin{pmatrix} 1 & 0 \\ 0 & -1 \end{pmatrix}\begin{pmatrix} 0 & 1 \\ 1 & 0 \end{pmatrix} = \begin{pmatrix} 0 & 1\\ -1 & 0\end{pmatrix} = \cancelto{0}{\delta_{zx}} + i\epsilon_{zxy}\sigma_{z} \\
\sigma_y\sigma_z &= \begin{pmatrix} 0 & -i \\ i & 0 \end{pmatrix}\begin{pmatrix} 1 & 0 \\ 0 & -1 \end{pmatrix} = \begin{pmatrix}0 & i\\ i & 0\end{pmatrix} = \cancelto{0}{\delta_{yz}} + i\epsilon_{yzx}\sigma_{x} \\
\sigma_z\sigma_y &= \begin{pmatrix} 1 & 0 \\ 0 & -1 \end{pmatrix}\begin{pmatrix} 0 & -i \\ i & 0 \end{pmatrix} = \begin{pmatrix} 0 & -i\\ -i & 0\end{pmatrix} = \cancelto{0}{\delta_{zy}} + i\epsilon_{zyx}\sigma_{x}
\end{align*}


\item \[ \frac{\partial x_i}{\partial x_j} = \delta_{ij} \qquad \mbox{ and } \qquad \left( \delta_{ij} - \frac{x_i x_j}{x^2} \right) \delta_{ij} = n-1. \]

\item
\begin{align*}
(\bvec{v} \times \bvec{w})_i &= v_j w_k\epsilon_{ijk}\\
\mathrm{det}(\textbf{A}) & =\frac12 A_{ij}A_{kl}\epsilon_{ik3}\epsilon_{jl3} \qquad \mbox{or} \qquad \mathrm{det}(\textbf{A}) = \varepsilon_{ij3} A_{1i} A_{2j}.
\end{align*}

\item
\begin{align*}
\left.
\begin{array}{r@{}l}
\frac{\partial A_{xx}}{\partial x} & {} + \frac{\partial A_{xy}}{\partial y} +\frac{\partial A_{xz}}{\partial z}\\
\frac{\partial A_{yx}}{\partial x} & {} + \frac{\partial A_{yy}}{\partial y} +\frac{\partial A_{yz}}{\partial z}\\
\frac{\partial A_{zx}}{\partial x} & {} + \frac{\partial A_{zy}}{\partial y} +\frac{\partial A_{zz}}{\partial z}
\end{array}
\right\rbrace \rightarrow \; \bvec{\nabla} \cdot \bvec{A}
\end{align*}

\[ v_k\partial_k v_i = (v_x\partial_x + v_y\partial_y+v_z\partial_z)v_i \; \rightarrow \; (\bvec{v}\cdot\bvec{\nabla})\bvec{v}. \]
\end{enumerate}
\fi