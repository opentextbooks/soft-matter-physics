In this problem, we'll derive the equation for the Stokes drag: the drag force on a sphere of radius~$R$ moving through a fluid with viscosity~$\eta$ at low Reynolds number. We denote the flow field in the fluid by~$\bvec{v}$ and the velocity of the sphere by $\bvec{u}$. By symmetry, this problem is exactly the same as that of finding the force on a fixed sphere by a fluid that moves at velocity $\bvec{u}$ at infinity, which is the easier route. The problem then is to find the solution of the combination of the Stokes equation,
\begin{equation}
\label{Stokeseq}
- \bvec{\nabla} p + \eta \Delta \bvec{v} = 0,
\end{equation}
with the continuity equation for an incompressible fluid
\begin{equation}
\label{incompcont}
\bvec{\nabla} \cdot \bvec{v} = 0,
\end{equation}
under the boundary condition that $\bvec{v} = \bvec{u}$ at infinity. Fortunately, as we'll see, we can exploit the symmetries of the system to reduce the problem from two coupled partial differential equations to two uncoupled equations, one for the pressure, and one for a scalar function known as the \index{Stokes stream function}\emph{Stokes stream function}.

\begin{enumerate}[(a)]
\item As the first step, take the divergence of the Stokes equation, combining your result with the continuity equation to get the Laplace equation ($\Delta p = 0$) for the pressure.
\item Second, take the curl of the Stokes equation, and, introducing the \emph{vorticity vector} (see eq.~(\bookref{defvorticitytensor}), the components of the vorticity vector are the three nonzero components of the vorticity tensor, just like the three components of the rotation vector are the three nonzero components of the rotation tensor) $\bvec{\omega} = \bvec{\nabla} \times \bvec{v}$, show that we also get the Laplace equation for the vorticity vector, i.e., $\Delta \bvec{\omega} = 0$. NB: you'll need some of the rules for second order vector derivatives.
\ifproblemset\setcounter{problemcounter}{\value{enumi}}\else\setcounter{problemcounter}{\value{enumii}}\fi
\end{enumerate}
Given the setup of the problem, it makes sense to work in cylindrical coordinates, with the direction of motion of the sphere (or the ambient fluid in far-field) along the $z$ axis. The problem is then rotationally symmetric around the $z$-axis, and the pressure and velocity field thus cannot depend on the angular coordinate~$\theta$. For this case, we can write the components of the velocity field as derivatives of the scalar Stokes stream function~$\psi$, which is a function of $r$ and $z$:
\begin{equation}
\label{Stokesstreamfunction}
v_r = -\frac{1}{r} \pdv{\psi}{z}, \qquad v_z = \frac{1}{r} \pdv{\psi}{r}.
\end{equation}
\begin{enumerate}[(a)]
\ifproblemset\setcounter{enumi}{\value{problemcounter}}\else\setcounter{enumii}{\value{problemcounter}}\fi
\item Show that for a velocity field given by equation~(\ref{Stokesstreamfunction}), the only nonzero component of the vorticity vector~$\bvec{\omega}$ is the $\theta$ component, and find an expression for that component. You may use that the curl of a vector field~$\bvec{w}$ in cylindrical coordinates is given by
\begin{equation}
\label{Curlcylindrical}
\bvec{\nabla} \times \bvec{w} = \left( \frac{1}{r} \pdv{w_z}{\theta} - \pdv{w_\theta}{z} \right] \unitvec{r} + \left( \pdv{w_r}{z} - \pdv{w_z}{r} \right) \unitvec{\theta} + \frac{1}{s} \left( \pdv{r w_\theta}{r} - \pdv{w_r}{\theta} \right) \unitvec{z}.
\end{equation}
\item The Laplace equation in cylindrical coordinates, applied to the $\theta$ component of the vorticity vector, reads
\begin{equation}
\label{Laplacecylindrical}
\Delta \omega_\theta = \frac{1}{r} \pdv{r} \left( r \pdv{\omega_\theta}{r} \right) + \pdv[2]{\omega_\theta}{z} - \frac{\omega_\theta}{r^2} = 0.
\end{equation}
Substitute your expression from~(c) to find a differential equation for the Stokes stream function~$\psi$.
\end{enumerate}

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item Taking the divergence of equation~(\ref{Stokeseq}), we have
\begin{align*}
0 &= \bvec{\nabla} \cdot \left(- \bvec{\nabla} p \right) + \eta \bvec{\nabla} \cdot \left( \Delta \bvec{v} \right) = - \Delta p + \eta \Delta \left( \bvec{\nabla} \cdot \bvec{v} \right) = - \Delta p,
\end{align*}
where the last equality follows from the continuity equation~(\ref{incompcont}).
\item Taking the curl of equation~(\ref{Stokeseq}), and using that the curl of the gradient of any scalar function vanishes, we have
\begin{align*}
0 &= - \bvec{\nabla} \times \bvec{\nabla} p + \eta \bvec{\nabla} \times \left(\Delta \bvec{v}\right) = \eta \bvec{\nabla} \times \left(\Delta \bvec{v}\right).
\end{align*}
Now it'd be nice if we could swap the curl and Laplacian, but in general, we can't. Fortunately however, there is a relation between them:
\begin{align*}
\Delta \bvec{v} &= \bvec{\nabla} \left( \bvec{\nabla} \cdot \bvec{v} \right) - \bvec{\nabla} \times \left(\bvec{\nabla} \times \bvec{v}\right)
\end{align*}
for any vector field~$\bvec{v}$. By the continuity equation, $\bvec{\nabla} \cdot \bvec{v} = 0$ in our problem; we thus have
\begin{align*}
0 &= \bvec{\nabla} \times \left(\Delta \bvec{v}\right) = - \bvec{\nabla} \times \left(\bvec{\nabla} \times \left(\bvec{\nabla} \times \bvec{v}\right)\right) = \bvec{\nabla} \times \left(\bvec{\nabla} \times \bvec{\omega} \right) = \Delta \bvec{\omega},
\end{align*}
where we used that the divergence of the curl of any vector is always zero.
\item The $r$ and $z$ components of $\bvec{\omega}$ vanishes trivially, as $\bvec{u}$ is independent of $\theta$ and has no $\theta$ component. For the $\theta$ component, we substitute (\ref{Stokesstreamfunction}) in the $\theta$ component of~(\ref{Curlcylindrical}) to get
\begin{align*}
\omega_\theta &= -\frac{1}{r}\pdv[2]{\psi}{z} - \pdv{r} \left( \frac{1}{r} \pdv{\psi}{r} \right) = -\frac{1}{r}\pdv[2]{\psi}{z} -\frac{1}{r}\pdv[2]{\psi}{r} + \frac{1}{r^2} \pdv{\psi}{r}.
\end{align*}
\item Substituting the answer of~(c) in~(\ref{Laplacecylindrical}), we get
\begin{align*}
0 &= \frac{1}{r} \left( \pdv[4]{\psi}{r} + \frac{\partial^4\psi}{\partial r^2 \partial z^2} \right) - \frac{1}{r} \pdv{r} \left( r \pdv{r} \left( \frac{1}{r^2} \pdv{\psi}{r} \right)\right) + \frac{1}{r} \left(\frac{\partial^4\psi}{\partial r^2 \partial z^2} + \pdv[4]{\psi}{z} \right) - \frac{1}{r^2} \frac{\partial^3 \psi}{\partial r \partial z^2} \\ \hbox{5cm} - \frac{1}{r} \left( \pdv[2]{\psi}{z} + \pdv[2]{\psi}{r} \right) + \frac{1}{r^4} \pdv{\psi}{r}, \\
0 &= \pdv[4]{\psi}{r} + 2 \frac{\partial^4\psi}{\partial r^2 \partial z^2} + \pdv[4]{\psi}{z} ... 
.
\end{align*}
\end{enumerate}
\fi