


\begin{subappendices}
\section{Derivation of the WLC autocorrelation function}
\label{app:WLCautocorrelation}
\begin{figure}[ht]
\begin{center}
\includegraphics[scale=1]{polymers/linewithtangents.pdf}
\end{center}
\caption{A curve with tangents at three points for calculating the tangent-tangent correlation function. Also shown is the plane perpendicular to the tangent (spanned by the normal and binormal) at the central point.}
%\label{fig:linewithtangents}
\end{figure}
In the Worm-Like Chain (WLC) model, the energy of a polymer is given by its elastic bending term only (equation~\ref{WLCbendingenergy}):
\begin{equation}
\EWLC = \frac12 K_\mathrm{eff} \int_0^L \left(\frac{\dd\unitvec{t}}{\dd s}\right)^2 \dd s = \frac12 \xi_\mathrm{p} \kB T \int_0^L \left(\frac{\dd\unitvec{t}}{\dd s}\right)^2 \dd s,
\end{equation}
where $\xi_\mathrm{p} = K_\mathrm{eff}/\kB T$ is the persistence length of our polymer. We claimed in section~\ref{sec:WLC} that the correlation of tangent vectors along such a polymer is given by an exponential decay, with a characteristic length scale equal to the persistence length. To prove that, let's first define the autocorrelation function $\mathcal{A}(x)$:
\begin{equation}
\label{defautocorrelationfunction}
\mathcal{A}(x) = \ev{\unitvec{t}(s) \cdot \unitvec{t}(s+x)}.
\end{equation}
In equation~(\ref{defautocorrelationfunction}), the average is taken over all polymer configurations, weighted by their Boltzmann factor (with the energy given by equation~(\ref{WLCbendingenergy})). For a long polymer, $\mathcal{A}(x)$ is independent of the starting point~$s$. Let us now consider three points along the polymer, located at $s$ (point A), $s+x$ (point B) and $s+x+y$ (point C), see figure~\ref{fig:linewithtangents}. To calculate the dot product between the tangent vectors in points A and C, we can decompose them in parts parallel to $\unitvec{t}_\mathrm{B}$ and parts lying in the plane perpendicular to $\unitvec{t}_\mathrm{B}$ (i.e. the plane spanned by the normal and binormal at B):
\begin{align}
\label{autocorrelationdecomposition}
\unitvec{t}_\mathrm{A} \cdot \unitvec{t}_\mathrm{C} &= \bvec{t}_{\mathrm{A}, \perp} \cdot \bvec{t}_{C, \perp} + (\unitvec{t}_\mathrm{A} \cdot \unitvec{t}_\mathrm{B}) (\unitvec{t}_\mathrm{B} \cdot \unitvec{t}_\mathrm{C}) \nonumber \\
&= \sin(\theta_\mathrm{AB}) \sin(\theta_\mathrm{BC}) \cos(\phi_{\mathrm{AC}}) + \cos(\theta_\mathrm{AB}) \cos(\theta_\mathrm{BC}).
\end{align}
Here $\theta_\mathrm{AB}$ is the angle between $\unitvec{t}_\mathrm{A}$ and $\unitvec{t}_\mathrm{B}$, $\theta_\mathrm{BC}$ that between $\unitvec{t}_\mathrm{B}$ and $\unitvec{t}_\mathrm{C}$, and $\phi_\mathrm{AC}$ that between $\bvec{t}_{\mathrm{A}, \perp}$ and $\bvec{t}_{\mathrm{C}, \perp}$. If we now take our ensemble average over all polymer configurations, the first term on the right hand side of equation~(\ref{autocorrelationdecomposition}) vanishes. To see why that happens, consider that the energy is independent of the direction of bending; hence all terms with the same value of $\phi_\mathrm{AC}$ are weighed equally. The ensemble contains all values of $\phi_\mathrm{AC}$, and since $\int_{0}^{2\pi} \cos\phi \, \dd \phi = 0$, the contributions average out. The second term is the product of two independent variables, and hence its ensemble average is the product of the averages of its two factors. We thus have:
\begin{equation}
\ev{\unitvec{t}_\mathrm{A} \cdot \unitvec{t}_\mathrm{C}} = \ev{\cos(\theta_\mathrm{AB})} \ev{\cos(\theta_\mathrm{BC})},
\end{equation}
or, returning to our autocorrelation function $\mathcal{A}(x)$:
\begin{equation}
\label{WLCautocorrelationrelation}
\mathcal{A}(x+y) = \mathcal{A}(x) \times \mathcal{A}(y).
\end{equation}
There is only one class of functions that satisfies equation~(\ref{WLCautocorrelationrelation}): exponentials. As usual, we take $e$ as the basis of our exponential, giving us $\mathcal{A}(x) = e^{qx}$ for some number $x$ as the solution of~(\ref{WLCautocorrelationrelation}). To determine the value of~$q$, consider a very short piece of polymer with length $\Delta s << \xi_\mathrm{p}$. Suppose over this length, the polymer is slightly bent. For short enough pieces, we can approximate this bent curve as a piece of arc with radius~$R$ and opening angle~$\psi$. We can then express $\unitvec{t}(s+\Delta s)$ in terms of the tangent and normal at $s$:
\begin{equation}
\label{tangentsplusdeltas}
\unitvec{t}(s+\Delta s) = \frac{1}{\sqrt{1+\psi^2}} \left( \unitvec{t}(s) + \psi \unitvec{n}(s)\right).
\end{equation}
Substituting~(\ref{tangentsplusdeltas}) into the energy (equation~\ref{WLCbendingenergy}), we find that this little piece of arc carries an energy given by:
\begin{equation}
\label{WLCarcenergy}
\mathcal{E}_{\mbox{\scriptsize WLC, arc}} = \frac12 \kB T \frac{\xi_\mathrm{p}}{\Delta s} \psi^2.
\end{equation}
Because equation~(\ref{WLCarcenergy}) is quadratic in the angle~$\psi$, we can invoke the equipartition theorem. That theorem tells us that per quadratic degree of freedom we get a contribution of $\frac12 \kB T$ to the thermal energy. We have two degrees of freedom here (the plane perpendicular to the rod is two-dimensional - we always bend in the direction of the normal, because that's how we define the normal, but there are still two directions in which we can bend), which gives us $\ev{\psi^2} = 2\Delta s / \xi_\mathrm{p}$. On the other hand, substituting~(\ref{tangentsplusdeltas}) into our expression for the autocorrelation function and expanding in the small angle~$\psi$, we get:
\begin{align}
\mathcal{A}(\Delta s) &= \frac{1}{\sqrt{1+\psi^2}} = 1 - \frac12 \ev{\psi^2} + \order(\psi^4) = 1 - \frac{\Delta s}{A} + \order{(\Delta s)^2} \\
&= e^{q \Delta s} = 1 + q \Delta s + \order{(\Delta s)^2}
\end{align}
from which we can read off that $q = -1/\xi_\mathrm{p}$.
\end{subappendices}