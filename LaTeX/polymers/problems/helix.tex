%Problemtitle: Helix
The contour of a helix is given by
\begin{equation}
\label{helix}
\bvec{r}(s) = \left[ r\cos\left(\frac{qs}{\sqrt{(qr)^2+1}}\right), \, r\sin\left(\frac{qs}{\sqrt{(qr)^2+1}}\right), \, \frac{s}{\sqrt{(qr)^2+1}} \right]
\end{equation}
as a function of the arclength~$s$.
\begin{enumerate}[(a)]
\item For a helix, find the tangent vector~$\unitvec{t}(s)$, curvature~$\kappa(s)$, the unit normal vector $\unitvec{n}(s)$, the binormal vector~$\unitvec{b}(s)$, and the torsion~$\tau(s)$. You may find it helpful to introduce the quantity $a = 1/\sqrt{1+(qr)^2}$.
\item Right- and left- handed helices can be distinguished from each other by the sign of~$q$. The direction of the helix therefore does not effect the curvature but only the torsion. Compute the bending, torsion, and total energy for the helix described by equation~(\ref{helix}).
%	\item Find the contour length~$s_1$ after which the helix has made one full turn, in terms of $q$ and $r$ (or $q$ and $a$).
\item The \emph{pitch}~$p$ of a helix is defined as the displacement along the $z$-axis after which the helix has made one full turn. Express $p$ in terms of $r$ and $q$. \textit{Hint}: first find the contour length $s_1$ after which the helix has made one full turn.
\item Express the curvature~$\kappa$ and torsion~$\tau$ in terms of $r$ and $p$ alone, and check that in the limits that $p \to 0$ (flat circle) you retrieve the curvature you'd expect.
\item Finally, invert your expressions at (d) to write $r$ and $p$ in terms of $\kappa$ and $\tau$, and check that in the limit that $\kappa \to 0$ (twisted straight line with no curvature) you retrieve the expressions you'd expect.
\end{enumerate}


\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item We first calculate the tangent vector $\unitvec{t}$. Define $a = \frac{1}{\sqrt{(qr)^2+1}}$, then
\begin{equation}
\unitvec{t}(s) = \frac{\dd \bvec{r}}{\dds} = \left( -qr a \sin(qs a), \, qra\cos(qsa), \, a \right).
\end{equation}

Now we can compute the curvature and the unit normal vector.
\begin{equation}
\frac{\dd\unitvec{t}}{\dds} = \kappa(s) \unitvec{n}(s) = \left( -q^2ra^2\cos(qsa), \, -q^2ra^2\sin(qsa), \, 0 \right)
\end{equation}
The unit normal vector has a length of $1$, so
\begin{equation}
\kappa(s) = \norm{\frac{\dd\unitvec{t}}{\dds}} = \left[ (rq^2a^2)^2\cos^2(qsa) + (rq^2a^2)^2 \sin^2(qsa) \right]^{1/2} = r q^2 a^2 
\end{equation}
and
\begin{equation}
\unitvec{n} = \left( -\cos(qsa), \, -\sin(qsa), \, 0 \right).
\end{equation}
Note that because $\kappa=rq^2a^2$, the curvature is a constant. 

The binormal vector~$\unitvec{b}$ is given by:
\begin{equation}
\unitvec{b} = \unitvec{t} \times \unitvec{n} = \left( a \sin(qsa), \, -a\cos(qsa), \, qra \right).
\end{equation}

Finally, we calculate the torsion $\tau$:
\begin{equation}
\tau = - \frac{\dd\unitvec{b}}{\dds} \cdot \unitvec{n}(s) = - \left( qa^2 \cos(qsa), \, qa^2\sin(qsa), \, 0 \right) \cdot \left( -\cos(qsa), \, -\sin(qsa), \, 0 \right) = q a^2.
\end{equation}
\item For the helix, the bending energy is given by
\begin{equation}
E_\mathrm{bend} = \frac{K_\mathrm{eff}}{2} \int_{0}^{L} \norm{\frac{\dd\unitvec{t}}{\dds}}^2 \dd{s} = \frac{K_\mathrm{eff}}{2} \int_{0}^{L}\kappa^2 \dd{s} = \frac{K_\mathrm{eff}}{2} \int_{0}^{L} q^4r^2 a^4 \dd{s} = \frac{K_\mathrm{eff}}{2} L (r q^2 a^2)^2.
\end{equation}
The torsional energy term is
\begin{equation}
E_\mathrm{torsion} = \frac{K_\mathrm{tor}}{2} \int_{0}^{L} \tau^2(s) \dd{s} = \frac{K_\mathrm{tor}}{2} L (qa^2)^2.
\end{equation}
The total energy thus is
\begin{equation}
E = E_\mathrm{bend} + E_\mathrm{torsion} = \frac{L q^2}{2(1 + (qr)^2)^2} \left( K_\mathrm{eff} r^2 q^2 + K_\mathrm{tor} \right).
\end{equation}
\item The helix has made a full turn if the $x$ coordinate is back at its starting point, i.e., when the argument of the cosine equals~$2\pi$, which is when $q s_1 a = 2\pi$, or $s_1 = 2\pi / (qa)$. While the $z$-coordinate is zero at $s=0$, at $s=s_1$ we get $s_1 a = 2 \pi / q$, so $p = 2\pi / q$.
\item We have
\begin{align*}
\kappa &= r q^2 a^2 = \frac{r q^2}{1+(rq)^2} = \frac{4\pi^2 r/p^2}{1+r^2 4\pi^2/p^2} = \frac{4 \pi^2 r}{p^2 + 4 \pi^2 r^2}, \\
\tau &= q a^2 = \frac{2\pi / p}{1 + 1+r^2 4\pi^2/p^2} = \frac{2 \pi p}{p^2 + 4 \pi^2 r^2}.
\end{align*}
In the limit that $p \to 0$, we get $\kappa = 1/r$ and $\tau = 0$, as we'd expect for a circle with radius~$r$.
\item We note that
\[ \frac{\kappa}{\tau} = \frac{2\pi r}{p}, \]
and
\[ r \kappa + \frac{p \tau}{2\pi} = \frac{4\pi^2 r^2}{p^2+r^2 4\pi^2} + \frac{p^2}{p^2+r^2 4\pi^2} = 1. \]
Rewriting the first expression as $p = 2 \pi r \tau / \kappa$, we can eliminate $p$ from the second to find $r$, then substitute $r$ back to get $p$:
\begin{align*}
r &= \frac{\kappa}{\kappa^2 + \tau^2}, \\
p &= \frac{2 \pi \tau}{\kappa^2 + \tau^2}.
\end{align*}
In the limit that $\kappa \to 0$, we get $r = 0$ and $p = 2\pi/\tau$, or $\tau = 2\pi / p$, as we had for twisting a straight rod.
\end{enumerate}
\fi