%Problemtitle: The linking number
%Source: Midterm 2023
Knots and links have inspired people for centuries, long before they became relevant in biology. For example, consider the archeological find of a Mjölnir (Thor's hammer, figure~\ref{fig:whiteheadlink}a) in Sweden, reported in a 1906 book, which features a knot of two rings now known as a Whitehead link after the British mathematician \bookhref{https://en.wikipedia.org/wiki/J._H._C._Whitehead}{Henry Whitehead}. A modern 3D representation of a Whitehead link is shown in figure~\ref{fig:whiteheadlink}b.

\begin{figure}
\centering
\includegraphics[scale=1]{polymers/problems/Whiteheadlink.pdf}
\caption[The Whitehead link]{The Whitehead link (a) on Thor's hammer [\ref{Thorshammerimage}] and (b) in a 3D representation [\ref{Whiteheadlink3Dimage}]. (c) The four types of crossings in a 2D projection of two linked curves in 3D.}
\label{fig:whiteheadlink}
\end{figure}
% Oscar Montelius, \textit{Kulturgeschichte Schwedens von den ältesten Zeiten bis zum elften Jahrhundert nach Christus} (1906) page 309

\begin{enumerate}[(a)]
\item Draw the 2D projection of the Whitehead link, with appropriate crossings. Choose (and indicate) the orientation such that  we loop along the blue O-shaped curve counter-clockwise, and we go down on the orange $\infty$-shaped curve on both the leftmost and rightmost points.
\item Determine the linking number of the Whitehead link.
\ifproblemset\setcounter{problemcounter}{\value{enumi}}\else\setcounter{problemcounter}{\value{enumii}}\fi
\end{enumerate}
In equation~(\bookref{linkingnumbercount}), we stated without proof that the calculation of the linking number can be simplified because we have $n_1 + n_2 = n_3 + n_4$, where $n_i$ is the number of crossings of type $i$ (in the order they appear in figure~\ref{fig:whiteheadlink}c). To prove this statement, we make use of the \bookhref{https://en.wikipedia.org/wiki/Jordan_curve_theorem}{\emph{Jordan curve theorem}}, which (in non-strict-math terms) states that any closed curve in the plane that does not intersect itself divides the plane in two regions, an `inner' and an `outer' one, and that if you draw any line between a point in the inner region to a point in the outer region, you have to cross the curve at least once.
\begin{enumerate}[(a)]
\ifproblemset\setcounter{enumi}{\value{problemcounter}}\else\setcounter{enumii}{\value{problemcounter}}\fi
\item Consider two linked closed curves (say a red and a blue one), drawn in their 2D projection, of which at least one (say the blue one) does not intersect itself. Relate the quantities $n_1 + n_2$ and $n_3 + n_4$ to the number of times the red curve enters or leaves the inner region of the blue curve. \textit{Hint: feel free to draw an example curve to see what's going on, but be sure to write your answer in general terms.}
\item Now use the Jordan curve theorem and your answer from (c) to show that $n_1 + n_2 = n_3 + n_4$.
\item Finally, express the linking number in two of the four types of crossing only (there are two possible correct answers, you need only one).
\end{enumerate}

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item See figure~\ref{fig:whiteheadlink2D}. Note that the + and - signs and type identifications are not needed in this part \score{2 pt}.
\item As labeled in figure~\ref{fig:whiteheadlink2D}, there is one crossing of each type, so there are two + and two - crossings, making the linking number zero \score{2 pt}.
\item As can be surmised from the example of the Whitehead link (cf. figure~\ref{fig:whiteheadlink2D}), the combination $n_1 + n_2$ is the number of times the red line crosses the blue line from outside to inside, and the combination $n_3 + n_4$ is the number of times the red line crosses the blue line from inside to outside. In general, if we start from inside the blue curve, which we assume to be oriented counterclockwise (`positive'), to go outside the red line would have to cross it either over (type 3) or under (type 4) the blue. Inversely, if we start outside, to go inside the red line would have to cross the blue line again either over (type 2) or under (type 1) the blue. Therefore, the observation on the Whitehead link generalizes to arbitrary links in which the blue curve is simple (i.e., non self-intersecting) \score{2 pt}.
\item By the Jordan curve theorem, if the red curve is closed, it needs to cross the blue curve from inside to outside exactly as many times as it crosses it from outside to inside, so we conclude that $n_1 + n_2 = n_3 + n_4$ \score{2 pt}.
\item We have
\begin{align*}
\mathrm{Lk} &= \frac{n_1 - n_2 + n_3 - n_4}{2} = \frac{n_1 + n_2 - 2n_2 + n_3 - n_4}{2} = \frac{n_3 + n_4 - 2n_2 + n_3 - n_4}{2} = n_3 - n_2 \\
&= n_1 - n_4
\end{align*}
\score{2 pt for either the answer in the first line or the equivalent one (with calculation) in the second line}.
\end{enumerate}

\begin{figure}
\centering
\includegraphics[scale=1]{polymers/problems/Whiteheadlink2D.pdf}
\caption{2D projection of the Whitehead link.}
\label{fig:whiteheadlink2D}
\end{figure}

\fi