% Problemtitle{Force-extension of the WLC in the high-force limit}.
% Source: Marantan & Mahadevan, AJP 2018.
In this problem we consider the force-extension curve of a Worm-Like Chain polymer in two dimensions. We parametrize the curve via the arc length $s$ and describe it's shape by the local angle~$\theta(s)$ it makes with the $x$-axis. We clamp the chain at both ends such that $\theta(0) = \theta(L) = 0$.

If the persistence length of a polymer is comparable to its chain length, the entropic contributions are irrelevant, and we retrieve the behavior of an elastic rod. We therefore consider the case that the total chain length is much larger than the persistence length (i.e., $L \gg \xi_\mathrm{p}$). In the limit of small forces, the elastic contribution to the energy does not matter, and we retrieve the force-extension curve of the freely-jointed chain model. In the limit of large forces (i.e., $F \xi_\mathrm{p} / \kB T \gg 1$), we know that the end-to-end distance $R$ must approach the contour length $L$. In that case, the tangent angle $\theta(s)$ becomes small everywhere, and we can approximate the cosine in equation~(\ref{WLCstretchenergy}) by the first two terms of its Taylor series, which gives us
\begin{equation}
\label{WLCstretchenergyapprox}
\frac{E\left[(\theta(s)\right]}{\kB T} = \frac12 \xi_\mathrm{p} \int_0^L \left( \dv{\theta}{s} \right)^2 \dd{s} + \frac{f}{2} \int_0^L \left(\theta(s)\right)^2 \dd{s} - fL,
\end{equation}
where $f = F/\kB T$. Note that the last term in~(\ref{WLCstretchenergyapprox}) is a constant and thus irrelevant. For the probability distribution of the configurations we find in this case
\begin{equation}
\label{WLCprobapprox}
P\left[\theta(s)\right] \propto \exp \left[ - \frac12 \xi_\mathrm{p} \kB T \int_0^L \left( \dv{\theta}{s} \right)^2 \dd{s} - \frac{f}{2} \int_0^L \left(\theta(s)\right)^2 \dd{s} \right],
\end{equation}
and the expectation value of the end-to-end distance becomes
\begin{equation}
\label{WLCendtoendapprox}
\ev{R} = L - \frac12 \int_0^L \ev{\left( \theta(s) \right)^2} \dd{s}.
\end{equation}
Unlike $\ev{\cos(\theta(s))}$, we have methods of evaluating $\ev{\left( \theta(s) \right)^2}$, which will allow us to find an expression for $\ev{R}$ in the high-force limit.

\begin{enumerate}[(a)]
\item Because we have clamped the angles at both ends of the chain, setting $\theta(0) = \theta(L) = 0$, we can expand the function $\theta(s)$ in a Fourier sine series
\begin{equation}
\label{tangentangleFouriersine}
\theta(s) = \sum_{n=1}^\infty a_n \sin(\frac{n\pi}{L} s).
\end{equation}
Use the expansion~(\ref{tangentangleFouriersine}) to re-write the integral in equation~(\ref{WLCendtoendapprox}) as a sum.
\item Likewise, re-write the integral over the square of the derivative of $\theta$ in equation~(\ref{WLCstretchenergyapprox}) as a sum.
\item Express the energy~(\ref{WLCstretchenergyapprox}) as a sum over the coefficients~$a_n$. You may leave out the constant term $-fL$.
\item Now take the ensemble average $\ev{\ldots}$ of the energy, and write it as a sum over Fourier modes, i.e. $\ev{E} = \sum_{n=1}^\infty \ev{E_n}$. Express $E_n$ in terms of the coefficients $a_n$.
\item In (d) you should have gotten a quadratic dependence of $E_n$ on $a_n$. By the equipartition theorem, each quadratic mode contributes an energy of $\frac12 \kB T$ to the total energy; therefore we have $\ev{E_n} = \frac12 \kB T$. Use this relation to find an expression for $\ev{a_n^2}$.
\item Substitute your expression for $\ev{a_n^2}$ in equation~(\ref{WLCendtoendapprox}) for $\ev{R}$, using your result of (a) to write the integral as a sum over modes, and re-write it such that the terms in the sum are dimensionless (hint: the dimension of $f$ is inverse length).
\item The sum you found actually converges, though no-one today will blame you for not recognizing it: it is related to the hyperbolic cotangent, which satisfies
\begin{equation}
\label{cothsum}
\sum_{n=1}^\infty \frac{1}{1 + \frac{\pi^2 n^2}{a^2}} = -\frac12 + \frac{a}{2} \coth(a).
\end{equation}
Use equation~(\ref{cothsum}) to get an approximate analytical expression for $\ev{R}$ as a function of $L$, $\xi_\mathrm{p}$ and $f$, which you then re-cast in an expression that, apart from an overall factor $L$, depends only on the product $f \xi_\mathrm{p}$ and the ratio $\xi_\mathrm{p} / L$.
\end{enumerate}

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item We have
\begin{align*}
\int_0^L \ev{\left( \theta(s) \right)^2} \dd{s} &= \int_0^L \ev{\left[\sum_{n=1}^\infty a_n \sin(\frac{n\pi}{L} s)\right]^2} \dd{s} \\
&= \ev{\sum_{n=1}^\infty \sum_{m=1}^\infty  \int_0^L a_n a_m \sin(\frac{n\pi}{L} s) \sin(\frac{m\pi}{L} s) \dd{s} } \\
&= \ev{\sum_{n=1}^\infty \sum_{m=1}^\infty  a_n a_m \frac{L}{2} \delta_{mn}} \\
&= \frac{L}{2} \ev{\sum_{n=1}^\infty a_n^2} = \frac{L}{2} \sum_{n=1}^\infty \ev{a_n^2},
\end{align*}
where we used int he third line that the sine functions are orthogonal. 
\item We have
\begin{align*}
\int_0^L \left( \dv{\theta}{s} \right)^2 \dd{s}  &= \int_0^L \left( \dv{s} \sum_{n=1}^\infty a_n \sin(\frac{n\pi}{L} s) \right)^2 \dd{s} \\
&=  \int_0^L \left(  \sum_{n=1}^\infty a_n \frac{n\pi}{L} \cos(\frac{n\pi}{L} s) \right)^2 \dd{s} \\
&= \sum_{n=1}^\infty \sum_{m=1}^\infty a_n a_m \frac{n\pi}{L} \frac{m\pi}{L} \int_0^L \cos(\frac{n\pi}{L} s) \cos(\frac{m\pi}{L} s) \dd{s} \\
&= \sum_{n=1}^\infty \sum_{m=1}^\infty a_n a_m \frac{n\pi}{L} \frac{m\pi}{L} \frac{L}{2} \delta_{mn} \\
&= \sum_{n=1}^\infty a_n^2 \frac{n^2 \pi^2}{2 L},
\end{align*}
where we used in the third line that the cosine functions are also orthogonal.
\item Substituting our expansions in equation~(\ref{WLCstretchenergyapprox}), we get
\begin{align*}
\frac{E}{\kB T} &= \frac{\xi_\mathrm{p}}{2} \sum_{n=1}^\infty a_n^2 \frac{n^2 \pi^2}{2 L} + \frac{f}{2} \frac{L}{2} \sum_{n=1}^\infty a_n^2 = \frac{L}{2} \sum_{n=1}^\infty \ev{a_n^2} = \frac14 \sum_{n=1}^\infty a_n^2 \left(\frac{\xi_\mathrm{p}}{L} n^2 \pi^2 + fL \right).
\end{align*}
\item We have
\begin{align*}
\ev{E} &= \sum_{n=1}^\infty \ev{E_n} = \left(\frac12 \kB T\right) \sum_{n=1}^\infty \left[ \frac12  \left(\frac{\xi_\mathrm{p}}{L} n^2 \pi^2 + fL \right) \right] a_n^2, \\
E_n &= \left(\frac12 \kB T\right) \left[ \frac12  \left(\frac{\xi_\mathrm{p}}{L} n^2 \pi^2 + fL \right) \right] a_n^2
\end{align*}
\item We simply equate $\ev{E_n}$ to $\frac12 \kB T$ and read off
\begin{align*}
\ev{a_n^2} &= \frac{2}{\frac{\xi_\mathrm{p}}{L} n^2 \pi^2 + fL}
\end{align*}
\item We have
\begin{align*}
\ev{R} &= L - \frac{L}{4} \sum_{n=1}^\infty \ev{a_n^2} = L - \frac{L}{4} \sum_{n=1}^\infty \frac{2}{\frac{\xi_\mathrm{p}}{L} n^2 \pi^2 + fL} \\
&= L - \frac{1}{2 f} \sum_{n=1}^\infty \frac{1}{1 + \frac{\xi_\mathrm{p}}{f L^2} \pi^2 n^2}.
\end{align*}
\item Comparing the given expression to the one we found in (f), we identify $a^2 = f L^2 / \xi_\mathrm{p}$, and therefore
\begin{align*}
\ev{R} &= L - \frac{1}{2 f} \left[ -\frac12 + \frac{L}{2} \sqrt{\frac{f}{\xi_\mathrm{p}}} \coth(L\sqrt{\frac{f}{\xi_\mathrm{p}}}) \right] = L  + \frac{1}{4 f} - \frac{L}{4f} \sqrt{\frac{f}{\xi_\mathrm{p}}} \coth(L\sqrt{\frac{f}{\xi_\mathrm{p}}}) \\
&= L \left[ 1 - \frac{1}{4 f \xi_\mathrm{p}} \frac{\xi_\mathrm{p}}{L} - \frac{1}{4\sqrt{f \xi_\mathrm{p}}} \coth(\frac{L}{\xi_\mathrm{p}} \sqrt{f \xi_\mathrm{p}}) \right].
\end{align*}
\end{enumerate}
\fi