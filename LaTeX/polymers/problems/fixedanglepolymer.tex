% Problemtitle: Polymers with rigid bends
% Source: Boal 3.19
Consider a polymer such as a linear alkane, where the bond angle between successive carbon atoms is a fixed value $\alpha$, although the bonds are free to rotate around one another. \newline
\begin{center}
\includegraphics[scale=1]{polymers/problems/fixedanglepolymer}\\
\end{center}
The length and the orientation of the bond between atom $i$ and atom $i+1$ defines a bond vector $\bvec{b}_i$. Assume all bonds have the same length~$b$, and that remote bonds can intersect.
\begin{enumerate}[(a)]
\item Show that the average projection of $\bvec{b}_{i+k}$ on $\bvec{b}_i$ is 
\begin{equation}
	\langle \bvec{b}_i \cdot \bvec{b}_{i+k} \rangle = b^2(-\cos\alpha)^k \qquad (k\geq 0)
\end{equation}
\textit{Hint}: Start with $\langle\bvec{b}_i \cdot \bvec{b}_{i+1}\rangle$ and iterate.
\item Write $\langle \bvec{r}_\mathrm{ee}^2\rangle$ (where $\bvec{r}_\mathrm{ee}$ is the end-to-end displacement vector) in terms of $\langle\bvec{b}_i \cdot \bvec{b}_j\rangle$ to obtain
\begin{equation}
\ev{\bvec{r}_\mathrm{ee}^2} = N b^2 + 2(N-1) (-\cos\alpha) + 2(N-2) (-\cos\alpha)^2 + \ldots + 2(-\cos\alpha)^{N-1}.
\end{equation}
%	\begin{equation}
%		\ev{\bvec{r}_\mathrm{ee}^2} = N b^2 \left[ 1 + \left( 2 - \frac{2}{N}\right) (-\cos\alpha) + \left( 2 - \frac{4}{N}\right) (-\cos\alpha)^2 + \dots + \frac{2}{N} \left( -\cos\alpha \right)^N \right]
%	\end{equation}
\item Use your result from (b) to establish that, in the large $N$ limit 
\begin{equation}
\langle \bvec{r}_\mathrm{ee}^2\rangle = N b^2 \frac{1-\cos\alpha}{1+\cos\alpha}.
\end{equation}
\item What is the effective bond length (in units of $b$) in this model for an angle at the tetrahedral value of $\alpha = 109.5^{\circ}$?
\end{enumerate}

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item We need to show that $\ev{\bvec{b}_i\cdot\bvec{b}_{i+k}} = b^2(-\cos\alpha)^{k}$. As suggested in the hint we first look at next neighbors and notice that this expression is deterministic since the angle between adjacent sections is fixed, so we get
\begin{equation*}
\ev{\bvec{b}_i\cdot\bvec{b}_{i+1}} = \bvec{b}_i\cdot\bvec{b}_{i+1} = b^2 \cos(\pi-\alpha) = - b^2 \cos(\alpha).
\end{equation*}
Similarly, we get $\ev{\bvec{b}_{i+1} \cdot \bvec{b}_{i+2}} = - b^2 \cos(\alpha)$, as $\bvec{b}_{i+2}$ has a component of length $-b \cos(\alpha)$ along $\bvec{b}_{i+1}$ (see figure).
\begin{center}
\includegraphics[scale=1]{polymers/problems/fixedanglepolymerdecomposition}
\end{center}
If we take the dot product between $\bvec{b}_i$ and $\bvec{b}_{i+2}$, we will thus on average get
\begin{equation}
\ev{\bvec{b}_i\cdot\bvec{b}_{i+2}} = \bvec{b}_i \cdot \ev{\bvec{b}_{i+1} \cdot \bvec{b}_{i+2}} \frac{\bvec{b}_{i+1}}{b} = \left(-\cos(\alpha) \right) \bvec{b}_i \cdot \bvec{b}_{i+1} = b^2 \left(-\cos(\alpha)\right).
\end{equation}
Iterating, we get the desired result
\begin{align}
\ev{\bvec{b}_i\cdot\bvec{b}_{i+k}} &= b^2 (-\cos(\alpha))^{k}.
\end{align}
\item $\bvec{r}_{ee}$ is the end-to-end vector, for which we have 
\begin{align*}
\bvec{r}_{ee} &= \sum_{i}\bvec{b}_i, \\
\bvec{r}_\mathrm{ee}^2 &= \left(\sum_i\bvec{b}_i\right) \cdot \left( \sum_j\bvec{b}_j \right).
\end{align*}
If we take the ensemble average, we get
\begin{align*}
\ev{\bvec{r}_\mathrm{ee}^2} &= \left\langle \left(\sum_i \bvec{b}_i\right) \cdot \left( \sum_j \bvec{b}_j \right) \right\rangle \\
&= \sum_i \ev{\bvec{b}_i^2} + 2 \sum_i \sum_{j>i} \ev{\bvec{b}_i \cdot \bvec{b}_j} \\
&= N b^2 + 2 \sum_i \sum_{j=i+1} \ev{\bvec{b}_i \cdot \bvec{b}_j} + 2 \sum_i \sum_{j=i+2} \ev{\bvec{b}_i \cdot \bvec{b}_j} + \ldots + 2 \sum_i \sum_{j=i+N-1} \ev{\bvec{b}_i \cdot \bvec{b}_j} \\
&= N b^2 + 2(N-1) (-\cos\alpha) + 2(N-2) (-\cos\alpha)^2 + \ldots + 2(-\cos\alpha)^{N-1},
\end{align*}
%\begin{align*}
%\ev{\bvec{r}_\mathrm{ee}^2} &= \left\langle \left(\sum_i\bvec{b}_i\right) \cdot \left( \sum_j\bvec{b}_j \right) \right\rangle \\
%&= \langle\bvec{b}_1\cdot\bvec{b}_1\rangle +\langle\bvec{b}_2\cdot\bvec{b}_2\rangle + \ldots + 2\langle\bvec{b}_1\cdot\bvec{b}_2\rangle + 2 \langle\bvec{b}_2\cdot\bvec{b}_3\rangle + \ldots + 2\langle\bvec{b}_1\cdot\bvec{b}_3\rangle + 2 \langle\bvec{b}_2\cdot\bvec{b}_4\rangle + \ldots + 2 \langle\bvec{b}_1\cdot\bvec{b}_N\rangle \\
%&= b^2 + b^2 + \ldots + 2 b^2 \left( -\cos\alpha \right) + \ldots + 2 b^2 \left( -\cos\alpha \right)^2 + \ldots + 2 b^2 \left( -\cos\alpha \right)^N \\
%&= b^2 \left[ N +2 (N-1) \left(-\cos\alpha \right)+ 2(N-2) \left(-\cos\alpha \right)^2 + \ldots + \frac{2}{N} \left( -\cos\alpha \right)^N \right],
%\end{align*}
which is the required result.
\item We first re-write the answer of (b) as
\begin{align*}
\ev{\bvec{r}_\mathrm{ee}^2} &= N b^2 \left[ 1 + 2 \left(1 - \frac{1}{N} \right) (-\cos\alpha) + 2 \left(1 - \frac{2}{N} \right) (-\cos\alpha)^2 + \ldots + \frac{2}{N}(-\cos\alpha)^{N-1} \right].
\end{align*}
For large $N$, we get $1/N \rightarrow 0$, $2/N \rightarrow 0$, etc. We then get a geometric series ($\frac{1}{1-x} = 1 + x + x^2 + \ldots $):
\begin{align*}
\ev{\bvec{r}_\mathrm{ee}^2} &= N b^2 \left[1 + 2 (-\cos\alpha) + 2 (-\cos\alpha)^2 + \ldots \right] \\
&= - N b^2 + 2 N b^2 \left[1 + (-\cos\alpha) + (-\cos\alpha)^2 + \ldots \right] \\
&= N b^2 \left[ -1 + \frac{2}{1 - (-\cos\alpha)} \right] \\
&= N b^2  \frac{1-\cos\alpha}{1+\cos\alpha}.
\end{align*}	
\item The effective bond length is defined by $\ev{\bvec{r}_\mathrm{ee}^2} = Nb_\mathrm{eff}^2$, so we have
\begin{equation}
b_\mathrm{eff}^2 = \frac{\ev{\bvec{r}_\mathrm{ee}^2}}{N} = b^2\frac{1-\cos\alpha}{1+\cos\alpha} \approx 2b^2
\end{equation}
and $b_\mathrm{eff} \approx 1.415b$
\end{enumerate}
\fi