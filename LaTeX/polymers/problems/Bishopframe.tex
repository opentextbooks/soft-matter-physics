%Problemtitle: The Bishop frame of a curve in space
While the Frenet-Serret frame for a curve in space has an intuitive geometrical interpretation, it also has a major drawback, namely that in this frame the torsion is ill-defined for a curve with zero curvature. In his 1975 paper `There is more than one way to frame a curve'~\cite{Bishop1975}, R. L. Bishop introduced an alternative, now sometimes known as the Bishop frame. The Bishop frame still has the tangent vector $\unitvec{t}(s)$, pointing along the curve, but uses different vectors to span the plane perpendicular to the curve. These vectors are chosen such that they are \emph{relatively parallel}, meaning that their derivatives are only in the tangential direction. They therefore do turn with the curve, but only to the degree that they remain normal to the curve, unlike the normal and binormal vectors of the Frenet-Serret frame.

Suppose now we have two relatively parallel unit vectors $\unitvec{m}_1$ and $\unitvec{m}_2$, then they satisfy $\dot{\unitvec{m}_1} = -k_1 \unitvec{t}$ and $\dot{\unitvec{m}_2} = -k_2 \unitvec{t}$ (the minus signs are a convention only, you'll see below why this convention was chosen). These vectors will be the basis vectors for the plane. The first order of business is to prove that the angle between $\unitvec{m}_1$ and $\unitvec{m}_2$ is invariant.
\begin{enumerate}[(a)]
\item Find the derivative of $\inprod{\unitvec{m}_1}{\unitvec{m}_2}$ and show that it vanishes.
\setcounter{problemcounter}{\value{enumii}}
\end{enumerate}
Given that the angle between $\unitvec{m}_1$ and $\unitvec{m}_2$ is conserved, we can always construct an orthogonal set of relatively parallel unit vectors that span the normal plane, and we have $\unitvec{t} \cdot \unitvec{m}_1 = \unitvec{t} \cdot \unitvec{m}_2 = \unitvec{m}_1 \cdot \unitvec{m}_2 = 0$. Because the tangent vector is a unit vector, its derivative is still perpendicular to $\unitvec{t}$, and hence lies in the plane spanned by $\unitvec{m}_1$ and $\unitvec{m}_2$, so we can write
\begin{equation}
\label{tangentderivativeBishop1}
\frac{\dd\unitvec{t}}{\dd s} = l_1 \unitvec{m}_1 + l_2 \unitvec{m}_2.
\end{equation}
\begin{enumerate}[(a)]
\setcounter{enumii}{\value{problemcounter}}
\item We can find $l_1$ and $l_2$ using the same trick we applied in section~\bookref{sec:spacecurves}: differentiate the relationship $\unitvec{t} \cdot \unitvec{m}_1$ to show that $l_1 = k_1$ (and likewise $l_2 = k_2$).
\setcounter{problemcounter}{\value{enumii}}
\end{enumerate}
Using the result we just found, we re-write equation~(\ref{tangentderivativeBishop1}) as
\begin{equation}
\label{tangentderivativeBishop}
\frac{\dd\unitvec{t}}{\dd s} = k_1 \unitvec{m}_1 + k_2 \unitvec{m}_2 = \kappa \left[ \cos(\theta) \unitvec{m}_1 + \sin(\theta) \unitvec{m}_2 \right],
\end{equation}
where the curvature $\kappa$ is still the magnitude of $\dot{\unitvec{t}}$.
\begin{enumerate}[(a)]
\setcounter{enumii}{\value{problemcounter}}
\item Relate the curvature $\kappa$ to $k_1$ and $k_2$.
\item Express the unit normal vector~$\unitvec{n}$ from the Frenet-Serret frame in terms of $\unitvec{m}_1$ and $\unitvec{m}_2$.
\item Now differentiate $\unitvec{n}$, to get expressions for both the unit binormal~$\unitvec{b}$ and the torsion~$\tau$.
\end{enumerate}
Note that the result for the torsion also applies to a curve with zero curvature, and is consistent with the twisted rod of section~\bookref{sec:rodtwisting}.

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item We simply calculate:
\begin{align*}
\frac{\dd}{\dd s} \inprod{\unitvec{m}_1}{\unitvec{m}_2} &= \inprod{\dot{\unitvec{m}}_1}{\unitvec{m}_2} + \inprod{\unitvec{m}_1}{\dot{\unitvec{m}}_2} = -\inprod{k_1\unitvec{t}}{\unitvec{m}_2} - k_2 \inprod{\unitvec{m}_1}{\unitvec{t}} = 0,
\end{align*}
so the relative angle is conserved.
\item We have
\begin{align*}
0 &= \frac{\dd}{\dd s} \left(\unitvec{t} \cdot \unitvec{m}_1 \right) = \left(l_1 \unitvec{m}_1 + l_2 \unitvec{m}_2 \right) \cdot \unitvec{m}_1 + \unitvec{t} \cdot \left(- k_1 \unitvec{t} \right) = l_1 - k_1,
\end{align*}
so indeed $l_1 - k_1$.
\item From equation~(\ref{tangentderivativeBishop}):
\begin{align*}
\kappa^2 = k_1^2 + k_2^2.
\end{align*}
\item As $\dot{\unitvec{t}} = \kappa \unitvec{n}$, we can simply read off that
\begin{align*}
\unitvec{n} &= \cos(\theta) \unitvec{m}_1 + \sin(\theta) \unitvec{m}_2.
\end{align*}
\item Differentiating $\unitvec{n}$ in both frames gives:
\begin{align*}
\frac{\dd \unitvec{n}}{\dd s} &= - \left[ \cos(\theta) k_1 + \sin(\theta) k_2 \right) \unitvec{t} + \dot{\theta} \left[ -\sin(\theta) \unitvec{m}_1 + \cos(\theta) \unitvec{m}_2 \right] \\
&= - \kappa \unitvec{t} + \tau \unitvec{b},
\end{align*}
from which we read off that
\begin{align*}
\unitvec{b} &= -\sin(\theta) \unitvec{m}_1 + \cos(\theta) \unitvec{m}_2, \\
\tau &= \dot{\theta}.
\end{align*}
\end{enumerate}
\fi
