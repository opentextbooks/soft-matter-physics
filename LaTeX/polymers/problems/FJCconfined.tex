%Problemtitle A freely jointed chain polymer under confinement
%Source: based on  http://www.pmaweb.caltech.edu/~mcc/Ph127/a/Lecture_8.pdf
A freely jointed chain polymer executes a random walk through space, occupying a volume that scales with the square root of the number of segments. If we confine the polymer to a space that is significantly smaller than that volume, we reduce the number of possible configurations, which comes at the cost of an increase in the free energy. Consequently, a confined polymer will exert an entropic pressure on the walls of its container.
\begin{enumerate}[(a)]
\item As the freely jointed chain executes a random walk through space, its probability distribution should satisfy the diffusion equation, with the number of segments~$N$ taking the role of time:
\begin{equation}
\label{FJCdiffusioneq}
\pdv{P}{N} = D \nabla^2 P.
\end{equation}
Verify that the Gaussian distribution of equation~(\bookref{FJCdist}) indeed satisfies equation~(\ref{FJCdiffusioneq}) and find the value of the diffusion constant $D$.
\ifproblemset\setcounter{problemcounter}{\value{enumi}}\else\setcounter{problemcounter}{\value{enumii}}\fi
\end{enumerate}
We will first consider the one-dimensional case, because, similar to the way we found the three-di\-men\-sio\-nal probability distribution, the three-dimensional case will simply be the product of three one-dimensional cases. In one dimension, we put walls at $x=0$ and $x=L$. For the unconfined polymer, the probability density tells us the probability of finding a polymer of length~$N$ with one end at position $x$ and the other at $y = x + r$. The result only depends on $r$; we normalize it such that the integral of the density over all of space vanishes. In this case, the problem is much the same, except that we put on additional boundary conditions, which state that $x$ and $y$ have to be in the interval $(0, L)$. Let us therefore define a confined probability density function $P_\mathrm{c}(y, N)$ given by
\begin{subequations}
\label{confinedFJCpdv}
\begin{align}
\label{confinedFJCpdvDE}
\pdv{P_\mathrm{c}}{N} &= D \pdv[2]{P_\mathrm{c}}{y}, \\
\label{confinedFJCpdvBC}
P_\mathrm{c}(0, N) &= P_\mathrm{c}(L, N) = 0, \\
\label{confinedFJCpdvIC}
P_\mathrm{c}(y, 0) &= \delta(y-x).
\end{align}
\end{subequations}
\begin{enumerate}[(a)]
\ifproblemset\setcounter{enumi}{\value{problemcounter}}\else\setcounter{enumii}{\value{problemcounter}}\fi
\item Argue why the probability density $P_\mathrm{c}(y, N)$ should satisfy the boundary conditions~(\ref{confinedFJCpdvBC}) and `initial condition'~(\ref{confinedFJCpdvIC}).
\item The form of the boundary conditions~(\ref{confinedFJCpdvBC}) suggests that it would be a good idea to expand $P_\mathrm{c}(y, N)$ in a Fourier sine series
\begin{equation}
\label{confinedFJCpdfFS}
P_\mathrm{c}(y, N) = \sum_{n=1}^\infty a_n \sin(\frac{n \pi}{L} y).
\end{equation}
There are no constant or cosine terms in~(\ref{confinedFJCpdfFS}) because they don't vanish at the boundaries. Substitute~(\ref{confinedFJCpdfFS}) in the diffusion equation~(\ref{confinedFJCpdvDE}) to get a first-order differential equation for each of the coefficients $a_n$. \textit{NB}: you will need to use the fact that the sine functions are orthogonal; if you have not seen this concept before, it means that we have
\begin{equation}
\frac{2}{L} \int_0^L \sin(\frac{n \pi}{L} y) \sin(\frac{m \pi}{L} y) \dd{y} = \delta_{nm},
\end{equation}
i.e. the integral is only nonzero if $n=m$. Therefore, for the sum in the Fourier sine series~(\ref{confinedFJCpdfFS}) to vanish, each term must vanish. Also, note that because the series is an expansion in $y$, the coefficients $a_n$ are independent of $y$, but not of $N$.
\item Solve the first-order differential equation for $a_n$ you got in (c). You should get an integration constant $C_n$ that depends on $n$ but not on $N$ or $y$.
\item To find the values of the constants $C_n$, we need the `initial condition'~(\ref{confinedFJCpdvIC}). The expansion of the Dirac delta function in a Fourier sine series on the interval $(0, L)$ is given by
\begin{subequations}
\label{DiracDeltaFourierSine}
\begin{align}
\delta(x-y) &= \sum_{n=1}^\infty b_n \sin(\frac{n \pi}{L} y), \\
b_n &= \frac{2}{L} \int_0^L \delta(x-y) \sin(\frac{n \pi}{L} y) \dd{y} = \frac{2}{L} \sin(\frac{n \pi}{L} x)
\end{align}
\end{subequations}
Use the expansion~(\ref{DiracDeltaFourierSine}) to find the $C_n$'s.
\item To get the free energy of a confined chain, we first calculate its partition function. Because the chains carry no internal energy, all chains are equally likely. The partition function is then just the normalization of our probability density function $P_\mathrm{c}$, accounting for all possible positions of both ends. Show that the partition function of a chain of length~$N$ evaluates to:
\begin{equation}
\label{confinedFJCpartitionfunction}
Z_N = \int_0^L \dd{x} \int_0^L \dd{y} P_\mathrm{c}(x, y, N) = \frac{8L}{\pi^3} \sum_{n \text{ odd}} \frac{1}{n^2} \exp\left(-\frac{D n^2 \pi^2}{L^2} N \right).
\end{equation}
\item Estimate the ratio of the magnitudes of the $n=3$ to the $n=1$ terms in $Z_N$ (you'll need the value of $D$ from (a) to do this). Under which conditions can we approximate $Z_N$ by just the first term in the sum?
\item This is a good point to go back to three dimensions: as the motion in the three directions is independent, the three-dimensional partition function is simply the cube of the one-dimensional one. Write it down in terms of $V = L^3$, in the approximation of (g) (i.e., taking only the $n=1$ term of equation~(\ref{confinedFJCpartitionfunction}).
\item Find the free energy of the confined chain.
\item From the free energy, find the pressure exerted by the confined chain on its container.
\end{enumerate}

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item As the probability distribution only depends on the end-to-end distance~$r$, we express the Laplacian in spherical coordinates, and only need the radial part, for which we get (with $\sigma^2 = N b^2/3$ and $A = \left( 3/ 2 \pi N b^2 \right)^{3/2} = (\sqrt{2 \pi} \sigma)^{-3}$)
\begin{align*}
\nabla^2 P(r) &= \frac{1}{r^2} \pdv{r} \left(r^2 \pdv{P}{r} \right) = \frac{A}{r^2} \pdv{r} \left[ r^2 \left(- \frac{r}{\sigma^2} \right) e^{-r^2/2\sigma^2} \right] \\
&= \frac{A}{r^2} \left[ - \frac{3r^2}{\sigma^2} + \frac{r^4}{\sigma^4} \right] e^{-r^2/2\sigma^2} = \frac{3A}{\sigma^2} \left[ \frac{r^2}{3\sigma^2} - 1 \right] e^{-r^2/2\sigma^2} = \left( \frac{3}{2 \pi N b^2} \right)^{3/2} \frac{3}{N b^2} \left[ \frac{r^2}{N b^2} - 1 \right] e^{-r^2/2\sigma^2}
\end{align*}
The derivative of $P$ to $N$ is given by
\begin{align*}
\pdv{P}{N} &= \pdv{N} \left[ \left(\frac{3}{2 \pi N b^2} \right)^{3/2} \exp\left(\frac{3 r^2}{2 N b^2} \right) \right] \\
&= \left[ \frac32 \left(\frac{3}{2 \pi N b^2} \right)^{1/2} \frac{3}{2 \pi b^2} \frac{-1}{N^2} + \left(\frac{3}{2 \pi N b^2} \right)^{3/2} \frac{-3 r^2}{2 b^2} \frac{-1}{N^2} \right] \exp\left(\frac{3 r^2}{2 N b^2} \right)  \\
&= \left(\frac{3}{2 \pi N b^2} \right)^{3/2} \left[ -\frac{3}{2N} + \frac{3 r^2}{2 N^2 b^2} \right] \exp\left(\frac{3 r^2}{2 N b^2} \right)
= \left(\frac{3}{2 \pi N b^2} \right)^{3/2} \frac{3}{2N} \left[ \frac{r^2}{N b^2} - 1 \right] \exp\left(\frac{3 r^2}{2 N b^2} \right) \\
&= \frac{b^2}{6} \nabla^2 P,
\end{align*}
so the probability distribution $P$ indeed satisfies the diffusion equation~(\ref{FJCdiffusioneq}) for $D = b^2/6$.
\item The boundary condition~(\ref{confinedFJCpdvBC}) simply states that the end position of the polymer cannot be on either of the walls. The `initial condition'~(\ref{confinedFJCpdvIC}) states that for a `polymer' of zero segments, the end point coincides with the initial point (as that is the only point with nonzero probability).
\item Substitution is easy, as we only have to take derivatives:
\begin{align*}
\pdv{P_\mathrm{c}}{N} = \sum_{n=1}^\infty \pdv{a_n}{N} \sin(\frac{n \pi}{L} y) &= D \pdv[2]{P_\mathrm{c}}{y} = - D sum_{n=1}^\infty a_n \left(\frac{n \pi}{L} \right)^2 \sin(\frac{n \pi}{L} y),
\end{align*}
which, by orthogonality of the sines, gives us for each coefficient $a_n$
\begin{align*}
\pdv{a_n}{N} = - D \left(\frac{n \pi}{L} \right)^2 a_n
\end{align*}
\item We simply separate variables and integrate to get
\begin{align*}
a_n &= C_n \exp\left(-\frac{D n^2 \pi^2}{L^2} N \right).
\end{align*}
\item Again invoking the orthogonality of the sines, we can simply compare $P_\mathrm{c}(y, 0)$ term-by-term with the expansion of $\delta(x-y)$, which gives
\begin{align*}
C_n &= \frac{2}{L} \sin(\frac{n \pi}{L} x).
\end{align*}
\item Evaluating the integrals is straightforward:
\begin{align*}
Z_N &= int_0^L \dd{x} \int_0^L \dd{y} P_\mathrm{c}(x, y, N) = \sum_{n=1}^\infty \frac{2}{L} int_0^L \dd{x} \int_0^L \dd{y} \sin(\frac{n \pi}{L} x) \sin(\frac{n \pi}{L} y) \exp\left(-\frac{D n^2 \pi^2}{L^2} N \right) \\
&= \sum_{n=1}^\infty \frac{2}{L} \exp\left(-\frac{D n^2 \pi^2}{L^2} N \right) \left(\frac{L}{n \pi} \left[ \cos(\frac{n \pi x}{L}) \right]_{x=0}^{x=L}\right)^2 = \sum_{n=1}^\infty \frac{2L}{n^2 \pi^2} \exp\left(-\frac{D n^2 \pi^2}{L^2} N \right) \left((-1)^2 - 1 \right)^2 \\
&= \frac{8L}{\pi^2} \sum_{n \text{ odd}} \frac{1}{n^2} \exp\left(-\frac{D n^2 \pi^2}{L^2} N \right).
\end{align*}
\item We have
\begin{align*}
\frac{\frac19 \exp\left(-9 D \pi^2 N / L^2\right)}{\exp\left(-D \pi^2 N / L^2\right)} &= \frac19 \exp(-\frac{3 b^2 \pi^2}{4 L^2} N) = \frac19 \left[1 - \frac{3 b^2 \pi^2}{4 L^2} N + \frac12 \left(\frac{3 b^2 \pi^2}{4 L^2} N\right)^2 + \cdots \right],
\end{align*}
so all higher-order terms are small if $Nb^2 \ll L^2$.
\item We have
\begin{align*}
Z_N^{(3\mathrm{D})} &= \left(\frac{8}{\pi}\right)^3 L^3 \exp(-\frac{\pi^2 b^2}{2 L^2} N) = \left(\frac{8}{\pi}\right)^3 V \exp(-\frac{\pi^2 b^2}{2 V^{2/3}} N). 
\end{align*}
\item We use equation~(\bookref{partitionfunctionfreeenergy}):
\begin{align*}
F &= -\kB T \log Z = \kB T \left[ \frac{\pi^2 b^2}{2 V^{2/3}} N - \log\left(\frac{8}{\pi})^3 V\right) \right].
\end{align*}
\item The pressure is simply (minus) the derivative of the free energy with respect to the volume:
\begin{align*}
p &= - \left.\pdv{F}{V}\right)_{T, N} = \kB T \left[\frac{\pi^2 b^2}{3 V^{5/3}} N + \frac{1}{V} \right].
\end{align*}
\end{enumerate}
\fi
