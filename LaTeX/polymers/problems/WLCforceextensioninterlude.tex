A common test for a polymer model is whether it can predict a \emph{force-extension} curve: the relation between an applied force and the resulting end-to-end distance of the polymer. In practice, experimental data often gives you the inverse - a standard method is to use optical tweezers to fix the position of the two ends of a polymer in place, then measure the force required to keep them there. Working in two dimensions for simplicity, we can relate the distance~$R$ between the beads to the shape of the polymer (given by the function $\theta(s)$) through
\begin{equation}
\label{WLCendtoendsingleshape}
R = \int_0^L \cos(\theta(s)) \dd{s}.
\end{equation}
Equation~(\ref{WLCendtoendsingleshape}) only applies to the shape given by $\theta(s)$. If we want to calculate the average value of $R$, we need to average over all possible shapes, weighing each shape by its probability. Formally, we write this average in the form of a \emph{path integral}
\begin{equation}
\label{WLCendtoendaveragepathintegral}
\ev{R} = \int \mathcal{D}\left[\theta(s)\right] R\left[\theta(s)\right] P\left[\theta(s)\right].
\end{equation}
Note that $R\left[\theta(s)\right]$ and $P\left[\theta(s)\right]$ are functions of the function $\theta(s)$; such functions-of-functions are known as \emph{functionals}. The notation $\int \mathcal{D}\left[\theta(s)\right]$ means we integrate over all possible functions $\theta(s)$ that satisfy whichever conditions we impose. As the functional $R\left[\theta(s)\right]$ is still given by equation~(\ref{WLCendtoendsingleshape}), we can, with a little abuse of notation, re-write equation~(\ref{WLCendtoendaveragepathintegral}) in the friendlier-looking form
\begin{equation}
\label{WLCendtoendaverage}
\ev{R} = \int_0^L \ev{\cos(\theta(s))} \dd{s},
\end{equation}
where we used that all integrals (including path integrals) are linear, and thus can be swapped with taking averages. The energy of the chain now includes a work term for the stretching, and is given by
\begin{equation}
\label{WLCstretchenergy}
E\left[(\theta(s)\right] = E_\mathrm{bend} - W = \frac12 \xi_\mathrm{p} \kB T \int_0^L \left( \dv{\theta}{s} \right)^2 \dd{s} - F \int_0^L \cos(\theta(s)) \dd{s}.
\end{equation}
