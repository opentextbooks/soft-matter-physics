% Problem based on Boal 4.24.
As introduced in\ifproblemset the course\else section~\bookref{sec:FJC}\fi, the freely jointed chain (FJC) model for polymers assumes that a polymer consists of straight segments with length~$b$, joined in vertices at which there is no constriction on the angle. In this problem, we'll derive the force-extension relation of such a polymer in one dimension (for mathematical simplicity), assuming that the segments and vertices don't feel each other. Figure~\ref{fig:FJC1d} shows an example configuration of such a one-dimensional chain: at each vertex, the next segment can either lie in the same or in the opposite direction as the previous one. If we give each segment an arrow, a chain of $N$ segments thus has $n_\mathrm{L}$ segments pointing left and $n_\mathrm{R}$ segments pointing right, where $N = n_\mathrm{L} + n_\mathrm{R}$. As you (hopefully) remember from combinatorics, the number of possible configurations is then given by $C(n_\mathrm{L}, n_\mathrm{R}) = N! / (n_\mathrm{L}! n_\mathrm{R}!)$.

\begin{figure}[ht]
\centering
\includegraphics[scale=1]{polymers/problems/FJC1D.pdf}
\caption[One-dimensional freely jointed chain polymer.]{Example configuration of a freely jointed chain in one dimension (overlaps displaced vertically for visual reasons only, colors likewise only for visual effect).}
\label{fig:FJC1d}
\end{figure}

\begin{enumerate}[(a)]
\item Use Stirling's approximation, $\log (N!) = N \log N - N$, valid at large $N$, to find the entropy of this configuration, $S = \kB \log(C(n_\mathrm{L}, n_\mathrm{R}))$, in terms of $N$ and $n_\mathrm{R}$ (so eliminate $n_\mathrm{L}$).
\item We now apply a tensile force~$f$ to the ends of the chain, which contributes an amount of work equal to~$F_W = -2 f b n_\mathrm{R}$ to the total free energy, $F = F_W - TS$. Minimize the free energy with respect to $n_\mathrm{R}$ to derive an expression for the ratio $n_\mathrm{R} / n_\mathrm{L}$.
\item Express the end-to-end length of the chain $x$ and the contour length $L_\mathrm{c}$ in terms of $n_\mathrm{R}$ and $n_\mathrm{L}$.
\item Combine your results from parts (b) and (c) to get an expression for $x/L_\mathrm{c}$ in terms of $f$ (i.e., eliminate $n_\mathrm{L}$ and $n_\mathrm{R}$).
\item Find the effective spring constant of the one-dimensional FJC for small~$x$.
\item Find the limiting value of $x$ as $f \to \infty$.
\end{enumerate}
(Problem source: Boal~\cite{Boal2012}, problem 4.24).

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item We have
\begin{align*}
S &= \kB \log(C(n_\mathrm{L}, n_\mathrm{R})) = \kB T \left[ \log(N!) - \log(n_\mathrm{L}!) - \log(n_\mathrm{R}!) \right] \\
&= \kB \left[ N \log N - N - n_\mathrm{L} \log(n_\mathrm{L}) + n_\mathrm{L} - n_\mathrm{R} \log(n_\mathrm{R}) + n_\mathrm{R} \right] \\
&= \kB \left[ N \log N - (N-n_\mathrm{R}) \log(N-n_\mathrm{R}) - n_\mathrm{R} \log(n_\mathrm{R}) \right] \\
&= \kB \left[ N \log\left(\frac{N}{N-n_\mathrm{R}}\right) + n_\mathrm{R}\log\left(\frac{N-n_\mathrm{R}}{n_\mathrm{R}}\right) \right]
\end{align*}
where we used that $N = n_\mathrm{L} + n_\mathrm{R}$ \score{1 pt; 3rd and 4th line both fine as final answer}.
\item We have
\[
F = -2 f b n_\mathrm{R} + \kB T \left[n_\mathrm{R} \log(n_\mathrm{R}) + (N-n_\mathrm{R}) \log(N-n_\mathrm{R}) - N \log N \right], \]
\score{0.25 pt} from which we can easily take the derivative with respect to $n_\mathrm{R}$ to get
\begin{align*}
\frac{\partial F}{\partial n_\mathrm{R}} &= -2 f b + \kB T \left[ \log(n_\mathrm{R}) - \log(N-n_\mathrm{R}) \right] \\
&= -2 f b + \kB T \log(n_\mathrm{R} / n_\mathrm{L}).
\end{align*}
\score{0.5 pt}. Equating the derivative to zero gives the required expression for $n_\mathrm{R} / n_\mathrm{L}$:
\[ \frac{n_\mathrm{R}}{n_\mathrm{L}} = \exp\left( \frac{2fb}{\kB T} \right). \]
\score{0.25 pt}.
\item The end-to-end distance~$x$ is simply the stepsize times the number of steps to the right minus the number of steps to the left: $x = (n_\mathrm{R} - n_\mathrm{L})b$ \score{0.25 pt}. The contour length~$L_\mathrm{c}$ is the total length of all the segments, so $L_\mathrm{c} = Nb = (n_\mathrm{R} + n_\mathrm{L})b$ \score{0.25 pt}.
\item We have
\begin{equation}
\label{FLCforceextension}
\frac{x}{L_\mathrm{c}} = \frac{n_\mathrm{R} - n_\mathrm{L}}{n_\mathrm{R} + n_\mathrm{L}} = \frac{n_\mathrm{R} / n_\mathrm{L} - 1}{n_\mathrm{R} / n_\mathrm{L} + 1} = \frac{\exp\left( 2fb/\kB T\right) - 1}{\exp\left( 2fb/\kB T\right) + 1} = \tanh\left(\frac{fb}{\kB T}\right).
\end{equation}
\score{0.5 pt. No penalty for leaving the fraction with the exponentials.}
\item Since at small displacements the force is linear in the displacements (otherwise we wouldn't have a spring constant in the first place), we simply expand the hyperbolic tangent in equation~(\ref{FLCforceextension}): $\tanh(x) = x + \mathcal{O}(x^3)$, so we get 
\[ f \approx \frac{\kB T}{b} \frac{x}{L_\mathrm{c}} \quad \Rightarrow \quad k_\mathrm{eff} =  \frac{\kB T}{b L_\mathrm{c}}. \]
Alternatively, we could first invert equation~(\ref{FLCforceextension}) and then expand $\mathrm{arctanh}(x) = x + \mathcal{O}(x^3)$, which (of course) gives the same answer \score{0.5 pt}.
\item $\lim_{x \to \infty} \tanh(x) = 1$, so for $f \to \infty$, we have $x \to L_\mathrm{c}$, as it should \score{0.5 pt}.
\end{enumerate}
\fi