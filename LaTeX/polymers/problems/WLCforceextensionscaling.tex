%Problemtitle Force-extension of the WLC: scaling argument
%Source: Marantan & Mahadevan, AJP 2018.
\begin{figure}[ht]
\centering
\includegraphics[scale=1]{polymers/problems/WLCscaling.png}
\caption[Force-extension experiment of a polymer.]{Force-extension experiment of a polymer. (a) The positions of the ends of the polymer are constrained by two beads; the distance between the beads is given by equation~(\ref{WLCendtoendsingleshape}). (b) `clinks' as used in the scaling approach of problem~\ref{pb:WLCforceextensionscaling}. Figure from Marantan and Mahadevan, Am. J. Phys. 2018.}
\label{fig:WLCforceextension}
\end{figure}
In this problem, we'll make an attempt at estimating the force-extension relation of the worm-like chain model using scaling arguments. At relatively large forces, we expect the individual segments of our chain to `more or less' line up with the direction of the force; the `more or less' here is of course colloquial, but also accurately represents the fact that we still have a distribution of alignments. We can now introduce a unit of length~$\xi$ in between the large total chain length~$L$ and the small size~$b$ of a single segment as we'd have in a freely-jointed chain. At very short length scales, the polymer is simply straight; at our new intermediate length scales, the polymer is still relatively straight (as it resists bending) but not necessarily aligned with the external force. Therefore, this segment consists of a number of correlated links, which (following Marantan and Mahadevan), we'll call `clinks'. Clinks are deformed through thermal fluctuations (of magnitude $\kB T$), and resist bending because of the bending energy. Their resulting shape is an arc-like structure, deforming a distance~$h$ away from a straight line, reducing the end-to-end distance of that straight line from $\xi$ to $\xi - \Delta$, see figure~\ref{fig:WLCforceextension}(b).
\begin{enumerate}[(a)]
\item If we approximate the shape of the clink as a circular arc, find its curvature in terms of $h$ and $\xi$, in the approximation where both $\Delta$ and $h$ are significantly smaller than $\xi$.
\item Calculate the bending energy of a clink, making use of appropriate approximations. You should get that the bending energy scales with $K_\mathrm{eff} h^2 / \xi^3$.
\item By comparing the bending energy in equilibrium with the thermal energy $\kB T$, estimate $\ev{h^2}$, and from that estimate, show that the end-to-end shrinkage of the clink is approximately given by $\Delta \sim \xi^2 / \xi_\mathrm{p}$ (where, as before, $\xi_\mathrm{p} = K_\mathrm{eff}/\kB T$).
\item The work done by the external force is now proportional to $F \Delta$. Argue why.
\item Show that thermal fluctuations at equilibrium then lead to a typical clink size $\xi \sim \sqrt{\xi_\mathrm{p} / f}$, where $f = F / \kB T$.
\item On average, clinks will align with the applied external force. Given that there are $L/\xi$ clinks in a chain of length $L$, which each have shrunk by an amount~$\Delta$, show that the average end-to-end distance will scale with the applied force as
\begin{equation}
\ev{R} \sim L \left( 1 - \frac{1}{\sqrt{f \xi_\mathrm{p}}} \right).
\end{equation}
(NB: the actual relation has an extra factor $\frac14$ in front of the second term, which we cannot get from a scaling argument. Even so, not bad for a calculation that requires nothing more complicated than Pythagoras' theorem!).
\end{enumerate}

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item We need to find the radius of the circular arc. Defining the opening angle of (half of) the arc as~$\alpha$, this radius is given by $\rho = \frac12 (\xi - \Delta) / \sin(\alpha)$. The angle also relates the base and height of the triangle in the arc, through $\tan(\alpha) = h / \frac12 (\xi - \Delta)$. Now if both $h$ and $\Delta$ are much smaller than $\xi$, we have $\tan(\alpha) \approx \sin(\alpha)$ and $\xi-\Delta \approx \xi$, so we get
\[ \rho = \frac{\frac12  (\xi-\Delta)}{\sin(\alpha)} \approx \frac{\left( \frac12 (\xi-\Delta)\right)^2}{h} \approx \frac14 \frac{\xi^2}{h}, \]
and for the curvature we find $\kappa = \rho^{-1} \approx 4 h / \xi^2$.
\item The bending energy is simply the integral of the square of the curvature:
\begin{equation}
E_\mathrm{bend} = \frac12 K_\mathrm{eff} \int_0^\xi \kappa(s)^2 \dd{s} = \frac12 K_\mathrm{eff} \xi \left(\frac{4h}{\xi^2}\right)^2 = 2 K_\mathrm{eff} \frac{h^2}{\xi^3}.
\end{equation}
\item We have
\begin{equation}
\ev{E_\mathrm{bend}} = 2 K_\mathrm{eff} \frac{\ev{h^2}}{\xi^3} \approx \kB T \qquad \Rightarrow \qquad \ev{h^2} \approx \frac{\kB T \xi^3}{2 K_\mathrm{eff}} = \frac{\xi^3}{2 \xi_\mathrm{p}}.
\end{equation}
To get $\Delta$, we use the approximation that in the triangle in figure~\ref{fig:WLCforceextension}(b), the hypothenuse is roughly $\xi/2$, and thus
\begin{equation}
\frac14 \xi^2 = h^2 + \frac14 \left(\xi - \Delta\right)^2 \approx h^2 + \frac14 \xi^2 - \frac12 \xi \Delta,
\end{equation}
so
\begin{equation}
\Delta \approx \frac{2\ev{h^2}}{\xi} = \frac{\xi^2}{\xi_\mathrm{p}}.
\end{equation}
\item The work done per clink to convert the current configuration to a completely straight one is the work necessary to stretch each clink by an additional amount $\Delta$, i.e., $F \Delta$.
\item We simply equate the work to the thermal energy:
\begin{equation}
\kB T = F \Delta \approx F \frac{\xi^2}{\xi_\mathrm{p}} \qquad \Rightarrow \qquad \xi \approx \sqrt{\frac{\kB T \xi_\mathrm{p}}{F}} = \sqrt{\frac{\xi_\mathrm{p}}{f}}.
\end{equation}
\item We have
\begin{equation}
\ev{R} = L - \frac{L}{\xi} \Delta = L \left(1 - \frac{1}{\xi} \frac{\xi^2}{\xi_\mathrm{p}} \right) = L \left(1 -  \frac{\xi}{\xi_\mathrm{p}} \right) \approx L \left( 1 - \frac{1}{\sqrt{f \xi_\mathrm{p}}} \right).
\end{equation}
\end{enumerate}
\fi
