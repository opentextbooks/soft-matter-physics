%Problemtitle End-to-end distance of a freely jointed chain: exact result
%Source: based on first part of http://www.pmaweb.caltech.edu/~mcc/Ph127/a/Lecture_8.pdf
The freely-jointed chain polymer makes a random walk through space. Based on this observation, we can write down the first and second moment of the probability distribution of the end-to-end distance of the chain, and, by invoking the central limit theorem, the full distribution for large enough chains (equation~\bookref{FJCdist}). In this problem, we'll compare this distribution with the exact one. While we can write down the exact distribution formally, it involves an integral that we have to solve numerically. As we'll find, already for short chains, the difference between the exact and the Gaussian distributions becomes negligible.

As we did in section~\bookref{sec:FJC}, we consider a chain of $N$ links of length~$b$. Each junction is free to assume any angle. Given the end point of one link, the tip of the next link can therefore take any position on a sphere of radius~$b$ around that point. The total number of configurations is then proportional to the area of $N$ such spheres. To find the probability that the end-to-end vector of the chain is given by the vector~$\bvec{r}$, we simply count how many of these configurations have that exact end-to-end distance, given by the sum over the vectors $\bvec{b}_i$ along the chains:
\begin{equation}
\label{FJCendtoenddistexact}
p(\bvec{r}) = \frac{\text{number of configurations where } \sum_i \bvec{b}_i = \bvec{r}}{\text{total number of configurations}} = \frac{\int \dd[2]{\bvec{x}_1} \cdots \dd[2]{\bvec{x}_N} \delta\left(b\sum_i \bvec{x}_i - \bvec{r}\right)}{\int \dd[2]{\bvec{x}_1} \cdots \dd[2]{\bvec{x}_N}},
\end{equation}
where the integral over $\dd[2]{\bvec{x}_i}$ represents the surface integral over the $i$th unit sphere.  While equation~(\ref{FJCendtoenddistexact}) is exact, it is hard to evaluate. Fortunately, our usual trick of going to Fourier space will help us out.
\begin{enumerate}[(a)]
\item We'll use a slightly modified form of the Fourier transform as defined in appendix~\bookref{sec:diffusionsolution}:
\begin{equation}
\tilde{p}(\bvec{q}) = \int e^{i \bvec{q} \cdot \bvec{r}} p(\bvec{r}) \dd[3]{\bvec{r}}
\end{equation}
with inverse
\begin{equation}
p(\bvec{r}) = \frac{1}{(2\pi)^3} \int e^{- i \bvec{q} \cdot \bvec{r}} \tilde{p}(\bvec{q}) \dd[3]{\bvec{q}}.
\end{equation}
Show that you can factorize $\tilde{p}(\bvec{q})$ as
\begin{equation}
\label{FJCendtoenddistexactFT}
\tilde{p}(\bvec{q}) = \left( \frac{\int \dd[2]{\bvec{x}} e^{i \bvec{q} \cdot \bvec{x}}}{\int \dd[2]{\bvec{x}}} \right)^N.
\end{equation}
\item The integral in the denominator of~(\ref{FJCendtoenddistexactFT}) is just the area of the sphere of unit radius, i.e. $4 \pi$. To evaluate the integral in the numerator, define the $x$ axis to lie along $q_x$, and define $\theta$ as the angle between $\bvec{q}$ and $\bvec{x}$, such that $\bvec{q} \cdot \bvec{x} = q \cos(\theta)$ (note that we already normalized $x$ by taking the factor $b$ out). The area element we're integrating over is a piece of solid angle, i.e. $\dd[2]{\bvec{x}} = \dd\Omega = \sin(\theta) \dd{\theta} \dd{\phi}$, with $\theta$ running from $0$ to $\pi$ and $\phi$ from $0$ to $2\pi$. Using these substitutions, show that $\tilde{p}(\bvec{q})$ is given by
\begin{equation}
\label{FJCendtoenddistexactFTevaluated}
\tilde{p}(\bvec{q}) = \left[ \frac{1}{qb} \sin(qb) \right]^N.
\end{equation}
\item To calculate the inverse Fourier transform, we use the same approach: let the $q_x$ axis lie along $\bvec{r}$, and $\theta$ be the angle between $\bvec{q}$ and $\bvec{r}$, then write the integral in spherical coordinates. The integral over the azimuthal angle~$\phi$ again becomes trivial, and the integral over the polar angle~$\theta$ can be evaluated easily. Do so to get $p(r)$ as
\begin{equation}
p(\bvec{r}) = \frac{1}{2\pi^2} \int_0^\infty \frac{q}{r} \sin(qr) \left[\frac{\sin(qb)}{qb}\right]^N \dd{q}.
\end{equation}
\item Introducing the total length $L = Nb$, change variables to $\bar{q} = qb$ and $\bar{r} = r/L$, to get an expression in which all terms in the integral are dimensionless (but depend on $N$), and only the prefactor has a dimension $1/b^3$. (NB: your result should thus not explicitly contain $L$).
\item As expected, our probability distribution only depends on the end-to-end distance $r = |\bvec{r}|$, not on the direction of $\bvec{r}$. Therefore, we can look at the probability distribution of $p(r)$ alone, by integrating out the angular coordinates, which again gives us the surface area of a sphere, $4 \pi r^2$. Normalize this term in the same way as in (d). NB: there's a subtle point here: if we only replace the $r^2$ by $\bar{r}^2 N^2 b^2$, the resulting function will not be a proper probability distribution, as it will not be normalized. To see how we can compensate, note that $4 \pi r^2 p(r)$ is normalized (by design), i.e., the integral over $r$ from $0$ to $\infty$ over this function should equal $1$. The same should hold when we integrate over $\bar{r}$. Make the substitution in the integral to find the correct expression for $4 \pi r^2 p(r)$ in terms of the nondimensional variables $\bar{q}$ and $\bar{r}$.
\item Now also re-write the Gaussian probability distribution (equation~\bookref{FJCdist}) in the same way, i.e., averaging over the angles and introducing the dimensionless variable $\bar{r}$.
\item Plot the exact and the Gaussian probability distribution in the same figure for $N=5$ and $N=10$ (you'll need a numerical integrator to evaluate the exact distribution).
\end{enumerate}

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item We have
\begin{align*}
\tilde{p}(\bvec{q}) &= \int e^{i \bvec{q} \cdot \bvec{r}} p(\bvec{r}) \dd[3]{\bvec{r}} = \frac{\int \dd[2]{\bvec{x}_1} \cdots \dd[2]{\bvec{x}_N} \exp\left(i b \bvec{q} \cdot \sum_i \bvec{x}_i \right)}{\int \dd[2]{\bvec{x}_1} \cdots \dd[2]{\bvec{x}_N}} \\
&= \Pi_i \frac{\int \dd[2]{\bvec{x}_i} e^{i \bvec{q} \cdot \bvec{x}_i}}{\int \dd[2]{\bvec{x}_i}} = 
\left( \frac{\int \dd[2]{\bvec{x}} e^{i \bvec{q} \cdot \bvec{x}}}{\int \dd[2]{\bvec{x}}} \right)^N
\end{align*}
\item For the integral in the numerator, we have
\begin{align*}
\int \dd[2]{\bvec{x}} e^{i \bvec{q} \cdot \bvec{x}} &= \int_0^\pi \sin(\theta) \dd{\theta} \int_0^{2\pi} \dd{\phi} e^{i q b \cos(\theta)} = -2 \pi \int_{\theta = 0}^{\theta = \pi} \dd{\cos(\theta)} e^{i q b \cos(\theta)} \\
&= \frac{2 \pi}{i q b} \left[e^{i q b z} \right]_{z=-1}^{z=1} = \frac{4 \pi}{qb} \sin(qb),
\end{align*}
where we set $z=\cos(\theta)$ and flipped the bounds to account for the minus sign. Substituting this expression and $4\pi$ for the denominator gives equation~(\ref{FJCendtoenddistexactFTevaluated}).
\item We have
\begin{align*}
p(r) &= \frac{1}{(2\pi)^3} \int e^{- i \bvec{q} \cdot \bvec{r}} \tilde{p}(\bvec{q}) \dd[3]{\bvec{q}} = \int_0^\infty q^2 \frac{\dd{q}}{2\pi} \int_0^\pi \sin(\theta) \frac{\dd{\theta}}{2\pi} \int_0^{2\pi} \frac{\dd{\phi}}{2\pi} e^{-i q r \cos(\theta)} \left[ \frac{1}{qb} \sin(qb) \right]^N \\
&= \int_0^\infty q^2 \frac{\dd{q}}{2\pi} \frac{1}{-i q r} \left[e^{-i q r z}\right]_{z=-1}^{z=1} \left[ \frac{1}{qb} \sin(qb) \right]^N = \frac{1}{2\pi^2} \int_0^\infty \frac{q}{r} \sin(qr) \left[\frac{\sin(qb)}{qb}\right]^N \dd{q}.
\end{align*}
\item To write all $q$'s in terms of $\bar{q} = qb$, we need to multiply with $b^2$. To write the radial position as a fraction of $L$, we need to multiply by $L$, so we pick up an $L$ in the denominator, which we eliminate again by $L=Nb$:
\begin{align*}
p(r) &= \frac{1}{2\pi^2} \frac{1}{b^2 L} \int_0^\infty \frac{qb L}{r} \sin(\frac{qbrL}{bL}) \left[\frac{\sin(qb)}{qb}\right]^N \dd{(qb)} \\
&= \frac{1}{2 \pi^2 N b^3} \int_0^\infty \frac{\bar{q}}{\bar{r}} \sin(N\bar{q}\bar{r}) \left[\frac{\sin(\bar{q})}{\bar{q}}\right]^N \dd{\bar{q}}
\end{align*}
\item We have
\begin{align*}
1 &= \int_0^\infty 4 \pi r^2 p(r) \dd{r} = \int_0^\infty 4 \pi \frac{r^2}{L^2} L^2 p(\bar{r}) L \dd{\frac{r}{L}} = 4 \pi N^3 b^3 \int_0^\infty \bar{r}^2 p(\bar{r}) \dd{\bar{r}},
\end{align*}
so our radial probability function becomes
\begin{align}
\label{FJCexactradialprobdist}
4 \pi r^2 p(r) &= \frac{2 N^2 \bar{r}}{\pi} \int_0^\infty \bar{q} \sin(N\bar{q}\bar{r}) \left[\frac{\sin(\bar{q})}{\bar{q}}\right]^N \dd{\bar{q}}.
\end{align}
\item Going through the same procedure with the Gaussian distribution is straightforward and gives
\begin{align}
\label{FJCGaussianradialprobdist}
4 \pi r^2 p_\mathrm{G}(r) &= 4 \pi N^3 \bar{r}^2 \left(\frac{3}{2\pi N}\right)^{3/2} \exp\left(-\frac32 N \bar{r}^2\right).
\end{align}
\item See figure~\ref{fig:FJCexactGaussian}
\end{enumerate}
\begin{figure}
\centering
\includegraphics[scale=1]{polymers/problems/FJC-exactvsGaussianradialprobability.pdf}
\caption{Comparison of the exact~(\ref{FJCexactradialprobdist}, blue) and Gaussian~(\ref{FJCGaussianradialprobdist}, orange) expressions for the probability distribution of the end-to-end distance of a freely jointed chain, for $N=5$ (solid lines) and $N=10$ (dashed and dashed-dotted lines).}
\label{fig:FJCexactGaussian}
\end{figure}
\fi
 