% Problemtitle Entropy and energy in the force-extension relation of a semiflexible polymer
% Source: "Polymer-based models of cytoskeletal networks" (F. C. MacKintosh), Ch. 8 of "Cytoskeletal mechanics", eds. Mofrad and Kamm
In this problem, we'll derive an alternative expression for the force-extension curve of a semiflexible (worm-like chain) polymer, which shows that entropic contributions matter for this force-extension even at sizes small compared to the persistence length. We consider a segment of the polymer of length~$\ell$ that is significantly shorter than the persistence length~$\xi_\mathrm{p}$. The segment will then be mostly straight, which allows us to define a $z$ axis as the line from the initial to the final point of the segment. Deviations from the axis are then in the $xy$ plane; we'll denote the deviation in the $x$ direction by $u(z, t)$ and the ones in the $y$ direction by $v(z, t)$. To start simple, we first consider again the 2D problem in which the $y$ coordinate is absent; adding it back in will only change a factor $2$ later.
\begin{enumerate}[(a)]
\item Assuming that the total contour length of the polymer is fixed, show that the deformation leads to a contraction of the chain along the $z$ axis of magnitude
\begin{equation}
\label{shortWLCcontraction}
\Delta \ell \approx \frac12 \int \dd{z} \left| \pdv{u}{z} \right|^2, 
\end{equation}
where we've ignored higher-order terms.
\ifproblemset\setcounter{problemcounter}{\value{enumi}}\else\setcounter{problemcounter}{\value{enumii}}\fi
\end{enumerate}
Given the function $u(z)$, we can write the bending energy of the segment as
\begin{equation}
\label{shortWLCbendingenergy}
\mathcal{E}_\mathrm{bend} = \frac{K}{2} \int \dd{z} \left(\pdv[2]{u}{z}\right)^2.
\end{equation}
We now invoke our usual trick, expanding $u(z, t)$ in Fourier modes via
\begin{equation}
u(z, t) = \sum_q u_q \sin(q z),
\end{equation}
where the sum is over wavenumbers $q = n \pi / \ell$, with $n = 1, 2, 3, \ldots$.
\begin{enumerate}[(a)]
\ifproblemset\setcounter{enumi}{\value{problemcounter}}\else\setcounter{enumii}{\value{problemcounter}}\fi
\item Use the Fourier expansion of $u(z, t)$ to express the bending energy as a sum over Fourier modes.
%\item The integrals in the bending energy~(\ref{shortWLCbendingenergy}) and contraction~(\ref{shortWLCcontraction}) run over the projected length, not the contour length of the polymer segment. Show that to quadratic order in the transverse displacement $u$, this projected length equals the contour length~$\ell$.
%\item Use the Fourier expansion of $u(z, t)$ to express the contraction~$\Delta \ell$ as a sum over Fourier modes.
\ifproblemset\setcounter{problemcounter}{\value{enumi}}\else\setcounter{problemcounter}{\value{enumii}}\fi
\end{enumerate}
The intensive variable conjugate to the contraction is the tension~$f$ in the chain. The work done by this tension is then $- f \Delta \ell$ (note that the work is negative because the chain has contracted). Either by invoking the principle of virtual work or through the addition of a corresponding potential energy, we then get an effective energy for our chain
\begin{equation}
\label{shortWLCeffectiveenergy}
\mathcal{E} = \frac12 \int \dd{z} \left[ K \left(\pdv[2]{u}{z}\right)^2 + f \left(\pdv{u}{z}\right)^2 \right].
\end{equation}
\begin{enumerate}[(a)]
\ifproblemset\setcounter{enumi}{\value{problemcounter}}\else\setcounter{enumii}{\value{problemcounter}}\fi
\item Re-write the effective energy~(\ref{shortWLCeffectiveenergy}) as a sum over Fourier modes.
\item Your answer at (c) should contain only terms that are quadratic in $u_q$. By the equipartition theorem, each quadratic mode contributes an energy $\frac12 \kB T$ to the thermal average of the energy. Given that result, find $\ev{\left|u_q\right|^2}$.
\item Substituting your answer to (d) in the series expansion of the contraction~$\Delta \ell$, show that
\begin{equation}
\label{shortWLCcontractionev}
\ev{\Delta \ell} = \frac12 \kB T \sum_q \frac{1}{K q^2 + f}.
\end{equation}
\ifproblemset\setcounter{problemcounter}{\value{enumi}}\else\setcounter{problemcounter}{\value{enumii}}\fi
\end{enumerate}
In three dimensions, we have two orthogonal contributions to the contraction, giving exactly twice the value of $\ev{\Delta \ell}$ as in equation~(\ref{shortWLCcontractionev}). At zero force, the sum can be evaluated exactly (it's one of those weird ways of writing $\pi$) to give
\begin{equation}
\ev{\Delta \ell}_{f=0} = \frac{\kB T}{K} \sum_q \frac{1}{q^2} = \frac{\kB T}{K} \frac{\ell^2}{\pi^2} \sum_{n=1}^\infty \frac{1}{n^2} = \frac{\kB T}{K} \frac{\ell^2}{\pi^2} \frac{\pi^2}{6} = \frac{\ell^2}{6 \xi_\mathrm{p}},
\end{equation}
where we used $\xi_\mathrm{p} = K / \kB T$. For finite tension however, the chain gets extended by an amount
\begin{equation}
\label{shortWLCforceextension}
\delta \ell = \ev{\Delta \ell}_{f=0} - \ev{\Delta \ell} = \frac{\kB T \ell^2}{K \pi^2} \sum_{n=1}^\infty \frac{\phi}{n^2(n^2 + \phi)},
\end{equation}
where $\phi = f / f_\mathrm{c} = f \ell^2 / (K \pi^2)$ is a non-dimensionalized version of the force. The critical force~$f_\mathrm{c}$ by which we scale the tension is nothing but the buckling force for an elastic beam (see equation~(\bookref{bucklingforce})). Equation~(\ref{shortWLCforceextension}) gives the force-extension relation of a short section of semiflexible polymer.
\begin{enumerate}[(a)]
\ifproblemset\setcounter{enumi}{\value{problemcounter}}\else\setcounter{enumii}{\value{problemcounter}}\fi
\item Linearize equation~(\ref{shortWLCforceextension}) to find the effective spring constant of our polymer. You'll need to remember or look up another infinite sum.
%\item For high force, the force-extension relation should diverge as $\delta \ell / \Delta \ell$ approaches~$1$. Show that in this regime, we find $f \sim \left| \delta \ell - \Delta \ell \right|^{-2}$.
\item \ifproblemset(Bonus)\fi Plot the force-extension curve (i.e., $\phi$ as a function of $\delta \ell / \Delta \ell$) on a log-log scale.
\end{enumerate}

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item We have 
\begin{align*}
\dd{s} &= \sqrt{\dd{z}^2 + \dd{u}^2} = \dd{z} \sqrt{1 + \left| \pdv{u}{z} \right|^2}
\end{align*}
and therefore
\begin{align*}
\Delta \ell &= \ell - \bar{\ell} = \int \dd{s} - \int \dd{z} = \int \dd{z} \left[ \sqrt{1 + \left| \pdv{u}{z} \right|^2} - 1 \right] \approx \frac12 \int \dd{z} \left| \pdv{u}{z} \right|^2.
\end{align*}
\item We simply substitute:
\begin{align*}
\mathcal{E}_\mathrm{bend} &= \frac{K}{2} \int \dd{z}  \left( \sum_q u_q q^2 \sin(q z) \right)^2 \\
&= \sum_q \sum_{q'} \frac{K}{2} \int \dd{z}  u_q u_{q'} q^2 (q')^2 \sin(q z) \sin(q' z) \\
&= \sum_q \sum_{q'} \frac{K}{2} u_q u_{q'} q^2 (q')^2 \frac{\bar{\ell}}{2} \delta_{q q'} \\
&= \frac{K \bar{\ell}}{4} \sum_q u_q^2 q^4,
\end{align*}
where $\bar{\ell} = \ell - \Delta \ell$ is the projected length of the polymer.
\item To get the series expansion of $\Delta \ell$ we again substitute:
\begin{align*}
\Delta \ell &= \frac12 \int \dd{z} \left| \sum_q u_q q \cos(q z) \right|^2 = \frac12 \sum_q \sum_{q'} u_q u_{q'} q q' \int \dd{z} \cos(q z) \cos(q' z) \\
&= \frac12 \sum_q \sum_{q'} u_q u_{q'} q q' \frac{\bar{\ell}}{2} \delta_{qq'} = \frac{\bar{\ell}}{4} \sum_q u_q^2 q^2.
\end{align*}
Combining with the answer of (b), we get for the energy
\begin{align*}
\mathcal{E} = \frac{\bar{\ell}}{4} \sum_q \left(K q^4 + f q^2\right) u_q^2.
\end{align*}
\item If each mode contributes $\frac12\kB T$ in thermal energy, we have
\begin{align*}
\frac12 \kB T &= \frac{\bar{\ell}}{4} \left(K q^4 + f q^2\right) \ev{\left|u_q\right|^2} \\
\ev{\left|u_q\right|^2} &= \frac{2 \kB T}{\bar{\ell} \left(K q^4 + f q^2\right)}.
\end{align*}
\item We have
\begin{align*}
\ev{\Delta \ell} &= \frac{\bar{\ell}}{4} \sum_q q^2 \ev{\left|u_q\right|^2} = \frac12 \kB T \sum_q \frac{1}{K q^2 + f}.
\end{align*}
\item Linearizing the term in the sum of equation~(\ref{shortWLCforceextension}) is easy:
\begin{align*}
\frac{\phi}{n^2 (n^2 + \phi)} &= \frac{\phi}{n^2} \frac{1}{n^2+\phi} = \frac{\phi}{n^2} \left[\frac{1}{n^2} - \frac{\phi}{n^4} + \order{\phi^2} \right] = \frac{\phi}{n^4} + \order{\phi^2}.
\end{align*}
So
\begin{align*}
\delta \ell \approx \frac{\kB T \ell^2}{K \pi^2} \sum_{n=1}^\infty \frac{\phi}{n^4} = \frac{\kB T \ell^2}{K \pi^2} \frac{\pi^4}{90} \phi = \frac{\kB T \ell^2}{K} \frac{\pi^2}{90} \frac{f \ell^2}{K \pi^2} = \frac{\ell^4}{90 K \xi_\mathrm{p}} f,
\end{align*}
and we can read off that our spring constant is $90 K \xi_\mathrm{p} / \ell^4$.
%\item In the limit of large force, the argument in the sum of equation~(\ref{shortWLCforceextension}) becomes
%\begin{align*}
%\frac{\phi}{n^2 (n^2 + \phi)} &= \frac{1}{n^2 \left(\frac{n^2}{\phi} + 1\right)} \to 
%\end{align*}
\item See figure~\ref{fig:WLCforceextensionentropy}.
\end{enumerate}
\begin{figure}
\centering
\includegraphics[scale=1]{polymers/problems/WLCforceextensionentropy.pdf}
\caption{Force-extension curve of a short segment of a semiflexible polymer; scaled force~$\phi$ as a function of extension~$\delta \ell / \Delta \ell$.}
\label{fig:WLCforceextensionentropy}
\end{figure}
\fi