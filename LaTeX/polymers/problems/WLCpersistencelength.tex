% Problemtitle Persistence length
% Source: Phillips 10.1
In order to compute the tangent-tangent correlation function for a semiflexible polymer such as DNA we must confront the mathematical question of how to sum over all states in this situation. What makes this a hard problem is that `all states' for a polymer means `all curves'. So, how do we sum over all curves? To answer this question we begin by simplifying the mathematics and assume that the polymer is confined to two-dimensions. This would be the situation that describes, say, DNA absorbed on a cationic lipid bilayer. In this case the curves representing polymer configurations all live in the plane, and therefore their tangent vectors can be parameterized by the angles $\theta(s)$ between $\unitvec{t}$ and the x-axis, $\unitvec{t} = (\cos \theta(s), \sin \theta(s))$.
\begin{enumerate}[(a)]
	\item Rewrite the equation for the bending energy~(\bookref{WLCbendingenergy}) in terms of the polar angle~$\theta(s)$.
\ifproblemset\setcounter{problemcounter}{\value{enumi}}\else\setcounter{problemcounter}{\value{enumii}}\fi
\end{enumerate}
The next issue we have to deal with is the question of boundary conditions for the polymer curves. If we fluorescently label the polymer chain, we can directly measure the angles $\theta(s)$. We can then choose to set the $x$-axis along the direction $\unitvec{t}(0)$ which amounts to fixing $\theta(0) = 0$ for every polymer curve. This gives us our first boundary condition. A second boundary condition, $\dd\theta/\dds = 0$ at $s = L$, states that the curvature of the polymer chain at its end is zero, which means that there is no force (no torque, more precisely) exerted on the chain end.
\begin{enumerate}[(a)]
\ifproblemset\setcounter{enumi}{\value{problemcounter}}\else\setcounter{enumii}{\value{problemcounter}}\fi
\item Expand the polar angle into a Fourier series\footnote{You may wonder if we shouldn't have $2\pi n s / L$ in the argument of the sines and cosines. The reason why we have $\pi/2L$ is that our curve is not periodic by itself - and the lowest order mode we can fit if we hold one end is a quarter of the wavelength, just like you can fit a quarter wavelength on a clamped string.}
\begin{equation}
\theta(s) = \frac{a_0}{2} + \sum_{n=1}^\infty \left[ a_n \cos\left( \frac{\pi n s}{2L} \right) + b_n \sin\left( \frac{\pi n s}{2L} \right) \right].
\end{equation}
Using the boundary conditions, determine which of the coefficients $a_n$ and $b_n$ are equal to zero.
\item Rewrite the bending energy in terms of the nonzero Fourier amplitudes $a_n$ and $b_n$, introduced in (b), and show that it takes on a form equivalent to that of many independent harmonic oscillators. Hint: for $m$ and $n$ both even or both odd, we have
\[ \int_0^L \cos\left(\frac{\pi m s}{2L}\right) \cos\left(\frac{\pi n s}{2L}\right) \dds = \int_0^L \sin\left(\frac{\pi m s}{2L}\right) \sin\left(\frac{\pi  n s}{2L}\right) \dds = \frac{L}{2} \delta_{mn}.  \]
\item Use the equipartition theorem to compute the thermal average of (the square of) each of the Fourier amplitudes.
	\item Make use of the identity $\ev{\sin X} = \ev{\cos X} =\exp\left(-\ev{X^2}/2\right)$, which holds for a Gaussian distributed random variable X, to obtain the equation for the tangent-tangent correlation function
\[ \ev{\unitvec{t}(s) \cdot \unitvec{t}(0)} = \exp\left(-\frac12\ev{\theta(s)^2}\right). \]
\item Compute $\ev{\theta(s)^2}$ by using the Fourier series representation of $\theta(s)$ and the average values of the squares of the Fourier amplitudes (the $a_n$ and $b_n$) obtained in (d). Convince yourself either by plotting or Fourier analysis that on the interval $0 < s < L$, we have $\ev{\theta(s)^2} = s / \xi_p$, where $\xi_p$ is the persistence length.
	\item How does the persistence length in two dimensions compare with the value obtained in three dimensions? Explain why the tangent-tangent correlation function decays slower in two dimensions. What is the situation in one dimension?
\end{enumerate}
