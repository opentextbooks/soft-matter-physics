\section{Curviliniar coordinates in space}
\label{sec:curviliniarcoordinates}
In the previous two sections, we discussed the description of one- and two-dimensional objects embedded in three-dimensional space. The coordinate systems of our manifolds are \emph{curvilinear}, in the sense the the lines following the direction of a coordinate may be curved when viewed from a Cartesian reference frame. There are likewise many examples in which it is useful to describe all of space in curvilinear coordinates, the best-known being polar, cylindrical and spherical coordinates. A physical example in which three-dimensional curvilinear coordinates are very useful is in the description of the flow of (complex) fluids, where the description is taken from the point of view of a particle moving with the fluid\footnote{This moving-along point of view is known as the Lagrangian picture; the point of view from a stationary observer is known as the Eulerian picture, and usually results in significantly more complicated equations describing the fluid motion.}.

Much of the mathematics of three-dimensional curvilinear coordinates is similar to the maths of surfaces embedded in a three-dimensional space, with the necessary extensions to three dimensions. I will give the definitions here separately, so this section can be read on its own, rather than you having to refer to section~\ref{sec:diffgeom_surfaces} all the time.

\subsection{Covariant and contravariant bases for curvilinear coordinates}
\label{sec:curvilinearbases}
Our starting point is again the Cartesian coordinate system with a basis $\{\unitvec{f}_i\}$ (where I've picked $\unitvec{f}$ so we can use $\unitvec{e}$ for our curvilinear coordinates). A vector $\bvec{x}$ can be expanded in the Cartesian basis in the usual way as $\bvec{x} = x^i \unitvec{f}_i$, where the $x^i$ are scalars. To define any other coordinate system, we need a one-to-one map from $\mathbb{R}^3$ onto itself\footnote{Technically, the map should be `one-to-one and onto' meaning that each point in the original Cartesian space is mapped to exactly one point in the target space and that for each point in the target space there is exactly one point in the original space that is mapped onto it. Since our map is from the space onto itself, the second condition is automatically met if the first is satisfied.}, which we'll denote by $\bvec{s}$. The map $\bvec{s}$ maps any vector $\bvec{x}$ onto a new vector $\bvec{u} = \bvec{s}(\bvec{x})$. Because our map is one-to-one, we can also invert it, and express $\bvec{x}$ in terms of the curvilinear variables~$\bvec{u}$: $\bvec{x} = \bvec{r}(\bvec{u})$, where $\bvec{r}$ is defined by the condition that $\bvec{x} = \bvec{r}(\bvec{s}(\bvec{x}))$. The map $\bvec{r}$ is the same as the embedding we had for our surface in $\mathbb{R}^3$ in section~\ref{sec:diffgeom_surfaces}.

There are two ways to construct a basis for the curvilinear space in which the mapped vectors~$\bvec{u}$ live. One is to look at how the map $\bvec{r}$ from that space to the Cartesians changes with changes in the components of the vector~$\bvec{u}$, that is to say, we take as the basis vectors the derivatives of $\bvec{r}$:
\begin{equation}
\label{contravariantbasis}
\bvec{e}_i \equiv \frac{\partial \bvec{r}}{\partial u^i}.
\end{equation}
The basis vectors given by equation~(\ref{contravariantbasis}) are known as the \emph{contravariant} basis vectors. To indicate that the basis vectors are contravariant, we place their indexes low. The contravariant basis vectors are the tangent vectors to the coordinate curves (curves on which one of the coordinates changes). In general, these basis vectors are not necessarily orthogonal. We could of course always construct an orthonormal basis from these through the Gram-Schmidt process and normalize, though in practice we rarely will.

The second way to define a basis for our curvilinear space is to use normals to coordinate surfaces instead of tangents to coordinate curves (this works in any dimension larger than one if we take hypersurfaces, which have a dimension one lower than the space they live in; in two dimensions for example, hypersurfaces are simply lines). To construct the basis, we use the map $\bvec{s}$ from the Cartesian to the curvilinear coordinates. For each of the components of $\bvec{u} = \bvec{s}(\bvec{x})$, we can calculate the gradient, which is indeed normal to the corresponding level surface (i.e., the surface on which the component is constant), so we arrive at another set of basis vectors:
\begin{equation}
\label{covariantbasis}
\bvec{e}^i \equiv \bvec{\nabla} u^i.
\end{equation}
Equation~(\ref{covariantbasis}) defines the \emph{covariant} basis whose vectors are indicated by upper indexes. The covariant basis is often referred to as the dual of the contravariant one. If the basis vectors are orthogonal, normals to the coordinate surfaces coincide with tangent vectors to coordinate curves, and the two basis sets are identical up to lengths (which can be corrected by normalization), so the choice for contravariant or covariant basis vectors is irrelevant. In general however, the choice matters, and the components of vectors expressed in the two bases are different.\vspace{0.1cm}\\

\noindent \textbf{Example} The (curvilinear) spherical coordinates $(r, \theta, \phi)$ are related to the Cartesian coordinates $(x, y, z)$ through
\[ \mathbf{r}(r, \theta, \phi) = r \sin\theta \cos \phi \mathbf{f}_x + r \sin\theta \sin \phi \mathbf{f}_y + r \cos\theta \mathbf{f}_z \]
and inversely the spherical coordinates are given in terms of the Cartesians by
\[ r = \sqrt{x^2 + y^2 + z^2}, \quad \theta = \arccos(z/\sqrt{x^2+y^2+z^2}), \quad \phi = \arctan(y/x) \]
The contravariant basis vectors are the derivatives of $\bvec{r}$:
\[ \bvec{e}_r = \threevector{\sin\theta \cos\phi}{\sin\theta \sin\phi}{\cos\theta}, \quad \bvec{e}_\theta = r \threevector{\cos\theta \cos\phi}{\cos\theta \sin\phi}{-\sin\theta}, \quad \bvec{e}_\phi = r \threevector{-\sin\theta \sin\phi}{\sin\theta \cos\phi}{0}.  \]
The covariant basis vectors are the gradients of the expressions for the spherical coordinates:
\[ \bvec{e}^r = \frac{1}{r} \threevector{x}{y}{z}, \quad \bvec{e}^\theta = \frac{1}{r^2\sqrt{x^2+y^2}} \threevector{xz}{yz}{-(x^2+y^2)}, \quad \bvec{e}^\phi = \frac{1}{x^2+y^2} \threevector{-y}{x}{0}, \]
%\[ \bvec{e}^r = \frac{1}{\sqrt{x^2+y^2+z^2}} \threevector{x}{y}{z}, \quad \bvec{e}^\theta = \frac{1}{\sqrt{x^2+y^2}(x^2+y^2+z^2)} \threevector{xz}{yz}{-(x^2+y^2)}, \quad \bvec{e}^\phi = \frac{1}{x^2+y^2} \threevector{-y}{x}{0}. \]
where we already substituted $r = \sqrt{x^2 + y^2 + z^2}$ for brevity. Substituting the expressions for $x$, $y$, and $z$ in terms of the spherical coordinates, we find that up to scalar factors the two sets of basis vectors are identical, consistent with the fact that they form an orthogonal basis.

Even if the contravariant and covariant bases don't coincide, they have the very useful property that the projection (i.e., the dot product) of a basis vector of one set on one of the other vanishes for unequal indexes, and is unity for equal indexes. This fact follows from the way we constructed the basis vectors (as is easy to see for the two-dimensional case), but can also be easily proven by calculating the dot product explicitly:
\begin{equation}
\label{cocontrabasisdotproduct}
\bvec{e}^i \cdot \bvec{e}_j = \bvec{\nabla} u^i \cdot \frac{\partial \bvec{r}}{\partial u^j} = \frac{\partial u^i}{\partial r^k} \frac{\partial r^k}{\partial u^j} = \frac{\partial u^i}{\partial u^j} = \delta_j^i,
\end{equation}
where we used $r^k$ for the components of $\bvec{r}$ (so $\bvec{r} = r^k \unitvec{f}_k$) and the chain rule. 

\subsection{Vectors in curvilinear coordinates}
When the contravariant and covariant bases are identical (or identical up to scalar factors), it doesn't matter which of the two bases you use to represent your vectors (remember, vectors are abstract objects, with a magnitude and a direction; the list of coordinates we use to perform calculations on them is a representation of the vector in a given basis). Therefore we usually just speak of `the' basis and `the' components of the vector. In general however, we have to distinguish between the contravariant and covariant components, as they need not be the same. Given a vector $\bvec{v}$ in a curvilinear coordinate system, we can express it in either basis by finding the relevant coefficients:
\begin{equation}
\label{vectorrepresentation}
\bvec{v} = v^i \bvec{e}_i = v_j \bvec{e}^j.
\end{equation}
We call the $v^i$ the contravariant components as they go with the contravariant basis, and likewise the $v_j$ are the covariant components. Note the notation: we use an upper index for the contravariant component, a lower one for the covariant component, and apply the summation convention over any repeated index that appears once as an upper and once as a lower one. Given the vector itself, calculating its components is easy by projecting on the relevant basis vector; for example:
\[ \bvec{v} \cdot \bvec{e}^j = v^i \bvec{e}_i \cdot \bvec{e}^j = v^i \delta_i^j = v^j. \]
Likewise, $v_j = \bvec{v} \cdot \bvec{e}_j$. We can also use both representations to calculate the dot product of two vectors $\bvec{v}$ and $\bvec{w}$:
\begin{equation}
\label{dotproductcontracomp}
\bvec{v} \cdot \bvec{w} = v^i \bvec{e}_i \cdot w^j \bvec{e}_j = v^i w^j g_{ij},
\end{equation}
where
\begin{equation}
\label{metriccovariant}
g_{ij} = \bvec{e}_i \cdot \bvec{e}_j
\end{equation}
are the covariant components of the metric tensor. Likewise:
\begin{equation}
\label{dotproductcocomp}
\bvec{v} \cdot \bvec{w} = v_i \bvec{e}^i \cdot w_j \bvec{e}^j = v_i w_j g^{ij},
\end{equation}
with
\begin{equation}
\label{metriccontravariant}
g^{ij} = \bvec{e}^i \cdot \bvec{e}^j
\end{equation}
for the contravariant components of the metric tensor. Like any tensor, the metric is in fact a map, which in this case is easy to identify: the map of two vectors onto their inner product. $g_{ij}$ and $g^{ij}$ are simply representations of this map (in the covariant and contravariant basis, respectively) that, when written as a matrix, can be used to do the calculations in the ordinary fashion of linear algebra.

Like most 2-tensors, the metric also has an interpretation as a map from vectors to vectors, namely mapping the covariant components onto the contravariant ones (or vice-versa), often referred to as the `raising' or the `lowering' of indexes. To see how this works, we exploit the fact that we can also write the inner product by taking a mixed approach, so we have:
\begin{equation}
\label{dotproductmixedcomp}
\bvec{v} \cdot \bvec{w} = v^i \bvec{e}_i \cdot w_j \bvec{e}^j = v^i w_j \delta_j^i = v^i w_i,
\end{equation}
so we can write the inner product in four ways:
\begin{equation}
\label{dotproductallrep}
\bvec{v} \cdot \bvec{w} = g_{ij} v^i w^j = g^{ij} v_i w_j = v^i w_i = v_i w^i.
\end{equation}
Since equation~(\ref{dotproductallrep}) holds for all vectors~$\bvec{w}$, we find that we have
\begin{equation}
\label{metricraiselower}
v^i = g^{ij} v_j \quad \mbox{and} \quad v_i = g_{ij} v^j
\end{equation}
for any vector~$\bvec{v}$. Moreover, we have $v^i = g^{ij} v_j = g^{ij} g_{jk} v^k$, again for any vector~$\bvec{v}$, so the two representations of the metric must be each others inverse:
\begin{equation}
\label{inversemetric}
g^{ij} g_{jk} = \delta_k^i.
\end{equation}
Finally, we note that the metric, by construction, is symmetric, so $g_{ij} = g_{ji}$ and $g^{ij} = g^{ji}$.
