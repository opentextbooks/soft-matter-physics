% Problemtitle Driven flow of a viscoelastic fluid between two parallel plates
% Source Inspired on Doi 9.3
A pressure gradient~$\Delta p / L$ is applied on a non-Newtonian fluid held between two parallel plates, as shown in figure~\ref{fig:drivenflowplates}. %By symmetry, the resulting flow field in the positive $x$ direction depends only on the $z$ coordinate, $\bvec{v} = v_x(z) \unitvec{e}_x$.
\begin{figure}[ht]
\centering
\includegraphics[scale=0.75]{complexfluids/problems/drivenflowplates.pdf}
\caption[The flow field generated by a pressure gradient applied to a fluid held between two parallel plates.]{Setting of problem~\ref{pb:drivenparallelplateflow}: a flow field generated by a pressure gradient applied to a fluid held between two parallel plates.}
\label{fig:drivenflowplates}
\end{figure}
\begin{enumerate}[(a)]
\item From the Navier-Stokes equations \ifproblemset(equation~(\bookref{UCMcomplete2}) in the notes, \else(equation~(\bookref{UCMcomplete2}),\fi which hold for any viscoelastic fluid, independent of the constitutive relation), show that the shear stress $\sigma_{xz}(z)$ is given by
\begin{equation}
\label{drivenparallelplateshearstress}
\sigma_{xz}(z) = \frac{\Delta p}{L} \left( \frac{h}{2} - z \right).
\end{equation}
\textit{Hint}: The flow is in steady-state, so the velocity (and the stress) are independent of time; use symmetry to simplify your equations.
\item If the dynamic viscosity ($\eta(\dot{\gamma}) = \sigma_{xz}/\dot{\gamma}_{xz}$, \ifproblemset see section~\bookref{sec:dynamicviscosity}) \else see equation~(\bookref{viscosityfunction})) \fi is inversely proportional to some power of the shear rate, i.e.,
\begin{equation}
\label{inversepowerlawviscosity}
\eta(\dot{\gamma}) = \eta_0 \left( \frac{\dot{\gamma}_{xz}}{\dot{\gamma}_0} \right)^{-n},
\end{equation}
as would be the case for a shear-thinning fluid, find the velocity profile $v_x(z)$ of the fluid in the channel.
\end{enumerate}

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item %We divide the vertical range in thin slices of thickness $\dd z$.
Because the velocity is only in the $x$ direction, and (by symmetry) can only be a function of~$z$, the left-hand side of the Navier-Stokes equations vanishes. The pressure has a gradient in the $x$ direction, given by $\partial p / \partial x = -\Delta p / L$. The only nontrivial equation then represents force balance in the $x$ direction (consistent with steady-state flow):
\begin{align*}
0 &= - \diff{p}{x} + \diff{\sigma_{xz}}{z}, \\
\diff{\sigma_{xz}}{z} &= - \frac{\Delta p}{L}. \\
\sigma_{xz}(z) &=  - \frac{\Delta p}{L} z + C,
\end{align*}
where $C$ is an integration constant. To fix the constant, we observe that by symmetry, the shear stress must be zero at $z = h/2$. Therefore $C = h \Delta p / 2L$, and the solution is given by equation~(\ref{drivenparallelplateshearstress}).
\item Writing $\sigma_{xz} = \eta(\dot{\gamma}) \dot{\gamma}$, we have, combining equations~(\ref{drivenparallelplateshearstress}) and~(\ref{inversepowerlawviscosity}):
\begin{align*}
\eta_0 \dot{\gamma} \left( \frac{\dot{\gamma}}{\dot{\gamma}_0} \right)^{-n} &= \frac{\Delta p}{L}\left(\frac{h}{2} - z \right),
\end{align*}
where $\dot{\gamma} = \dot{\gamma}_{xz}$. Solving for $\dot{\gamma} = \dd v_x / \dd z$, we get
\begin{align*}
\frac{\dd v_x}{\dd z} &= \dot{\gamma} = \dot{\gamma}_0 \left( \frac{h \Delta p}{2 L \eta_0 \dot{\gamma}_0} \left| 1 - \frac{2z}{h} \right| \right)^{1/(1-n)}.
\end{align*}
We can now integrate to find the velocity; with a no-slip boundary condition ($v_x(0) = 0$), we get
\begin{align*}
v_x(z) &= \frac{1-n}{2-n} \frac{h \dot{\gamma}_0}{2} \left(\frac{h \Delta p}{2 L \eta_0 \dot{\gamma}_0} \right)^{1/(1-n)} \left( 1 - \left|1-\frac{2z}{h} \right|^{(2-n)/(1-n)} \right).
\end{align*}
For $n=0$ (Newtonian fluid), we retrieve the quadratic profile.
\end{enumerate}
\fi