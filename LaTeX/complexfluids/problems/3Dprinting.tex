% Problemtitle 3D printing
% Source Exam 2021-2022, suggested by Bob van Dijk
%3D printing is a wonderful technology that allows us to easily create all kinds of objects. This technique is used by many hobbyist, but also in the engineering of laboratory equipment, robots, and spacecraft. You only have to upload a digital model of the object you want to have printed and the machine will add small layers of molten polymer (or an increasing range of other materials) onto a base to construct your creation.

The `ink' used in 3D printers is a big spool of solid polymer wire. The polymer is fed into a heating element by a gear, where it melts and becomes liquid (see figure~\ref{fig:3Dprinter}a). The molten polymer is then pushed through a nozzle by a pressure difference. When to polymer exits the nozzle it quickly cools down and starts to solidify. Those who are experienced with 3D printing know that the printing is a relatively slow process. One way to massively speed it up is by increasing the speed at which polymer is fed into the heating chamber, effectively increasing the pressure.
\begin{figure}[!ht]
    \centering
    \includegraphics[scale=1]{complexfluids/problems/3Dprinter.pdf}
    \caption[3D printing.]{3D printing. (a) Working of a 3D printer with a filament driver. The filament (1) is mechanically driven through an extruder (2) and a heating element (3) at the nozzle. The printed object (4) is built layer-by-layer on the printing stage (5). Image adapted from~[\bookref{3Dprinterdiagram}]. (b) Coordinates of the nozzle.}
    \label{fig:3Dprinter}
\end{figure}

To find out whether we can speed up the 3D printing process, we will look at the final part of the nozzle, right before the polymer exits. We can approximate this final part as a cylinder with length~$L$ and radius~$R$ over which a pressure difference $\Delta p$ is applied, see figure~\ref{fig:3Dprinter}b. We assume that the nozzle is far away from the heater element, that $L$ is much larger than $R$ and that the flow is fully developed. The polymer will exit the nozzle at $z = L$.
%\begin{figure}[!ht]
%    \centering
%    \includegraphics[scale = 0.3]{complexfluids/problems/pipe.png}
%    \caption{Sketch of the final part of the nozzle.}
%    \label{fig:pipe}
%\end{figure}

Using conservation of momentum, we can relate the shear stress~$\sigma_{rz}$ in the pipe to the applied pressure difference, through
\begin{equation}
\label{pipeshearstressde}
\frac{1}{r}\dv{r} \left( r \sigma_{rz} \right) = \frac{\Delta p}{L}.
\end{equation}
We derived equation~(\ref{pipeshearstressde}) in problem~\bookref{ch:fluiddynamics}.\bookref{pb:Poiseuilleflowstresstensor}; it's equation~(\bookref{Poiseuilleflowviscousstress}). In the derivation we did not specify what the stress tensor is; equation~(\ref{pipeshearstressde}) therefore holds for both Newtonian and non-Newtonian fluids. % See https://www.syvum.com/cgi/online/serve.cgi/eng/fluid/fluid802.html

\begin{enumerate}[(a)]
\item From equation~(\ref{pipeshearstressde}) and using a physical argument, show that the shear stress~$\sigma_{rz}$ in the pipe is given by
\begin{equation}
\label{pipeshearstress}
\sigma_{zr} = \frac{\Delta p}{2 L} \cdot r.
\end{equation}
%\textit{Hint: Keep the shape of the pipe in mind and look for planes of symmetry; remember that the equations sheet is there for you.}
\ifproblemset\setcounter{problemcounter}{\value{enumi}}\else\setcounter{problemcounter}{\value{enumii}}\fi
\end{enumerate}
We can now determine the shear stress on the `fluid packets' that travel near the sides of the pipe. When these packets exit the pipe, there stress suddenly disappears.
\begin{enumerate}[(a)]
\ifproblemset\setcounter{enumi}{\value{problemcounter}}\else\setcounter{enumii}{\value{problemcounter}}\fi
\item We follow a fluid packet that starts at $z = 0$ at $t=0$ and flows very closely to the edge of the pipe and exits the nozzle at $t = t_0$. Determine the (shear) strain response (i.e., $\gamma = \gamma_{rz}$) as a function of time, when we assume that the polymer is a Kelvin-Voigt solid, with an elastic modulus~$G$ and viscosity~$\eta$. \textit{Hint: The sudden drop of stress can be approximated by a step-function times a constant. Remember that the stress of the pipe on the fluid (the applied external stress) is minus the stress in the fluid at the pipe's surface.}
\item Using your found strain response, determine the radial velocity of the fluid packet as a function of $t$ and $z$, when we assume that there is no change in the velocity in the $z$-direction.
\item Explain why an increase in pressure is not beneficial for the quality of the result of the 3D printer. What material parameter needs to change in order to allow higher pressures?
\end{enumerate}

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item We can solve the differential equation~(\ref{pipeshearstressde}) by direct integration, which gives
\begin{equation}
\sigma_{rz} = \frac{\Delta p}{L} \int r \dd r = \frac{\Delta p}{2L} r^2 + C,
\end{equation}
where $C$ is an integration constant. The shear stress should be $0$ in the middle of the pipe ($r=0$), which gives $C=0$ and equation~(\ref{pipeshearstress}) \score{2 pt}.
\item The stress response for a Kelvin-Voigt solid is given by
\begin{equation}
    \frac{\sigma}{\eta} = \dot{\gamma} + \frac{1}{\tau}{\gamma},
    \label{KV}
\end{equation}
where $\tau = \eta/G$ \score{0.5 pt}. Following the fluid packet, we distinguish the time it is in the nozzle ($0 < t < t_0$) and after it left the nozzle ($t > t_0$). Eq.~(\ref{KV}), together with the boundary condition that $\gamma(0) = 0$ will give us the strain response when the fluid packet is in the pipe. As the stress near $r = R$ is constant we can say that
\begin{equation}
\sigma_\mathrm{applied} = -\sigma_{zr}(R) = -\frac{\Delta p R}{2L} \equiv -\sigma_0
\end{equation}
\score{0.5 pt}.

Solving the differential equation~(\ref{KV}), we find:
\begin{equation}
    \gamma(t) = -\frac{\sigma_0}{G} \left(1 - e^{-\frac{t}{\tau}}\right)
\end{equation}
for $0 < t < t_0$ \score{0.5 pt}. For the final part, the stress is again $0$. Solving eq.~(\ref{KV}) with $\sigma = 0$ and the value for $\gamma(t_0)$ as boundary condition, we find:
\begin{equation}
    \gamma(t) = -\frac{\sigma_0}{G}(1 - e^{-\frac{t_0}{\tau}})e^{-\frac{t}{\tau}}
\end{equation}
for $t > t_0$ \score{0.5 pt}.

\item From the equations sheet, we read off $\bvec{\gamma}$ (and thus $\dot{\bvec{\gamma}}$) in cylindrical coordinates. The shear strain $\dot{\gamma}_{rz}$ is given by
\begin{equation}
\label{strainratetensorcylinder}
\dot{\gamma}_{rz} = \frac{\partial v_r}{\partial z} + \frac{\partial v_z}{\partial r}.
\end{equation}
We also have $\frac{\partial v_z}{\partial r} = 0$. As $\dot{\gamma} = \frac{\partial \gamma}{\partial t}$ we can calculate the strain rate and substitute it in equation~(\ref{strainratetensorcylinder}):
\begin{equation}
    \dot{\gamma} = \frac{\partial \gamma}{\partial t} = \frac{1}{\tau}\frac{\sigma_0}{G} \left(1 - e^{-\frac{t_0}{\tau}} \right) e^{-\frac{t}{\tau}} = \frac{\partial v_r}{\partial z}
\end{equation}
\score{1 pt}.

Solving the above differential equation with the boundary condition that $v_r(L) = 0$ (no slip boundary condition), we find
\begin{equation}
    v_r(z,t) = \frac{1}{\tau}\frac{\sigma_0}{G} \left(1 - e^{-\frac{t_0}{\tau}} \right) e^{-\frac{t}{\tau}} (z-L) = \frac{\Delta p R}{2L\eta} \left(1 - e^{-\frac{t_0}{\tau}} \right) e^{-\frac{t}{\tau}} (z-L)
    \label{eq:final}
\end{equation}
where we substituted $\sigma_0$ in the last expression \score{1 pt}.

\item Eq.~(\ref{eq:final}) shows that the radial velocity of fluid packets near the edge of the pipe increases and only after a certain amount of time decreases back to zero. When plotting the displacement, this gives a kind of onion shaped profile. As the nozzle is close to the base where the polymer need to be applied, this radial velocity decreases the precision of the machine. Looking at eq.~(\ref{eq:final}) we see that the magnitude of the radial velocity depends on $\Delta p$, thus increasing this pressure difference will result in an even bigger spread of polymer. Increasing $\tau$ (or equivalently the viscosity of the polymer) will result in a decrease in radius of the polymer spread \score{1 pt}.
\end{enumerate}
\fi