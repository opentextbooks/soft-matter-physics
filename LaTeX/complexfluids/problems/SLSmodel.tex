% Problemtitle The standard linear solid model of viscoelastic materials
% Source Final 2020-2021
In section~\bookref{sec:MaxwellKelvingviscoelasticmaterials}, we discussed the two simplest models for viscoelastic materials, Maxwell fluids and Kelvin-Voigt solids, which we can construct pictorially by putting a dashpot and spring in series or in parallel, respectively. While both models capture an essential component of viscoelastic materials, they also each lack one, as a Maxwell fluid has no creep, and a Kelvin-Voigt solid no stress relaxation. We can however construct a three-component model that has both these features. This model is known as the \emph{standard linear solid} model. It consists of one dashpot, with viscosity~$\eta$, and two springs, with moduli $G_1$ and $G_2$. Given these four elements, we could construct (in principle) four different models, by putting them various series or parallel arrangements.

\begin{enumerate}[(a)]
\item Explain why we don't get qualitatively `new' models by either putting all three elements in series, or all three elements in parallel to each other. 
\item\label{pb:Mslsconstitutiverelation} One of the two remaining options is to put the new spring (with modulus $G_2$) in parallel to a Maxwell model (which consists of the spring with modulus $G_1$ in series with the dashpot). Derive the constitutive relation (the relation between the stress~$\sigma$ and the strain~$\gamma$) for this model.
\item Argue, either from your answer at (\ref{pb:Mslsconstitutiverelation}) or directly from the pictorial representation of the model, why the model with a spring in parallel with a Maxwell fluid indeed exhibits creep. (Quick reminder, as we did not discuss creep in great detail: creep or cold flow is a permanent deformation of a material after prolonged stress; the creep compliance $J(t)$ is the strain response to a step stress, i.e., for $\sigma(t) = \sigma_0 \Theta(t)$, we have $\gamma(t) = \sigma_0 J(t)$).
\item\label{pb:KVslsrelaxationmodulus} The other option is to put the new spring in series with a Kelvin-Voigt solid. The constitutive relation for that material is given by
\begin{equation}
\label{KelvinSLS}
\sigma + \frac{\eta}{G_1+G_2} \dot{\sigma} = \frac{G_1 G_2}{G_1 + G_2} \gamma + \frac{G_1 \eta}{G_1+G_2} \dot{\gamma}.
\end{equation}
Find and plot the relaxation modulus $G(t)$ (the stress response to a step strain) for the constitutive relation in equation~(\ref{KelvinSLS}) for $t>0$ (Including the point $t=0$ complicates the maths, but doesn't qualitatively change the solution for $t>0$. Note that you will get an undetermined integration constant; assume it to be positive). \textit{Hint}: Remember that the solution of a linear ode can be written as the sum of a homogeneous and a particular solution.
\item Argue, either from your answer at (\ref{pb:KVslsrelaxationmodulus}) or directly from the pictorial representation of the model, why the model with a spring in series with a Kelvin-Voigt solid indeed exhibits stress relaxation.
\item Argue, either by comparing your answer at (\ref{pb:Mslsconstitutiverelation}) with equation~(\ref{KelvinSLS}) or by the pictorial representations of the two models, whether you would expect there to be a \emph{qualitative} difference (e.g., a different behavior at short or long timescales, or a different functional form for the relaxation modulus) between the two versions of the standard linear solid model.
\end{enumerate}

\ifincludesolutions
\Solutions
\begin{enumerate}[(a)]
\item If we put all three elements in series, the two springs are in series with each other. Two springs in series have the exact same response as a single spring, with a spring constant~$k$ given by $1/k = 1/k_1 + 1/k_2$. Likewise, two springs in parallel have the exact same response as a single spring, with a spring constant~$k$ given by $k = k_1 + k_2$ \score{2 pt; also give if spring constants or moduli not explicitly given}.
\item We know that for elements in series, the stresses are the same, while the strains add up; for elements in parallel, the strains are the same and the stresses add up. Therefore, the total stress is given by $\sigma = \sigma_1 + \sigma_2$, where $\sigma_1$ is the stress of the Maxwell fluid, and $\sigma_2 = G_2 \gamma$ that of the extra spring, with $\gamma$ the total strain \score{1 pt}. For the Maxwell fluid element itself, we have
\begin{align*}
\gamma &= \gamma_\mathrm{spring} + \gamma_\mathrm{dashpot} \\
\dot{\gamma} &= \dot{\gamma}_\mathrm{spring} + \dot{\gamma}_\mathrm{dashpot} = \frac{\dot{\sigma}_1}{G_1} + \frac{\sigma_1}{\eta} \\
\eta \dot{\gamma} &= \sigma_1 + \tau \dot{\sigma}_1
\end{align*} 
\score{1 pt, also give if just given (from book, or otherwise) instead of derived}, where $\tau = \eta/G_1$. To combine the two equations, we add the expression for $\sigma$ to that of its time derivative:
\begin{align*}
\sigma + \tau \dot{\sigma} &= \sigma_1 + \tau \dot{\sigma}_1 + \sigma_2 + \tau \dot{\sigma}_2\\
&= \eta \dot{\gamma} + G_2 \gamma + \tau G_2 \dot{\gamma}
\end{align*}
\score{2 pt}.
\item \textit{Method 1: From the equation}. If we have a step stress, for $t>0$ we have $\sigma = \sigma_0$ and $\dot{\sigma} = 0$. Therefore, in steady-state (when $\dot{\gamma} = 0$), we get $\gamma = \sigma_0 / G_2 > 0$, and thus we get a finite permanent deformation, i.e., a finite creep \score{2 pt; of course you can also actually solve the differential equation and then show that in the limit $t \to \infty$ we get the same value of $\gamma$}.\\
\textit{Method 2: From the pictorial representation}. If we apply a nonzero stress, initially the springs will be stretched, but over time, part of the stretching will dissipate through the dashpot in series with spring~$G_1$. However, like in the Kelvin-Voigt solid, spring $G_2$ will continue to be under finite stress, and thus under finite deformation. In the limit that $t \to \infty$, all deformations in spring $G_1$ will dissipate, but those in spring $G_2$ will not, so we end up with a finite strain $\gamma = \sigma_0 / G_2 > 0$, and thus a finite creep \score{2 pt; also give if value of finite strain not explicitly given}.
\item The step strain is given by $\gamma(t) = \gamma_0 \Theta(t)$, $\dot{\gamma}(t) = \gamma_0\delta(t)$. Substituting in equation~(\ref{KelvinSLS}), we find a first-order differential equation for $\sigma$:
\begin{equation}
\label{stressode}
\sigma + \frac{\eta}{G_1+G_2} \dv{\sigma}{t} = \frac{G_1 G_2}{G_1 + G_2} \gamma_0\Theta(t) + \frac{G_1 \eta}{G_1+G_2} \gamma_0\delta(t),
\end{equation}
so for $t>0$ we have
\begin{align*}
\sigma + \frac{\eta}{G_1+G_2} \dv{\sigma}{t} &= \frac{G_1 G_2}{G_1 + G_2} \gamma_0
\end{align*}
\score{1 pt}. The particular solution is trivial: we simply take $\sigma_\mathrm{p} = \gamma_0 G_1 G_2 / (G_1 + G_2)$ \score{1 pt}. For the homogeneous solution, we separate variables and integrate:
\begin{align*}
\dv{\sigma_\mathrm{h}}{t} &= - \frac{G_1 + G_2}{\eta} \sigma_\mathrm{h} \\
\frac{1}{\sigma_\mathrm{h}} \dd{\sigma_\mathrm{h}} &= - \frac{G_1 + G_2}{\eta} \dd{t} \\
\log(\sigma_\mathrm{h}) &= - \frac{G_1 + G_2}{\eta} t + C,\\
\sigma_\mathrm{h} &= A \exp\left(- \frac{G_1 + G_2}{\eta} t \right),
\end{align*}
where $C$ and $A = e^C$ are integration constants \score{2 pt}. The full solution for $t>0$ then reads
\begin{align*}
\sigma(t) &= A \exp\left(- \frac{G_1 + G_2}{\eta} t \right) + \gamma_0 \frac{G_1 G_2}{G_1 + G_2} = \gamma_0 G(t).
\end{align*}
The function $G(t)$ is plotted in figure~\ref{fig:SLSstressrelaxation} \score{1 pt}.
\begin{figure}[ht]
\centering
\includegraphics[scale=1]{complexfluids/problems/SLSstressrelaxation.pdf}
\caption{Stress relaxation modulus $G(t)$ of the standard linear solid model.}
\label{fig:SLSstressrelaxation}
\end{figure}
%The time derivative of this solution at $t=0$ is $-A (G_1 + G_2)/\eta$, so
%\begin{align*}
%\sigma(t=0) + \frac{\eta}{G_1+G_2} \left. \dv{\sigma}{t}\right|_{t=0} &= A + \gamma_0 \frac{G_1 G_2}{G_1 + G_2} + \frac{\eta}{G_1+G_2} \left(-A \frac{G_1+G_2}{\eta}\right) = \gamma_0 \frac{G_1 G_2}{G_1 + G_2}.
%\end{align*}
%Comparing with equation~(\ref{stressode}) we see that if we multiply the particular solution with $\Theta(t)$, we retrieve the first term on the right-hand side. In that case, we also get an extra term on the left-hand side due to the derivative of $\Theta(t)$:
%\begin{align*}
%\frac{\eta}{G_1+G_2} \dv{\sigma_\mathrm{p}}{t} = \frac{\eta}{G_1+G_2} \gamma_0 \frac{G_1 G_2}{G_1 + G_2} \delta(t) = 
%\end{align*}
\item \textit{Method 1: From the equation}. As we can immediately read off from the equation, the stress relaxes over time, from an initial value $A+B$ (where $B = \gamma_0 G_1 G_2 / (G_1 + G_2)$) to a final value $B$ when $t \to \infty$ \score{2 pt}.\\
\textit{Method 2: From the pictorial representation}. If we apply a sudden strain, initially the two springs and the dashpot will all be stretched. For a stand-alone Kelvin-Voigt material, the dashpot cannot relax the deformation, as it can't change the deformation of the spring~$G_1$ with which it is in series. In this model however, it can relax the deformation of the spring~$G_2$ by deforming itself further, thus transferring stress from spring $G_2$ to spring $G_1$ and dissipation of the dashpot (through viscous flow). Therefore, some (but not all) of the stress will relax over time \score{2 pt}.
\item \textit{Method 1: From the equations}. Although the equations are quantitatively different, the terms in the answer to~(\ref{pb:Mslsconstitutiverelation}) and equation~(\ref{KelvinSLS}) are qualitatively identical (just with different values for the various combinations of elastic moduli). Therefore, there is no qualitative difference between the two models \score{2 pt}.\\
\textit{Method 2: From the pictorial representation}. We can `redraw' one representation to the other. Starting from the spring-in-parallel-with-Maxwell, we can draw the single spring as two springs in series (with spring constants $G_\mathrm{a}$ and $G_\mathrm{b}$ satisfying $1/G_2 = 1/G_\mathrm{a} + 1/G_\mathrm{b}$, then treat springs $G_\mathrm{a}$ and $G_1$ as in parallel, replacing them with a single spring (with spring constant $G_3 = G_2 + G_\mathrm{a}$, and we arrive at the representation spring-in-series-with-Kelvin-Voigt. For the opposite direction, we reverse the steps. As we have not added any qualitatively different elements, the models are all qualitatively the same \score{2 pt}.
\end{enumerate}
\fi


