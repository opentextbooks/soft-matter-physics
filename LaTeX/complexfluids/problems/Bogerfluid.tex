%Problemtitle: Boger fluid
Boger fluids are described by the Oldroyd-B model we introduced in section~\bookref{sec:OldroydB}. In the pictorial representation, the Oldroyd-B model combines a polymer, modeled as a Maxwell fluid, in parallel with a simple dashpot for the solvent \ifproblemset (figure~\ref{fig:OldroydBmodel}).
\begin{figure}[ht]
\centering
\includegraphics[scale=1]{complexfluids/OldroydBmodel.pdf}
\caption[Schematic of the Oldroyd-B model.]{Material elements picture of the Oldroyd-B  model: a viscous solvent with viscosity~$\eta_\mathrm{s}$ in parallel with a Maxwell fluid with viscosity~$\eta_\mathrm{p}$ and elasticity~$G_\mathrm{p}$.}
\label{fig:OldroydBmodel}
\end{figure}
\else (figure~\bookref{fig:OldroydBmodel}).
\fi
In section~\bookref{sec:OldroydB}, we wrote the Oldroyd-B model with separate equations for the stress of the polymer, $\mathbf{\sigma}_\mathrm{p}$, given by the UCM model, equation~(\bookref{OldroydBcomplete1}), and that of the solvent, $\mathbf{\sigma}_\mathrm{s}$, described as a Newtonian fluid, equation~(\bookref{OldroydBcomplete2}). We can also write a constitutive equation for the total stress of the material, $\mathbf{\sigma} = \mathbf{\sigma}_\mathrm{p} + \mathbf{\sigma}_\mathrm{s}$, which reads
\begin{equation}
\label{OldroydBcomplete5}
\bvec{\sigma} + \tau_\mathrm{p} \frac{\nabla \bvec{\sigma}}{\nabla t} = \eta \left[ \dot{\bvec{\gamma}} + \tau_\mathrm{r} \frac{\nabla \bvec{\gamma}}{\nabla t} \right],
\end{equation}
where $\tau_\mathrm{p}$ as before is the relaxation time of the polymer, and $\eta$ and $\tau_\mathrm{r}$ are respectively the viscosity and a timescale known as the \emph{retardation time}, both of which are properties of the combined fluid.
\begin{enumerate}[(a)]
\item Derive equation~(\ref{OldroydBcomplete5}) from~(\bookref{OldroydBcomplete1}), (\bookref{OldroydBcomplete2}) and $\mathbf{\sigma} = \mathbf{\sigma}_\mathrm{p} + \mathbf{\sigma}_\mathrm{s}$, and express $\eta$ and $\tau_\mathrm{r}$ in terms of the viscosities and relaxation time of the polymer and the solvent.
\ifproblemset\setcounter{problemcounter}{\value{enumi}}\else\setcounter{problemcounter}{\value{enumii}}\fi
\end{enumerate}
Suppose we want to reproduce the flow instability sketched in figure~\bookref{fig:polymerrotationalflowinstability}. To simplify the calculations, rather than considering a central rotating rod, we consider a setup with two parallel circular disks, with the Boger fluid sandwiched in between. We keep the bottom disk stationary and rotate the top disk at angular velocity~$\omega$. We choose coordinates such that the bottom disk is in the $xy$ plane and the top disk at $z=h$. Symmetry then tells us that the laminar flow field can only have a component in the angular direction, and only depend on the radial and vertical coordinate, i.e., $\bvec{v} = v(r, z) \unitvec{\theta}$. No-slip boundary conditions on the flow imply that we have $v(r, 0) = 0$ and $v(r, h) = \omega r$.
\begin{enumerate}[(a)]
\ifproblemset\setcounter{enumi}{\value{problemcounter}}\else\setcounter{enumii}{\value{problemcounter}}\fi
\item Find the laminar flow field from the (incompressible) continuity equation, $\bvec{\nabla} \cdot \bvec{v} = 0$ and the boundary conditions. \textit{Hint}: work in cylindrical coordinates. You'll have to separate variables; there will be qualitatively different solutions depending on the value of the resulting constant.
\item For the laminar flow, find the (total) stress tensor and the pressure of the Boger fluid, in terms of the Deborah number, $\mathrm{De} = \omega \tau_\mathrm{p}$, and the total viscosity~$\eta$.
\end{enumerate}






\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item We simply calculate the left-hand side of equation~(\ref{OldroydBcomplete5}):
\begin{align*}
\bvec{\sigma} + \tau_\mathrm{p} \frac{\nabla \bvec{\sigma}}{\nabla t} &= \bvec{\sigma_\mathrm{p}} + \bvec{\sigma_\mathrm{s}} + \tau_\mathrm{p} \frac{\nabla \bvec{\sigma_\mathrm{p}}}{\nabla t} + \tau_\mathrm{p} \frac{\nabla \bvec{\sigma_\mathrm{s}}}{\nabla t} \\
&= \eta_\mathrm{p} \dot{\bvec{\gamma}} + \eta_\mathrm{s} \dot{\bvec{\gamma}} + \tau_\mathrm{p} \eta_\mathrm{s} \frac{\nabla \bvec{\gamma_\mathrm{s}}}{\nabla t} \\
&= \eta \left[\dot{\bvec{\gamma}} + \tau_\mathrm{r} \frac{\nabla \bvec{\gamma_\mathrm{s}}}{\nabla t} \right],
\end{align*}
where
\[ \eta = \eta_\mathrm{p} + \eta_\mathrm{s} \qquad \mbox{and} \qquad \tau_\mathrm{r} = \frac{\tau_\mathrm{p} \eta_\mathrm{s}}{\eta_\mathrm{p} + \eta_\mathrm{s}}. \]
\item Substituting $\bvec{v} = v(r, z) \unitvec{\theta}$ in $\bvec{\nabla} \cdot \bvec{v} = 0$ in cyclindrical coordinates gives:
\[ 0 = \bvec{\nabla} \cdot \bvec{v} = \pdv[2]{v}{r} + \frac{1}{r} \pdv{v}{r} - \frac{v}{r^2} + \pdv[2]{v}{z}. \]
Writing $v(r, z) = \rho(r) \zeta(z)$ and separating, we get (primes denoting derivatives to the respective coordinates):
\begin{align*}
0 &= \zeta \rho'' + \frac{\zeta}{r} \rho' - \frac{\rho \zeta}{r^2} + \rho \zeta'', \\
C &= - \frac{1}{\zeta} \zeta'' = \frac{1}{\rho} \rho'' + \frac{1}{r \rho} \rho' - \frac{1}{r^2}, \\
\zeta'' &= - C \zeta, \\
r^2 \rho'' + r \rho' - \rho &= C r^2,
\end{align*}
where $C$ is a constant. Now for any value of $C$ that is not $0$, the equation for $\zeta$ gives
\[ \zeta(z) = A \sin(n \pi z) + B \cos(n \pi z) \qquad \mbox{with} \qquad n^2 \pi^2 = C. \]
In order to satisfy the boundary conditions, we must have $\zeta(0) = 0$, which gives $B = 0$, and without loss of generality we can set $A=1$. Alternatively, we could have $C=0$, in which case the solution for the $\zeta$ equation is $\zeta = z/h$. Proceeding with the $\rho$ equation, we have an inhomogeneous equation if $C \neq 0$; the $C=0$ equation is the homogeneous one. For the homogeneous equation, we try $\rho = r^k$, which gives two solutions, $k = \pm 1$. A particular solution is readily found from trying $\rho = \alpha C r^2$, which gives $\alpha = \frac13$, so the full solution (either case) is
\[ \rho(r) = A r + \frac{B}{r} + \frac13 C r^2. \]
The second boundary condition gives that $\rho(r) \sim r$ at $z=h$ (at which point $\zeta$ is a constant). The only term which satisfies this condition is the one with $A$, so we must have $B = C = 0$. Therefore, the full solution is
\[ v(r, z) = \omega \frac{rz}{h}. \]
\item In the laminar rotational flow, the inertial term in the Navier-Stokes equations trivially vanishes, and the Laplacian of the velocity is given by
\[ \nabla^2 v = \frac{1}{r} \pdv{}{r} \left( r \pdv{v}{r} \right) + \pdv[2]{v}{z} = \frac{\omega z}{r h}. \]
By symmetry, the stress and pressure cannot depend on the angular coordinate. Substituting, we thus find
\begin{align*}
-\pdv{p}{r} + \frac{1}{r} \frac{\partial}{\partial r}(r \sigma_{rr}) + \diff{\sigma_{rz}}{z} - \frac{\sigma_{\theta\theta}}{r} &= 0, \\
\frac{1}{r} \frac{\partial}{\partial r}(r \sigma_{r\theta}) + \diff{\sigma_{\theta z}}{z} + \frac{\sigma_{r\theta}}{r}  &= 0, \\
-\pdv{p}{z} + \frac{1}{r} \frac{\partial}{\partial r}(r \sigma_{rz}) + \diff{\sigma_{zz}}{z} &= 0.
\end{align*}
\end{enumerate}
\fi