% Problemtitle Properties of the Rouse model
The Rouse model consists of a collection of $N+1$ beads connected by $N$ springs with spring constant~$k$. In section~\bookref{sec:Rousemodel}, the relaxation modulus of the Rouse model is given as (equation~\bookref{Rouserelaxationmodulus}):
\begin{equation}
\label{Rouserelaxationmodulus2}
G(t) = n_\mathrm{p} \kB T \sum_{n=1}^N \exp\left( - \frac{n^2 t}{\tau_\mathrm{R}} \right),
\end{equation}
where $n_\mathrm{p}$ is the number density of the polymers and $\tau_\mathrm{R} = \zeta N^2/2 \pi^2 k$. For $t \ll \tau_\mathrm{R}$, we can approximate the sum in~(\ref{Rouserelaxationmodulus2}) as an integral, which gives $G(t) \approx \frac12 n_\mathrm{p} \kB T \sqrt{\pi \tau_\mathrm{R}/t}$.
\begin{enumerate}[(a)]
\item Find the storage $G'(\omega)$ and loss $G''(\omega)$ moduli of the Rouse model for high frequencies~$\omega$.
\item Sketch the storage and loss moduli of the Rouse model for all values of $\omega$ on a double-logarithmic and on a linear plot.
\item Find the steady-state viscosity (equation~\bookref{steadystateviscosity})
\[ \eta_0 = \int_0^\infty G(t) \,\dd t \]
for the Rouse model. You may approximate the (numerical value of) the sum in your expression by its value for $N \to \infty$. 
\end{enumerate}

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item We have
\begin{align*}
G'(\omega) &= \omega \int_0^\infty G(t) \sin(\omega t) \dd t = \frac12  n_\mathrm{p} \kB T \sqrt{\pi \tau_\mathrm{R}} \omega \int_0^\infty \frac{\sin(\omega t)}{\sqrt{t}} \dd t = \frac12  n_\mathrm{p} \kB T \sqrt{\pi \tau_\mathrm{R} \omega}, \\
G''(\omega) &= \omega \int_0^\infty G(t) \cos(\omega t) \dd t = \frac12  n_\mathrm{p} \kB T \sqrt{\pi \tau_\mathrm{R} \omega},
\end{align*}
as the Fourier sine and cosine transform of $1/\sqrt{t}$ are both $1/\sqrt{\omega}$.
\item For high frequencies, we found in (a) that both $G'(\omega)$ and $G''(\omega)$ scale with $\sqrt{\omega}$. For (very) low frequencies, only the lowest-order mode in the Rouse model remains, and the relaxation modulus is given by $G(t) = n_\mathrm{p} \kB T \exp(-t/\tau_\mathrm{R})$ (same as for the Maxwell model). For this model, we have
\begin{align*}
G'(\omega) &= n_\mathrm{p} \kB T \frac{(\omega \tau_\mathrm{R})^2}{1 + (\omega \tau_\mathrm{R})^2}, \\
G''(\omega) &= n_\mathrm{p} \kB T \frac{(\omega \tau_\mathrm{R})}{1 + (\omega \tau_\mathrm{R})^2}
\end{align*}
(see equation \bookref{exprelaxmoduli}). We've plotted both the low-frequency (blue and orange) and high-frequency (green) behavior of both moduli and an interpolation in the figure below.
\begin{center}
\includegraphics[scale=1]{complexfluids/problems/Rousestoragelossplots.pdf}
\end{center}
\item We have
\[ \eta_0 = \int_0^\infty G(t) \dd t = n_\mathrm{p} \kB T \sum_{n=1}^N \frac{\tau_\mathrm{R}}{n^2} = n_\mathrm{p} \kB T \tau_\mathrm{R} H_2(N) \to n_\mathrm{p} \kB T \tau_\mathrm{R} \frac{\pi^2}{6},\]
if $N \to \infty$ ($H_2(N)$ is the harmonic number of order 2; naturally we don't expect students to know that, but they should remember or be able to look up the sum with $N \to \infty$). 
\end{enumerate}
\fi