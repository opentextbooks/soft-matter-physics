% Exam 2020
A famous example of a viscoelastic material is the \index{Burgers material}Burgers material, named after the Dutch physicist Jan Burgers. The model is used to represent polymer viscoelastic behavior and is therefore highly applicable within the field of soft matter physics. To model the Burgers material a Maxwell fluid and a Kevin-Voigt solid are connected together in series. 
\begin{enumerate}[(a)]
\item Construct (out of springs and dashpots) the mechanical model describing the Burgers material.
\item Would you characterize this system as a solid-like liquid or a liquid-like solid?
\item Find the constitutive equation (the equation relating the stress~$\sigma$ and strain~$\gamma$) of the Burgers material, and show that it can be rewritten as
\begin{equation}
\label{Burgersstressstrain}
\sigma + \left(\frac{\eta_\mathrm{M}}{G_\mathrm{K}}+\frac{\eta_\mathrm{K}}{G_\mathrm{K}}+\frac{\eta_\mathrm{M}}{G_\mathrm{M}}\right) \dot{\sigma} + \frac{\eta_\mathrm{M}\eta_\mathrm{K}}{G_\mathrm{M} G_\mathrm{K}}\ddot{\sigma} = \eta_\mathrm{M}\dot{\gamma}+\frac{\eta_\mathrm{M}\eta_\mathrm{K}}{G_\mathrm{K}}\ddot{\gamma},
\end{equation}
where $\eta_\mathrm{M}$ and $\eta_\mathrm{K}$ are the viscosities of the Maxwell fluid and Kevin-Voigt solid, respectfully, and $G_\mathrm{M}$ and $G_\mathrm{K}$ are their elastic moduli. \textit{Hint: express the strain of the Kelvin-Voigt solid in terms of the total stress and strain, then substitute that expression in the equation for the Kelvin-Voigt solid itself}.
\item Find and plot the relaxation modulus $G(t)$ of a Burgers material, i.e., the stress response to a step strain (i.e., for $\gamma(t) = \gamma_0 \Theta(t)$ we get $\sigma(t) = \gamma_0 G(t)$, where $\Theta(t)$ is the step function that is zero for $t<0$ and one for $t>0$). You may simplify the problem to the case that the viscosities and the elasticities of the two parts are equal, i.e., $\eta_\mathrm{M}=\eta_\mathrm{K}=\eta$ and $G_\mathrm{M} = G_\mathrm{K} = G_\mathrm{E}$. Also, you don't have to worry about what happens at the discontinuity at $t=0$, simply solve for $t>0$.
\end{enumerate}

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item To construct the Burgers material we use that a Maxwell fluid can be constructed as a dashpot and spring in series, whereas the Kevin-Voigt is constructed from combining these two elements in parallel. Thus, for the Burgers material, we arrive at the four element model depicted below \score{1 pt}:
\begin{figure}[h]
\centering
\includegraphics[scale=1]{complexfluids/problems/Burgersmaterialmodel.pdf}
\caption{Schematic depiction of the Burgers material, consisting of a Maxwell fluid and a Kelvin-Voigt solid in series.}
\label{fig: burgers_model}
\end{figure}

\item One would expect the Burgers model to be characterized as a solid-like liquid because for short time-scales when applying a step-change in strain the springs are going to dominate (like a solid) while over time the dashpots will dominate and the strain will be reduced to 0 (like a liquid) \score{1 pt}. 

\item The total strain $\gamma$ can be expressed as a sum of the different strains of each element connected in series. That is,
\[ \gamma = \gamma_1 + \gamma_2 + \gamma_3, \]
where we have:
\[ \gamma_1 = \frac{\sigma}{G_\mathrm{M}}, \quad \dot{\gamma}_2 = \frac{\sigma}{\eta_\mathrm{M}}\quad \text{and} \quad \dot{\gamma}_3 + \frac{G_\mathrm{K}}{\eta_\mathrm{K}}\gamma_3 = \frac{\sigma}{\eta_\mathrm{K}} \]
\score{1 pt}. Next, we want to rewrite our expression for $\gamma$ to arrive at equation~(\ref{Burgersstressstrain}). We start by taking the derivative:
\[ \dot{\gamma} = \dot{\gamma}_1 + \dot{\gamma}_2 + \dot{\gamma}_3 \]
Take the derivative of the expression for $\dot{\gamma}_3$:
\[ \ddot{\gamma}_3+\frac{G_\mathrm{K}}{\eta_\mathrm{K}}\dot{\gamma}_3 = \frac{\dot{\sigma}}{\eta_\mathrm{K}} \]
Next, we perform black algebra magic (rewrite the expression for $\gamma$):
\[ \dot{\gamma}_3=\dot{\gamma}-\dot{\gamma}_1-\dot{\gamma}_2=\dot{\gamma}-\frac{\dot{\sigma}}{G_\mathrm{M}}-\frac{\sigma}{\eta_\mathrm{M}} \]
\score{1 pt}.
Take the derivative of this expression:
$$ \ddot{\gamma_3}=\ddot{\gamma}-\ddot{\gamma_1}-\ddot{\gamma_2}=\ddot{\gamma}-\frac{\ddot{\sigma}}{G_\mathrm{M}}-\frac{\dot{\sigma}}{\eta_\mathrm{M}}.$$
Using the expression for $\dot{\gamma}_3$ and inserting the other achieved expressions gives:
$$ \frac{\dot{\sigma}}{\eta_\mathrm{K}} = \ddot{\gamma} - \frac{\ddot{\sigma}}{G_\mathrm{M}} + \frac{G_\mathrm{K}}{\eta_\mathrm{K}} \left( \dot{\gamma}-\frac{\dot{\sigma}}{G_\mathrm{M}}-\frac{\sigma}{\eta_\mathrm{M}} \right) $$
Restructuring: 
$$ \frac{G_\mathrm{K}}{\eta_\mathrm{K}\eta_\mathrm{M}} + \left(\frac{1}{\eta_\mathrm{K}}+\frac{1}{\eta_\mathrm{M}}+\frac{G_\mathrm{K}}{G_\mathrm{M}\eta_\mathrm{K}}\right) \dot\sigma + \frac{\ddot{\sigma}}{G_\mathrm{M}} = \frac{G_\mathrm{K}}{\eta_\mathrm{K}}\dot{\gamma}+\ddot{\gamma} $$
$$ \sigma + \left(\frac{\eta_\mathrm{M}}{G_\mathrm{K}}+\frac{\eta_\mathrm{K}}{G_\mathrm{K}}+\frac{\eta_\mathrm{M}}{G_\mathrm{M}}\right)\dot{\sigma}+\frac{\eta_\mathrm{M}\eta_\mathrm{K}}{G_\mathrm{M} G_\mathrm{K}}\ddot{\sigma} = \eta_\mathrm{M}\dot{\gamma}+\frac{\eta_\mathrm{M}\eta_\mathrm{K}}{G_\mathrm{K}}\ddot{\gamma}$$
which is the same expression as the one given in the exercise text \score{1 pt}.

\item For $t > 0$, the strain is a constant, so its derivative vanishes, and equation~(\ref{Burgersstressstrain}), with the simplifying assumptions, becomes
\[ \sigma + 3\tau \dot{\sigma} + \tau^2 \ddot{\sigma} = 0, \]
where $\tau = \eta / G_\mathrm{E}$ \score{0.5 pt}. To solve this differential equation, we try the usual Ansatz, $\sigma \sim e^{\lambda t}$, which gives for the characteristic polynomial
\[ 1 + 3 \tau \lambda + \tau^2 \lambda^2 = 0, \]
or
\[ \lambda = - \frac{3}{2\tau} \pm \frac{1}{2\tau^2} \sqrt{9 \tau^2 - 4 \tau^2} = \frac{-3 \pm \sqrt{5}}{2\tau}. \]
As $\sqrt{5} < 3$, both solutions decay over time (as they should). We thus find:
\[ G(t) = A \exp\left(-\frac{3 + \sqrt{5}}{2} \frac{t}{\tau}\right) + B \exp\left(-\frac{3 - \sqrt{5}}{2} \frac{t}{\tau}\right), \]
where $A$ and $B$ are integration constants that could in principle be determined from the initial conditions \score{1 pt}. The plot below shows $G(t)$ as a function of time in units of $\tau$ for $A = B = \frac12$:
\begin{center}
\includegraphics[scale=0.75]{complexfluids/problems/Burgersrelaxationmodulus.pdf}
\end{center}
\score{0.5 pt}.
\end{enumerate}
\fi