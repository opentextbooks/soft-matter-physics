% Problemtitle Plane Couette flow in the UCM model
For the upper convected Maxwell (UCM) model, find the stress, pressure and velocity in under a plane Couette shear: the fluid is sheared between two parallel plates, a distance~$h$ apart, of which the top one moves with velocity $v_0$ in the positive $x$ direction (see figure~\ifproblemset\ref{fig:plateshear}\else\bookref{fig:plateshear}\fi). \textit{Hint}: start with writing down the components of the velocity gradient tensor, $\dot{\bvec{\gamma}}$, then use the constitutive equation~(\bookref{UCMcomplete1}) of the UCM model to find the components of the stress tensor in terms of the velocity, and finally use Navier-Stokes (eq.~\bookref{UCMcomplete2}) to close your system; you should end up with decoupled differential equations for the pressure and the velocity. The solution of the stress tensor is given by equation~(\bookref{UCMshearflow}).

\ifproblemset
\begin{figure}[h]
\begin{center}
\includegraphics[scale=0.75]{fluiddynamics/plateshear.pdf}
\end{center}
\caption[Plate shear experiment.]{Plate shear experiment (Couette flow). An amount of fluid is held between two solid plates. The bottom plate is fixed, the top plate moves to the right with a velocity $v_\mathrm{top}$, resulting in a displacement $\dd x$ per time interval $\dd t$.}
\label{fig:plateshear}
\end{figure}
\fi

\ifincludesolutions
\Solution
We'll position the top plate at $z=d$, the bottom plate at $z=0$, and take the velocity of the top plate along the positive $x$-direction. By symmetry, the stress, pressure and velocity of the fluid can then only be functions of~$z$, and any fluid flow must happen in the $x$-direction, so $\bvec{v} = v_x(z) \bvec{e}_x$. We also get $\bvec{v} \cdot \bvec{\nabla} = v_x \partial_x$, and because both the velocity and the stress are functions of $z$ alone, the convective terms vanish. For the components of the velocity gradient tensor, we have
\[ \frac{\partial v_j}{\partial x_i} = \frac{\partial v_x}{\partial z} \delta_{i3} \delta_{j1}, \]
i.e., there is only one nonzero component. Finally, we're considering steady-state flow, so all explicit time derivatives vanish. We thus get from the constitutive equation of the UCM model:
\begin{align*}
\sigma_{xx} -2 \tau \diff{v_x}{z} \sigma_{xz} &= 0, \\
\sigma_{xy} - \tau \diff{v_x}{z} \sigma_{yz} &= 0, \\
\sigma_{xz} - \tau \diff{v_x}{z} \sigma_{zz} &= \eta \diff{v_x}{z}, \\
\sigma_{yy} &= 0, \\
\sigma_{yz} &= 0, \\
\sigma_{zz} &= 0.
\end{align*}
By substituting $\sigma_{yz}$ back into the equation for $\sigma_{xy}$, we find (unsurprisingly) that $\sigma_{xy} = 0$ as well. The only two nonzero components are $\sigma_{xx}$ and $\sigma_{xz}$, for which we get:
\begin{align*}
\sigma_{xx} &= 2 \tau \eta \left(\diff{v_x}{z}\right)^2, \\
\sigma_{xz} &= \eta \diff{v_x}{z}.
\end{align*}
From the Navier-Stokes equations we get:
\[ \diff{\sigma_{xz}}{z} = 0, \qquad \mbox{and} \qquad \diff{p}{z} = 0, \]
so we find that $p$ is a constant, and that $\partial^2 v_x / \partial z^2 = 0$, so the velocity is linear in $z$. From the (no-slip) boundary conditions we have $\bvec{v}(z=0) = 0$ and $\bvec{v}(z=d) = v_0 \bvec{e}_x$, so we find $\bvec{v} = v_0 (z/d) \bvec{e}_x = \dot{\gamma} z \bvec{e}_x$, where $\dot{\gamma} = v_0/d$ is the applied strain rate. Substituting back into the equations for the stress gives
\[ \sigma_{xx} = 2 \tau \eta \dot{\gamma}^2, \qquad \sigma_{xz} = \eta \dot{\gamma}, \]
as given in the lecture notes.
\fi

