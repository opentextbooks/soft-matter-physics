

\subsection{Twistingrodsold}
Stretching a rod is a complicated business: it involves strains both along the stretch direction ($\gamma_{zz}$ in equation~\ref{rodextensionzz}) and in the directions perpendicular to it ($\gamma_{xx}$ and $\gamma_{yy}$ in equation~\ref{rodextensionxy}). However, in a sense we've recovered the one-dimensional version of Hooke's law: the applied stretching force on the ends of the rods is proportional to the change in the length of the rod:
\begin{equation}
\label{Hooke1Drod}
F = \sigma_{zz} A = E A \gamma_{zz} = \frac{E A}{L} \Delta L,
\end{equation}
with $A$ the cross-sectional area and $L$ the length of the rod. The proportionality constant (or spring constant) $E A / L$ contains both the geometry of the rod and a material parameter, its Young's modulus, which itself is a combination of the bulk and shear modulus, thus capturing the complicated stretch nicely in a single parameter.

A similar thing happens when we apply a torque to the rod to twist it: we'll find that the applied torque is proportional to the twist angle. The proportionality constant will now depend on the shear modulus of the rod, as twisting turns out to be predominantly shearing, and thus a qualitatively different operation than stretching.

To set the stage, let's consider a cylindrical rod of radius~$R$ and length~$L$, to which we've applied a torque such that one end is twisted over an angle~$\phi$, see figure~\ref{fig:rodstretchingtwisting}c. The deformation of the rod will depend on the distance to its center. Therefore, let's first consider a thin cylindrical shell of radius $r < R$ and thickness $\dd r$, figure~\ref{fig:rodstretchingtwisting}d. We can divide this thin shell up into small squares of side~$\dd a$, which during the twist all undergo a pure shear, deforming them into parallelograms, see figure~\ref{fig:rodstretchingtwisting}e. The shear angle~$\theta$ is given by $\theta = (r/L) \phi$. We defined the shear strain as the ratio of the displacement to the height of the object, which equals the tangent of the shear angle~$\theta$. For small angles, the tangent can be approximated by the angle itself, and we have $\gamma = \theta = (r/L) \phi$. Now for a pure shear, the stress is given by $\sigma = G \gamma$. Here the applied stress is the applied shear force $\Delta F$ (itself due to the applied torque) per unit area, which here is the side of the square~$\dd a$ times the thickness of the shell~$\dd r$ (the light blue side of the block sketched in figure~\ref{fig:rodstretchingtwisting}e). The torque equals the force times the lever arm, which here is the radius of the hollow shell. Put together, we have
\begin{equation}
\tau_\mathrm{square} = r \Delta F = r \sigma \,\dd a \,\dd r = r G \frac{r}{L} \phi \,\dd a \,\dd r.
\end{equation}
To find the torque on the whole shell, we integrate over its circumference\footnote{Note that the length of the square along the length of the tube doesn't show up in the equation. That's because two consecutive squares exert canceling forces on each other; only the contributions along the circumference add to the torque.}
\begin{equation}
\tau_\mathrm{shell} = \int_0^{2\pi r} \left(\frac{G r^2}{L} \phi \,\dd r \right) \, \dd a = 2 \pi \frac{G r^3}{L} \phi \, \dd r.
\end{equation}
For the torque on the whole cylindrical rod, we integrate over the shells to get
\begin{equation}
\label{rodtorque}
\tau_\mathrm{rod} = 2 \pi \frac{G}{L} \phi \int_0^R r^3 \, \dd r = \frac{\pi G R^4}{2L} \phi.
\end{equation}
As advertised, the twist torque on the rod is proportional to the twist angle~$\phi$. The proportionality constant depends on the shear modulus~$G$, as well as on its geometric properties. 



\section{Buckling of thin rods}
\label{sec:rodbuckling}
In addition to the bending energy of a thin rod, we can also calculate its bending moment, i.e., the torque exerted by the bending forces. Since the only nonzero component of the stress is $\sigma_{zz}$, the only nonzero force on a small element~$\dd A$ of the cross section of the beam is given by is $\sigma_{zz} \dd A = (Ex/R) \dd A$. To get the moment about the $y$-axis (i.e, the axis around which we're bending; into the paper in figure~\ref{fig:rodextensionbending}c), we simply multiply with the distance $x$ to that axis, and integrate over the entire cross sectional area:
\begin{equation}
\label{bendingmoment}
M_y = \int x \sigma_{zz} \dd A = \frac{E}{R} \int x^2 \dd A = \frac{E I_y}{R}.
\end{equation}
Perhaps unsurprisingly, the bending moment is proportional to the imposed curvature ($1/R$).

\begin{figure}
\begin{center}
\includegraphics[scale=1]{elasticity/rodbuckling.pdf}
\end{center}
\caption{Buckling of a rod under compression.}
\label{fig:rodbuckling}
\end{figure}

Let's now consider what happens when we compress a thin rod: we apply a force towards the center from both ends. Initially, the rod will slightly shorten, but quickly it will \emph{buckle}, meaning that it will assume a bent shape that deviates from a straight line and has no net compression, see figure~\ref{fig:rodbuckling}. The shape of the rod can then be described by its distance from the line, $w(z)$ in figure~\ref{fig:rodbuckling}. For a buckled rod, the bending moment is given by $M_y = F h(z)$, which should equal the moment given in equation~(\ref{bendingmoment}). The shape of the rod therefore needs to satisfy
\begin{equation}
\label{buckledrodequation1}
\frac{E I_y}{R(z)} = F w(z)
\end{equation}
at any point along the rod. Note that here we parametrize the rod by the position~$z$ instead of the rod's arc length~$s$. Now the radius of curvature of a line (i.e., the radius of the osculating circle\footnote{`Osculare' is Latin for `kissing'. The osculating circle at a point on a curved line has both the same tangent (first derivative) and the same curvature (second derivative) as the line at that point.}) is given by its second derivative, so we have $1/R(z) = - \dd^2 w(z) / \dd z^2$, where we include a minus sign because our shape is bending downwards (if you haven't seen this before, you can derive this equation using the methods introduced in section~\ref{sec:planecurves}). Equation~(\ref{buckledrodequation1}) then becomes:
\begin{equation}
\label{buckledrod}
\frac{\dd^2 w(z)}{\dd z^2} = - \frac{F}{E I_y} w(z),
\end{equation}
of which the solution is a linear combination of sines and cosines. For the case sketched in figure~\ref{fig:rodbuckling}, we get $w(z) = w_\mathrm{max} \sin (\pi z / L_\mathrm{c})$, where $L_\mathrm{c}$ is the contour length of the rod. By substituting this solution back into equation~(\ref{buckledrod}), we find an expression relating the buckling force (the minimal force at which the rod will buckle) to the contour length:
\begin{equation}
\label{bucklingforce}
F_\mathrm{buckle} = \frac{\pi^2 E I_y}{L_\mathrm{c}^2} = \frac{\pi^2 K_\mathrm{eff}}{L_\mathrm{c}^2}.
\end{equation}

