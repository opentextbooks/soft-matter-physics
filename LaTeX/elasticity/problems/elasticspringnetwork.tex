%Problemtitle: Deformations of an elastic spring network.
%Source: Boal
\ifproblemset In class, \else In this chapter, \fi we discussed the elastic properties of homogeneous and isotropic bulk materials (e.g. a rubber band).  In biological systems, elastic materials are often constructed as networks of elastic filaments. A good example is the spectrin network underlying the plasma membrane of a red blood cell, which has a roughly triangular structure. In this problem, we'll study an (ideal) two-dimensional triangular network of springs. Each spring has spring constant~$k$, rest length $s_0$, and potential energy $V(s) = \frac12 k (s-s_0)^2$. Note that because we are dealing with a two-dimensional system here, we will use the free energy not per unit volume but per unit area; the two-dimensional version of Hooke's law reads
\begin{equation}
\label{Hooke2D}
\sigma_{ij} = \frac12 K_2 \gamma_{kk} \delta_{ij} + G_2 \left( \gamma_{ij} - \frac12 \gamma_{kk} \delta_{ij}\right),
\end{equation}
where $K_2$ and $G_2$ are the two-dimensional bulk and shear modulus.

\begin{figure}[ht]
	\centering
	\includegraphics[scale=0.9]{elasticity/problems/triangularnetwork2.pdf}
	\caption[Triangular spring network.]{Triangular network of springs, as a model for the e.g. the spectrin network in a red blood cell. (a) Configuration of the equilateral triangles. (b) Pure compression by decreasing the length of each spring from $s_0$ to $s_0-\delta$. (c) Pure shear by moving the top vertex of a triangle by a distance~$\delta$ in the direction parallel to the bottom line. (d) Some geometric properties of an equilateral triangle.}
	\label{fig:triangularnetwork}
\end{figure}

\begin{enumerate}[(a)]
\item Suppose we apply a pure compression to our triangular spring network, starting from its equilibrium configuration. The compression results in shortening the length of each spring by a small amount $\delta = s_0 - s$ (see figure~\ref{fig:triangularnetwork}b). Express the change in potential energy per triangle, $\Delta V$, in terms of~$\delta$. Note that each spring is shared by two triangles.
\item By dividing your expression in (a) by the area per triangle (in the equilibrium configuration), find the change in the free energy density $\Delta f$ for the small compression. % \sqrt{3}k (\delta/s_0)^2.
\item Express the strain tensor for the pure compression in terms of $s_0$ and $\delta$. You may ignore any higher-order terms in $\delta$. % \gamma_{ij} = -(\delta/s_0) \delta_{ij}$
\item Substitute the expression for the strain tensor in the expression of the free energy for a continuous material, $\Delta f = \frac14 \sigma_{ij}\gamma_{ij}$, to obtain an expression for $\Delta f$ in $s_0$ and (to lowest order in) $\delta$. You can use the 2D version of Hooke's law from equation~(\ref{Hooke2D}) here.  You can use the tensor version of Hooke's law to relate the stress and the strain, but note that here we have a 2D material whereas the expressions in equation~(\bookref{HookeLaw}) \ifproblemset of the notes \fi is for 3D. % $\Delta F = \frac12 \sigma_{ij}\gamma_{ij}$, $\sigma_{ij} = K \gamma_{kk} \delta_{ij} = K Tr(\gamma) \delta_{ij} = (-2\delta/s_0) K \delta_{ij}$, so $\Delta F = 2 (\delta/s_0)^2 K$.
\item Compare the expression at (d) with your answer at (b) to show that the bulk modulus of the spring network is given by $K_2 = \frac12 \sqrt{3} k$.
\item \ifproblemset(Bonus problem) From the pure shear deformation in figure~\ref{fig:triangularnetwork}c, find an expression relating the shear modulus of the network to the spring constant~$k$.\else From the pure shear deformation in figure~\ref{fig:triangularnetwork}c, show that the shear modulus of the network is given by $G_2 = \frac14 \sqrt{3} k$.\fi
\end{enumerate}

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item We have $V(s_0-\delta) = \frac12 k \delta^2$ per spring \score{0.5 pt}, three springs per triangle, and each spring shared by two triangles, so an average of $3/2$ spring per triangle \score{0.5 pt}. The change in energy per triangle is thus given by $\Delta V = \frac32 \frac12 k \delta^2 = \frac34 k \delta^2$ \score{0.5 pt}.
\item The triangles have equilibrium area $\frac12 s_0 (\frac12 \sqrt{3} s_0) = \frac14 \sqrt{3} s_0^2$ \score{0.5 pt}. The change in energy density per triangle is thus given by
\begin{equation}
\label{triangularspringnetworkfreenergy}
\Delta f = \frac{\Delta V}{A} = \frac{\frac34 k \delta^2}{\frac14 \sqrt{3} s_0^2} = \sqrt{3} k \frac{\delta^2}{s_0^2}
\end{equation}
\score{1 pt}.
\item The trace of the strain tensor gives the (relative) change in volume, so we have
\begin{equation}
\Tr(\gamma_{ij}) = 2\frac{\Delta A}{A} = \frac{(s_0-\delta)^2 - s_0^2}{s_0^2} \approx - 4 \frac{\delta}{s_0}
\end{equation}
\score{1 pt}. Now by symmetry (the network shrinks as much in the horizontal as in the vertical direction), and by virtue of the fact that our strain is a pure compression, we have
\begin{equation}
\gamma_{ij} = \frac12 \Tr(\gamma_{ij}) \delta_{ij} = - 2 \frac{\delta}{s_0} \delta_{ij}
\end{equation}
\score{1 pt}.
\item The stress tensor is given by Hooke's law (2D version):
\begin{equation}
\sigma_{ij} = \frac12 K_2 \gamma_{kk} \delta_{ij} = - 2\frac{\delta}{s_0} K_2 \delta_{ij},
\end{equation}
where we left out the shear term as it equals zero \score{1 pt}. Substituting the stress and strain in the expression for the free energy, we get
\begin{equation}
\label{continuumcompressionfreeenergy}
\Delta f = \frac14 \gamma_{ij} \sigma_{ij} = \frac12 \left(-\frac{\delta}{s_0}\right) \delta_{ij} \left(-2 \frac{\delta}{s_0}\right) K \delta_{ij} = \frac{\delta^2}{s_0^2} K_2 \delta_{ij}^2 = 2 \frac{\delta^2}{s_0^2} K_2
\end{equation}
\score{1 pt}.
\item Comparing (\ref{triangularspringnetworkfreenergy}) and (\ref{continuumcompressionfreeenergy}), we can immediately read off that $\sqrt{3} k = 2 K_2$, or $K_2 = \frac12 \sqrt{3} k$ \score{1 pt}.
\item The steps we need to take for the shear deformation are the same as the ones we took in (a)-(e) for the compression. Note that under the pure shear, the area of the triangle does not change. Moreover, the length of the bottom spring (or any horizontal spring) is unchanged. For the lengths of the left and right spring in figure~\ref{fig:triangularnetwork}c, we get
\begin{align*}
l_\mathrm{left} &= \sqrt{\left(\frac12 s_0 + \delta\right)^2 + \left(\frac12 \sqrt{3} s_0\right)^2} = \sqrt{s_0^2 + s_0 \delta + \delta^2} \approx s_0 + \frac12 \delta, \\
l_\mathrm{right} &= \sqrt{\left(\frac12 s_0 - \delta\right)^2 + \left(\frac12 \sqrt{3} s_0\right)^2} = \sqrt{s_0^2 - s_0 \delta + \delta^2} \approx s_0 - \frac12 \delta.
\end{align*}
Substituting the spring lengths in the potential energy for a triangle, we get
\begin{equation}
\Delta V_\mathrm{shear} = \frac12 \left[ \frac12 k \left(\frac12 \delta \right)^2 + \frac12 k \left(-\frac12 \delta \right)^2 \right] = \frac18 k \delta^2.
\end{equation}
For the change in free energy per unit area, we then get
\begin{equation}
\label{freeenergyperareachangeshear}
\Delta f = \frac{\Delta V}{A} = \frac{\frac18 k \delta^2}{\frac14 \sqrt{3} s_0^2} = \frac16 \sqrt{3} k \left( \frac{\delta}{s_0} \right)^2.
\end{equation}
The deformation is a pure shear, of magnitude
\begin{equation}
\frac{\Delta x}{\Delta y} = \frac{\delta}{\frac12 \sqrt{3} s_0},
\end{equation}
and for the strain tensor we thus have
\begin{equation}
\gamma_{ij} = \frac23 \sqrt{3} \frac{\delta}{s_0} \begin{pmatrix} 0 & 1 \\ 1 & 0 \end{pmatrix}.
\end{equation}
The stress tensor follows from the two-dimensional version of Hooke's law, equation~(\ref{Hooke2D}):
\begin{equation}
\sigma_{ij} = \frac23 \sqrt{3} G_2 \frac{\delta}{s_0} \begin{pmatrix} 0 & 1 \\ 1 & 0 \end{pmatrix}.
\end{equation}
For the change in free energy per unit area we then get
\begin{equation}
\label{shearfreeeenrgy}
\Delta f = \frac14 \gamma_{ij} \sigma_{ij} = \frac23 G_2 \left( \frac{\delta}{s_0} \right)^2.
\end{equation}
Comparing equations~(\ref{freeenergyperareachangeshear}) and~(\ref{shearfreeeenrgy}), we can read off that
\begin{equation}
G_2 = \frac14 \sqrt{3} k = \frac12 K_2.
\end{equation}
\end{enumerate}
\fi