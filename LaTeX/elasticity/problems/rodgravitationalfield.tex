% Problemtitle Deformation of an elastic rod in a gravitational field
\ifproblemset\else In this problem, we determine the deformation of an elastic rod in a gravitational field.\fi
\begin{center}
\includegraphics{elasticity/problems/standingrod.pdf}
\end{center}
The stress tensor is defined as the tensor of which the divergence equals the force per unit volume:
\begin{equation}
\label{defstresstensor}
f_i = \partial_j\sigma_{ij}
\end{equation}
In equilibrium, the sum of the forces is zero. In a gravitational field, the force per unit volume is $\rho g_i$, with $\rho$ the
density of the object, and $g_i$ the gravitational acceleration, which points (of course) right down (we'll usually	take that as the negative $z$ direction). In equilibrium, we then have
\begin{equation}
\label{gravityequilibrium}
\partial_j\sigma_{ij} = \rho g_i.
\end{equation}
Equation~(\ref{gravityequilibrium}) works in both Cartesian (with $i = x, y, z$) and cylindrical (with $i = \rho, \theta, z$) coordinates, cf. equations~(\bookref{stresscylindricalcoords}) and~(\bookref{straincylindricalcoords}). The problem can be solved in either coordinate system.
\begin{enumerate}[(a)]
	\item Determine the stress components using equation (\ref{gravityequilibrium}). What are the boundary conditions?
	\item Using the three-dimensional version of Hooke's law (eqs.~\bookref{HookeYoungPoisson} and \bookref{HookeYoungPoissoninverse}), find the strain.
	\item Determine the deformation of the rod. \textit{Hint}: you will need a symmetry argument to find some of the integration constants.
\end{enumerate}

\ifincludesolutions
\Solution
\ifproblemset\subsection{Cartesian coordinates}\else\textit{Cartesian coordinates}\fi
\begin{enumerate}[(a)]
	\item We already have the differential equation for the stress inside the rod $f_i+\partial_j\sigma_{ij} = 0$. As the only forces acting on the rod are the body force of gravity and the normal force from the solid support at the bottom, the forces all point in the $z$ direction. As there are no forces in either the $x$ or $y$ direction anywhere, we get $\sigma_{xx} = \sigma_{xy} = \sigma_{yy} = 0$. Moreover, all forces are constant on any horizontal slice through the rod, so the stress can only depend on the $z$ coordinate. Our three equations then simplify to $\partial_z \sigma_{xz} = \partial_z \sigma_{yz} = 0$, $\partial_z \sigma_{zz} = \rho g$. As there are no forces acting perpendicular to the sides of the rod, we get $\sigma_{xz} = \sigma_{yz} = 0$ at all points along the side, and, by integration, $\sigma_{xz} = \sigma_{yz} = 0$ throughout.
%	We already have the differential equation for the stress inside the rod $f_i+\partial_j\sigma_{ij} = 0$. We still need the boundary conditions. As the only force is gravity, pointing down, there are no forces acting perpendicular to the sides of the rod, so $\sigma_{xx} = \sigma_{xy} = \sigma_{yy} = \sigma_{xz} = \sigma_{yz} = 0$ at all points along the side. Because in any horizontal slice the derivatives of all of these with respect to x and y are also zero, they are constants throughout, equal to their boundary values of zero.
The only nonzero component of the stress is therefore $\sigma_{zz}$. At the top, all stresses are zero, including $\sigma_{zz}$. We thus arrive at the simple boundary value problem (treating $g$ as a positive number here):
	\begin{align}
		\partial_z\sigma_{zz} &= \rho g, \\
		\sigma_{zz}(L) &= 0.
	\end{align}
	Note that as we only have a first order problem, we only get to set the boundary condition at one of the ends (we can of course calculate the stress at the bottom once we have the solution). Integrating this system is easy, and we find
	\begin{equation}
		\sigma_{zz} = -\rho g(L-z).
	\end{equation}
	\item To get the strain, we can use the three-dimensional version of Hooke's law. The easiest version to use is that in terms of the Young's modulus $E$ and Poisson ratio $\nu$, as those are specifically introduced for extensions / compressions of rods (for the case where the stress is along the long axis of the rod), as we have here. In general, we have
	\begin{equation}
		\gamma_{ij} = \frac{2}{E}[(1+\nu)\sigma_{ij}-\nu\sigma_{kk}\delta_{ij}]
	\end{equation}
	which for the present case gives
	\begin{align*}
	\gamma_{xy} = \gamma_{xz} = \gamma_{yz} &= 0,\\
	\gamma_{zz} &= \frac{2\sigma_{zz}}{E},\\
	\gamma_{xx} = \gamma_{yy} = -\nu\gamma_{zz} &= -2\nu\frac{\sigma_{zz}}{E}.
	\end{align*}		
%	 , $\gamma_{zz} =2\sigma_{zz}/E$, and $\gamma_{xx} = \gamma_{yy} = -\nu\gamma_{zz} = -2\nu\sigma_{zz}/E$.
	\item  From (b), we get six differential equations for the three components of the displacement vector $\bvec{u} = (u_x, u_y, u_z)$. The first three come from the diagonal elements of $\gamma_{ij}$:
	\begin{equation}
		\partial_x u_x = \frac{\nu\rho g}{E}(L-z) \rightarrow u_x(x,y,z) = \frac{\nu\rho g}{E}(L-z)x+C_x(y,z)
	\end{equation}
	\begin{equation}
		\partial_y u_y= \frac{\nu\rho g}{E}(L-z) \rightarrow u_y(x,y,z) = \frac{\nu\rho g}{E}(L-z)y+C_y(x,z)
	\end{equation}
	\begin{equation}
		\partial_z u_z = -\frac{\rho g}{E}(L-z) \rightarrow u_z(x,y,z) = \frac{\rho g}{2E}(2L-z)z+C_y(x,y)
	\end{equation}
	where $C_x$, $C_y$ and $C_z$ are `integration constants' which are really functions of the indicated variables. We also have, from the off-diagonal elements of $\epsilon_{ij}$:
	\begin{equation}
	\label{CxCyeq}
		\partial_xu_y+\partial_yu_x = 0 \quad\rightarrow\quad \partial_y C_x + \partial_x C_y = 0
	\end{equation}
	\begin{equation}
	\label{CxCzeq}
		\partial_zu_x+\partial_xu_z = 0 \quad\rightarrow\quad -\frac{\nu\rho g}{E}x + \partial_z C_x+\partial_xC_z=0
	\end{equation}	
	\begin{equation}
	\label{CyCzeq}
		\partial_zu_y+\partial_yu_z = 0 \quad\rightarrow\quad -\frac{\nu\rho g}{E}y + \partial_z C_y+\partial_yC_z=0
	\end{equation}
	As by symmetry the system should remain the same if we swap $x$ and $y$, and by equation~(\ref{CxCyeq}) the $y$ derivative of $C_x$ must equal minus the $x$ derivative of $C_y$, the functions $C_x$ and $C_y$ cannot depend on $y$ and $x$, respectively. By equations~(\ref{CxCzeq}) and~(\ref{CyCzeq}) they could be linear in $z$, but then $C_z$ would have to be linear in $x$ and $y$ (with the same magnitude but opposite sign), which violates the reflection symmetry through the planes at $x=0$ and $y=0$. By symmetry, we thus get $C_x = C_y = 0$, which gives $C_z(x,y) = -(\nu\rho g/2E)(x^2+y^2)$ and hence,
	%We have some freedom in choosing our functions here, so we go for the simplest possible solution: $C_x = C_y = 0$ which gives $C_z(x,y) = -(\nu\rho g/2E)(x^2+y^2)$ and hence,
	\begin{equation}
		u_z(x,y,z)=-\frac{\rho g}{2E}[(2L-z)z+\nu(x^2+y^2)]
	\end{equation}

\end{enumerate}

\ifproblemset\subsection{Cylindrical coordinates}\else\textit{Cylindrical coordinates}\fi
\begin{enumerate}[(a)]
	\item We already have the differential equation for the stress inside the rod $\bvec{f} + \bvec{\nabla}\cdot\bvec{\sigma} = 0$. As the only forces acting on the rod are the body force of gravity and the normal force from the solid support at the bottom, the forces all point in the $z$ direction. As there are no forces in either the $r$ or $\theta$ direction anywhere, we get $\sigma_{rr} = \sigma_{r\theta} = \sigma_{\theta\theta} = 0$. Moreover, all forces are constant on any horizontal slice through the rod, so the stress can only depend on the $z$ coordinate. Our three equations then simplify to $\partial_z \sigma_{rz} = \partial_z \sigma_{\theta z} = 0$, $\partial_z \sigma_{zz} = \rho g$. As there are no forces acting perpendicular to the sides of the rod, we get $\sigma_{rz} = \sigma_{\theta z} = 0$ at all points along the side, and, by integration, $\sigma_{rz} = \sigma_{\theta z} = 0$ throughout.
%We already have the differential equation for the stress inside the rod $\bvec{f} + \bvec{\nabla}\cdot\bvec{\sigma} = 0$. We still need the boundary conditions. As the only force is gravity, pointing down, there are no forces acting perpendicular to the sides of the rod, so $\sigma_{rr} = \sigma_{r\theta} = \sigma_{\theta\theta} = \sigma_{rz} = \sigma_{\theta z} = 0$ at all points along the side. Because in any horizontal slice the derivatives of all of these with respect to $r$ and $\theta$ are also zero, they are constants throughout, equal to their boundary values of zero. 
The only nonzero component of the stress is therefore $\sigma_{zz}$. At the top, all stresses are zero, including $\sigma_{zz}$. We thus arrive at the simple boundary value problem (treating $g$ as a positive number here):
	\begin{align}
		\partial_z\sigma_{zz} &= \rho g, \\
		\sigma_{zz}(L) &= 0.
	\end{align}
	Note that as we only have a first order problem, we only get to set the boundary condition at one of the ends (we can of course calculate the stress at the bottom once we have the solution). Integrating this system is easy, and we find
	\begin{equation}
		\sigma_{zz} = -\rho g(L-z)
	\end{equation}
	\item To get the strain, we can use the three-dimensional version of Hooke's law. The easiest version to use is that in terms of the Young's modulus $E$ and Poisson ratio $\nu$, as those are specifically introduced for extensions / compressions of rods (for the case where the stress is along the long axis of the rod), as we have here. In general, we have
	\begin{equation}
		\bvec{\gamma} = \frac{2}{E}[(1+\nu) \bvec{\sigma} - \nu \Tr(\bvec{\sigma})\bvec{I}],
	\end{equation}
	with $\bvec{I}$ the identity tensor. For the present case, we have $\Tr(\bvec{\sigma}) = \sigma_{zz}$, and we can read off that $\gamma_{r\theta} = \gamma_{rz} = \gamma_{\theta z} = 0$, $\gamma_{zz} =2\sigma_{zz}/E = - 2 (\rho g / E) (L-z)$, and $\gamma_{rr} = \gamma_{\theta \theta} = 2 \nu (\rho g/E) (L-z) = - \nu \gamma_{zz}$.
	\item  From (b), we get six differential equations for the three components of the displacement vector $\bvec{u} = u_r \unitvec{r} + u_{\theta} \unitvec{\theta} + u_z \unitvec{z}$, but we already know from symmetry that $u_\theta = 0$. Also by symmetry, $u_r$ and $u_z$ can be functions of $r$ and $z$ only. With that, we're left with two differential equations, and one algebraic one, for the two unknown parts of $\bvec{u}$. The equation for the $z$ component reads:
	\begin{equation}
	\gamma_{zz} = 2 \diff{u_z}{z} \quad \Rightarrow \quad u_z = - \frac{\rho g}{E} \int_0^z (L-z')\, \ddz' = -\frac{\rho g}{E} \left(Lz - \frac12 z^2\right) + C_z(r).
	\end{equation}
For the $r$ component, we get a differential equation from $\gamma_{rr}$:
\begin{equation}
\gamma_{rr} = 2 \diff{u_r}{r} \quad \Rightarrow \quad u_r = \frac{\rho g \nu}{E} \int_0^r (L-z) \dd r' = \frac{\rho g \nu}{E} (L-z) r + C_r(z).
\end{equation}	
However, we also get an algebraic equation for the $r$ component from $\gamma_{\theta \theta}$:
\begin{equation}
\gamma_{\theta \theta} = 2 \frac{u_r}{r} \quad \Rightarrow \quad u_r = \frac{\rho g \nu}{E} \int_0^r (L-z) \dd r' = \frac{\rho g \nu}{E} (L-z) r.
\end{equation}
Combining the last two equations we find that they are identical if the integration function $C_r(z)$ vanishes. The equations that follow from $\gamma_{r\theta}$ and $\gamma_{\theta z}$ simply give $0 = 0$. We find $C_z(r)$ from the equation for $\gamma_{rz}$:
\begin{equation}
0 = \gamma_{rz} = \diff{u_r}{z} + \diff{u_z}{r} = - \frac{\rho g \nu}{E} r + \diff{C_r}{r} \quad \Rightarrow \quad C_r(r) = \frac12 \frac{\rho g \nu}{E} r^2 + C,
\end{equation}
where $C$ is simply a constant, that we fix by setting $u_z(r=z=0) = 0$. We thus obtain our solution:
\begin{align}
u_r &= \nu \frac{\rho g}{E} (L-z) r,\\
u_z &= - \frac{\rho g}{E} \left[ L z - \frac12 z^2 - \frac12 \nu r^2 \right].
\end{align}
\end{enumerate}
\fi