Find the second moment of the area of solid beams with cross-sections shaped like \ifproblemset (pick any two of the following five, except not both a and b): \fi
\begin{center}
\includegraphics[scale=1]{elasticity/problems/momentsofinertiacrosssections.pdf}
\end{center}
\begin{enumerate}[(a)]
\item A square with sides~$a$, with the axis of bending through the center, parallel to one of the sides.
\item A square with sides~$a$, with the axis of bending through the center, diagonally across the square.
\item A hexagon with sides~$a$, with the axis of bending through the center, connecting two vertices.
\item A rectangle with sides~$a$ and~$b$, with the axis of bending through the center, parallel the side with length~$b$.
\item A cylinder with radius~$R$, with the axis of bending through the center.
\ifproblemset\setcounter{problemcounter}{\value{enumi}}\else\setcounter{problemcounter}{\value{enumii}}\fi
\end{enumerate}
Also find the second moment of the area for \ifproblemset one of the following three \fi (where you may assume that the thickness~$d$ is much smaller than the other dimensions):
\begin{enumerate}[(a)]
\ifproblemset\setcounter{enumi}{\value{problemcounter}}\else\setcounter{enumii}{\value{problemcounter}}\fi
\item A configuration of four beams of length $a$ and thickness~$d$ in a square, with the axis of bending through the center of the square, parallel to one of the sides.
\item A hollow cylindrical beam of radius~$R$ and thickness~$d$, with the axis of bending through the center.
\item An I-shaped beam of height~$H$, width~$b$, and thickness~$d$, with the axis of bending through the center, parallel to the beams at the end. You may assume that $d \ll H$ and $d \ll b$. Hint: you may find the theorems for calculating the moment of inertia useful.
\end{enumerate}

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item We have
\[ I = \int_{-a/2}^{a/2} x^2 \mathrm{d} A = a \int_{-a/2}^{a/2} x^2 \mathrm{d} x = \frac{1}{12} a^4. \]
\item The `height' of a strip (parallel to the axis of bending, i.e., the dashed y-axis in the cross section) is given by $h(x) = a/\sqrt{2} - x$, for $0<x<a/\sqrt{2}$. Note that we need this height four times: twice above the axis (for $-a/\sqrt{2} < x < 0$ and $0<x<a/\sqrt{2}$) and twice below. Integration gives
\begin{align*}
I &= \int_{-a/\sqrt{2}}^{a/\sqrt{2}} x^2 \mathrm{d} A = 4 \int_{0}^{a/\sqrt{2}} x^2 h(x) \mathrm{d} x \\
&= 2 \sqrt{2} a \int_{0}^{a/\sqrt{2}} x^2 \ddx - 2 \int_{0}^{a/\sqrt{2}} x^3 \ddx \\
&= 2 a^4 \left( \frac16 - \frac18\right) = \frac{a^4}{12}.
\end{align*}
\item The `height' of a strip (parallel to the axis of bending) is now given by $h(x) = a - x/\sqrt{3}$. Integration gives
\begin{align*}
I &= \frac{5 \sqrt{3}}{16} a^4.
\end{align*}
\item The `height' is now constant ($b$), so the integration is very easy:
\[ I = \int_{-a/2}^{a/2} x^2 \mathrm{d} A = b \int_{-a/2}^{a/2} x^2 \mathrm{d} x = \frac{1}{12} a^3 b. \]
\item \textit{Direct method}: The `height' of a strip (parallel to the axis of bending) is now given by $h(x) = \sqrt{R^2 - x^2}$. Integration gives (I used Mathematica):
\[ I = \int_{-R}^R x^2 \mathrm{d} A = 4 \int_0^R x^2 \sqrt{R^2 - x^2} \mathrm{d} x = \frac{\pi}{4} R^4.\]
\textit{Smart method}: By the perpendicular axis theorem, we have $I_x + I_y = I_z$, and by symmetry, we have $I_x = I_y$, so $I_y = \frac12 I_z$. We can easily find $I_z$ (for an axis through the cylinder) as for every ring, we have 
$I_\mathrm{ring} = 2 \pi r \cdot r^2 = 2 \pi r^3$, and integrating $r$ from $0$ to $R$ we get $I_z = \frac12 \pi R^4$, so $I_y = \frac14 \pi R^4$.
\item The square consists of four beams, all of width~$d$ and length~$a$. For the two beams perpendicular to the axis, the moment is given by the result of~(d): $I_{\mathrm{beam}, \perp} = a^3 d /12$. For the two beams parallel to the axis, the moment is given by the result of~(d), displaced (parallel-axis theorem) over $a/2$, so $I_{\mathrm{beam},\parallel} = d^3 a / 12 + (a \cdot d) (a/2)^2$. Summing up, we find
\[ I_\mathrm{square} = \frac{1}{6} \left( a^3 d + d^3 a + 3 d a^3 \right) = \frac{ad}{6} (4 a^2 + d^2). \]
\item Although the direct method can work here, it's much simpler to use the perpendicular-axis theorem again, now integrating over $R$ to $R+d$:
\[ I_y = \frac12 I_z = \frac12 \int_R+{R+d} 2 \pi r^3 \mathrm{d} r = \frac{\pi}{4} \left( (R+d)^4 - R^4 \right) \approx \pi R^3 d, \]
where the approximation holds if $d \ll R$.
\item The I-shape consists of three beams. The calculation goes entirely analogous to the open square of~(f). We have
\[ I_\mathrm{I} = \frac{1}{12} H^3 d + 2 \left( \frac{d^3 b}{12} + d b \frac{H^2}{4} \right) = \frac{d}{12} \left( 6 H^2 b + H^3 + 2 d^2 b \right).\]
\end{enumerate}
\fi