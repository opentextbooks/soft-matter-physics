%Problemtitle: Beam shapes
\ifproblemset
In class, we found that the shape of a bent beam is given by the (Euler-Bernoulli) beam equation
\begin{equation}
\label{EBbeameq}
q(z) = \frac{\mathrm{d}^2}{\mathrm{d}z^2}\left( E I_y \frac{\mathrm{d}^2 w}{\mathrm{d}z^2} \right),
\end{equation}
where $w(z)$ is the function describing the shape of the beam, $q$ is the applied load (force per unit length), $E$ the beam's Young's modulus, and $I_y$ its second moment of the area. We'll assume constant $E$ and $I_y$ here. There are various possible boundary conditions on the beam equation, some of which are given in table~\bookref{table:beamboundaryconditions} of the notes.
\else
In this problem, we will solve for the shape of a beam described by the Euler-Bernoulli beam equation, with a constant value of the material parameter $E I_y$. % See http://ocw.nthu.edu.tw/ocw/upload/8/258/Chapter_9-98.pdf for a bunch of examples.
\fi
\begin{center}
\ifproblemset\includegraphics[scale=1]{elasticity/problems/rodbendingproblem2.pdf}\else\includegraphics[scale=1]{elasticity/problems/rodbendingproblem3.pdf}\fi
\end{center}
\begin{enumerate}[(a)]
\item Find the shape of a cantilever beam of length~$L$ and with density~$\rho$, attached horizontally to a wall, and deformed under the force of gravity. % For uniform load $q$, $w(z) = - \frac{q z^2}{24 E I_y} (6 L^2 - 4 L z + z^2)$; gravity gives a uniform load $q = \rho A g$.
\item Find the shape of a beam of length~$L$ supported at both ends, with a point force~$F$ pushing down the middle. % $w(z) = - \frac{F b z}{6 L E I} (L^2 - b^2 - z^2)$, for $0 \leq z \leq a$, and $w(z) = - \frac{F b z}{6 L E I} (L^2 - b^2 - z^2) - \frac{F(z-a)^3}{6 E I}$, for $a \leq z \leq L$ if the force acts a distance a from the left end (= origin) and a+b = L. Here we have a=b=L/2.
%\item Find the maximum deflection~$\delta$ of both deformed beams. % \delta = F L^3 / 48 E I_y for second.
\ifproblemset \else \item Find the shape of an overhanging beam of length~$L$, which is hinged at its left end and simply supported at a distance $\frac34 L$ from the left end, under the action of a point force~$F$ at the right end.\fi
\end{enumerate}
\ifproblemset
You can verify your answers by checking that the maximum deflections of the deformed beams are $\delta = \rho g A L^4 / 8 E I_y$ and $\delta = F L^3 / 48 E I_y$, respectively.
\fi

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item Gravity gives a uniform load of $m g / L = \rho g A$ (taking downwards deflections as positive). We can simply substitute this load into the Euler-Bernouilli beam equation and integrate four times, which gives (with $a = \rho g A / E I_y$):
\begin{align*}
w''''(z) &= a \\
w'''(z) &= \left[ a \left(\frac{z}{L}\right) + b \right] L \\
w''(z) &= \left[\frac{a}{2} \left(\frac{z}{L}\right)^2 + b \left(\frac{z}{L}\right) + c \right] L^2 \\
w'(z) &= \left[ \frac{a}{6} \left(\frac{z}{L}\right)^3 + \frac{b}{2} \left(\frac{z}{L}\right)^2 + c \left(\frac{z}{L}\right) + d \right] L^3 \\
w(z) &= \left[ \frac{a}{24} \left(\frac{z}{L}\right)^4 + \frac{b}{6} \left(\frac{z}{L}\right)^3 + \frac{c}{2} \left(\frac{z}{L}\right)^2 + d \left(\frac{z}{L}\right) + g \right] L^4,
\end{align*}
where $b$, $c$, $d$ and $g$ are integration constants, which we scaled such that they all have the same dimension. 
%\[ w(z) = \left[\frac{a}{24} \left(\frac{z}{L}\right)^4 + \frac{B}{6} \left(\frac{z}{L}\right)^3 + \frac{C}{2} \left(\frac{z}{L}\right)^2 + D \left(\frac{z}{L}\right) + E \right]L^4, \]
%where the capitals are integration constants.
Setting the cantilever boundary conditions that $w(0) = w'(0) = 0$ gives $d = g = 0$. The zero-force and zero-torque boundary conditions at the free end give $w''(L) = w'''(L) = 0$, or
\begin{align*}
0 &= \left(\frac{a}{2} + b + c \right) L^2 \\
0 &= (a + b ) L,
\end{align*}
so $b = - a$ and $c = \frac12 a$. Combined, we find
\[ w(z) = \frac{\rho g A L^4}{E I_y} \left[ \frac{1}{24} \left(\frac{z}{L}\right)^4 - \frac16 \left(\frac{z}{L}\right)^3 + \frac14 \left(\frac{z}{L}\right)^2 \right]. \]
\item Taking $q(z) = - F \delta(z - L/2)$, and integrating the beam equation once, we find
\begin{equation}
\label{EBpointforce}
F = E I_y \frac{\mathrm{d}^3w}{\mathrm{d}z^3}.
\end{equation}
Because the beam is symmetric at $z = L/2$, we first solve for the left part, where $0 \leq z \leq L/2$. At the left side, we have a simple support / free hinge, so the boundary conditions read $w(0) = w''(0) = 0$. At the right side (i.e., at $L/2$) we have a fixed force (given by the equation above, though replacing the force by $F/2$) and a zero angle (by symmetry), so $w'(L/2) = 0$. Integrating the above equation three times, we find
\begin{equation}
\label{EBpointforcegensol}
w(z) = \frac{F}{2E I_y} \left( \frac16 z^3 + B \frac12 z^2 + C z + D \right).
\end{equation}
The boundary conditions at $z=0$ give $B = D = 0$. The boundary condition at $z=L/2$ now gives
\[ w(L/2) = \frac{F}{2E I_y} \left[ \frac12 \left( \frac{L}{2} \right)^2 + C \right] = 0, \]
so $C = -L^2/8$. Substituting the values of the constants, we find for the shape
\[ w(z) = \frac{F}{2E I_y} \left( \frac16 z^3 - \frac{L^2}{8} z \right) = -\frac{FL^3}{48E I_y} \left[ 3 \frac{z}{L} - 4 \left( \frac{z}{L} \right)^3 \right]. \]
Note that at $z=L/2$, we find the maximum deflection, given by $\delta = -w(L/2) = F / 48 E I_y$. For the shape of the right-hand side of the beam, we simply mirror in $z=L/2$, so the shape is given by
\[ w(L-z) = -\frac{FL^3}{48E I_y} \left[ 3 \frac{L-z}{L} - 4 \left( \frac{L-z}{L} \right)^3 \right]. \]
\ifproblemset
\item The maximum deflection of the cantilever beam is at $z=L$, given by $\delta = w(L) = \rho g A L^4 / 8 E I_y$. We already found the maximum deflection of the supported beam as $\delta = -w(L/2) = F / 48 E I_y$.
\else
\item We again have a point force, applied at $z=L$, so our load is $q(z) = - F \delta(z-L)$. However, we cannot simply apply the boundary conditions of table~\bookref{table:beamboundaryconditions}, as we have an extra support in our system. Instead, we need to analyze the force and torque balance of this system with some care. First, we note that in the final equilibrium shape, the torques about the three points of interest ($z=0$, $z=\frac34 L$, and $z=L$) all must vanish. Taking the pivot at $z=0$, the torque due to the force at $z=L$ equals $M = F L$, which must be balanced by a torque from the support at $z=\frac34 L$; the (upwards) force from that point must therefore equal $F_{3/4} = F L / (\frac34 L) = \frac43 F$. Either by another torque balance, or by global force balance, we get that the hinge at the origin must exert a downward force of $\frac13 F$. The easiest way to incorporate these forces is to split our problem in two parts: one for $0 < z < \frac34 L$, where we consider a beam with a point force of magnitude $\frac13 F$ acting at $z=0$, and one for $\frac34 L < z < L$, where we consider a beam with a point force of magnitude $F$ acting at $z=L$. The two pieces must connect smoothly (derivatives must equal at $z=\frac34 L$, i.e. $w(z\uparrow \frac34 L) = w(z \downarrow \frac34 L)$); the net moments at the hinged ends $z=0$ and $z=L$ must vanish (i.e., $w''(0) = w''(L) = 0$), and the position of the beam is fixed at zero at the two supports, i.e. $w(0) = w(z \uparrow \frac34L) = w(z \downarrow \frac34L) = 0$.

NB: You might wonder why we can (seemingly) ignore the upwards force at $z=\frac34 L$. We don't, it just has no moment if we take our pivot there. We did the same for a simple cantilever beam under a point load: the clamping point exerts a force, but if we take the pivot there, it exerts no torque.

The differential equations to solve are identical to~(\ref{EBpointforce}) in (b), as is the general solution~(\ref{EBpointforcegensol}), only the boundary conditions are different. It is however easier in this case to tackle the boundary conditions along with the integration straight away. Splitting our equation in parts for $0 < z < \frac34 L$ and $\frac34 L < z < L$, we get
\begin{alignat*}{3}
-\frac13 F &= E I_y w'''(z) &\qquad \mbox{for} \quad 0 < z < \frac34 L, \\
F &= E I_y w'''(z) &\qquad \mbox{for} \quad \frac34 L < z < L,
\end{alignat*}
where we've taken the upwards direction as positive. Note that we get an extra minus sign for the first term, as the position where the force acts is left of the pivot. Integrating once, we get
\begin{alignat*}{3}
w''(z) &= -\frac{F L}{3 E I_y} \left[\frac{z}{L} + C_1\right] &\qquad \mbox{for} \quad 0 < z < \frac34 L, \\
w''(z) &= \frac{F L}{E I_y} \left[\frac{z}{L} + C_2\right] &\qquad \mbox{for} \quad \frac34 L < z < L.
\end{alignat*}
As the second derivatives must vanish at their respective ends, we get $w''(0) = C_1 = 0$ and $w''(L) = FL / E I_y \left(1 + C_2\right)$, so $C_2 = -1$. Substituting these and integrating again, we get
\begin{alignat*}{3}
w'(z) &= -\frac{F L^2}{3 E I_y} \left[ \frac12 \left(\frac{z}{L}\right)^2 + D_1 \right] &\qquad \mbox{for} \quad 0 < z < \frac34 L, \\
w'(z) &= \frac{F L^2}{E I_y} \left[ \frac12 \left(\frac{z}{L}\right)^2 - \frac{z}{L} + D_2 \right] &\qquad \mbox{for} \quad \frac34 L < z < L.
\end{alignat*}
From the condition that the beam has a continuous derivative at $z=\frac34 L$, we get
\begin{align*}
w'\left(z \uparrow \frac34 L \right) = -\frac{F L^2}{3 E I_y} \left[ \frac12 \left(\frac34\right)^2 + D_1 \right] &= w'\left(z \downarrow \frac34 L \right) = \frac{F L^2}{E I_y} \left[ \frac12 \left(\frac34\right)^2 - \frac34 + D_2 \right] \\
D_2 &= -\frac13\left(\frac12 \frac{9}{16} + D_1 \right) + \frac12 \frac{9}{16} - \frac34 =  -\frac13 D_1 + \frac38.
\end{align*}
Substituting and integrating once more, we find
\begin{alignat*}{3}
w(z) &= -\frac{F L^3}{3 E I_y} \left[ \frac16 \left(\frac{z}{L}\right)^3 + D_1 \frac{z}{L} + G_1  \right] &\qquad \mbox{for} \quad 0 < z < \frac34 L, \\
w(z) &= \frac{F L^3}{E I_y} \left[ \frac16 \left(\frac{z}{L}\right)^3 - \frac12 \left(\frac{z}{L}\right)^2 + \left(-\frac13 D_1 + \frac38 \right) \frac{z}{L} + G_2 \right] &\qquad \mbox{for} \quad \frac34 L < z < L.
\end{alignat*}
Now $w(0) = 0$ gives $G_1 = 0$, and we find $D_1$ and $G_2$ from the conditions at $z = \frac34L$:
\begin{align*}
0 &= w\left(z \uparrow \frac34 L \right) = -\frac{F L^3}{3 E I_y} \left[ \frac16 \left(\frac34\right)^3 + D_1 \frac34 \right] = -\frac{F L^3}{4 E I_y} \left[\frac38 + D_1 \right] \quad \Rightarrow \quad D_1 = -\frac{3}{32}, \\
0 &= w\left(z \downarrow \frac34 L \right) = \frac{F L^3}{E I_y} \left[ \frac16 \left(\frac34\right)^3 - \frac12 \left(\frac34 \right)^2 + \left(\frac13 \frac{3}{32} + \frac38 \right) \frac34 + G_2 \right] = \frac{F L^3}{E I_y} \left[\frac{3}{32} + G_2 \right] \quad \Rightarrow \quad G_2 = -\frac{3}{32}.
\end{align*}
The full solution is thus given by (see figure~\ref{fig:overhangingrodshapesln})
\begin{alignat*}{3}
w(z) &= -\frac{F L^3}{3 E I_y} \left[ \frac16 \left(\frac{z}{L}\right)^3 - \frac{3}{32} \frac{z}{L}  \right] &\qquad \mbox{for} \quad 0 < z < \frac34 L, \\
w(z) &= \frac{F L^3}{E I_y} \left[ \frac16 \left(\frac{z}{L}\right)^3 - \frac12 \left(\frac{z}{L}\right)^2 + \frac{13}{32} \frac{z}{L} - \frac{3}{32} \right] &\qquad \mbox{for} \quad \frac34 L < z < L.
\end{alignat*}
\begin{figure}[ht]
\centering
\includegraphics[scale=1]{elasticity/problems/overhangingrodshape.pdf}
\caption{Shape of the overhanging beam with a point force acting at the free end.}
\label{fig:overhangingrodshapesln}
\end{figure}


\fi
\end{enumerate}
\fi
