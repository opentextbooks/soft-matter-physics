%Problemtitle: The deformation of the Earth under its own gravitational field
%Source: L&L elasticity problem 7.3
In this problem, we will determine the deformation of a uniform solid sphere under its own gravitational field. We take the sphere to have radius~$R$ and mass density~$\rho$; we denote its total mass as $M = \frac43 \pi R^3 \rho$.
\begin{enumerate}[(a)]
\item To find the force of gravity at any point inside the sphere, we use two results from classical mechanics:
\begin{itemize}
\item The gravitational forces on a test mass inside a massive shell of uniform thickness and density exactly cancel out.
\item The gravitational force from a spherical object is equivalent to the gravitational force from a point with the same mass at the center of the sphere.
\end{itemize}
Using these two results, show that the (Newtonian) gravitational force on a test mass of magnitude~$\mu$ at any point inside the sphere is given by
\begin{equation}
\label{gravitationalforcetestmass}
\bvec{F}_z = - \frac43 \pi \rho G r \cdot \mu \unitvec{r} = - g \frac{r}{R} \cdot \mu \unitvec{r},
\end{equation}
where we defined $g = G M / R^2$ (note that here $G$ is Newton's gravitational constant, not the shear modulus).
\item From the equilibrium condition~(\bookref{staticstressdiffeq}) and the three-dimensional version of Hooke's law in terms of the Young's modulus and Poisson ratio~(\bookref{HookeYoungPoisson}), first express the external force in terms of the components $u_i$ of the deformation field, and then re-cast the expression in coordinate-free form, to arrive at
\begin{subequations}
\label{externalforcecoordinatefree}
\begin{align}
\label{externalforcecoordinatefreeV1}
\bvec{f}^\mathrm{ext} &= - \frac{E}{2(1+\nu)} \left[ \nabla^2 \bvec{u} + \frac{1}{1-2\nu} \bvec{\nabla} \left( \bvec{\nabla} \cdot \bvec{u} \right) \right], \\
\label{externalforcecoordinatefreeV2}
&= \frac{E}{2(1+\nu)} \left[ \bvec{\nabla} \times \bvec{\nabla} \times \bvec{u} - 2 \frac{1-\nu}{1-2\nu} \bvec{\nabla} \left( \bvec{\nabla} \cdot \bvec{u} \right) \right],
\end{align}
\end{subequations}
where we used the vector identity $\nabla^2 \bvec{u} = \bvec{\nabla} \left( \bvec{\nabla} \cdot \bvec{u} \right) - \bvec{\nabla} \times \bvec{\nabla} \times \bvec{u}$.
\item Use a symmetry argument to explain why the double curl term in equation~(\ref{externalforcecoordinatefreeV2}) vanishes.
\item \label{pb:spheregravitationalfieldode} The external force in equation~(\ref{externalforcecoordinatefree}) is a force per unit volume. To translate the gravitational force of equation~(\ref{gravitationalforcetestmass}) to a force per unit volume, we simply take as our test mass the mass of a small unit of volume of our sphere; then the mass per unit volume is simply the density~$\rho$, and our external force is $\bvec{f}^\mathrm{ext} = - \rho g (r/R) \unitvec{r}$. For the remaining term on the right-hand side, use that in spherical coordinates, for a displacement function $u$ that only depends on the radial position, we have
\begin{equation}
\bvec{\nabla} \left( \bvec{\nabla} \cdot \bvec{u} \right) = \unitvec{r} \partial_r \left( \frac{1}{r^2} \partial_r \left( r^2 u_r \right) \right),
\end{equation}
to write down an ordinary differential equation for $u_r(r)$.
\item \label{pb:spheregravitationalfieldbc} Write down the boundary conditions for the differential equation in (\ref{pb:spheregravitationalfieldode}).
\item Solve the differential equation for $u_r(r)$ from (\ref{pb:spheregravitationalfieldode}) under the boundary conditions from~(\ref{pb:spheregravitationalfieldbc}).
\item Plot the displacement field $u_r(r)$ and the $rr$ component of the strain tensor as a function of $r/R$.
\item Find the pressure at the center of the sphere. \textit{Hint}: you may find the relation $E = 3 K (1-2\nu)$ for the Young's modulus, bulk modulus and Poisson ratio useful.
\end{enumerate}

\ifincludesolutions
%\clearpage
\Solution
\begin{enumerate}[(a)]
\item We put our test mass at a distance $r$ from the center of the sphere (centered at the origin). The parts of the sphere further away from the origin form a uniform shell; by the first result their gravitational forces cancel out. The part of the sphere closer to the origin form a smaller sphere with radius $r$ and mass $m(r) = \frac43 \pi r^3 \rho$, which, by the second result, exert a gravitational force on the test mass of magnitude
\begin{equation}
\label{gravitationalforcetestmasssln}
\bvec{F}_z = - \frac{G m(r)}{r^2} \cdot \mu \unitvec{r} = - \frac43 \pi G \rho r \cdot \mu \unitvec{r} = - \frac{GM}{R^2} \frac{r}{R} \cdot \mu \unitvec{r} = - g \frac{r}{R} \cdot \mu \unitvec{r}.
\end{equation}
\item We have
\begin{subequations}
\begin{align}
f_i^\mathrm{ext} &= \partial_j \sigma_{ij} = \frac{E}{2(1+\nu)} \left[ \partial_j \left(\partial_i u_j + \partial_j u_i \right) + \frac{\nu}{1-2\nu} \partial_i \left(2 \partial_k u_k \right) \right] \nonumber \\
&= \frac{E}{2(1+\nu)} \left[ \partial_j \partial_j u_i + \left(1 + \frac{2\nu}{1-2\nu} \right) \partial_i \partial_j u_j \right] \\
\bvec{f}^\mathrm{ext} &= - \frac{E}{2(1+\nu)} \left[ \nabla^2 \bvec{u} + \frac{1}{1-2\nu} \bvec{\nabla} \left( \bvec{\nabla} \cdot \bvec{u} \right) \right].
\end{align}
\end{subequations}
\item The problem is spherically symmetric, therefore there can only be deformations in the radial direction, and the amount of deformation can only depend on the radial position. Hence we have $\bvec{u}(\bvec{r}) = u_r(r) \unitvec{r}$, which is a radial field with zero curl.
\item Substituting the given external field and expression for the gradient of the divergence in~(\ref{externalforcecoordinatefreeV2}), we get
\begin{equation}
\label{spheregravitationalfieldode}
\rho g \frac{r}{R} = \frac{E(1-\nu)}{(1+\nu)(1-2\nu)} \partial_r \left( \frac{1}{r^2} \partial_r \left( r^2 u_r \right) \right)
\end{equation}
\item As there is no force / pressure acting on the surface of the sphere, we have $\sigma_{ij}(r=R) = 0$. Because the sphere undergoes a pure compression, the off-diagonal components of the stress are zero throughout, and for the on-diagonal components we have (using the expressions~(\bookref{strainsphericalcoords}) for the components of the strain tensor in spherical coordinates):
\begin{equation}
\sigma_{rr} = \sigma_{\theta\theta} = \sigma_{\phi \phi} = \frac12 K \Tr(\bvec{\gamma}) = K \left( \pdv{u_r}{r} + 2 \frac{u_r}{r} \right).
\end{equation}
Our boundary condition thus becomes $u_r'(r=R) + 2u_r(R)/R = 0$. Moreover, as we are uniformly compressing the sphere, the displacement at the origin should vanish, so we also have $u_r(r=0) = 0$.
\item We can simply integrate equation~(\ref{spheregravitationalfieldode}) twice, meanwhile moving factors to the other side:
\begin{align}
\label{spheregravitationalfieldodeintegration}
\int \partial_r \left( \frac{1}{r^2} \partial_r \left( r^2 u_r \right) \right) \dd{r} &= \int \frac{\rho g}{E R} \frac{(1+\nu)(1-2\nu)}{1-\nu} \int r \dd{r} = \frac{\rho g}{2 E R} \frac{(1+\nu)(1-2\nu)}{1-\nu} \left(r^2 + C\right), \nonumber \\
\int \partial_r \left( r^2 u_r \right) \dd{r} &= \frac{\rho g}{2 E R} \frac{(1+\nu)(1-2\nu)}{1-\nu} \int \left( r^4 + C r^2 \right) \dd{r} = \frac{\rho g}{2 E R} \frac{(1+\nu)(1-2\nu)}{1-\nu} \left( \frac15 r^5 + \frac13 C r^3 + D \right) \nonumber \\
u_r(r) &= \frac{\rho g}{2 E R} \frac{(1+\nu)(1-2\nu)}{1-\nu} \left( \frac15 r^3 + \frac13 C r + D \frac{1}{r^2} \right),
\end{align}
where $C$ and $D$ are integration constants. The last term with $D$ blows up at the origin, which sets $D=0$, satisfying our second boundary condition. The first boundary condition gives us the value of the integration constant $C$:
\begin{subequations}
\begin{align}
\left[\pdv{u_r}{r} + 2 \frac{u_r}{r} \right]_{r=R} &= \frac{\rho g}{2 E R} \frac{(1+\nu)(1-2\nu)}{1-\nu} \left( \frac35 R^2 + \frac13 C + \frac25 R^2 + \frac23 C \right) = 0,\\
C &= - R^2.
\end{align}
\end{subequations}
For the full displacement field, we thus have
\begin{equation}
u_r(r) = \frac{\rho g R^2}{2 E} \frac{(1+\nu)(1-2\nu)}{1-\nu} \left[ \frac15 \left(\frac{r}{R}\right)^3 - \frac13 \frac{r}{R} \right].
\end{equation}
% Note that Landau & Lifshitz have a different answer, with a strange factor involving \nu. If we set \nu=\frac12, the answers are identical.
\item For the $rr$ component of the strain tensor we find
\begin{equation}
\gamma_{rr}(r) = 2 \pdv{u_r}{r} = \frac{\rho g R}{E} \frac{(1+\nu)(1-2\nu)}{1-\nu} \left[ \frac35 \left(\frac{r}{R}\right)^2 - \frac13 \right].
\end{equation}
The displacement field and strain are shown in figure~\ref{fig:spheregravitationalfield} below.
\begin{figure}
\centering
\includegraphics[scale=.8]{elasticity/problems/spheregravitationalfield.pdf}
\caption{The deformation and strain of the uniform sphere under the action of its own gravitational field.}
\label{fig:spheregravitationalfield}
\end{figure}
\item The pressure at the center of the sphere is given by (minus) the diagonal elements of the stress tensor. For the stress tensor at the center we find:
\begin{align*}
\left.\sigma_{ij}\right|_{r=0} &= \frac12 K \left.\Tr(\gamma_{ij})\right|_{r=0} \delta_{ij} \\
&= \frac{\rho g R}{2 E} \frac{(1+\nu)(1-2\nu)}{(1-\nu)} \left(-\frac13\right) K \Delta_{ij} \\
&= - \frac{\rho g R}{18} \frac{1+\nu}{1-\nu} \delta_{ij},
\end{align*}
where we used the hint in the last line. Thus
\begin{align*}
p &= - \sigma_{rr} = \frac{\rho g R}{18} \frac{1+\nu}{1-\nu}.
\end{align*}
\end{enumerate}
\fi