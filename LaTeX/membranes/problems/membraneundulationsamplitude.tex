% Problemtitle Amplitude of membrane undulations
% Source Based on Boal Sect 8.5 / Pb. 8.18.
In section~\bookref{sec:membranefluctuations}, we calculated the thermal average of the amplitudes of the modes $u_\mathrm{q}$ of a fluctuating membrane as
\begin{equation}
\label{uFmodethermalaverage}
\ev{|u_q|^2} = \ev{u_{\bvec{q}} u_{\bvec{q}}^*} = \frac{1}{L^2} \frac{\kB T}{\kappa q^4 + \sigma q^2}.
\end{equation}
In this problem, we'll calculate the amplitude of thermal undulations in real space, using this result.
\begin{enumerate}[(a)]
\item Expanding $u(\bvec{x})$ in Fourier modes as before
\[ u(\bvec{x}) = \sum_{\bvec{q}} u_{\bvec{q}} e^{i \bvec{q} \cdot \bvec{x}}, \]
find an expression for $u(\bvec{x})^2$ in Fourier modes. NB: take care that your expression gives a real number.
\item Evaluate the positional average of $u(\bvec{x})^2$ by integrating it over the membrane area:
\[ \overline{u(\bvec{x})^2} = \frac{1}{L^2} \int u(\bvec{x})^2 \ddx \ddy. \]
Your resulting expression should contain a single sum over the wavevector~$\bvec{q}$.
\item Take the thermal average of $\overline{u(\bvec{x})^2}$, in which you can substitute~(\ref{uFmodethermalaverage}).
\item To evaluate the sum, convert it to an integral, with as usual a `volume' of $2\pi/L$ per mode. Re-write the integral in polar coordinates, with the length of~$q$ running from $\pi/L$ to $\pi/a$. Evaluate the (trivial) angular integral.
\item Finally, evaluate the $q$ integral (feel free to use a computer; you can do it by hand by partial fraction decomposition). In the limit that $a \ll \lambda$, show that your result becomes
\[ \ev{\overline{u(\bvec{x})^2}} = \frac{\kB T}{4 \pi \sigma} \log\left(1 + \frac{\sigma L^2}{\pi^2 \kappa} \right). \]
\end{enumerate}


\ifincludesolutions
\ifproblemset\newpage\fi
\Solution
\begin{enumerate}[(a)]
\item We have
\begin{align*}
u(\bvec{x}) &= \sum_{\bvec{q}} u_{\bvec{q}} e^{i \bvec{q} \cdot \bvec{x}},
\left(u(\bvec{x})\right)^2 = \sum_{\bvec{q},\bvec{q}'} u_{\bvec{q}} u^*_{\bvec{q}'} e^{i \bvec{q} \cdot \bvec{x}} e^{-i \bvec{q}' \cdot \bvec{x}}.
\end{align*}
\item Substituting the expression from (a) in the given integral, we find
\begin{align*}
\overline{u(\bvec{x})^2} &= \frac{1}{L^2} \int u(\bvec{x})^2 \ddx \ddy = \frac{1}{L^2} \sum_{\bvec{q},\bvec{q}'} u_{\bvec{q}} u^*_{\bvec{q}'} \int e^{i (\bvec{q}-\bvec{q}') \cdot \bvec{x}} \ddx \ddy 
= \sum_{\bvec{q},\bvec{q}'} u_{\bvec{q}} u^*_{\bvec{q}'} \delta_{\bvec{q} \bvec{q}'} = \sum_{\bvec{q}} u_{\bvec{q}} u^*_{\bvec{q}}.
\end{align*}
\item We get
\[ \ev{\overline{u(\bvec{x})^2}} = \sum_{\bvec{q}} \ev{u_{\bvec{q}} u^*_{\bvec{q}}} = \sum_{\bvec{q}} \frac{1}{L^2} \frac{\kB T}{\kappa q^4 + \sigma q^2}. \]
\item Following the indicated steps, we find
\[ \ev{\overline{u(\bvec{x})^2}} = \kB T \int \frac{\dd^2 \bvec{q}}{(2\pi)^2} \frac{1}{\kappa q^4 + \sigma q^2} = \kB T \int_{\pi/L}^{\pi/a} q \frac{\dd q}{2\pi} \int_0^{2\pi} \frac{\dd\theta}{2\pi} \frac{1}{\kappa q^4 + \sigma q^2} = \frac{\kB T}{2\pi} \int_{\pi/L}^{\pi/a} \frac{q}{\kappa q^4 + \sigma q^2} \dd q. \]
\item To evaluate the integral, we need to decompose $\frac{1}{q(1+\lambda^2 q^2)}$, where $\lambda^2 = \kappa/\sigma$. We have
\[ \frac{1}{q(1+\lambda^2 q^2)} = \frac{A}{q} + \frac{B}{1 + \lambda^2 q^2} = \frac{A + Bq + A \lambda^2 q^2}{q(1+\lambda^2 q^2)}, \]
so $A=1$ and $Bq = -\lambda^2 q^2$ or $B = -\lambda^2 q$. The integral is then given by
\begin{align*}
\ev{\overline{u(\bvec{x})^2}} &= \frac{\kB T}{2\pi\sigma} \int_{\pi/L}^{\pi/a} \left( \frac{1}{q} - \frac{\lambda^2 q}{1 + \lambda^2 q^2} \right) \dd q \\
&= \frac{\kB T}{2\pi\sigma} \left[ \log(q) - \frac12 \log\left(1+\lambda^2 q^2\right) \right]_{\pi/L}^{\pi/a} = \frac{\kB T}{2\pi\sigma} \left. \log\left(\frac{q}{\sqrt{1+\lambda^2 q^2}}\right) \right|_{\pi/L}^{\pi/a} \\
&=  \frac{\kB T}{2\pi\sigma} \log\left( \frac{\pi/a}{\pi/L} \sqrt{\frac{1 + \lambda^2 \pi^2 / L^2}{1 + \lambda^2 \pi^2 / a^2}} \right) = \frac{\kB T}{4\pi\sigma} \log\left( \frac{L^2 + \pi^2 \lambda^2}{a^2 + \pi^2 \lambda^2} \right)  \\
&\approx  \frac{\kB T}{4\pi\sigma} \log\left( 1 + \frac{\sigma L^2}{\pi^2 \kappa} \right),
\end{align*}
where the last approximation holds if $a \ll \lambda$.
\end{enumerate}
\fi
