% Problemtitle Model of virus particle uptake
% Source: Suggestion by Felix (?).
In this exercise we consider a simple model for the uptake of a non-enveloped spherical virus particle of radius $R$ at the plasma membrane of a cell. Because the virus particle is covered with ligands it experiences an adhesion energy upon making contact with the membrane that is decorated with suitable receptors. We assume that the ligands on the virus particle are distributed homogeneously. Therefore we do not consider adhesion on single ligand-receptor contacts in the model. Instead we assume that the adhesion strength is determined by the energy~$W$ per adhered area. The progress of the uptake process is described by the uptake angle $\theta$. The situation considered in the model is shown in Figure~\ref{fig:virusparticleuptake}.

\begin{figure}[ht]
\centering
\includegraphics[scale=3]{membranes/problems/virusparticleuptakesketch.pdf}
\caption[Model of virus particle uptake.]{Model of virus particle uptake. A virus particle (blue) of radius $R$ is wrapped by the membrane (gray). The adhered part of membrane is shown in red. The uptake angle $\theta$ describes the progress of the uptake process.}
\label{fig:virusparticleuptake}
\end{figure}

While the adhesion between the virus particle and the membrane drives uptake, it costs energy to bend the membrane and pull out excess area. The total energy of the membrane plus virus particle is then given by
\begin{equation}
\label{fullenergywithadhesion}
E = E_\mathrm{bend} + E_\mathrm{ten} + E_\mathrm{ad} = \frac{\kappa}{2} \int (2H)^2 \dd{A} + \sigma \int \dd{A} - W \int_{A_\mathrm{adh}} \dd{A},
\end{equation}
where $A_\mathrm{adh}$ is the `adhered area' where the membrane is directly in contact with the virus particle. Note that we have put a minus sign in front of the adhesion energy, reflecting the fact that adhesion lowers the total energy.

For the purpose of this problem, we'll ignore deformations of the membrane outside the adhered region.
\begin{enumerate}[(a)]
\item \label{pb:adheredmembraneexcessenergyeq} Write down the (excess) total energy of the membrane + adhered particle as a function of the radius~$R$ of the particle and the adhesion angle~$\theta$ (`excess' energy here means that you consider the energy difference between the deformed and the undeformed state). You may use that the area of a spherical cap with opening angle~$\theta$ is given by $A=2 \pi R^2 (1-\cos \theta)$.
\item \label{pb:adheredmembraneexcessenergyplot} For a fixed radius, plot the excess total energy as a function of the uptake angle~$\theta$, for three combinations of the parameters that give qualitatively different cases. Feel free to use a computer or graphic calculator to make your plots and play around with the parameters. If you make a plot with the computer, you can paste (a picture or screenshot of it) in your solutions; in that case do indicate which parameters you used for which plot.
\item From your plot at (\ref{pb:adheredmembraneexcessenergyplot}), physically interpret the three qualitatively different cases.
\item From your energy at (\ref{pb:adheredmembraneexcessenergyeq}), find the minimum radius the virus particle must have for complete uptake. \textit{Hint}: Think about (and write down!) what the minimum condition is in terms of the energy.
\item \label{pb:wrappinguptakeminimalsizeexplicit} Find the minimal radius for the uptake of a virus with $W = 0.1 \;\mathrm{mJ} \cdot \mathrm{m}^{-2}$ at a tensionless membrane with $\kappa = 25\,\kB T$ at room temperature.
\item How does your answer at (\ref{pb:wrappinguptakeminimalsizeexplicit}) change qualitatively when the membrane is under tension?
\end{enumerate}

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item For the three terms we get:
\begin{itemize}
\item Bending energy: 
\[ E_\mathrm{bend} = \frac{\kappa}{2} \int (2H)^2 \dd{A} = 2 \kappa \frac{1}{R^2} 2 \pi R^2 (1-\cos \theta) = 4 \pi \kappa (1-\cos \theta) \]
\score{2 pt}.
\item For the tension energy, we first determine the excess area: 
\[ \Delta A = A_\mathrm{wrapped}-A_\mathrm{projected} = 2 \pi R^2 (1-\cos \theta)-\pi R^2 \sin^2 \theta = \pi R^2 (1-\cos \theta)^2 \] \score{1 pt}. The tension energy is then given by 
\[ E_\mathrm{ten} = \sigma \pi R^2 (1-\cos \theta)^2 \] \score{1 pt}.
\item For the adhesion energy, we can simply write
\[ E_\mathrm{ad} = - W A_\mathrm{adh} = -W 2 \pi R^2 (1-\cos \theta) \]
\score{1 pt}.
\end{itemize}
The total energy thus reads
\begin{equation}
\label{adheredparticletotalenergy}
E_\mathrm{tot}=E_\mathrm{ad}+E_\mathrm{bend}+E_\mathrm{ten}= -2 \pi W R^2 (1-\cos \theta)+
4 \pi \kappa (1-\cos \theta)+\sigma \pi R^2 (1-\cos \theta)^2.
\end{equation}
\item See figure~\ref{fig:adheredparticletotalenergyplot} \score{3 pt}.
\begin{figure}
\centering
\includegraphics[scale=1]{membranes/problems/adheredparticletotalenergyplot.pdf}
\caption{Qualitatively different cases of the total energy~(\ref{adheredparticletotalenergy}) of virus particle adhered to a membrane, in units of $2 \pi \kappa$. $R=1$ for all three plots. Blue line: $W/\kappa = 4$, $\sigma/\kappa = 1$, orange line: $W/\kappa = 1$, $\sigma/\kappa = 1$, green line: $W/\kappa = 4$, $\sigma/\kappa = 2.5$.}
\label{fig:adheredparticletotalenergyplot}
\end{figure}
\item \begin{itemize}
\item Blue case (large adhesion~$W$, small tension~$\sigma$): adhesion energy dominates, particle spontaneously gets fully wrapped \score{1 pt}.
\item Orange case (small adhesion~$W$): tension and/or bending energy dominate over adhesion, no wrapping \score{1 pt}.
\item Green case (comparable adhesion and tension energies): While adhesion initially favors wrapping, the increasing tension energy counters it, resulting in partial wrapping \score{1 pt}.
\end{itemize}
\item The particle will be wrapped to the angle at which the energy has its minimum. For the particle to be fully wrapped, this angle must be $\pi$ \score{1 pt}. To find the wrapping angle, we simply take the derivative of the energy and equate it to zero:
\begin{align*}
0 &= \pdv{E}{\theta} = 4 \pi \kappa \sin(\theta) - 2 \pi W R^2 \sin(\theta) + 2 \pi \sigma R^2 (1-\cos(\theta))\sin(\theta) \\
&= 2 \pi \sin(\theta) \left[2 \kappa - W R^2 + \sigma R^2 (1-\cos(\theta)) \right].
\end{align*}
The energy therefore has an extremum either if $\sigma(\theta) = 0$, which is at $\theta = 0$ or $\theta = \pi$, or if
\[ \cos(\theta) = 1 - \frac{W R^2 - 2 \kappa}{\sigma R^2}. \]
We transition from the green to the blue curve (i.e., go from partial wrapping to full wrapping) when this equation gives $\theta = \pi$, i.e., when
\[ \frac{W R^2 - 2 \kappa}{\sigma R^2} = 2 \quad \Rightarrow \quad R = \sqrt{\frac{2\kappa}{W - 2 \sigma}} \]
\score{2 pt}.
%\item The minimum radius necessary for wrapping is that radius for which the green curve in figure~\ref{fig:adheredparticletotalenergyplot} just touches the $E=0$ axis at $\theta = \pi$ \score{1 pt for formulating the correct condition, or by using it, showing that the student understood that this is the condition}. (Usually we'd demand that the derivative of the energy also be zero at that point, but here that will turn out always to be the case, independent of $R$). We get
%\begin{align*}
%0 &= E(\theta) = - 4 \pi R^2 W + 8 \pi \kappa + 4 \pi R^2 \sigma = 4 \pi \left[ 2 \kappa - \left(W-\sigma\right)R^2 \right],\\
%R &= \sqrt{\frac{2\kappa}{W-\sigma}}
%\end{align*}
%\score{1 pt}.
\item 
\[ R = \sqrt{\frac{2 \kappa}{W}}= \sqrt{\frac{2 \cdot 25 \cdot 4.1 \;\mathrm{pN}\,\mathrm{nm}}{0.1\;\mathrm{mJ}\cdot\mathrm{m}^{-2}}} = 45\;\mathrm{nm} \]
\score{2 pt}.
\item For nonzero tension, the minimum radius for full uptake increases, as we effectively subtract the tension from the adhesion energy \score{2 pt}.
\end{enumerate}
\fi



%Because the area of a spherical cap with opening angle~$\theta$ is given by $A=2 \pi R^2 (1-\cos \theta)$, we can write the adhesion energy as (with a minus sign indicating that adhesion is favored):
%\begin{equation}
%\label{adhesionenergy}
%E_\mathrm{ad}=-W A= -W 2 \pi R^2 (1-\cos \theta).
%\end{equation}