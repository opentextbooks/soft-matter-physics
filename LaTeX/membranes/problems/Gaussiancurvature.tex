% Problemtile Gaussian curvature
Show that the integral over the Gaussian curvature is a constant for an arbitrary closed axisymmetric surface. 
\ifproblemset\ifincludesolutions\else\textit{Hint}: first read section~\bookref{sec:CHaxisymmetric} of the notes, which I've summarized below.

\section*{Some notes on axisymmetric vesicles}
Axisymmetric vesicles are closed membranes whose shape is invariant under rotations about a symmetry axis, which we'll take to be the $z$-axis. The shape of such a vesicle can be parametrized by cylindrical-like coordinates: the distance to the rotation axis~$r$ and the rotation angle~$\phi$. Alternatively, the shape can be parametrized by the same rotation angle and the contour length~$s$ as measured along the vesicle shape from one of the poles, as we'll do here (figure~\bookref{fig:axisymmetriccoordinates}b in the notes). We then have $\bvec{r}(s, \phi) = (r(s) \cos\phi, r(s) \sin\phi, z(s))$. It turns out to be quite handy to introduce the contact angle $\psi(s)$ as the angle between the line tangent to the vesicle in the $rz$ plane and the horizontal ($r$-direction). Basic geometry then gives (with dots denoting derivatives to $s$):
\begin{equation}
\label{axisymgeometry}
\dot{r} = \frac{\dd r}{\dd s} = \cos\psi(s), \qquad \dot{z} = \frac{\dd z}{\dd s} = - \sin\psi(s).
\end{equation}
Expressing the mean and Gaussian curvatures and the area element in terms of $s$ is straightforward and gives
\begin{equation}
\label{axisymcurvatures}
H = \frac12 \left(\dot{\psi} + \frac{\sin\psi(s)}{r(s)} \right), \qquad K = \frac{\sin\psi(s)}{r(s)}\dot{\psi}, \qquad \dd A = r \dd s \dd \phi.
\end{equation}
The Canham-Helfrich free energy (including the area and volume terms) can now be written as the one-dimensinonal integral over a Lagrangian:
\begin{align}
\mathcal{E}_\mathrm{CH} &= 2 \pi \kappa \int_{s_1}^{s_2} \mathcal{L} \dd s, \\
\label{HelfrichLagrangian}
\mathcal{L} &= \frac{r}{2}\left(\dot \psi + \frac{\sin \psi}{r} \right)^2 + \frac{\sigma}{\kappa} r + \frac{p}{2\kappa} r^2 \sin\psi + \gamma (\dot r - \cos\psi) + \eta (\dot z + \sin\psi).
\end{align}
In equation~(\ref{HelfrichLagrangian}), we introduced two Lagrange multipliers~$\gamma$ and~$\eta$ to enforce the geometrical relations~(\ref{axisymgeometry}). The Euler-Lagrange equations for~(\ref{HelfrichLagrangian}) now give rise to a second-order ode in $\psi(s)$ which in principle gives us the shape of the axisymmetric vesicle, but unfortunately can only be solved numerically.
\fi \fi

\ifincludesolutions
\Solution
Expressing the integral over the Gaussian curvature in terms of the axisymmetric coordinate system we have (with $s_\mathrm{max}$ the value of the arc length at the south pole)
\begin{align*}
\int K \dd{A} &= 2\pi \int_{0}^{s_\mathrm{max}} \frac{\sin(\psi(s))}{r(s)} \dot{\psi} r \dd{s} = 2\pi \int_{0}^{s_\mathrm{max}} \sin(\psi(s)) \dot{\psi} \dd{s} \\
&= 2\pi \int_{\psi=0}^{\psi=\pi} \sin(\psi) \dd{\psi} = 2 \pi \left[-\cos(\psi)\right]_{\psi=0}^{\psi=\pi} \\
&= 4 \pi,
\end{align*}
which is independent of the vesicle shape, and (unsurprisingly) the same value as we got for a sphere. Note that we tacitly assumed a spherical topology here; we can do a toroidal topology in the same way, by integrating `full circle'. In that case, $s_\mathrm{max}$ is the value of $s$ at which the line describing the shape comes back to the north pole, and the associated value of $\psi$ is $2\pi$; the answer in that case is $0$, consistent with Gauss-Bonnet. There are no axisymmetric shapes with genus larger than one.
\fi