% Problemtitle Micropipette aspiration of lipid bilayer vesicles
% Source Exam 2018. Based on a suggestion by Edwin Erdem and Bas van der Hoeven
A commonly used technique to experimentally determine the material properties of lipid vesicles is by micropipette aspiration. The idea behind this technique is to use mechanical force to probe the vesicle's ability to deform. The mechanical force is applied by sucking part of the vesicle into a micropipette of radius smaller than the radius of the vesicle, as sketched in Figure~\ref{fig:pipetteaspiration}a. The relation between the geometry of the vesicle and the pipette pressure can be used to calculate and control the surface tension.

\begin{figure}[ht]
	\centering
	\includegraphics{membranes/problems/pipetteaspirationsketch2.pdf}
	\caption[Micropipette aspiration.]{Micropipette aspiration with relevant geometric parameters. (a) The diameter of the pipette is~$d$, the radius of the remaining (spherical) vesicle is $R_\mathrm{V}$, and the aspired material is modeled as a cylinder with length $L$ connected to a spherical cap with radius $R_\mathrm{p}$. (b) Properties of a spherical cap. You may use without proof that the area of the cap is given by $A_\mathrm{cap} = 2\pi r h = \pi (a^2 + h^2) = 2 \pi r^2 (1-\cos\theta)$.}
	\label{fig:pipetteaspiration}
\end{figure}

\begin{enumerate}[(a)]
\item Let the pressure in the fluid inside the vesicle be $p_0$, and the pressure in the fluid surrounding it be $p_1$ before aspiring. Suppose now that you've applied an pressure $p_2 = p_1 + \Delta p$ in the micropipette. Show that the surface tension of the aspired vesicle is then given by
\begin{equation}
\label{aspiredvesiclesurfacetension}
\sigma = \frac{\Delta p}{2} \frac{R_\mathrm{p}}{1-(R_\mathrm{p}/R_\mathrm{V})}.
\end{equation}
\textit{Hint}: Remember the Young-Laplace law for the surface tension of a spherical drop.
\item Find the surface area of the aspirated vesicle. You may use any of the expressions for the area of a spherical cap given in figure~\ref{fig:pipetteaspiration}b.
\item Show that the bending energy of the aspired vesicle in figure~\ref{fig:pipetteaspiration} is given by
\begin{equation}
\label{aspiredvesiclebendingenergy}
\mathcal{E}_\mathrm{bend} = 8 \pi \kappa + \pi \kappa \frac{L}{R_\mathrm{p}} + 4 \pi \kappa\sqrt{1-\frac{R_\mathrm{p}^2}{R_\mathrm{V}^2}}.
\end{equation}
\item Why can we ignore the Gaussian curvature of the vesicle in the calculation of its deformation energy?
\item Unfortunately, we cannot measure the bending energy directly, so we cannot use the results of this problem to determine the bending modulus~$\kappa$. Suggest a possible additional measurement we could do to determine $\kappa$, and indicate how that measurement would give you the value of the bending modulus.
\end{enumerate}

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item The Young-Laplace law states that $\sigma = \Delta p / 2R$. We apply this law to both the vesicle and the aspirated region, which gives:
\[ p_1-p_0 = \frac{2\sigma}{R_\mathrm{V}}, \quad \mbox{and} \quad p_2-p_0 = \frac{2\sigma}{R_\mathrm{p}} \]
\score{1 pt}. In equilibrium, the surface tension (= 2D pressure) must be the same throughout the membrane, so both terms have the same value of $\sigma$. We can now simply subtract the first expression from the second to get
\[ \Delta p = p_2-p_1 = \frac{2\sigma}{R_\mathrm{p}} - \frac{2\sigma}{R_\mathrm{V}} = \frac{2\sigma (R_\mathrm{V}-R_\mathrm{p}) }{R_\mathrm{V}R_\mathrm{p}}, \]
\score{0.5 pt} which we can re-write to the given expression
\[ \sigma = \frac{\Delta p}{2} \frac{R_\mathrm{V}R_\mathrm{p}}{R_\mathrm{V}-R_\mathrm{p}} = \frac{\Delta p}{2} \frac{R_\mathrm{p}}{1-R_\mathrm{p}/R_\mathrm{V}}\]
\score{0.5 pt}.
\item The area of the aspirated vesicle is the sum of the area of a hemisphere with radius~$R_\mathrm{p}$, a cylinder with radius $R_\mathrm{p}$ and length~$L$, and a sphere with radius~$R_\mathrm{V}$ minus a cap with base radius~$R_\mathrm{p}$ \score{1 pt}. To be able to use any of the equations for the cap area given in figure~\ref{fig:pipetteaspiration}b we need either the height of the cap or it's opening angle; the latter is easily found as $\sin \theta = R_\mathrm{p} / R_\mathrm{V}$. Substituting $\cos \theta = \sqrt{1-\sin^2\theta} = \sqrt{1-(R_\mathrm{p} / R_\mathrm{V})^2}$, we find for the total area:
\begin{align}
\label{aspiratedvesiclearea}
A_\mathrm{vesicle} &= 2 \pi R_\mathrm{p}^2 + 2 \pi R_\mathrm{p} L + 4 \pi R_\mathrm{V}^2 - 2 \pi \left( R_\mathrm{V}^2 - R_\mathrm{V} \sqrt{R_\mathrm{V}^2 - R_\mathrm{p}^2} \right) \nonumber \\
&= 2 \pi R_\mathrm{p} (R_\mathrm{p} + L) + 2 \pi R_\mathrm{V} \left(R_\mathrm{V} + \sqrt{R_\mathrm{V}^2 - R_\mathrm{p}^2} \right)
\end{align}
\score{1 pt}.
\item The bending energy of the vesicle is the sum of the bending energy of a hemisphere with radius~$R_\mathrm{p}$, a cylinder with radius $R_\mathrm{p}$ and length~$L$, and a sphere with radius~$R_\mathrm{V}$ minus a cap with base radius~$R_\mathrm{p}$. For the sphere, we have $E_\mathrm{bend} = 8 \pi \kappa$, so for a hemisphere we have half that \score{0.5 pt}. For the cylinder we have $E_\mathrm{bend} = \pi \kappa L / R_\mathrm{p}$ \score{0.5 pt}. For the remaining part of the vesicle, we have the bending energy of a sphere, minus that of the spherical cap, which still has constant (mean) curvature of $2/R_\mathrm{V}$, which for the energy is integrated over the area of the cap (calculated in b) \score{0.5 pt}. Putting all together we have
\begin{align}
E_\mathrm{bend} &= 4 \pi \kappa + \pi \kappa \frac{L}{R_\mathrm{p}} + 8 \pi \kappa - \frac{2 \kappa}{R_\mathrm{V}^2} A_\mathrm{cap} \nonumber \\
&= 4 \pi \kappa + \pi \kappa \frac{L}{R_\mathrm{p}} + 8 \pi \kappa - \frac{4 \pi \kappa}{R_\mathrm{V}^2} \left( R_\mathrm{V}^2 - R_\mathrm{V} \sqrt{R_\mathrm{V}^2 - R_\mathrm{p}^2} \right) \nonumber \\
&= 8 \pi \kappa + \pi \kappa \frac{L}{R_\mathrm{p}} + 4 \pi \kappa\sqrt{1-\frac{R_\mathrm{p}^2}{R_\mathrm{V}^2}}
\end{align}
\score{0.5 pt}.
\item Because the topology of the vesicle does not change during the asipiration process, the Gauss-Bonnet theorem tells us that the integral over the Gaussian curvature over the whole vesicle remains constant \score{1 pt}.
\item \textit{Option 1}: You could measure the fluctuation spectrum of the membrane, from which (as discussed in class), we can obtain the bending modulus and surface tension directly \score{1 pt}.\\
\textit{Option 2}: You could use a bead trapped in an optical tweezer to extract a membrane tether from the aspirated vesicle, and measure the required force. Once the tether is formed, the force is a constant depending on surface tension and bending modulus ($f = 2\pi \sqrt{2\kappa\sigma}$ to be precise). With the surface tension from the shape and applied pressure, we can then extract the bending modulus \score{1 pt}.
\end{enumerate}
\fi