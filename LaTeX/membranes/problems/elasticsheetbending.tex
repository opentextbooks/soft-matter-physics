% Problemtitle Bending of an elastic sheet
% Based on Boal 7.16 and 7.17
Consider a thin solid sheet of thickness~$d$ made from an isotropic material having a Young's modulus~$E$ and a Poisson ratio~$\nu$ (figure~\ref{membranefig1}a). When subjected to a pressure~$P$ along its edges, it deforms according to a strain tensor whose only non-vanishing components are (Section 5 of Landau and Lifshitz, 1986):
\begin{equation}
\label{sheetcompression}
\gamma_{xx} = \gamma_{yy} = - \frac{2P}{E}(1-\nu), \qquad \gamma_{zz} = \frac{4 P \nu}{E}.
\end{equation}
\begin{enumerate}[(a)]
\item Prove that the three-dimensional energy density (i.e., the energy per unit volume) of this deformation is given by
\begin{equation}
F_\mathrm{3D} = \frac{1-\nu}{E} P^2.
\end{equation}
You may use that $F = \frac14 \sigma_{ij} \gamma_{ij}$ and the three-dimensional version of Hooke's Law to find the stress tensor.
\newcounter{counterone}
\setcounter{counterone}{\value{enumi}}
\end{enumerate}
We will now treat the sheet as a two-dimensional system with strain elements $\gamma_{xx} = \gamma_{yy}$ as given in equation~(\ref{sheetcompression}) and $\gamma_{xy} = 0$. The two-dimensional energy density (i.e., the energy per unit area) is given by
\begin{equation}
\label{Elasticenergytwodim}
F_\mathrm{2D} = \frac{K_\mathrm{A}}{2} \left[ \mathrm{Tr}\left(\frac12\gamma_{ij}\right)\right]^2 + G \left[ \frac18 \left(\gamma_{xx} - \gamma_{yy} \right)^2 +  \gamma_{xy} \right],
\end{equation}
where $K_\mathrm{A}$ is the area compression modulus and $G = E / 2(1+\nu)$ the shear modulus.
\begin{enumerate}[(a)]
\setcounter{enumi}{\value{counterone}}
\item Show that the two-dimensional energy density of the deformation of the plate is given by
\begin{equation}
F_\mathrm{2D} = 2 K_\mathrm{A} \left(\frac{1 - \nu}{E} P\right)^2.
\end{equation}
\item Compare your results from (a) and (b) to establish $K_\mathrm{A} = \alpha K_\mathrm{V} d$, where $K_\mathrm{V} = E / 3(1-2\nu)$ is the volume compression modulus (aka the bulk modulus) and
\begin{equation}
\label{twotothreedimbulkmodulusfactor}
\alpha = \frac32 \frac{1-2\nu}{1-\nu}.
\end{equation}
\end{enumerate}

\ifincludesolutions
\newpage
\Solution
\begin{enumerate}[(a)]
\item We invoke the three-dimensional version of Hooke's law in terms of the Young's modulus and Poisson ratio (equation~\bookref{HookeYoungPoisson}):
\begin{align*}
\sigma_{ij} &= \frac{E}{2(1+\nu)} \left[ \gamma_{ij} + \frac{\nu}{1-2\nu} \gamma_{kk} \delta_{ij} \right] = \frac{E}{2(1+\nu)} \left[ \gamma_{ij} + \frac{\nu}{1-2\nu} \left( -2 \frac{2 P}{E} (1-\nu) + \frac{4 P \nu}{E} \right) \delta_{ij} \right] \\
&= \frac{E}{2(1+\nu)} \gamma_{ij} + 4 P \frac{\nu (2 \nu - 1)}{2(1+\nu)(1-2\nu)} \delta_{ij} = \frac{E}{2(1+\nu)} \gamma_{ij} - 2 P \frac{\nu}{1+\nu} \delta_{ij}.
\end{align*}
Using $F = \frac14 \sigma_{ij} \gamma_{ij} = \frac12 \sigma_{xx} \gamma_{xx} + \frac14 \sigma_{zz} \gamma_{zz}$ (because $\gamma_{xx} = \gamma_{yy}$ and $\gamma_{ij} = 0$ for $i\neq j$) we have
\begin{align*}
F_\mathrm{3D} &= \frac{E}{4(1+\nu)} \left(\gamma_{xx}^2 + \frac12 \gamma_{zz}^2 \right) - P \frac{\nu}{1+\nu} \left(\gamma_{xx} + \frac12 \gamma_{zz} \right) \\
&= \frac{E}{4(1+\nu)} \left(\frac{4 P^2}{E^2} (1-\nu)^2 + \frac12 \frac{16 P^2 \nu^2}{E^2} \right) - P \frac{\nu}{1+\nu} \left(- \frac{2P}{E} (1-\nu) + \frac{2P}{E} \nu \right) \\
&= \frac{P^2}{E} \left[\frac{1 - 2 \nu + 3 \nu^2}{1+\nu} + \frac{2\nu(1-2\nu)}{1+\nu} \right] = \frac{P^2}{E} \frac{1-\nu^2}{1+\nu}\\
&= \frac{P^2}{E} (1-\nu).
\end{align*}
\item This is a simple matter of substitution. Because we have a uniform compression, the shear contribution to the energy is zero. For the bulk part we get
\begin{align*}
F_\mathrm{2D} &= \frac12 K_\mathrm{A} \left(- \frac{2P}{E}(1-\nu) \right)^2 = 2 K_\mathrm{A} \left(\frac{1 - \nu}{E} P\right)^2.
\end{align*}
\item We have $F_\mathrm{2D} = F_\mathrm{3D} \cdot d$, so
\begin{align*}
2 K_\mathrm{A} \frac{P^2}{E^2} (1-\nu)^2 &= \frac{P^2}{E} (1-\nu) d, \\
2 K_\mathrm{A} (1-\nu) &= E \cdot d = 3(1-2\nu) K_\mathrm{V} d \\
K_\mathrm{A} &= \frac32 \frac{1-2\nu}{1-\nu} K_\mathrm{V} d \equiv \alpha K_\mathrm{V} d,
\end{align*}
where we used the given expression for $K_\mathrm{V}$ in the second line and $\alpha$ is given by equation~(\ref{twotothreedimbulkmodulusfactor}).
\end{enumerate}
\fi