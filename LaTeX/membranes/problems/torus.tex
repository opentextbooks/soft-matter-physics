% Problemtitle Doughnut membrane
% Source final 2019, suggestion Myrthe Smit and Raman van Wee
In this problem, we study a doughnut-shaped membrane, mathematically a torus with major radius $R$, minor radius $r$, and two rotation angles $\alpha$ and $\beta$. Angle $\alpha$ rotates counterclockwise with $\alpha = 0$ on the positive $x$-axis. Angle $\beta = 0$ on the positive $y$-axis also rotating counterclockwise (figure~\ref{fig:torus}).
\begin{figure}
\centering
\includegraphics[scale=1]{membranes/problems/torus.pdf}
\caption[Coordinates used to describe the torus.]{Coordinates used to describe the torus. $R$ is the major radius and $r$ is the minor radius. The angle $\alpha$ is the rotation in the $xy$-plane, whereas $\beta$ is the angle in the cross-section of the torus.}
\label{fig:torus}
\end{figure}

\begin{enumerate}[(a)]
\item Find a parametrization of the surface of the torus, i.e. give equations for the $x$, $y$, and $z$ components of the vector~$\bvec{r}$ describing the surface, as a function of the two angles $\alpha$ and $\beta$.
\item Using your parametrization for the torus, calculate the metric tensor $g_{ij}$.
\item The curvature tensor $C_{ij}$ is is given by
\begin{equation}
\label{toruscurvaturetensor}
C_{ij} =
\begin{pmatrix}
-\cos(\beta)\left(R+r\cos(\beta)\right) & 0 \\
 0 & -r
\end{pmatrix}
\end{equation}
Interpret the diagonal values of $C_{ij}$ in terms of the geometry of the torus.
\item Find the mean and Gaussian curvature of the torus.
\item Explain why it is very unlikely that you will find a doughnut-shaped cell in nature.
\end{enumerate}

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item The torus is parametrized as:
\[ \bvec{r}(\alpha,\beta) = \begin{pmatrix}
\left(R+r\cos(\beta)\right)\cos(\alpha)\\
\left(R+r\cos(\beta)\right)\sin(\alpha)\\
r\sin(\beta)
\end{pmatrix} \]
\score{1 pt}.
\item Using the definitions for the basis vectors and the metric, we find:
\[ \bvec{e}_{\alpha} = \begin{pmatrix}
-\left(R+r\cos(\beta)\right)\sin(\alpha)\\
\left(R+r\cos(\beta)\right)\cos(\alpha)\\
0
\end{pmatrix}, \quad \mbox{and} \quad 
\bvec{e}_{\beta} = \begin{pmatrix}
-r\sin(\beta)\cos(\alpha)\\
-r\sin(\beta)\sin(\alpha)\\
r\cos(\beta)
\end{pmatrix} \]
\score{0.5 pt}. To find the components of the metric, we need the dot products of the basis vectors:
\begin{align*}
\bvec{e}_{\alpha} \cdot \bvec{e}_{\alpha} &= \left(R+r\cos(\beta)\right)^2 \\
\bvec{e}_{\beta} \cdot \bvec{e}_{\beta} &= r^2 \\
\bvec{e}_{\beta} \cdot \bvec{e}_{\alpha} &= \bvec{e}_{\alpha} \cdot \bvec{e}_{\beta} = 0
\end{align*}
and we obtain
\[ g_{ij} =
\begin{pmatrix}
\left(R+r\cos(\beta)\right)^2 & 0\\
0 & r^2
\end{pmatrix} \]
\score{0.5 pt}.
\item Since $C_{ij}$  has no off-diagonal terms, the diagonal terms are the eigenvalues with an extra minus sign. The eigenvalues of $C_{ij}$ are related to the principal radii of curvature of the torus, but weighted with the relevant term in the inverse of the metric (which is also diagonal). We have
\[ g^{ij} = \frac{1}{r^2\left(R+r\cos(\beta)\right)^2} \begin{pmatrix}
r^2 & 0 \\
0 & \left(R+r\cos(\beta)\right)^2
\end{pmatrix}. \]
The principal curvatures are thus
\[ \frac{\cos(\beta)}{\left(R+r\cos(\beta)\right)} \quad \mbox{and} \quad \frac{1}{r}. \]
The first of these is the curvature in the $xy$ plane, which is the inverse of the major radius $R$ plus any contributing part from the minor radius $r$, which rotates as well, explaining the second cosine. The other cosine is there because by definition the radii of curvature are perpendicular. The second curvature is one over the radius~$r$ along the cross-section of the torus \score{1 pt}.
\item The mean and Gaussian curvatures are:
\begin{align*}
H &= -\frac12 g^{ij} C_{ij} = \frac{R+2r\cos(\beta)}{2r\left(R+r\cos(\beta)\right)}, \\
K &= \frac{\det(C_{ij})}{\det(g_{ij})} = \frac{\cos(\beta)}{r(R+r\cos(\beta))}.
\end{align*}
\score{0.5 pt each. No subtraction if in the mean curvature they did not include the minus.}
\item A torus has a hole and thus a different genus than a sphere. By the Gauss-Bonnet theorem, for any closed shape with the same genus, the integral of the Gaussian curvature over the whole surface is a constant. Comparing the torus to the sphere however carries an extra energy for the hole, which makes the torus unstable (if the Gaussian modulus is negative, which it has to be or spheres themselves would be unstable to spontaneous splitting), and thus a very unlikely shape to be found in nature \score{1 pt}.
\end{enumerate}
\fi

