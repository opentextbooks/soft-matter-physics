%Problemtitle: Minimal surfaces
Surfaces that minimize the surface area energy~(\bookref{Earea}) are known as \emph{minimal surfaces}. Unsurprisingly, soap films always take on the shape of a minimal surface, as long as they don't enclose any volume (i.e., soap bubbles are not minimal surfaces, but a soap film in a wire frame will be). Perhaps more surprising is a minimal surface will have zero mean curvature everywhere, a theorem we'll prove in this problem:
\begin{theorem}[Minimal surfaces have vanishing mean curvature]
\label{thm:minimalsurfacemeanenergy}
The mean curvature of a surface that minimizes the surface area energy $\mathcal{E}_\mathrm{area}$ is identically zero at every point on the surface.
\end{theorem}
To prove this theorem, we'll work in the Monge gauge, but without the linearization; the result will thus be exact. Our mean curvature and area element are thus given by the exact expressions in equations~(\bookref{meancurvMonge}) and (\bookref{areaelementMonge}).
\begin{enumerate}[(a)]
\item Write the surface area energy functional as an integral over a Lagrangian density, i.e., find $\mathcal{L}$ such that
\begin{equation}
\mathcal{E}_\mathrm{area} = \sigma \int \dd{S} = \sigma \int \mathcal{L}(u, u_x, u_y) \dd{x} \dd{y}.
\end{equation}
\item Given a Lagrangian, you can find the minimum of the associated action (here the surface area energy functional) through the Euler-Lagrange equations. Usually these are given in terms of a functional of a function of a single variable, but the derivation extends easily to the case that the function depends on multiple variables (cf. \cite[Chapter 9]{IdemaPhysics1Abook}). For our specific case, we have
\begin{equation}
\label{ELtwovariables}
\pdv{\mathcal{L}}{u} - \dv{}{x} \pdv{\mathcal{L}}{u_x} - \dv{}{y} \pdv{\mathcal{L}}{u_y} = 0.
\end{equation}
Substitute your Lagrangian from (a) in equation~(\ref{ELtwovariables}) to get a differential equation for the minimal surface.
\item Now compare your answer at (b) to the expression~(\bookref{meancurvMonge}) and conclude that for a minimal surface, the mean curvature must identically vanish, thus completing the proof of theorem~\ref{thm:minimalsurfacemeanenergy}.
\end{enumerate}

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item We simply substitute equation (\bookref{areaelementMonge}) for $\dd{S}$ and read off:
\begin{align*}
\mathcal{E}_\mathrm{area} &= \sigma \int \dd{S} = \sigma \int \sqrt{1+u_x^2+u_y^2} \dd{x} \dd{y} = \sigma \int \sqrt{1+ (\nabla_\perp u)^2} \dd{x} \dd{y} = \sigma \int \mathcal{L}(u, u_x, u_y) \dd{x} \dd{y}, \\
\mathcal{L}(u, u_x, u_y) &= \sqrt{1+u_x^2+u_y^2}
\end{align*}
\item As our Lagrangian does not explicitly depend on $u$ itself, only its derivatives, the first term in~(\ref{ELtwovariables}) vanishes. The other two give
\begin{align*}
0 &= \dv{}{x} \left( \frac{u_x}{\sqrt{1+ (\nabla_\perp u)^2}} \right) + \dv{}{y} \left( \frac{u_y}{\sqrt{1+ (\nabla_\perp u)^2}} \right) \\
&= \frac{u_{xx} + u_{yy}}{\sqrt{1+ (\nabla_\perp u)^2}} - \frac12 \frac{u_x \dv{}{x} \left(u_x^2 + u_y^2 \right) + u_y \dv{}{y} \left(u_x^2 + u_y^2 \right)}{\left[1+ (\nabla_\perp u)^2\right]^{3/2}} \\
&= \frac{u_{xx} + u_{xx} \left(u_x^2 + u_y^2 \right) + u_{yy} + u_{yy} \left(u_x^2 + u_y^2 \right) - u_x^2 u_{xx} - 2 u_x u_y u_{xy} - u_y^2 u_{yy}}{\left[1+ (\nabla_\perp u)^2\right]^{3/2}} \\
&= \frac{u_{xx} + u_{yy} + u_y^2 u_{xx} + u_x^2 u_{yy} - 2 u_x u_y u_{xy}}{\left[1+ (\nabla_\perp u)^2\right]^{3/2}} \\
&= -2 H.
\end{align*}
\item As indicated in the last line in (b), the differential equation describing the minimal surface is a numerical factor times the expression for $H$; thus $H$ will vanish anywhere on the minimal surface.
\end{enumerate}
\fi