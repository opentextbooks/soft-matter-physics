% Problemtitle Bilayer around a nuclear pore
% Source Exam 2017-2018 and final 2022-2023, based on Boal 10.2.
The inner and outer membranes of the nuclear envelope are joined by circular, protein-lined pores as shown in cross section in figure~\ref{fig:bilayerpore}. The pore has axial symmetry with the bilayer joined smoothly around the ring, as shown by the dashed lines.

\begin{figure}[ht]
\centering
\includegraphics[scale=1]{membranes/problems/Pore.pdf}
\caption[Schematic cross section of the bilayer connected to a nuclear pore.]{Schematic cross section of the bilayer connected to a nuclear pore. The bilayer (blue) and the nuclear pore complex (red) are both rotationally symmetric. The bilayer's radius of curvature is~$\rho$, the total radius of the part of the bilayer that is curved is $R_0$ (so $R_0$ equals the radius of the nuclear pore complex plus the radius of curvature of the bilayer).}
\label{fig:bilayerpore}
\end{figure}

\begin{enumerate}[(a)]
\item Set the origin at the center of the pore, and let $R_0$ be the pore radius and $\rho$ be the radius of curvature of the bilayer connected to the pore (we assume this section has uniform curvature). Find the functions $\psi(s)$, $r(s)$ and $z(s)$ describing the axisymmetric shape of the lower half of the pore (with $s=0$ at the equator, and therefore $\psi(0) = \pi/2$, $r(0) = R_0 - \rho$, $z(0) = 0$), in terms of $R_0$ and $\rho$. (For definitions of these functions, see figure~\bookref{fig:axisymmetriccoordinates}).
\item Find the mean and the Gaussian curvature of this shape as a function of~$s$.
\item Calculate the total contribution of the Gaussian curvature to the pore, i.e., evaluate the integral
\[ \mathcal{E}_\mathrm{Gauss} = 2\pi \kappa_\mathrm{G} \int_\mathrm{pore} K \cdot r(s) \dd{s}, \]
setting appropriate boundaries for the arc length~$s$. To simplify the notation, use $a = R_0/\rho$ and $\bar{s} = s/\rho$. NB: the assignment is to do the calculation, but you may be able to guess the answer - use that (and explain your guess) if you get stuck in this part.
\item The integral for the contribution of the mean curvature contains nasty hyperbolic arctangents, but for $R_0 \gg \rho$ it simplifies to an easy expression:
\[ \mathcal{E}_\mathrm{bend} = 2\pi \int_\mathrm{pore} \frac{\kappa}{2} \left( 2H \right)^2 r(s) \dd s = \pi \kappa \left( \pi \frac{R_0}{\rho} - 8 \right). \]
In section~\bookref{sec:CH}, we found that general stability considerations implied that $-2\kappa < \kappa_\mathrm{G} < 0$; numerical work suggests $\kappa_\mathrm{G} \approx -1.5 \kappa$. For this value, determine the maximum radius of a pore for which adding one does not increase the total bending energy of the membrane (you may assume that the membrane without the pore is flat).
\item In the membrane energy as given by equation~(\bookref{simpleenergy}), there is an additional term apart from that due to curvature. Would including this term increase or decrease the value of the maximum radius you found at (d)? (NB: give a physical argument, not a calculation).
\end{enumerate}

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item We have $\psi(0) = \pi/2$, $\psi(s=\pi \rho / 2) = 0$, and constant curvature, so $\dot \psi = -1/\rho$, which gives $\psi(s) = \pi/2 - s/\rho$. Integrating $\dot{r} = \cos\psi = \cos(\pi/2 - s/\rho) = \sin(s/\rho)$ we get $r(s) = C - \rho \cos(s/\rho)$, where we find the integration constant $C$ by setting $R_0 - \rho = r(0) = C - \rho$, so $C = R_0$. Likewise, integrating $\dot{z} = -\sin\psi = -\sin(\pi/2 - s/\rho) = -\cos(s/\rho)$ we get $z(s) = C - \rho \sin(s/\rho)$, and from $z(0) = 0$ we get that $C=0$ in this case. Combined, we thus have
\[ \psi(s) = \frac{\pi}{2} - \frac{s}{\rho}, \qquad r(s) = R_0 - \rho\cos\left(\frac{s}{\rho}\right), \qquad z(s) = -\rho\sin\left(\frac{s}{\rho}\right). \]
\score{0.5 pt per correct expression, so max 1.5 pt}.
\item Simply substituting $\psi(s)$ and $r(s)$ in the expressions~(\bookref{Hpsi}) and~(\bookref{Kpsi}) for the mean and Gaussian curvatures gives
\begin{align*}
H &= \frac12 \left[ -\frac{1}{\rho} + \frac{\sin\left(\frac{\pi}{2} - \frac{s}{\rho}\right)}{R_0 - \rho\cos\left(\frac{s}{\rho}\right)} \right] = \frac{1}{2\rho} \left[ -1 + \frac{\cos\left(\frac{s}{\rho}\right)}{\frac{R_0}{\rho} - \cos\left(\frac{s}{\rho}\right)} \right] = \frac{2 \cos\left(\frac{s}{\rho}\right) - \frac{R_0}{\rho}}{2\rho \left[ \frac{R_0}{\rho} - \cos\left(\frac{s}{\rho}\right) \right]} \\
K &= -\frac{1}{\rho} \frac{\cos\left(\frac{s}{\rho}\right)}{R_0 - \rho\cos\left(\frac{s}{\rho}\right)}
\end{align*}
\score{0.5 pt per correct expression}.
\item To evaluate the Gaussian energy, we calculate the given integral:
\begin{align*}
\mathcal{E}_\mathrm{Gauss} &= 2\pi \kappa_\mathrm{G} \int_\mathrm{pore} K \cdot r(s) \dd s = 2 \pi \kappa_\mathrm{G} \cdot 2 \int_0^{\pi \rho / 2} \left[ - \frac{1}{\rho} \cos\left(\frac{s}{\rho}\right) \right] \dd s \\
&= - 4 \pi \kappa_\mathrm{G} \int_0^{\pi/2} \cos(\bar{s}) \dd \bar{s} = - 4 \pi \kappa_\mathrm{G}
\end{align*}
This answer should not surprise us, as it is the value given by the Gauss-Bonnet theorem \score{1 pt}.
\item We have $\mathcal{E}_\mathrm{pore} = \pi \kappa \left( \pi \frac{R_0}{\rho} - 8\right) - 4 \pi \kappa_\mathrm{G} = \pi \kappa \left( \pi \frac{R_0}{\rho} - 2\right)$ if $\kappa_\mathrm{G} = -1.5 \kappa$. Adding a pore has no net effect on the energy when $\mathcal{E}_\mathrm{pore} = 0$, which is when $\rho = \frac{\pi}{2} R_0$ \score{1 pt}.
\item The surface tension term penalizes changes in the area of the membrane. Opening up a hole to create a pore changes the membrane area, so there is an energy cost associated with that. The minimal radius therefore decreases (which results in a negative value for $\mathcal{E}_\mathrm{pore}$) \score{0.5 pt}.
\end{enumerate}
\fi
