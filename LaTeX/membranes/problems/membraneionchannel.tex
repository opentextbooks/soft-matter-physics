% Problemtitle Membrane deformation around an ion channel
% Source based on Phillips 11.10.

\begin{figure}[h]
\begin{center}
\includegraphics[scale=1]{membranes/problems/membraneionchannel.pdf}
\end{center}
\caption{Schematic of a cylindrical membrane channel embedded in a lipid bilayer with a hydrophobic mismatch. \ifproblemset Image from Phillips et al., \href{https://doi.org/10.1038/nature08147}{Nature 459, 379-385} (2009).\else Image from Phillips et al.~\cite{Phillips2009}.\fi}
\end{figure}

For a cylindrical protein, such as an ion channel, there are no deformations of the membrane due to imposed curvature. However, there may be deformations due to a hydrophobic mismatch: the thickness of the hydrophobic region of the protein may be larger or smaller than that of the lipid bilayer. In that case, the membrane will still be deformed. The interactions between channels due to these deformations may be involved in the process of opening and closing them \ifproblemset(see Phillips et al., \href{https://doi.org/10.1038/nature08147}{Nature 459, 379-385} (2009))\else(see Phillips et al.~\cite{Phillips2009})\fi. To describe the shape of the membrane around the channel, we use the Monge gauge, with $u(x,y)$ the  deformation away from a flat membrane. As the length scales are small, we can ignore the contribution of the surface tension. There are then two terms in the deformation energy, one due to the bending of the two leaflets of the bilayer, which is the bending term we already encountered before, and one due to the mismatch itself. The energy as a function of the shape $u$ then reads
\begin{equation}
\label{mismatchenergy}
\mathcal{E} = \int \left[ \frac{\kappa}{2} ( \nabla^2 u)^2 + \frac{A}{2} \left(\frac{u}{a}\right)^2 \right] \dd^2 r,
\end{equation}
where the integral is taken over the flat reference plane (so $\dd^2r = \dd x \, \dd y = r \, \dd r \, \dd \theta$) and $\nabla$ is the gradient in the reference coordinates (which we've sometimes written as $\nabla_\perp$). In equation~(\ref{mismatchenergy}), $a$ is the membrane thickness and $A$ a constant that determines the energy of the mismatch. The function $u(x,y)$ must satisfy four boundary conditions, two at the surface of the inclusion and two at infinity:
\begin{equation}
\label{mismatchbc}
u(r=R) = u_0, \quad \left.\frac{\partial u}{\partial r}\right|_{r=R} = 0, \quad u(r\to\infty) = 0, \quad \left.\frac{\partial u}{\partial r}\right|_{r \to \infty} = 0.
\end{equation}

\begin{enumerate}[(a)]
\item Through variational analysis, find the partial differential equation for the function $u(x,y)$ that minimizes the energy functional given in equation~(\ref{mismatchenergy}).
\item The partial differential equation you found in (a) is fourth order. It strongly resembles a version of the modified Bessel equation, which is given by
\begin{equation}
\label{modifiedbessel}
\nabla^2 u = k^2 u.
\end{equation}
By taking the Laplacian of~(\ref{modifiedbessel}) and invoking~(\ref{modifiedbessel}) for the right-hand-side of the resulting equation, show that your differential equation from~(a) can be constructed from the modified Bessel equation. Determine the value of $k$ for which the match is exact (you should get a fourth-order equation with four complex roots).
\item The solutions of the modified Bessel equation~(\ref{modifiedbessel}) are the modified Bessel functions of the first and second kind, $I_0(k r)$ and $K_0(k r)$, with $r$ the radial polar coordinate. As $I_0(k r)$ blows up for $r \to \infty$, we are left with the $K_0(k r)$ solutions, in which we can substitute the roots of $k$ you found in (b). Two solutions will still blow up, identify which ones. Using the remaining two solutions, write the general solution of your differential equation from (a) in the form
\begin{equation}
\label{pdesol}
u(r) = B_1 K_0(k_1 r) + B_2 K_0(k_2 r),
\end{equation}
and determine the values of $k_1$ and $k_2$.
\item From the boundary conditions~(\ref{mismatchbc}) at $r=R$, determine $B_1$ and $B_2$ in equation~(\ref{pdesol}). You may use that $\dd K_0(x) / \ddx = -K_1(x)$.
\item Plot your solution~$u(r)$ for $u_0 = 0.5\;\mathrm{nm}$, $\kappa = 20\;\kB T$, $A = 60\;\kB T / \mathrm{nm}^2$, and $R = a = 2\;\mathrm{nm}$.
\end{enumerate}


\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item The variational analysis follows the one given in the notes and lecture:
\begin{align*}
\mathcal{E} + \delta \mathcal{E} &= \int \left[ \frac{\kappa}{2} ( \nabla^2 (u + \delta u))^2 + \frac{A}{2} \left(\frac{u+\delta u}{a}\right)^2 \right] \dd^2 r  \\
&= \mathcal{E} + \int \left[ \kappa \left( (\nabla^2 u) \nabla^2 \delta u) \right) + A \frac{u \delta u}{a^2} \right] \dd^2 r + \mathcal{O}(\delta u^2)\\
&= \mathcal{E} + \int \left[ \kappa (\nabla^4 u) + A \frac{u}{a^2} \right] \delta u \dd^2 r  + \mathcal{O}(\delta u^2),
\end{align*}
where we integrated the first term by parts twice in the last equality. As $\delta E$ must vanish for arbitrary $\delta u$, we arrive at the following pde for $u$:
\begin{equation}
\label{upde}
\kappa \left(\nabla^4 u\right) + \frac{A}{a^2} u = 0.
\end{equation}
\item The modified Bessel equation is given by
\begin{equation}
\label{modifiedbessel}
\nabla^2 u = k^2 u.
\end{equation}
Applying the Laplacian to~(\ref{modifiedbessel}) gives $\nabla^4 u = k^2 \nabla u$. Invoking~(\ref{modifiedbessel}) again for the right-hand side gives $\nabla^4 u = k^4 u$, which indeed looks just like equation~(\ref{upde}), if we have $k^4 = - A / (\kappa a^2)$. The four roots of this equation come in two pairs of complex conjugates (with $\lambda = (A/\kappa a^2)^{1/4}$:
\begin{align*}
k_1 &= k_4^* = \lambda e^{i\pi/4} = \lambda \frac{1+i}{\sqrt{2}}, \\
k_2 &= k_3^* = \lambda e^{3i\pi/4} = \lambda \frac{-1+i}{\sqrt{2}}.
\end{align*}
\item The two solutions with negative real part will blow up; we are thus left with
\[ u(r) = B_1 K_0\left(\lambda \frac{1+i}{\sqrt{2}} r\right) + B_2 K_0\left(\lambda \frac{1-i}{\sqrt{2}} r\right). \]
\item We have
\begin{align*}
u_0 &= B_1 K_0(k_1 R) + B_2 K_0(k_1^* R), \\
0 &= -B_1 k_1 K_1(k_1 R) - B_2 k_1^* K_1(k_1^* R).
\end{align*}
which we can easily solve for $B_1$ and $B_2$:
\begin{align*}
B_1 &= \frac{k_1^* K_1(k_1^* R)}{k_1^* K_0(k_1 R) K_1(k_1^* R) - k_1 K_0(k_1^* R) K_1(k_1 R)} u_0, \\
B_2 &= -\frac{k_1 K_1(k_1 R)}{k_1^* K_0(k_1 R) K_1(k_1^* R) - k_1 K_0(k_1^* R) K_1(k_1 R)} u_0.
\end{align*}
\item We have $\lambda \approx 0.93\;\mathrm{nm}^{-1}$. The plot is given in figure~\ref{fig:membraneinclusionshape}.
\end{enumerate}

\begin{figure}[ht]
\begin{center}
\includegraphics[scale=1]{membranes/problems/inclusionmembraneshape.pdf}
\end{center}
\caption{Shape of the membrane next to a cylindrical inclusion for the parameters given in problem (e).}
\label{fig:membraneinclusionshape}
\end{figure}
\fi