%Problemtitle Catenoid
A \emph{catenoid} is the surface of revolution of a catenary, which in turn is the shape an ideal chain assumes under gravity (close, but not equal to, a parabola). The catenoid is an example of a \emph{minimal surface}, i.e., a surface that minimizes the surface area energy. 

The shape of a catenoid can be described with the hyperbolic cosine. Hyperbolic sines and cosines are closely related to ordinary sines and cosines, but some of the details (particularly signs!) differ. We have
\begin{alignat*}{4}
\cosh(x) &= \frac{e^x + e^{-x}}{2} = \cos(ix) & \quad & \dv{\cosh(x)}{x} = \sinh(x) \\
\sinh(x) &= \frac{e^x - e^{-x}}{2} = -i \sin(ix) & \quad & \dv{\sinh(x)}{x} = \cosh(x) \\
&&&\cosh^2(x) - \sinh^2(x) &= 1.
\end{alignat*}
The 3D embedding of the catenoid is given by
\begin{equation}
\label{catenoid}
\bvec{r}(\phi, z) = \threevector{a \cosh(z/a) \cos(\phi)}{a \cosh(z/a) \sin(\phi)}{z},
\end{equation}
where $a > 0$ is a number that simply sets the size of the catenoid. An example catenoid surface is plotted in figure~\ref{fig:catenoidsurface}.
\begin{figure}[ht]
\centering
\includegraphics[scale=.75]{membranes/problems/catenoidsurface.png}
\caption[Surface of a catenoid.]{Surface of a catenoid (equation~(\ref{catenoid}) with $a=1$), the surface of revolution of a catenary.}
\label{fig:catenoidsurface}
\end{figure}

\begin{enumerate}[(a)]
\item Find the tangent vectors of the catenoid surface.
\item Show that the metric tensor of the catenoid (abusing, as usual, matrix notation) is given by
\begin{equation}
\label{catenoidmetric}
g_{ij} = \cosh^2\left(\frac{z}{a}\right) \begin{pmatrix}
a^2 & 0 \\ 0 & 1
\end{pmatrix},
\end{equation}
where $i,j=1$ corresponds to the $\phi$ coordinate and $i,j=2$ to the $z$ coordinate.
\item Find the mean curvature of a catenoid (the answer applies to all minimal surfaces; the proof of that statement is actually not that difficult but we'll skip it for now).
\item Find the Gaussian curvature of a catenoid (\textit{Hint}: you should be able to guess its sign from the shape as plotted in figure~\ref{fig:catenoidsurface}).
\end{enumerate}

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item We have
\begin{align*}
\bvec{e}_\phi &= \pdv{\bvec{r}}{\phi} = a \cosh(\frac{z}{a}) \threevector{-\sin(\phi)}{\cos(\phi)}{0}, \\
\bvec{e}_z &= \pdv{\bvec{r}}{z} = \threevector{\sinh(z/a) \cos(\phi)}{\sinh(z/a) \sin(\phi)}{1}
\end{align*}
\score{2 pt each}.
\item The components of the metric are simply the inner products of the tangent vectors. We have
\begin{align*}
g_{\phi \phi} &= \bvec{e}_\phi \cdot \bvec{e}_\phi = a^2 \cosh^2(\frac{z}{a}), \\
g_{\phi z} &= \bvec{e}_\phi \cdot \bvec{e}_z = 0, \\
g_{z z} &= \bvec{e}_z \cdot \bvec{e}_z = 1 + \sinh^2(z/a) = \cosh^2(z/a),
\end{align*}
which gives equation~(\ref{catenoidmetric}) \score{3 pt}.
\item For the mean curvature, we first need the components of the curvature tensor. For that, we need the normal, which is simply the normalized cross product of the tangent vectors:
\begin{align*}
\unitvec{n} &= \frac{\bvec{e}_\phi \cross \bvec{e}_z}{\sqrt{\det(g_{ij})}} = \frac{1}{\sqrt{1+\sinh^2(z/a)}} \threevector{\cos(\phi)}{\sin(\phi)}{-\sinh(z/a)} = \frac{1}{\cosh(z/a)} \threevector{\cos(\phi)}{\sin(\phi)}{-\sinh(z/a)}
\end{align*}
\score{2 pt}. For the components of the curvature tensor we then get:
\begin{align*}
C_{\phi \phi} &= \unitvec{n} \cdot \pdv{\bvec{e}_\phi}{\phi} = \unitvec{n} \cdot a \cosh(z/a) \threevector{-\cos(\phi)}{-\sin(\phi)}{0} = - a,\\
C_{\phi z} &= \unitvec{n} \cdot \pdv{\bvec{e}_\phi}{z} = \unitvec{n} \cdot \sinh(z/a) \threevector{-\sin(\phi)}{\cos(\phi)}{0} = 0, \\
C_{z z} &= \unitvec{n} \cdot \pdv{\bvec{e}_z}{z} = \unitvec{n} \cdot \frac{1}{a} \cosh(z/a) \threevector{\cos(\phi)}{\sin(\phi)}{0} = \frac{1}{a}.
\end{align*}
\score{3 pt}. For the mean curvature, we then get
\begin{align*}
H = -\frac12 \tr(\bvec{C}) = - \frac12 g^{ij} C_{ij} = -\frac12 \frac{1}{\cosh^2(z/a)} \left( \frac{1}{a^2} \cdot -a + 1 \cdot \frac{1}{a} \right) = 0
\end{align*}
\score{1 pt}.
\item The Gaussian curvature is given by
\begin{align*}
K &= \frac{\det(C_{ij})}{\det(g_{ij})} = \frac{-a \cdot \frac{1}{a}}{\left(a^2 \cdot 1\right) \cosh^4(z/a)} = - \frac{1}{a^2 \cosh^4(z/a)}
\end{align*}
\score{2 pt}.
\end{enumerate}
\fi