

\subsection{A different approach}
The simplest case, for an asymptotically flat membrane with point-like inclusions, was already studied in the 1990s~\cite{Goulian1993,Weikl1998}. The presence of the inclusions locally puts constraints on the membrane shape. If the inclusions are linked to an underlying substrate (such as the cytoskeleton in a cell, or a coverslip in an experiment with a supported lipid bilayer), the constraints simply fix the position and possibly the slope of the membrane at the location of the inclusion. For free membranes, the position obviously isn't fixed, as the membrane can adapt its shape to minimize the total energy. Likewise, an inclusion can be tilted if that results in a lower total energy. For point-like inclusions in free membranes, what is imposed is therefore the curvature, or the second derivative of the shape. For a rotationally symmetric conical inclusion, the imposed curvature~$c$ equals the ratio of the inclusion's opening angle~$\alpha$ to its radius~$a$, $c = \alpha / a$. Other possible inclusions are banana-shaped (imposing a curvature in one direction only, like BAR-domain proteins do) or saddle-shaped (imposing opposite curvatures in the $x$ and $y$ directions).

To determine the equilibrium shape of a flat membrane with point-like inclusions, we can use equation~(\ref{CHMonge}) for the Canham-Helfrich energy in the Monge gauge. To this energy we can add a number of Lagrange multipliers imposing the constraints, which are all of the form $(\partial^2_{ij} u) \delta(\bvec{x}-\bvec{x}_\mathrm{p}) = c$, where $\bvec{x}_\mathrm{p}$ is the position of the inclusion, $\bvec{x} = (x,y)$, and $i$ and $j$ can be either $x$ or $y$. We can then calculate the change in the energy~$\delta E$ due to a small change in the shape~$\delta u$, which needs to vanish for an equilibrium shape. This procedure gives us the shape equation for $u$:
\begin{equation}
\label{inclusionshapeeq}
\kappa \nabla_\perp^4 u - \sigma \nabla_\perp^2 u = \sum_a \Lambda_a D_a(\bvec{x}),
\end{equation}
where the elements of $D_a$ are of the form $\partial_{ij} \delta(\bvec{x}-\bvec{x}_\mathrm{p})$ and the $\Lambda_a$ are the Lagrange multipliers. As equation~(\ref{inclusionshapeeq}) is linear, it can be solved if we have its Green's function, which is given by~\cite{Evans2003}
\begin{equation}
\label{inclusionGreen}
G(\bvec{x}) = \frac{1}{2 \pi \sigma} \left[ K_0(\lambda r) + \log(\lambda r) \right], 
\end{equation}
where $\lambda = \sqrt{\sigma/\kappa}$, $r = |\bvec{x}|$, and $K_0$ is the zeroth order modified Bessel functions of the second kind. The shape is now given by $u(\bvec{x}) = \sum_a \Lambda_a G_a(\bvec{x})$, where the elements of $G_a$ are of the form $\partial_{ij} G(\bvec{x}-\bvec{x}_\mathrm{p})$~\cite{Dommersnes2002}.

