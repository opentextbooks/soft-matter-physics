%-------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%-------------------------------------------------------------------------

\documentclass[a4paper,11pt]{article} % A4 paper and 11pt font size
%\usepackage[T1]{fontenc} % Use 8-bit encoding that has 256 glyphs
\usepackage{fourier} % Use the Adobe Utopia font for the document - comment this line to return to the LaTeX default
%\usepackage[english]{babel} % English language/hyphenation
\usepackage{fullpage}
\usepackage{amsmath,amsfonts,amsthm} % Math packages
%\usepackage{enumitem}
\usepackage{enumerate} % For letter-enumerated lists.
\usepackage{graphicx}
\usepackage{physics}
\usepackage{sectsty} % Allows customizing section commands
\usepackage[utf8]{inputenc}
\usepackage{titlesec}
\usepackage{bm} % Bold math

\newcommand{\bvec}[1]{\bm{#1}}
\newcommand{\unitvec}[1]{\bm{\hat #1}}
\newcommand{\diff}[2]{\frac{\partial #1}{\partial #2}}
%\newcommand{\dd}{\mathrm{d}}
\newcommand{\ddt}{\mathrm{d}t}
\newcommand{\ddx}{\mathrm{d}x}
\newcommand{\ddy}{\mathrm{d}y}
\newcommand{\dds}{\mathrm{d}s}
\newcommand{\kB}{k_{\mbox{\tiny B}}}

%\newenvironment{•}{•}{•}

\allsectionsfont{ \normalfont\scshape} % Make all sections centered, the default font and small caps
\titleformat{\section}[hang]
{\normalfont\Large\scshape}
{Problem \thesection}{0.5em}{}



%-------------------------------------------------------------------------
%	TITLE SECTION
%-------------------------------------------------------------------------

\newcommand{\horrule}[1]{\rule{\linewidth}{#1}} % Create horizontal rule command with 1 argument of height

\title{	
\normalfont \normalsize 
\textsc{TU Delft, Soft Matter (NB4070) / Biophysics (AP3511)} \\ [15pt] % Your university, school and/or department names)
\horrule{0.5pt} \\[0.4cm] % Thin top horizontal rule
\huge Equations sheet \\ % The assignment title
\horrule{2pt} \\[0.5cm] % Thick bottom horizontal rule
}

%\author{} % Your name

%\date{\normalsize\today} % Today's date or a custom date
\date{}
\begin{document}

\maketitle % Print the title
\vspace{-2cm}

\section*{Elasticity}
\begin{itemize}
\item Strain $\gamma=\left((\nabla u)+(\nabla u)^\mathrm{T}\right)$, 
$\gamma_{ij}=\left(\frac{\partial u_i}{\partial x_j}+\frac{\partial u_j}{\partial x_i}\right)$
\item Stress / force per volume: $f_i=\partial_j\sigma_{ij}$
\item Force on surface: $\bvec{F}=\int \bvec{f} \dd V=\int \nabla\cdot\bvec{\sigma} \dd V=\oint\bvec{\sigma}\cdot \dd\bvec{A}=\oint\bvec{\hat{n}}\cdot\bvec{\sigma} \dd A$
\item Hooke's law:
\begin{align*}
f &= \frac{E}{2(1+\nu)}\left[\gamma_{ij}^{2}+\frac{\nu}{1-2\nu}\gamma_{kk}^{2}\right]\\
\sigma_{ij} &= \frac12 K\gamma_{kk}\delta_{ij} + G\left(\gamma_{ij}-\frac{1}{3}\gamma_{kk}\delta_{ij}\right) = \frac{E}{2(1+\nu)}\left[\gamma_{ij}+\frac{\nu}{1-2\nu}\gamma_{kk}\delta_{ij}\right]\\
\gamma_{ij} &= \frac{2}{9K}\sigma_{kk}\delta_{ij} + \frac{1}{G}\left(\sigma_{ij} - \frac13 \sigma_{kk}\delta_{ij}\right) = \frac{2}{E}\left[(1+\nu)\sigma_{ij}-\nu\sigma_{kk}\delta_{ij}\right]
\end{align*}
\item Young's modulus: $E=\frac{9GK}{G+3K}$
\item Poisson ratio: $\nu=\frac{1}{2}\frac{3K-2G}{3K+G}$
\item Second moment of the area: 
\[ I_y = \int x^2 \dd A.\]
\item Timoshenko beam equation:
\begin{align*}
q(z) &= \frac{\dd^2}{\dd z^2}\left( E I_y \frac{\dd \phi}{\dd z} \right), \\
\frac{\dd w}{\dd z} &= \phi(z) - \frac{1}{\kappa G A} \frac{\dd}{\dd z} \left( E I_y \frac{\dd \phi}{\dd z} \right).
\end{align*}
\item Euler-Bernoulli beam equation:
\[ q(z) = \frac{\dd^2}{\dd z^2}\left( E I_y \frac{\dd^2 w}{\dd z^2} \right). \]
\item Some common boundary conditions for Euler-Bernoulli beam in table~\ref{table:beamboundaryconditions}.
\item Free energy per unit volume of a neo-Hookean material
\begin{align*}
f(\bvec{\gamma}) &= \frac12 n_\mathrm{c}\kB T \, \Tr(\bvec{\gamma}) = \frac12 n_\mathrm{c}\kB T \left( \sum_i \lambda_i^2 - 3 \right)
\end{align*}
\end{itemize}

\begin{table}
	\centering
	\begin{tabular}{|l|c|c|}
		\hline
		Boundary condition & general & Euler-Bernoulli shape function \\
		\hline
		Clamped end at $z=0$ & fixed position and angle & $w(0) = 0$, $w'(0) = \phi_0$ \\
		Free end at $z=L$ & zero torque and shear force & $w''(L) = w'''(L) = 0$ \\
		Hinged end at $z=0$ & fixed position, zero torque & $w(0) = w''(0) = 0$ \\
		Simple support at $z=z_0$ & fixed position, zero angle, zero torque & $w(z_0) = w_0$, $w'(z_0) = w''(z_0) = 0$. \\
		\hline
	\end{tabular}
	\caption{Common boundary conditions for the beam equation.}
	\label{table:beamboundaryconditions}
\end{table}

\section*{Polymers}
\begin{itemize}
\item FJC Probability distribution 3D: $P(\bm{r})=\left(\frac{3}{2\pi Nb^2}\right)^{3/2} \exp\left(-\frac{3r^2}{2Nb^2}\right)$
\item Curvature energy: 
\[\mathcal{E}_\mathrm{curv}=\frac{K}{2}\int_0^L\frac{\dd\bm{\hat{t}}}{\dd s}\cdot\frac{\dd \bm{\hat{t}}}{\dd s} \dd s=\frac{K}{2}\int_0^L\kappa(s)^2 \dd s\]
\item Worm-like chain bending energy:
\[\mathcal{E}_\mathrm{WLC}=\frac{1}{2}K_\mathrm{eff}\int_{0}^{L}\left(\frac{\dd\hat{\bm{t}}}{\dd s}\right)^2\dd s=\frac{1}{2}\xi_p \kB T\int_0^L\left(\frac{\dd\hat{\bm{t}}}{\dd s}\right)^2 \dd s \]
\item Torsional energy of a polymer:
\[\mathcal{E}_\mathrm{torsion} = \frac{K_\mathrm{tor}}{2} \int_0^L \tau^2(s) \dds. \]
\item Binormal: $\bm{\hat{b}(s)}\equiv\bm{\hat{t}(s)}\times\bm{\hat{n}(s)}$
\item Frenet Serret:
\[ \frac{\dd}{\dd s}\begin{pmatrix} \unitvec{t}(s) \\ \unitvec{n}(s) \\ \unitvec{b}(s) \end{pmatrix} = \begin{pmatrix}
0 & \kappa(s) & 0 \\
-\kappa(s) & 0 & \tau(s) \\
0 & -\tau(s) & 0
\end{pmatrix}
\begin{pmatrix} \unitvec{t}(s) \\ \unitvec{n}(s) \\ \unitvec{b}(s) \end{pmatrix}.
\]
\end{itemize}

\section*{Fluid mechanics}
\begin{itemize}
\item Continuity equation: $\nabla\cdot\bvec{v}=0$, $\partial_i v_{i}=0$
\item Bernouilli: $p+\frac{1}{2}\rho v^2+\rho gh=\mbox{constant}$
\item Euler's equation: $-\nabla p + \rho \bvec{g} =\rho \left[\frac{\partial v}{\partial t} +(v \cdot \nabla ) v\right]$
\item Navier-Stokes: $\rho [\partial_t\bvec{v}+(\bm{v}\cdot\nabla)\bvec{v}]=-\nabla p+\eta\Delta\bvec{v}+\bvec{f}^\mathrm{ext}$
\item Navier-Stokes: $\rho [\partial_t v_{i} + v_{j}\partial_j v_{i}]=-\partial_i p+\partial_j\sigma_{ij}+f_{i}$
\item Stokes' equation: $-\nabla p+\eta\Delta\bvec{v}+\bvec{f}^\mathrm{ext}=0$
\item Free energy of depletion:
\[ F_\mathrm{depletion} = N \kB T  \left[ -\log\left(\frac{V-V_\mathrm{ex}}{v} \right) + \log\left(\frac{V}{v} \right) \right] = N \kB T \log\left(\frac{V}{V-V_\mathrm{ex}} \right) \approx N \kB T \frac{V_\mathrm{ex}}{V}, \]
where $N$ is the number of small particles with volume~$v$, and $V-\mathrm{ex}$ the excluded volume of the two large particles.
\end{itemize}

\section*{Solutions and gels}
\begin{itemize}
\item Gibbs-Duhem relation: $S \,\dd T - V \,\dd p + \sum_i N_i \,\dd \mu_i = 0$.
\item Virial expansion of the ideal gas law: $\frac{p}{\kB T} = n + B_2 (T) n^2 + B_3(T) n^3 + \mathcal{O}(n^4)$.
\item Flory-Huggins free energy for binary mixtures:
\begin{align*}
F = N \kB T  \left[ \phi \log(\phi) + (1-\phi) \log(1-\phi) + \chi \phi (1-\phi) \right].
\end{align*}
\item Gel free energy per unit volume
\begin{align*}
f_\mathrm{gel}(\phi) = \frac32 G_0 \left[ \left( \frac{\phi_0}{\phi} \right)^{2/3} - 1 \right] + \frac{\phi_0}{\phi} f_\mathrm{mix}(\phi).
\end{align*}
\item Linearized Poisson-Boltzmann equation
\begin{align*}
\nabla^2 \psi(\bvec{r}) = \frac{1}{\xi_\mathrm{D}^2} \psi(\bvec{r}) \qquad \mbox{with} \qquad \xi_\mathrm{D} = \sqrt{\frac{\varepsilon \kB T}{2 c_0 e^2}} \qquad \mbox{the Debye length.}
\end{align*}
\item Gibbs-Donnan equilibrium
\begin{align*}
n_\mathrm{p}^\pm = n_\mathrm{s} \exp(\pm \frac{e \Delta \psi}{\kB T}) = \frac12 \left[ \pm n_\mathrm{b} + \sqrt{n_\mathrm{b}^2 + 4 n_\mathrm{s}^2} \right].
\end{align*}
\end{itemize}

\section*{Complex fluids}
\begin{itemize}
\item Rheology: $G^*(\omega)=G'(\omega)+iG"(\omega)=\text{storage+loss}$. Special cases:
\[
    G^*(\omega) = 
\begin{cases}
    G_\mathrm{e},& \text{solid}\\
    i\omega\eta,& \text{liquid}
\end{cases}
\]
\item UCM: $\sigma_{ij}+\tau\left[\frac{\partial\sigma^{ij}}{\partial t}+v_{k}\frac{\partial\sigma^{ij}}{\partial x^{k}}-\frac{\partial v_{i}}{\partial x^{k}}\sigma^{kj}-\frac{\partial v^{j}}{\partial x^{k}}\sigma^{ik}\right]=\eta \dot{\gamma}_{ij}$, \quad $\dot{\gamma}_{ij}=\partial_i v_{j}+\partial_j v_{i}$
\end{itemize}

\section*{Membranes}
In general, a two-dimensional surface embedded in three-dimensional space can be described as a function of two parameters, say $u_1$ and $u_2$:
\begin{equation}
\label{surfaceembedding}
\bvec{r}(u_1, u_2) = \begin{pmatrix} x(u_1, u_2) \\ y(u_1, u_2) \\ z(u_1, u_2) \end{pmatrix}.
\end{equation}
The tangent vectors (contravariant basis vectors) of the surface are given by $\bvec{e}_i = \partial_i \bvec{r} = \partial \bvec{r} / \partial u_i$, the components of the metric are $g_{ij} = \bvec{e}_i \cdot \bvec{e}_j$, the normal is $\unitvec{n} = \bvec{e}_1 \times \bvec{e}_2 / \sqrt{\det(g_{ij})}$, and the components of the second fundamental form are $C_{ij} = (\partial_{ij} \bvec{r}) \cdot \unitvec{n} = (\partial_i \bvec{e}_j) \cdot \unitvec{n}$. The mean and Gaussian curvature of the surface can be calculated from the first and second fundamental forms:
\begin{equation}
H = -\frac12 \mathrm{Tr}(\bvec{C}) = -\frac12 g^{ij} C_{ij} = \frac12 \bvec{\nabla} \cdot \unitvec{n}, \qquad K = \frac{\det(C_{ij})}{\det(g_{ij})}.
\end{equation}

\noindent The Canham-Helfrich free energy for a membrane is given by the expansion in the curvature to second order:
\begin{equation}
\label{CH}
\mathcal{E}_\mathrm{CH} = \int_S \left[ \frac{\kappa}{2} (2H - C_0)^2 + \bar{\kappa} K + \sigma \right] \,\dd A,
\end{equation}
where $\kappa$ is the bending modulus, $\bar{\kappa}$ (sometimes denoted $\kappa_\mathrm{G}$) the Gaussian modulus, $C_0$ the spontaneous curvature, $\sigma$ the surface tension (2D pressure), and $\dd A$ the area element, which can be calculated as $\dd A = \sqrt{\det(g_{ij})} \,\dd u_1 \,\dd u_2$. The Gauss-Bonnet theorem states that the integral of $K$ over a closed surface depends only on the topology of the surface:
\begin{equation}
\label{GB}
\oint_S K \,\dd A = 2 \pi \chi(S) = 4 \pi (1-g(S)),
\end{equation}
where $\chi(S)$ is the Euler characteristic and $g(S)$ the genus (the number of holes) of the surface.

\bigskip\noindent For axisymmetric vesicles we have (see figure~\ref{fig:membranecoordinates}b)
\begin{alignat*}{2}
\dot{r} &= \frac{\dd r}{\dd s}=\cos\psi(s), &\quad \dot{z} &= \frac{\dd z}{\dd s}=-\sin\psi(s) \\
H &= \frac{1}{2} \left(\dot{\psi}+\frac{\sin\psi(s)}{r(s)}\right), &\quad K &= \frac{\sin\psi(s)}{r(s)}\dot{\psi}
\end{alignat*}
and in the Monge gauge:
\begin{align*}
r(x,y) &= \left(x, y, u(x,y)\right), \quad H \approx -\frac{1}{2}(u_{xx} + u_{yy}), \quad \dd S \approx \left( 1 + \frac{1}{2} (u_x^2 + u_y^2)\right) \,\dd x \,\dd y \\
\mathcal{E}_\mathrm{CH} &= \frac12 \int_S \left[ \kappa (\nabla_\perp^2 u)^2 + \sigma (\nabla_\perp u)^2 \right] \,\ddx \,\ddy
\end{align*}

\begin{figure}[htb]
\begin{center}
\includegraphics[scale=.95]{membranecoordinates.pdf}
\end{center}
\caption{Membrane curvature. (a) Curvature of a two-dimensional surface embedded in three dimensions. The thick red lines indicate the principal directions, the inverse of the radii of their tangent circles the principal curvatures. (b) Coordinate system on an axisymmetric vesicle. The $z$-axis coincides with the axis of symmetry. The vesicle is parametrized using the arc length~$s$ along the contour. The radial coordinate $r$ gives the distance from the symmetry axis and the coordinate~$z$ the distance along that axis. The shape of the vesicle be given as $r(z)$, $r(s)$, or in terms of the contact angle $\psi$ of the contour as a function of either $s$ or $r$. The geometric relations between $r$, $z$ and $\psi$ are given above.}
\label{fig:membranecoordinates}
\end{figure}

\clearpage
\section*{Vector derivatives in cylindrical and spherical coordinates}
Expressions are for a function~$f$ and a vector field~$\bvec{v}$.
\subsection*{Cylindrical coordinates}
\begin{align*}
\nabla f &= \diff{f}{r} \unitvec{e}_r + \frac{1}{r}\diff{f}{\theta} \unitvec{e}_\theta + \diff{f}{z} \unitvec{e}_z \\
\nabla^2 f &= \frac{1}{r} \frac{\partial}{\partial r} \left( r \diff{f}{r} \right) + \frac{1}{r^2} \frac{\partial^2 f}{\partial \theta^2} + \frac{\partial^2 f}{\partial z^2}\\
\nabla \cdot \bvec{v} &= \frac{1}{r} \frac{\partial}{\partial r} (r v_r) + \frac{1}{r} \frac{\partial}{\partial\theta} (\sin\theta v_\theta) + \diff{v_z}{z} \\
\nabla^2 \bvec{v} &= \left( \nabla^2 v_r - \frac{v_r}{r^2} - \frac{2}{r^2} \diff{v_\theta}{\theta} \right) \unitvec{e}_r + \left( \nabla^2 v_\theta - \frac{v_\theta}{r^2} + \frac{2}{r^2} \diff{v_r}{\theta} \right) \unitvec{e}_\theta + \nabla^2 v_z \unitvec{e}_z
\end{align*}


\subsection*{Spherical coordinates}
\begin{align*}
\nabla f &= \diff{f}{r} \unitvec{e}_r + \frac{1}{r}\diff{f}{\theta} \unitvec{e}_\theta + \frac{1}{r \sin \theta} \diff{f}{\phi} \unitvec{e}_\phi \\
\nabla^2 f &= \frac{1}{r^2} \frac{\partial}{\partial r} \left( r^2 \diff{f}{r} \right) + \frac{1}{r^2 \sin\theta} \frac{\partial}{\partial \theta} \left( \sin\theta \diff{f}{\theta} \right) + \frac{1}{r^2\sin^2\theta} \frac{\partial^2 f}{\partial \phi^2} \\
\nabla \cdot \bvec{v} &= \frac{1}{r^2} \frac{\partial}{\partial r} (r^2 v_r) + \frac{1}{r \sin \theta} \frac{\partial}{\partial\theta} (\sin\theta v_\theta) + \frac{1}{r \sin\theta} \diff{v_\phi}{\phi} \\
\nabla^2 \bvec{v} &= \left( \nabla^2 v_r - \frac{2v_r}{r^2} - \frac{2}{r^2\sin\theta} \diff{\sin\theta v_\theta}{\theta} - \frac{2}{r^2\sin\theta} \diff{v_\phi}{\phi}\right) \unitvec{e}_r + \left( \nabla^2 v_\theta - \frac{v_\theta}{r^2\sin^2\theta} + \frac{2}{r^2} \diff{v_r}{\theta} - \frac{2\cos\theta}{r^2\sin^2\theta} \diff{v_\phi}{\phi} \right) \unitvec{e}_\theta \\
& \quad + \left( \nabla^2 v_\phi - \frac{v_\phi}{r^2\sin^2\theta} + \frac{2}{r^2\sin\theta} \diff{v_r}{\phi} + \frac{2\cos\theta}{r^2\sin^2\theta} \diff{v_\theta}{\phi} \right) \unitvec{e}_\phi
\end{align*}


\section*{Components of the strain tensor in cylindrical and spherical coordinates}
\label{sec:stresspolarcoords}
In cylindrical coordinates ($r$, $\theta$, $z$), the components of the strain tensor are:
\begin{subequations}
\label{straincylindricalcoords}
\begin{align}
\gamma_{rr} &= 2 \diff{u_r}{r},\\
\gamma_{r\theta} &= \diff{u_\theta}{r} - \frac{u_\theta}{r} + \frac{1}{r}\diff{u_r}{\theta},\\
\gamma_{\theta \theta} &= \frac{2}{r}\diff{u_\theta}{\theta} + 2\frac{u_r}{r},\\
\gamma_{\theta z} &= \frac{1}{r} \diff{u_z}{\theta} + \diff{u_\theta}{z},\\
\gamma_{zz} &= 2\diff{u_z}{z},\\
\gamma_{rz} &= \diff{u_r}{z} + \diff{u_z}{r}.
\end{align}
\end{subequations}
In spherical coordinates ($r$, $\theta$, $\phi$), the components read:
\begin{subequations}
\label{strainsphericalcoords}
\begin{align}
\gamma_{rr} &= 2 \diff{u_r}{r},\\
\gamma_{r\theta} &= \diff{u_\theta}{r} - \frac{u_\theta}{r} + \frac{1}{r}\diff{u_r}{\theta},\\
\gamma_{\theta \theta} &= \frac{2}{r}\diff{u_\theta}{\theta} + 2\frac{u_r}{r},\\
\gamma_{\theta \phi} &= \frac{1}{r} \left( \diff{u_\phi}{\theta} - u_\phi \cot\theta \right) + \frac{1}{r \sin\theta} \diff{u_\theta}{\phi},\\
\gamma_{\phi \phi} &= 2\left(\frac{1}{r \sin\theta} \diff{u_\phi}{\phi} + \frac{u_\theta}{r} \cot\theta + \frac{u_r}{r}\right),\\
\gamma_{r\phi} &= \frac{1}{r \sin\theta} \diff{u_r}{\phi} + \diff{u_\phi}{r} - \frac{u_\phi}{r}.
\end{align}
\end{subequations}


\section*{Components of equation for static equilibrium in cylindrical and spherical coordinates}
\label{sec:strainpolarcoords}
The equations for equilibrium in any coordinate system are given by:
\begin{alignat}{2}
\label{staticstressdiffeqgeneral}
\bvec{\nabla} \cdot \bvec{\sigma} + \bvec{f}^{\mathrm{ext}} &= 0 &\qquad & \mbox{in the bulk}, \\
\label{staticstressbcgeneral}
\bvec{\sigma} \cdot \unitvec{n} &= \bvec{P} &\qquad &\mbox{on the boundaries}.
\end{alignat}
In cylindrical coordinates ($r$, $\theta$, $z$), the bulk equation for equilibrium becomes:
\begin{subequations}
\label{stresscylindricalcoords}
\begin{align}
\frac{1}{r} \frac{\partial}{\partial r}(r \sigma_{rr}) + \frac{1}{r} \diff{\sigma_{r\theta}}{\theta} + \diff{\sigma_{rz}}{z} - \frac{\sigma_{\theta\theta}}{r} + f_r^{\mathrm{ext}} &= 0, \\
\frac{1}{r} \frac{\partial}{\partial r}(r \sigma_{r\theta}) + \frac{1}{r} \diff{\sigma_{\theta\theta}}{\theta} + \diff{\sigma_{\theta z}}{z} + \frac{\sigma_{r\theta}}{r} + f_\theta^{\mathrm{ext}} &= 0, \\
\frac{1}{r} \frac{\partial}{\partial r}(r \sigma_{rz}) + \frac{1}{r} \diff{\sigma_{\theta z}}{\theta} + \diff{\sigma_{zz}}{z} + f_z^{\mathrm{ext}} &= 0.
\end{align}
\end{subequations}
In spherical coordinates ($r$, $\theta$, $\phi$), we have for the bulk equation:
\begin{subequations}
\label{stresssphericalcoords}
\begin{align}
\diff{\sigma_{rr}}{r} + \frac{2}{r} \sigma_{rr} + \frac{1}{r} \diff{\sigma_{r \theta}}{\theta} + \frac{\cot(\theta)}{r} \sigma_{r\theta} + \frac{1}{r \sin(\theta)} \diff{\sigma_{r \phi}}{\phi} - \frac{1}{r} \left( \sigma_{\theta \theta} + \sigma_{\phi \phi} \right) + f_r^{\mathrm{ext}} &= 0, \\
\diff{\sigma_{r\theta}}{r} + \frac{2}{r} \sigma_{r\theta} + \frac{1}{r} \diff{\sigma_{\theta \theta}}{\theta} + \frac{\cot(\theta)}{r} \sigma_{\theta \theta} + \frac{1}{r \sin(\theta)} \diff{\sigma_{\theta \phi}}{\phi} + \frac{1}{r} \sigma_{r \theta} - \frac{\cot(\theta)}{r} \sigma_{\phi \phi} + f_\theta^{\mathrm{ext}} &= 0, \\
\diff{\sigma_{r\phi}}{r} + \frac{2}{r} \sigma_{r\phi} + \frac{1}{r} \diff{\sigma_{\theta \phi}}{\theta} + \frac{1}{r \sin(\theta)} \diff{\sigma_{\phi \phi}}{\phi} + \frac{1}{r} \sigma_{r\phi} + \frac{2\cot(\theta)}{r} \sigma_{\theta \phi} + f_\phi^{\mathrm{ext}} &= 0.
\end{align}
\end{subequations}

%
%





\end{document}


Drag force: $\bm{F}_d=-6\pi\eta R\bm{\upsilon}$\par
Reynolds Number: $Re=\frac{\rho\upsilon L}{\eta}$ 
\par
Elastic solid: $\sigma=G\gamma $\par
Newtonian fluid: $\sigma=\eta\dot{\gamma}$\par
Strain: $\epsilon_{ij}=\frac{1}{2}((\nabla u)+(\nabla u)^T)$
$\epsilon=\frac{1}{2}(\frac{\partial u_i}{\partial x_j}+\frac{\partial u_j}{\partial x_i})$\par
Hooke's law: $\sigma_{ij}=K\epsilon_{kk}\delta_{ij}+2G(\epsilon_{ij}-\frac{1}{3}\epsilon_{kk}\delta_{ij})$\par
Inverse Hooke's:
$\epsilon_{ij}=\frac{1}{9K}\sigma_{kk}\delta_{ij}+\frac{1}{2G}(\sigma_{ij}-\frac{1}{3}\sigma_{kk}\delta_{ij})$\par
Navier-Stokes: $\rho [\partial_t\bm{\upsilon}+(\bm{\upsilon}\cdot\nabla)\bm{\upsilon}]=-\nabla p+\eta\Delta\upsilon+\bm{f}^{ext} $
\par
Force per volume: $f_i=\partial_j\sigma|_{ij}$\par
Complete NS: $\rho [\partial_tv_{i}+v_{j}\partial_jv_{i}]=-\partial_ip+\partial_j\sigma_{ij}+f_{i}$\par
Weissenberg Number: $Wi=\frac{elastic}{viscous}=\frac{v\tau}{L}$, $\tau=\frac{\eta}{G_{e}}$
\par
Deborah number: $De=\frac{\tau}{T}=\omega\tau$
\par
Stokes' equation: $-\nabla p+\eta\Delta\bm{\upsilon}+\bm{f}^{ext}=0$
\par
Continuity equation: $\nabla\cdot\bm{\upsilon}=0$, $\partial_iv_{i}=0$\par

Bernouilli: $p+\frac{1}{2}\rho\upsilon^2+\rho gh=constant$\par
Canham-Helfrich Energy: $\mathcal{E}=\int_{\mathcal{M}}(\frac{\kappa}{2}(2H)^2+\sigma)dS+p\int dV$ \par
Force on surface: $\bm{F}=\int \bm{f}dV=\int \nabla\cdot\bm{\sigma}dV=\oint\bm{\sigma}\cdot d\bm{A}=\oint\bm{\hat{n}}\cdot\bm{\sigma}dA$
\par
Hooke's law: $\sigma_{ij}=K\gamma_{kk}\delta_{ij}+2G(\gamma_{ij}-\frac{1}{3}\sigma_{kk}\delta_{ij})$\par
Young's modulus: $E=\frac{9GK}{G+3K}$\par
Poisson ratio: $\nu=\frac{1}{2}\frac{3K-2G}{3K+G}$\par
Free energy: $\mathcal{F}=\frac{E}{2(1+\nu)}[\gamma_{ij}^2+\frac{\nu}{1-2\nu}\gamma_{kk}^2]$\par
Hooke's law:\par
$\mathcal{F}=\frac{E}{2(1+\nu)}[\gamma_{ij}^{2}+\frac{\nu}{1-2\nu}\gamma_{kk}^{2}]$\par
$\sigma_{ij}=\frac{E}{1+\nu}[\gamma_{ij}+\frac{\nu}{1-2\nu}\gamma_{kk}\delta_{ij}]$\par
$\gamma_{ij}=\frac{1}{E}[(1+\nu)\sigma_{ij}-\nu\sigma_{kk}\delta_{ij}]$\par
UCM: $\sigma_{ij}+\tau[\frac{\partial\sigma^{ij}}{\partial t}+v_{k}\frac{\partial\sigma^{ij}}{\partial x^{k}}-\frac{\partial v_{i}}{\partial x^{k}}\sigma^{kj}-\frac{\partial v^{j}}{\partial x^{k}}\sigma^{ik}]=\eta \dot{\gamma_{ij}}$, $\dot{\gamma_{ij}}=\partial_i v_{j}+\partial_j v_{i}$\par
FJC Probability distribution 3D: $P(\bm{r})=(\frac{3}{2\pi Nb^2})^{3/2}e^{-\frac{3r^2}{2Nb^2}}$\par
Curvature energy: $\mathcal{E}_{curv}=\frac{K}{2}\int_0^L\frac{d\bm{\hat{t}}}{ds}\cdot\frac{d}{ds}=\frac{K}{2}\int_0^L\kappa(s)^2ds$\par
3D Curvature: $binormal\ \bm{\hat{b}(s)}\equiv\bm{\hat{t}(s)}\times\bm{\hat{n}(s)}$

\[Frenet Seret:\ \frac{d}{ds}\begin{pmatrix}
\bm{\hat{t}(s)}\\ 
\bm{\hat{n}(s)}\\ 
\bm{\hat{b}(s)}
\end{pmatrix}=\begin{pmatrix}
0 &\kappa  &0 \\ 
-\kappa &0  &\tau \\ 
0 &-\tau  & 0
\end{pmatrix}\begin{pmatrix}
\bm{\hat{t}(s)}\\ 
\bm{\hat{n}(s)}\\ 
\bm{\hat{b}(s)}
\end{pmatrix}\]

Wormlike chain bending energy: $\mathcal{E}_{WLC}=\frac{1}{2}K_{eff}\int_{0}^{L}(\frac{d\hat{\bm{t}}}{ds})^2ds=\frac{1}{2}\xi_pk_BT\int_0^L(\frac{d\hat{\bm{t}}}{ds})^2ds$\par
Axisymmetric vesicles:\par
$\dot{r}=\frac{dr}{ds}=cos\psi(s), \dot{z}=\frac{dz}{ds}=-sin\psi(s)$\par
$H=-\frac{1}{2}(\dot{\psi}+\frac{sin\psi(s)}{r(s)})$\par
$K=\frac{sin\psi(s)}{r(s)}\dot{\psi}$\par
Monge Gauge:\par
$u(x,y)=\left<x,y,r(x,y)\right>$\par
$H\approx\frac{1}{2}(u_{xx}+u_{yy})$\par
$dS\approx(1+\frac{1}{2}(u_x^2+u_y^2))dxdy$\par
Euler's equation: $-\nabla p + \rho \textbf{g} =\rho [\frac{\partial v}{\partial t} +(v*\nabla ) v]$\par
Change in Entropy: $dE=TdS-pdV+\mu dN$\par
Ideal gas law: $pV=NK_B T$\par
Rheology: 
$G^*(\omega)=G'(\omega)+iG"(\omega)=\text{storage+loss}$
\[
    G^*(\omega)
\begin{cases}
    Ge,& \text{Solid}\\
    i\omega\eta,& \text{Liquid}
\end{cases}
\]




