\chapter{Liquid crystals}
\label{ch:liquidcrystals}

\begin{center}
\parbox{12cm}{%We introduce the Canham-Helfrich free energy which will allow us to calculate the minimal-energy shapes of biomembranes.
In this chapter we study the properties of liquid crystals, materials that exhibit phases `between' liquid and solid, due to the elongated shape of their constituent particles. Next to the positional degrees of freedom, these elongated particles also have orientational degrees of freedom, which leads to the emergence of additional phases and additional modes of deformation compared to materials consisting of isotropic particles. Moreover, the elongated particle shape leads to the emergence of topological defects in the material, places where the orientational order is not defined. These defects have many applications, from identifying fingerprints to creating spots of growth in biological tissues.}
\end{center}

\newpage
\section{Liquid crystal phases}
Many materials consist of components that can be modeled with reasonable accuracy as spheres. These materials can exist in a crystalline solid phase, in which the spheres are all neatly arranged in a lattice. They can also exist in a fluid state, in which the spheres are unordered; depending on the density, the state can correspond to a liquid or a gas. This model can also describe a `jammed' or `glassy' state, in which the packing of the spheres is not ordered, but at high enough density that they cannot move.

The same phases exist for materials that consist of particles that have an elongated shape. However, due to their shape, the particles have more degrees of freedom than their positional ones. Their orientations also matter, as you have doubtlessly experienced many times when trying to pack elongated objects like pencils, matches, or nails into a container. Even for the simplest examples, where the particles are elongated along one axis, but retain their mirror-symmetry (e.g. ellipsoids and (sphero)-cylinders), two distinct new phases emerge, see figure~\ref{fig:liquidcrystalphases}. At high temperatures, we have an isotropic fluid, meaning that both the positions and the orientations of the particles are random. When lowering the temperature, the phase that usually emerges first is a \index{nematic phase}\emph{nematic}: the particles align their orientational direction, but their positions remain random. Cooling down further leads to the formation of a \index{smectic phase}\emph{smectic}, in which the particles arrange in layers, but keep random positions within the layer, making in effect something resembling a stack of lipid bilayer membranes. Depending on the orientation of the particles within the layer, the phase can be smectic-A, with the orientation perpendicular to the layer, or smectic-C, with a net orientation at some angle relative to the layer normal.

\begin{figure}[ht]
\centering
\includegraphics[width=\columnwidth]{liquidcrystals/liquidcrystalphases}
\caption[Liquid crystal phases.]{Liquid crystal phases. (a) Nematic phase, in which there is orientational order but no positional order. (b) Smectic-A phase, in which the particles are ordered in layers, but disordered within the layers, and the orientational order is perpendicular to the layers. (c) Smectic-C phase, which is layered like the smectic-A phase, but with the orientational order at an angle to the layer normal. (d) Chiral nematic or cholosteric phase, which only occurs for chiral particles. Particles are colored by their angle in the horizontal plane to better illustrate their orientations. All images from~[\ref{liquidcrystalimages}], CC-BY S.A. 3.0.}
\label{fig:liquidcrystalphases}
\end{figure}

If the constituent particles are chiral, the liquid crystal phases change. The nematic changes into chiral nematic phases (also known as cholosteric phases, as they were first observed in derivatives of cholesterol), characterized by a twist of the molecules perpendicular to their long axis. Likewise, there is a chiral smectic phase, denoted smectic-C*, where the molecules are not only orientationally ordered at an angle with respect to the layer normal, but the order direction also twists from one layer to the next with a fixed angle, allowing us to define a chiral pitch in analogy with the helical pitch of a chiral polymer, see figure~\ref{fig:twistednematicandlcd}(a). In between the chiral nematic and the isotropic phase, chiral liquid crystals moreover have an additional phase, known as the \emph{blue phase}, where not the particles, but their (orientational) defects form a lattice that can scatter light.

In addition to the phases described above, which all apply to uniaxial, elongated particles, there are many other liquid crystal phases. Examples include discotic phases of disk-shaped particles, both nematic and columnar, conic phases of conically shaped particles, and lyotropic phases, which emerge in mixtures of solvent and amphiphilic particles; prime examples are lipid bilayer membranes and lipid micelles. In this chapter, we'll focus on the simplest case, of elongated uniaxial particles, though much of the theory we develop here can be combined with the theory of membranes to create a more accurate but also more complicated model.

Undoubtedly the best known application of liquid crystals in everyday life are liquid crystal displays, or LCDs. These displays exploit the liquid crystal property of \emph{birefringence}, which means that the way they transmit light is not isotropic. In particular, the polarization of the light may change when passing through a layer of material in a nematic or smectic phase. Liquid crystal displays consist of a layered structure (see figure~\ref{fig:twistednematicandlcd}(b)), with two polarizers oriented at right angles (so no light would pass if there would be nothing between the polarizers), two layers of electrodes, and a central layer of (typically) a chiral liquid crystal. The electrodes can switch the state of the liquid crystal from nematic to isotropic and back; in the nematic state, they re-orient the light, resulting in an illuminated pixel, while in the isotropic state, they at best scatter it, resulting in a black pixel.

\begin{figure}[ht]
\centering
\includegraphics[scale=1]{liquidcrystals/twistednematicandlcd.pdf}
\caption[Chiral liquid crystal phase and application.]{Chiral liquid crystal phase and application. (a) Twist in a chiral smectic-C* phase, where the orientation of the particles changes from one layer to the next, giving rise to a chiral pitch~$p$. Source~[\ref{twistednematicimage}], CC-BY S.A. 3.0. (b) LCD display layers. 1. Vertical polarizing filter. 2. Indium tin oxide (ITO) electrodes that block or pass light depending on voltage. 3. liquid crystal (typically a twisted nematic). 4. Glass substrate with a horizontally arranged layer of ITO electrodes. 5. Horizontal polarizing filter. 6. Either a reflective layer (to reflect incoming light back to the viewer, as common in a watch) or a light-emitting layer (as common in an LCD screen on a computer or television). Source~[\ref{LCDdisplayimage}], CC-BY S.A. 3.0.}
\label{fig:twistednematicandlcd}
\end{figure}

\section{Phase transitions}
\subsection{Director and order parameter}
Matter in a liquid crystal phase has a (at least locally) well-defined orientation. Usually we denote an orientation direction with a unit vector~$\unitvec{n}$. For a typical liquid crystal particle however, flipping the particle will not change the orientation, as they have a mirror symmetry. Instead of a vector, we therefore use a \index{director}\emph{director}, which is a vector without a head, sometimes also drawn as a vector with two heads. In three dimensions, we essentially identify two opposing points on the unit sphere. As we have no mathematical object to describe the director, we'll still use the unit vector~$\unitvec{n}$, but with the understanding that we have the added symmetry of $\unitvec{n} = - \unitvec{n}$.

As usual, we'll be looking for an order parameter to distinguish between the various possible phases. To construct one, we start with the individual particles. The orientation of each particle can be described by a unit director, just like the total orientation; we'll use $\unitvec{u}$ for the particle director. In the isotropic phase, the directors $\unitvec{u}$ will be uniformly distributed on the unit sphere, and the average director~$\unitvec{n}$ will be zero. In the nematic phase, $\unitvec{u}$ will on average align with $\unitvec{n}$. Note that this alignment need not be perfect for $\unitvec{n}$ to be well-defined; thermal fluctuations will allow for variations in the value of~$\unitvec{u}$ as we move through the material.

We can describe the distribution of the particle directors~$\unitvec{u}$ with a distribution function~$\psi(\unitvec{u})$. Naturally, $\psi(\unitvec{u})$ should be normalized, which means that we have
\begin{equation}
\label{LCdirectordistributionnormalization}
\int \psi(\unitvec{u}) \dd{S} = 1,
\end{equation}
where the integral is taken over the surface of the unit sphere. In the isotropic case, the distribution is uniform, and by equation~(\ref{LCdirectordistributionnormalization}), we have $\psi(\unitvec{u}) = 1/4\pi$. In the nematic phase, we expect that the average of $\unitvec{u}$ aligns with $\unitvec{n}$. Calculating this average, given the distribution $\psi(\unitvec{u})$, is straightforward:
\begin{equation}
\label{LCdirectoraverage}
\ev{\unitvec{u}} = \int \unitvec{u} \, \psi(\unitvec{u}) \dd{S}.
\end{equation}
Because the director is symmetric under inversion, $\unitvec{u} = - \unitvec{u}$, equation~(\ref{LCdirectoraverage}) will always evaluate to~$0$. We therefore consider the second moment of $\unitvec{u}$, or rather its components $u_i$, which is given by
\begin{equation}
\ev{u_i u_j} = \int u_i u_j \psi(\unitvec{u}) \dd{S}.
\end{equation}
In the isotropic case, we get\footnote{The factor $\frac13$ is because we're in three dimensions, and $\ev{u_x^2} = \ev{u_y^2} = \ev{u_z^2}$. In two dimensions, we'd get $\frac12$.} $\ev{u_i u_j} = \frac13 \delta_{ij}$ in three dimensions. To measure the order of our system, i.e., the distance away from the `disordered' (isotropic) state, we therefore use the following tensor order parameter:
\begin{equation}
\label{LCtensororderparameterdefinition}
Q_{ij} = \ev{u_i u_j - \frac13 \delta_{ij}}.
\end{equation}
By construction, $\bvec{Q}$ is symmetric and $\Tr(\bvec{Q}) = 0$. Nonetheless, it has a magnitude and (if the magnitude is not zero) a direction, which correspond to the preferred direction~$\unitvec{n}$ in the material and the degree of alignment~$S$ with that preferred direction. To find~$S$, we first observe that if $\unitvec{u}$ is perfectly aligned with $\unitvec{n}$, we get $\ev{u_i u_j} = n_i n_j$. In a nematic phase, we may expect the distribution of $\unitvec{u}$ to have rotational symmetry around~$\unitvec{n}$. By construction, the tensor order parameter~$\bvec{Q}$ then also has rotational symmetry around $\unitvec{n}$, and can therefore in general be written as $Q_{ij} = A n_i n_j + B \delta_{ij}$. Because the trace of $\bvec{Q}$ vanishes, we have $B = -\frac13A$, and  can write $Q_{ij}$ as
\begin{equation}
\label{LCtensororderparameter}
Q_{ij} = S \left( n_i n_j - \frac13 \delta_{ij} \right).
\end{equation} 
Note that by equation~(\ref{LCtensororderparameter}), $\bvec{Q}$ still contains two pieces of information: the direction of average alignment~$\unitvec{n}$, and the degree of alignment~$S$ of the individual particles along~$\unitvec{n}$. The parameter~$S$ nicely runs from $0$ (no alignment) to $1$ (perfect alignment). To directly calculate the scalar order parameter~$S$, we calculate the average of the square of the projection of $\unitvec{u}$ on $\unitvec{n}$, which gives
\begin{equation}
\ev{\left(\unitvec{u} \cdot \unitvec{n} \right)^2 - \frac13} = \ev{u_i n_i u_j n_j - \frac13} = n_i n_j Q_{ij} = \frac23 S,
\end{equation}
where we used equation~(\ref{LCtensororderparameterdefinition}) in the second equality and~(\ref{LCtensororderparameter}) in the third. We can thus calculate the scalar order parameter directly as
\begin{equation}
\label{LCscalarorderparameter}
S = \frac32 \ev{\left(\unitvec{u} \cdot \unitvec{n} \right)^2 - \frac13} = \frac12 \ev{3\cos^2(\theta)-1},
\end{equation}
where $\theta$ is the angle between $\unitvec{u}$ and $\unitvec{n}$. We note that we can also write $S$ as $S = \ev{P_2(\cos(\theta))}$, with $P_2$ the second Legendre polynomial. As this polynomial describes deformations of the sphere where one axis is compressed or extended (and the other two correspondingly extend or compress), it is the lowest-order mode that satisfies the rotational and inversion symmetry of the nematic phase, and it is therefore not surprising that we encounter it here.

In two dimensions, the calculation goes analogously, substituting factors $\frac12$ for $\frac13$ where appropriate. The result can then be elegantly written in terms of $2\theta$:
\begin{equation}
\label{LCscalarorderparameter2D}
S_\mathrm{2D} = 2 \ev{\cos^2(\theta) - \frac12} = \ev{\cos(2\theta)}.
\end{equation}

\subsection{Landau free energy for the nematic-to-isotropic phase transition}
\label{LCLandau}
In the spirit of section~\ref{sec:Landautheory}, we can write down an expansion of the free energy density of our liquid crystal material, for the case where the order parameter is close to zero. As usual, our free energy density should be a scalar function and independent of any choice of coordinates. In terms of our tensor order parameter~$\bvec{Q}$, these conditions restrict us to invariants of~$\bvec{Q}$, which can all be cast in terms of the trace of integer powers of~$\bvec{Q}$, i.e., terms like $\Tr(\bvec{Q}^n)$, with $n$ any positive integer, and powers of such terms. As, by construction, $\Tr(\bvec{Q}) = 0$, the lowest-order terms in our expansion are given by\footnote{The factors $\frac32$ and $\frac92$ in the first line of~(\ref{LCLandauFE}) are just to make the second line come out nicely. For the third term, we could also have written $\Tr\left(\bvec{Q}^4\right)$; in general, these two fourth-order terms are different, but for a $3 \times 3$ symmetric traceless tensor they are multiples of each other and thus do not contribute separate terms.}
\begin{align}
\label{LCLandauFE}
f(T) &= f_2(T) \left(\frac32 \Tr\left(\bvec{Q}^2\right)\right) - f_3(T) \left(\frac92 \Tr\left(\bvec{Q}^3\right)\right) + f_4(T) \left(\frac32 \Tr\left(\bvec{Q}^2\right)^2\right) \nonumber \\
&= f_2(T) S^2 - f_3(T) S^3 + f_4(T) S^4.
\end{align}
As we would expect, the free energy density is a function of $S$ but not of $\unitvec{n}$, as there is no preferred direction in space. We expect a phase transition as we change the temperature, accompanied by a change in the sign of $f_2$. As we'll see below, the temperature at which this happens will not be the critical temperature, but at the point where the system becomes unstable, i.e., at the spinodal. We therefore label this temperature $T_\mathrm{s}$. Proceeding like in section~\ref{sec:Landautheory}, we then write, to linear order in the temperature, $f_2(T) = A (T-T_\mathrm{s})$, $f_3(T) = B$, and $f_4(T) = C$, with $A$, $B$ and $C$ positive constants. We can then plot the free energy as a function of $S$ for various values of $T$, see figure~\ref{fig:LCLandaufreenergy}. From the figure, we see that we get a first order phase transition at a critical temperature $T_\mathrm{c}$, for which there are two minima at equal height, one at $S=0$ (corresponding to the isotropic phase) and one at $S = S_\mathrm{c} > 0$ (corresponding to the nematic phase). However, metastable states can exist beyond the critical temperature. When cooling down, the local minimum at $S=0$ does not disappear until we reach the temperature $T_\mathrm{s}$, at which point the system becomes globally unstable and will always transition into the nematic phase. Likewise, when heating up from the nematic phase, the system can stay in the metastable local minimum at nonzero $S$ until another critical temperature is reached, the local minimum disappears, and the only remaining option is the isotropic phase\footnote{Note though that the only temperature at which the isotropic and nematic phases can coexist is $T_\mathrm{c}$. Any isotropic patch in a system with temperature above $T_\mathrm{c}$ will grow, as that lowers the total energy; likewise, any nematic patch will grow if the temperature is below $T_\mathrm{c}$. There is no chemical potential associated with these phases, as there is only one type of particle in the system; therefore, there will be no common tangent like construction describing coexisting phases at a different temperature, as we had in section~\ref{sec:mixtures}.}.

\begin{figure}[ht]
\centering
\includegraphics[scale=1]{liquidcrystals/LCLandaufreeenergy.pdf}
\caption[Landau free energy density of a liquid crystal.]{Landau free energy density~(\ref{LCLandauFE}) of a liquid crystal, showing two minima at equal depth at the critical temperature~$T_\mathrm{c}$ (blue line) and the disappearance of the minimum at $S=0$ at the instability temperature~$T_\mathrm{s}$. Note that the system can be in the metastable local minimum at $S = 0$ for $T_\mathrm{s} < T < T_\mathrm{c}$ when cooling it down from the isotropic phase (green line). Likewise, when heating up from the nematic phase, the system can stay in a local minimum at $S > 0$ (purple line) until it reaches the point where this minimum disappears (between the purple and brown lines). Both graphs show the same curves, with a different zoom level.}
\label{fig:LCLandaufreenergy}
\end{figure}

\section{Deformations: liquid crystal elasticity}
\label{sec:LCelasticity}

\subsection{Splay, twist and bend}
Liquid crystals in an ordered phase, like the lipid molecules in a bilayer membrane, respond elastically to deformation. Compared to the membrane however, they have more deformation modes, due to their three-dimensional arrangement. For a nonchiral nematic, the three deformation modes are known as \emph{splay}, \emph{twist} and \emph{bend}. A splay deformation is like a cone, with particles `fanning out', corresponding to a nonzero divergence of the director field~$\unitvec{n}$. A bend deformation is like a curve, with particles following a bent line, corresponding to a nonzero rotation of the director field~$\unitvec{n}$ about a vector perpendicular to the plane of the particles. A twist deformation is a relative shift in the direction of the director field~$\unitvec{n}$ as we move from one plane of particles to the next.

All three deformations can be described by considering a quadratic form in the gradient of the tensor order parameter~$\bvec{Q}$. In contrast to the previous section, where the free energy was independent of the direction, but dependent on the scalar order parameter~$S$, the value of the scalar order parameter will be roughly constant in a material at fixed temperature. Deforming the material however will result in gradients $\bvec{\nabla} \unitvec{n}$ of the director field. As usual, we'll invoke symmetry to rule out linear terms in this gradient, and thus the lowest-order terms in the deformation free energy of the liquid crystal should be of the form
\begin{equation}
\label{LCeleasticfreeenergygeneral}
F_\mathrm{el} = \frac12 \int K_{ijkl} \nabla_i n_j \nabla_k n_l \dd{V}.
\end{equation}
In equation~(\ref{LCeleasticfreeenergygeneral}), the $K_{ijkl}$ are the components of a four-tensor that themselves may depend on~$\unitvec{n}$ (but not its gradients). There are however more symmetries that we should take into account, and will reduce the number of independent components of $\bvec{K}$: the inversion symmetry of $\unitvec{n}$ and global rotational symmetry. Moreover, as $\unitvec{n}$ is a unit vector, we have $n_i \nabla_j n_i = 0$. With these constraints, after some algebra, we find ourselves left with three independent components of $\bvec{K}$, which correspond to the three deformation modes indicated above: a term with nonzero $\bvec{\nabla} \cdot \unitvec{n}$ for the splay, a term with nonzero $\unitvec{n} \cdot (\bvec{\nabla} \cross \unitvec{n})$ measuring twist, and a term with nonzero $\unitvec{n} \cross (\bvec{\nabla} \cross \unitvec{n})$ accounting for the bend. Together, they form the \index{Frank free energy}\emph{Frank free energy} of a nematic liquid crystal:
\begin{equation}
\label{LCFrankFE}
F_\mathrm{Frank} = \frac12 \int \left[ K_1 \left(\bvec{\nabla} \cdot \unitvec{n}\right)^2 + K_2 \left(\unitvec{n} \cdot (\bvec{\nabla} \cross \unitvec{n}) \right)^2 + K_3 \left(\unitvec{n} \cross (\bvec{\nabla} \cross \unitvec{n}) \right)^2 \right] \dd{V}.
\end{equation}
The constants~$K_i$ have dimension of energy/length, or force. We can estimate their magnitude in a `soft' (thermal energies matter) liquid crystal as $\kB T_\mathrm{c} / a$, with $T_\mathrm{c} = 400\;\mathrm{K}$ the temperature at which the isotropic-to-nematic phase transition occurs and $a \approx 2\;\mathrm{nm}$ the typical size of a molecule; the constants are then of order $3\;\mathrm{pN}$. As they are typically of comparable size, in many papers they are taken to have the same value (the one-constant approximation), in which case the second and third term of equation~(\ref{LCFrankFE}) can be combined into $|\bvec{\nabla} \cross \unitvec{n}|^2$.

For a chiral nematic, the equilibrium phase will have a nonzero twist. Like for a membrane with nonzero spontaneous curvature, we can account for it by including a linear term in the twist, which amounts to a `spontaneous twist' $q_0 = 2 \pi / p_0$, with $p_0$ the pitch of the cholosteric phase. We add this spontaneous twist to the second term in~(\ref{LCFrankFE}), which then reads $\left(\unitvec{n} \cdot (\bvec{\nabla} \cross \unitvec{n}) + q_0 \right)^2$.

Finally, another common extension is to include a coupling between the deformation of the bulk of the liquid crystal material and interactions with the surface. This term is known as the saddle-splay energy, which is given by
\begin{equation}
\label{LCsaddlesplayenergy}
F_\mathrm{surf} = \frac12 \int K_4 \bvec{\nabla} \cdot \left[ \left(\unitvec{n} \cdot \bvec{\nabla}\right) \unitvec{n} - \unitvec{n} \left(\bvec{\nabla} \cdot \unitvec{n}\right) \right] \dd{V}.
\end{equation}

\subsection{Topological defects}
\label{sec:LCdefects}
From the Frank free energy, it is clear that a nematic liquid crystal in equilibrium will have a well-defined director, and all particles in the material will be aligned with that director. There may be small fluctuations due to thermal motion, but globally the director is well-defined. If the particles would be polar and carry a magnetic dipole moment, they would form a magnetized state.

Just like for a magnet however, if we quickly cool down (`quench') a system that is initially in an isotropic state, the particles won't have time to all align to a global director. Instead, we'll get local domains of aligned particles with a well-defined director, separated by domain walls where two such domains meet. At a domain wall, we can still define a director, as the average of the directors of the two domains. When multiple domain walls come togehter however, the director becomes ill-defined. Such isolated points in a director field are known as \index{topological defects}\emph{topological defects}.

To get a feel for topological defects, we'll start in two dimensions. A good example is your fingerprint. Mostly, it consists of lines, which could represent the local director field of a nematic-like liquid crystal (the lines are then like the pathlines of the particles, tangent everywhere to the particle direction). However, as you'll quickly see when looking at a fingerprint, the overall round shape near the tip and roughly straight line at the bottom often result in at least two points where a line either terminates or splits\footnote{Fingerprints come in many shapes and forms. It is possible (though uncommon) to have a fingerprint without any defect (known as an arch), a `tented arch' with a pair of defects, a `whorl' with four defects (two $-\frac12$ at the bottom, with lines leading to two $+\frac12$ defects where the lines spiral around each other), and so on.}. At these points, the director field~$\unitvec{n}$ is not defined, see figure~\ref{fig:LCdefects}(a).

We can assign a topological charge to a defect by considering how it affects the (otherwise continuous) director field. To do so, we consider a closed loop around a single defect. As we traverse the loop, we measure how the director changes, see figure~\ref{fig:LCdefects}(b). Our loops here are simple circles that we traverse clockwise. For easier visualization, we've overlayed an arrow on each of the particles. Starting from the red arrow at the bottom, if we go around clockwise, we see that the arrows turn clockwise as well. However, when we return to the bottom, the arrow points in the opposite direction. The arrow thus rotated over only~$180^\circ$, i.e., the director field rotated by half a turn. As the rotation of the director field is in the same direction as we traversed the circle (both clockwise), we call this a $+\frac12$ defect.

For the lower (green) defect, the same operation also yields a rotation of the arrow by $180^\circ$, however, the rotation of the arrow now goes counterclockwise as we traverse the circle clockwise. We therefore classify this defect as $-\frac12$.

Some people refer to the `value' of the defects as their charge, others as their index (the convention we'll use). In addition to the $\pm \frac12$ defects, there are also $\pm 1$ defects, see figure~\ref{fig:LCdefects}(c). Note that there are two qualitatively different defects that both are $+1$: one with zero curl, and one with zero divergence. The $-1$ defect is a saddle point, with an unstable and a stable manifold meeting at the defect. Unlike the $\pm \frac12$ defects, $\pm 1$ defects also occur in vector fields. As will probably not surprise you, we can continue this exercise, and create defects with index $\pm \frac32$, $\pm 2$, and so on.

\begin{figure}[ht]
\centering
\includegraphics[width=\columnwidth]{liquidcrystals/defects.pdf}
\caption[Orientational defects in a liquid crystal.]{Orientational defects in a liquid crystal. (a) Defects occur where the director field (gray lines) is not defined. Examples are a point where the line terminates (red dot) and where it splits (green dot). Similar patterns occur in many settings, including fingerprints and leaves. (b) Topological defects carry a charge, which can be found traversing a closed curve around the defect, and measuring how the director field changes as we traverse the curve. In both examples, the director field makes a rotation of~$180^\circ$ (visualized by giving the director a polarization); however, for the red (top; right) defect, the director field rotates in the same direction as we traverse the curve, and for the green (bottom; left) defect, it rotates in the opposite direction. We therefore give the red defect a charge of $+\frac12$ (half rotation, same direction as the loop) and the green defect a charge of $-\frac12$ (half rotation, opposite direction of the loop). (c) Next to the $\pm\frac12$ defects, which only occur in director fields but not in vector fields, we also have $\pm 1$ defects, which occur in both director and vector fields. Note that the two defects on the left both have charge $+1$. We expect two such defects on a sphere with a vector field (hedgehog theorem); on a globe, they're at the poles, either in the upper form (as the point where lines of longitude originate or meet) or the lower form (as the point where lines of latitude disappear). A $-1$ defect is a saddle point.}
\label{fig:LCdefects}
\end{figure}

We can formalize the definition of the index by defining the \emph{index of a curve} index of any closed, oriented, simple\footnote{S curve is oriented if it has a defined direction in which we traverse it (in 2D, clockwise or counterclockwise), and simple if it does not intersect itself.} curve~$C$ as the number of rotations the field $\unitvec{u}$ makes as we traverse the curve. In two dimensions, we can write $\unitvec{u} = (\cos(\theta), \sin(\theta))$, where $\theta = \arctan(u_y/u_x)$ is the angle $\unitvec{u}$ makes with the positive $x$-axis. Outside a defect, the field $\unitvec{u}$ is smooth; therefore, the change in $\theta$ is continuous as we traverse~$C$. Moreover, when we return to the starting point, the director $\unitvec{u}$ must return to its initial orientation. The angle~$\theta$, however, can have changed by any integral multiple of $\pi$. If we define $[\Delta \theta]_C$ as the change in $\theta$ as we traverse $C$, we can define the \emph{index} of the curve $C$ with respect to the director field $\unitvec{u}$ as
\begin{equation}
\label{defcurveindex}
I_C = \frac{1}{\pi} [\Delta \theta]_C.
\end{equation}
The index has a couple of useful properties that we can prove rigorously.
\begin{theorem}[Properties of the curve index] Let $C$ be a closed, oriented curve and $I_C$ the index of that curve with respect to the director field $\unitvec{u}$. Then the following properties hold:
\begin{enumerate}
\item If $C$ can be deformed continuously without passing through a fixed point of $\unitvec{u}$ (i.e., a defect), then $I_C = I_{C'}$.
\item If $C$ does not enclose any fixed points of $\unitvec{u}$, then $I_C = 0$.
\item If $\unitvec{u}$ is everywhere tangent to $C$, then $I_C = + 1$. Likewise, if $\unitvec{u}$ is everywhere perpendicular to $C$, then $I_C = + 1$
\end{enumerate}
\end{theorem}
\begin{proof}
\begin{enumerate}
\item A continuous deformation of $C$ that does not cross a fixed point of $\unitvec{u}$ can only result in a continuous change in $I_C$. As $I_C$ is an integer, continuous changes in its value are impossible, hence $I_C$ must remain constant (a `continuous change' of zero).
\item As $C$ does not contain any fixed points, by property 1, we can change it continuously to a tiny circle without changing the value of its index $I_C$. On a small circle that does not enclose a fixed point of $\unitvec{u}$, by the smoothness of $\unitvec{u}$, the angle $\theta$ will essentially be constant, and thus $[\Delta \theta]_C = 0$, and therefore $I_C$ must equal zero.
\item The standard examples of these cases are shown in the leftmost column of figure~\ref{fig:LCdefects}(c). Given that any continuous deformation of $C$ without crossing through a defect does not affect the value of its index, we can deform the curves in the figure to arbitrary shapes, matching any tangent or perpendicular director field~$\unitvec{u}$, without changing the value of the index.
\end{enumerate}
\end{proof}
Given the notion of the index of a closed oriented curve, we can also define the index of a fixed point or defect. This index is simply the index of any closed oriented curve that encompasses this fixed point but no others.

\begin{theorem}
\label{thm:multipledefectsindex}
If a closed oriented curve~$C$ encompasses multiple isolated fixed points, then the index of the curve is the sum of the indexes of the fixed points.
\end{theorem}
\begin{proof}
In lieu of a full rigorous mathematical proof, we'll sketch the central idea. Consider an oriented curve that encompasses multiple isolated defects, as in figure~\ref{fig:multipledefectsindex}. Without crossing any of the defects, we can continuously deform the curve into a collection of circles, each surrounding an individual defect, connected by pairs of lines, which we traverse in opposite directions as we go around the deformed curve. For each pair of lines, as the local director field $\unitvec{u}$ is smooth, the change in the angle $\theta$ in one direction exactly cancels the change in the opposite direction. Therefore, the only contributions to the index are from the isolated defects, each of which is given by the index of the defect.
\end{proof}

\begin{figure}[ht]
\centering
\includegraphics[scale=1]{liquidcrystals/multipledefectsindex.pdf}
\caption[A curve surrounding multiple defects.]{Sketch of the proof of theorem~\ref{thm:multipledefectsindex}. The smooth curve surrounding multiple isolated fixed points (defects), can be continuously deformed (without crossing any defects) into a series of circles surrounding individual defects, connected by lines that are close together.}
\label{fig:multipledefectsindex}
\end{figure}

From the two theorems given above, it should be clear that the total topological charge inside a closed loop is conserved, as long as no defects cross the loop (or the loop is not deformed in such a way that it crosses any defects). That does not mean that defects cannot be formed or destroyed, but it does mean that in a system without a boundary, or with a set boundary on which the director field $\unitvec{u}$ is fixed everywhere, the total topological charge cannot change.

From the Frank free energy, it is easy to see that two $+\frac12$ defects have lower energy than a single $+1$ defect, and likewise two $-\frac12$ defects have lower energy than a single $-1$ defect. Therefore, in general, defects with higher charge will fall apart into defects with lower charge, and in liquid crystals we will only find defects with charge of $\pm\frac12$. Naturally, we can lower the energy further by bringing a $+\frac12$ and a $-\frac12$ defect together, letting them cancel each other (annihilate). Doing so will require us to move the defects around, i.e., make changes to the (local) director field~$\unitvec{u}$, which requires some energy. Thermal fluctuations can provide that energy up to some order; through thermal equilibration, a liquid crystal with no net topological charge can always be put into a homogeneous nematic state without defects. However, there are examples that carry intrinsic charge. A prime example is a field constrained to the surface of a sphere. The \bookhref{https://en.wikipedia.org/wiki/Euler_characteristic}{Euler characteristic}~$\chi$ of a sphere is $2$. We can get that number by drawing any graph on the surface of the sphere; the characteristic is then given by $\chi = V - E + F$, with $V$ the number of vertices, $E$ the number of edges, and $F$ the number of faces of the graph. For example, the Platonic solids, all of which represent graphs on the sphere, all have $\chi = 2$. We can also get it by calculating the solid angle of the whole surface, and dividing by $2 \pi$; this is why, in a standard soccer ball (or in the `buckyball' molecule $\mathrm{C}_{60}$), you always need $12$ pentagons in addition to the hexagons. For our liquid crystal, the total charge also must be $+2$. As a $+2$ charge would split in two $+1$ charges, which again each split in two $+\frac12$ charges, in equilibrium we expect to find four $+\frac12$ charges separated as far as possible from each other, making an tetrahedron. The resulting shape strongly resembles that of a baseball or tennis ball.
