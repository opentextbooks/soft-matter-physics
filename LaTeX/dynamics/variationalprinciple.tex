\chapter{A variational principle for overdamped particle motion}
\label{ch:softvariationalprinciple}

\begin{center}
\parbox{12cm}{In classical mechanics, you can start from Newton's laws of motion and predict trajectories of particles in arbitrary configurations. You can also derive results like conservation of energy and momentum. However, you can also start from Hamilton's variational principle, which tells you that particle trajectories are given by the minimization of an action, which is the integral of the Lagrangian. From this principle, you can derive the equations of motion using the Euler-Lagrange formalism. For hydrodynamical systems, a similar principle exists. It is known as the Onsager principle, with the Rayleighian replacing the Laplacian. In this chapter, we'll work out the formalism, and use it to study some examples that are hard to do with the methods of chapter~\ref{ch:dynamics}.}
\end{center}

\newpage

\section{Generalizing the Langevin and Stokes equations}
\label{sec:generalizedstokes}
\subsection{Brownian motion of particles of arbitrary shape}
When discussing diffusion in chapter~\ref{ch:dynamics}, we have tacitly assumed that our particles are spherical, and therefore their degrees of freedom are only positional. There are many nonspherical particles around though, so it would be nice if we could generalize to arbitrary shape and include rotational degrees of freedom. Fortunately, doing so is straightforward. Consider a collection of particles of arbitrary shape, the configuration of which is described by a set of generalized coordinates $\bvec{x} = (x_1, x_2, \ldots, x_k)$. We then generalize the potential force $F = -\dd U / \dd x$ by observing that for any of the coordinates, a small change $\dd x_i$ leads to a change in the potential $\dd U = - F_i \dd x_i$, so we can define a generalized `force'
\begin{equation}
\label{genpotforce}
F_i = - \diff{U}{x_i},
\end{equation}
where $F_i$ (the generalized potential force conjugate to $x_i$) can now be either a force (if $x_i$ represents a positional degree of freedom) or a torque (if $x_i$ represents a rotational degree of freedom). Likewise, the drag force can be genearlized by considering the (irreversible) work it does: $W = \sum_i F_{\mathrm{d},i} \dot{x}_i$, which gives
\begin{equation}
\label{generalizeddragforce}
F_{\mathrm{d},i} = -\sum_j \zeta_{ij}(x) \dot{x}_j,
\end{equation}
with $\zeta_{ij}(x)$ the generalized friction coefficients. Finally, the generalized random force $F_{\mathrm{r},i}(t)$ satisfies
\begin{equation}
\label{generalizedrandomforce}
\ev{F_{\mathrm{r},i}(t)} = 0, \qquad \ev{F_{\mathrm{r},i}(t) F_{\mathrm{r},j}(t')} = 2 \zeta_{ij} \kB T \delta(t-t').
\end{equation}
Force balance between the three generalized forces gives the generalized version of the Langevin equation:
\begin{equation}
\label{generalizedLangevin}
0 = \sum_j \zeta_{ij} \dot{x}_j - \diff{U}{x_i} + F_{\mathrm{r},i}(t).
\end{equation}

\subsection{Reciprocal relation of the generalized friction coefficients}
The generalized friction coefficients~$\zeta_{ij}(x)$, relating the change in the generalized coordinates $x_i$ to the generalized drag force (equation~\ref{generalizeddragforce}), have two useful properties. First, they are symmetric:
\begin{equation}
\label{Lorentzreciprocalrelation}
\zeta_{ij}(x) = \zeta_{ji}(x),
\end{equation}
and second they form a positive definite matrix, i.e., for any set of generalized velocities $\dot{x}_i$, we have
\begin{equation}
\label{genfrictposdef}
\sum_{i,j} \zeta_{ij}(x) \dot{x}_i \dot{x}_j \geq 0.
\end{equation}
The symmetry property, equation~(\ref{Lorentzreciprocalrelation}), is known as the \emph{Lorentz reciprocal relation}. It ultimately stems from the time reversal symmetry of Brownian motion, see Doi~\cite[Appendix D]{Doi2013}. It states that if a hydrodynamic drag in the $y$-direction on a particle moving in the $x$-direction is related to the drag in the $x$ direction for a particle moving in the $y$ direction, $F_{\mathrm{d},y}/v_x = F_{\mathrm{d},x}/v_y$. While this statement is trivial for a spherical particle (as the drag will be parallel to the direction of motion), it is nontrivial for rod-shaped and helical particles.

\subsection{Generalized Stokes flow}
The generalized Langevin equation~(\ref{generalizedLangevin}) reduces to a generalized version of the Stokes equation for fluid flow at zero Reynolds number if we ignore the random forces (valid when considering macroscopic objects). In that case the equation reads
\begin{equation}
\label{generalizedStokes}
\sum_j \zeta_{ij} \dot{x}_j - \diff{U}{x_i} = 0.
\end{equation}
Exploiting the two symmetry and positive definiteness of the generalized friction coefficients, we can write equation~(\ref{generalizedStokes}) as a total derivative. Introducing the function $R(\dot{x}; x)$ (the \index{Rayleighian}\emph{Rayleighian})
\begin{equation}
\label{Rayleighian}
R(\dot{x}; x) = \frac12 \sum_{i,j} \zeta_{ij} \dot{x}_i \dot{x}_j + \sum_i \diff{U(x)}{x_i} \dot{x}_i,
\end{equation}
it is straightforward to show that equation~(\ref{generalizedStokes}) follows from the condition that
\begin{equation}
\label{Rayleighianvariation}
0 = \diff{R(\dot{x}; x)}{\dot{x}_i} \quad \mbox{for}\; i = 1, 2, \ldots, k.
\end{equation}
Equation~(\ref{Rayleighianvariation}) bears strong similarity to the Euler-Lagrange equations from classical and quantum mechanics, with the Rayleighian replacing the Lagrangian. The main difference is that the Euler-Lagrange equations have an extra term with the derivative of the Lagrangian with respect to the coordinate itself, which is absent here (effectively because we are operating at zero Reynolds number). Like the Lagrangian, the Rayleighian is the sum of two terms, which can be considered as equivalents of the kinetic and potential energy. We have $R(\dot{x}; x) = \Phi(\dot{x}) + \dot{U}(\dot{x}; x)$, where
\begin{align}
\label{Rayleighianenergydissipation}
\Phi(\dot{x}) &= \frac12 \sum_{i,j} \zeta_{ij} \dot{x}_i \dot{x}_j, \\
\label{Rayleighianpotentialenergychange}
\dot{U}(\dot{x}; x) &= \sum_i \diff{U(x)}{x_i} \dot{x}_i.
\end{align}
The first term, $\Phi(\dot{x})$, is half\footnote{The factor~$\frac12$ is historical; it would have been more practical to leave it out of the definition of $\Phi$, but we're stuck with it.} the rate of energy dissipation in the system, while the second, $\dot{U}(\dot{x}; x)$, gives the rate of change of the potential energy if a particle moves (or rotates) at generalized velocity $\dot{x}_i$.

It may not be immediately obvious that equation~(\ref{generalizedStokes}) reduces to the Stokes equation~(\ref{Stokeseq}) of section~\ref{sec:Stokeseq} with the proper choice of $\Phi$ and $\dot{U}$. Let us therefore show it explicitly. The work done by the viscous drag per unit volume and unit time is given by
\begin{equation}
\label{viscousdragwork}
\sigma_{ij}' \dot{\gamma}_{ij} = \frac{\eta}{2} \left( \diff{v_i}{x_j} + \diff{v_j}{x_i} \right)^2.
\end{equation}
The energy dissipation rate $\Phi$ now becomes a \emph{functional} (function of a function) of the velocity $\bvec{v}$ (itself a function of position):
\begin{equation}
\label{Stokesenergydissipation}
\Phi[\bvec{v}(\bvec{x})] = \frac{\eta}{4} \int \left( \diff{v_i}{x_j} + \diff{v_j}{x_i} \right)^2 \dd \bvec{x}.
\end{equation}
The potential energy rate term just equals the work done by the external force
\begin{equation}
\label{Stokespotentialenergychange}
\dot{U}[\bvec{v}(\bvec{x})] = - \int ( \bvec{f}^\mathrm{ext} \cdot \bvec{v} ) \dd \bvec{x}.
\end{equation}
Finally, we include the condition that the fluid is incompressible, $\bvec{\nabla} \cdot \bvec{v} = 0$ via a Lagrange multiplier to the Rayleighian\footnote{We \emph{need} not do this - we could try the Rayleighian without the Lagrange multiplier and then try to enforce the condition. If you feel masochistic, you can try, and will quickly discovery that this is the much less painful road.}, which then reads:
\begin{equation}
\label{StokesRayleighian}
R[\bvec{v}(\bvec{x})] = \frac{\eta}{4} \int \left( \diff{v_i}{x_j} + \diff{v_j}{x_i} \right)^2 \dd \bvec{x} - \int ( \bvec{f}^\mathrm{ext} \cdot \bvec{v} ) \dd \bvec{x} - \int p(\bvec{x}) (\bvec{\nabla} \cdot \bvec{v}) \dd \bvec{x}.
\end{equation}
The condition~(\ref{Rayleighianvariation}) that the Rayleighian be minimized now means that its functional derivative (see appendix~\ref{sec:mathsfunctionalderivatives}) with respect to the velocity field vanishes:
\begin{equation}
0 = \frac{\delta R[\bvec{v}(\bvec{x})]}{\delta \bvec{v}},
\end{equation}
which (unsurprisingly) gives us exactly the Stokes equation, with the Lagrange multiplier $p(\bvec{x})$ cast in the role of the pressure:
\begin{equation}
0 = \eta \nabla^2 \bvec{v} - \bvec{\nabla} p + \bvec{f}^\mathrm{ext}.
\end{equation}

\section{Onsager variational principle}
\label{sec:variationalprinciple}
In section~\ref{sec:generalizedstokes} we considered translational and rotational degrees of freedom in a hydrodynamical system. The equations easily generalize to overdamped systems described by an arbitrary set of (slow) variables $\bvec{x} = (x_1, x_2, \ldots, x_k)$. As the system is overdamped, the time evolution of each variable is given by
\begin{equation}
\label{generalizedvariablesoverdampeddynamics}
\frac{\dd x_i}{\dd t} = \mu_{ij}(\bvec{x}) \diff{A}{x_j},
\end{equation}
where $A(\bvec{x})$ is the free energy of the system. The coefficients $\mu_{ij}(\bvec{x})$ are the kinetic coefficients, defined as the inverse of the friction coefficients~$\zeta_{ij}$ of section~\ref{sec:generalizedstokes}:
\begin{equation}
\sum_k \zeta_{ik} \mu_{kj} = \delta_{ij}.
\end{equation}
Equation~(\ref{generalizedvariablesoverdampeddynamics}) thus generalizes equation~(\ref{generalizedStokes}) to arbitrary variables. The corresponding Rayleighian is given by
\begin{equation}
\label{generalizedvariablesRayleighian}
R(\dot{\bvec{x}}, \bvec{x}) = \frac12 \sum_{i,j} \zeta_{ij} \dot{x}_i \dot{x}_j + \sum_i \diff{A(\bvec{x})}{x_i} \dot{x}_i = \Phi + \dot{A},
\end{equation}
and the Onsager variational principle states that minimizing $R(\dot{\bvec{x}}, \bvec{x})$ with respect to each of the $\dot{x}_i$ gives the time evolution of the system.

\section{Linear and rotational diffusion}
\subsection{A re-derivation of the Smoluchowski equation from the variational principle}
We take diffusion as a second example application of the variational principle. Let $c(\bvec{x}, t)$ be the particle concentration (as a function of position~$\bvec{x}$ and time~$t$), which satisfies the continuity equation
\begin{equation}
\label{concentrationcontinuity}
\diff{c}{t} + \bvec{\nabla} \cdot (c \bvec{v}) = 0.
\end{equation}
The energy dissipation function is given by
\begin{equation}
\label{diffusionenergydissipation}
\Phi[\bvec{v(\bvec{x})}] = \frac12 \int \zeta n v^2 \dd \bvec{x}, 
\end{equation}
while for the free energy we can write
\begin{equation}
\label{diffusionfreeenergy}
A[c(\bvec{x})] = \int \left[ \kB T c \log c + c U(\bvec{x}) \right] \dd \bvec{x}
\end{equation}
The time derivative of the free energy can be expressed in terms of the velocity through the continuity equation and integrating by parts. We find:
\begin{equation}
\dot{A} = \int \left[ \kB T (1+ \log c) + U(\bvec{x}) \right] \dot{c} \dd \bvec{x} = \int \left[ \kB T (\bvec{\nabla}c) + c (\bvec{\nabla}U) \right] \cdot \bvec{v} \dd \bvec{x}.
\end{equation}
Adding $\Phi$ and $\dot{A}$ to find the Rayleighian, and setting its variational derivative with respect to $\bvec{v}$ to zero, we find
\begin{equation}
\label{diffusionRayleighianminimization}
0 = \zeta c \bvec{v} + \kB T (\bvec{\nabla}c) + c (\bvec{\nabla}U).
\end{equation}
Taking the divergence of~(\ref{diffusionRayleighianminimization}) and again using the continuity equation we reproduce the Smoluchowski equation~(\ref{Smoluchowski}), with the diffusion constant given by the Stokes-Einstein relation~(\ref{StokesEinstein}), $D = \kB T / \zeta$.
