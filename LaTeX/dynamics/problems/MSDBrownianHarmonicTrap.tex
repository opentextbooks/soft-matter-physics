%Problemtitle: MSD of a Brownian particle in a harmonic potential
%Source: Doi section 6.3.2 (page 102).
In this problem, we'll calculate the mean squared displacement of a Brownian particle in a harmonic potential, as given by equation~(\bookref{Brownianmotionharmpotmsd}).
\begin{enumerate}[(a)]
\item Using separation of variables, derive the formal solution~(\bookref{Brownianmotionharmpot}) of the overdamped version of the Langevin equation~(\bookref{Langevinposition}) with a harmonic potential, i.e., $U(x) = \frac12 k x^2$.
\item Multiply $x(t)$ by $x(0)$ and find the ensemble average $\ev{x(t) x(0)}$. Using the properties of the random force, argue why the second term of your expression vanishes.
\item As the only force with nonzero average acting on the particle comes from the harmonic potential, the distribution of the position of the particle at a given point in time (and specifically at $t=0$) is a Boltzmann distribution, with the probability of finding the particle at position~$x$ proportional to $\exp(-U(x) / \kB T) = \exp(-kx^2 / 2 \kB T)$. From this distribution, find $\ev{x(0)}$ and $\ev{x(0)^2}$. Substitute these answers in the remaining term of (b) to get
\begin{equation}
\label{Brownianmotionharmpotmsdroot}
\ev{x(t) x(0)} = \frac{\kB T}{k} e^{-t/\tau_\mathrm{e}}.
\end{equation}
\item Finally, calculate the mean squared displacement $\ev{(x(t) x(0))^2}$. \textit{Hint}: the distribution of $x(t)$ is the same as that of $x(0)$.
\end{enumerate}


\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item We first solve the homogeneous equation
\begin{align*}
\zeta \dv{x}{t} &= -k x \\
\int \frac{\dv{x}}{x} &= - \frac{k}{\zeta} \int \dd{t} \\
x_\mathrm{h}(t) &= A e^{-t/\tau_\mathrm{e}},
\end{align*}
with $\tau_\mathrm{e} = \zeta / k$. For the general solution, we try variation of constants, so our Ansatz is $x(t) = A(t) e^{-t/\tau_\mathrm{e}}$:
\begin{align*}
F_\mathrm{r}(t) &= \zeta \dv{x}{t} + k x(t) = \zeta \dv{A}{t} e^{-t/\tau_\mathrm{e}} - A(t) k e^{-t/\tau_\mathrm{e}} + k  A(t) e^{-t/\tau_\mathrm{e}} = \zeta \dv{A}{t} e^{-t/\tau_\mathrm{e}}, \\
A(t) &= \frac{1}{\zeta} \int_{-\infty}^t \dd{t'} e^{t'/\tau_\mathrm{e}} F_\mathrm{r}(t') \\
x(t) &= \frac{1}{\zeta} \int_{-\infty}^t \dd{t'} e^{-(t-t')/\tau_\mathrm{e}} F_\mathrm{r}(t') = x(0) e^{-t/\tau_\mathrm{e}} + \frac{1}{\zeta} \int_0^t \dd{t'} e^{-(t-t')/\tau_\mathrm{e}} F_\mathrm{r}(t'),
\end{align*}
where
\begin{align*}
x(0) &= \frac{1}{\zeta} \int_{-\infty}^0 \dd{t'} e^{t'/\tau_\mathrm{e}} F_\mathrm{r}(t').
\end{align*}
\item We get
\begin{align*}
\ev{x(t) x(0)} &= \ev{x(0)^2} e^{-t/\tau_\mathrm{e}} + \frac{1}{\zeta} \int_0^t \dd{t'} e^{-(t-t')/\tau_\mathrm{e}} \ev{x(0) F_\mathrm{r}(t')} = \ev{x(0)^2} e^{-t/\tau_\mathrm{e}}.
\end{align*}
The second term vanishes because, substituting the expression for $x(0)$ and using equation~(\bookref{randomforcetimecorrelation}),
\begin{align*}
\int_0^t \dd{t_1} e^{-(t-t_1)/\tau_\mathrm{e}} \ev{x(0) F_\mathrm{r}(t_1)} &= \frac{1}{\zeta} \int_0^t \dd{t_1} e^{-(t-t_1)/\tau_\mathrm{e}} \int_{-\infty}^0 \dd{t_2} e^{t_2/\tau_\mathrm{e}} \ev{F_\mathrm{r}(t_2) F_\mathrm{r}(t_1)} \\
&= 2 \kB T \int_0^t \dd{t_1} e^{-(t-t_1)/\tau_\mathrm{e}} \int_{-\infty}^0 \dd{t_2} e^{t_2/\tau_\mathrm{e}} \delta(t_2 - t_1),
\end{align*}
which vanishes because the second integral is only nonzero if $t_1 < 0$ but we only integrate $t_1$ over a positive range.
\item As the distribution in this case is a Gaussian, we can immediately `read off' that $\ev{x(0)} = 0$ and\\ $\ev{x(0)^2} = \kB T / k$. Substituting in (b) we get equation~(\ref{Brownianmotionharmpotmsdroot}).
\item We have
\begin{align*}
\ev{(x(t) x(0))^2} &= \ev{x(t)^2} - 2 \ev{x(t) x(0)} + \ev{x(0)^2} \\
&= 2 \frac{\kB T}{k} - 2 \frac{\kB T}{k} e^{-t/\tau_\mathrm{e}} =  2 \frac{\kB T}{k} \left(1 - e^{-t/\tau_\mathrm{e}}\right).
\end{align*}
\end{enumerate}
\fi

