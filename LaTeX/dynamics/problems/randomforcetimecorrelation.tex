%Problemtitle: Equipartiation theorem for random forces
%Source: Doi section 6.2.2 (page 98).
In this problem, we'll work out the prefactor in equation~(\bookref{randomforcetimecorrelation}) for the time correlation of the random force~$\bvec{F}_\mathrm{r}$. Therefore, let us set the prefactor to an arbitrary value~$A$ and write
\begin{equation}
\label{randomforcetimecorrelation2}
\frac{1}{m^2}\ev{\bvec{F}_\mathrm{r}(t_1) \cdot \bvec{F}_\mathrm{r}(t_2)} = A \delta(t_2-t_1).
\end{equation}
\begin{enumerate}[(a)]
\item Using separation of variables, derive the formal solution~(\bookref{Langevinsolution}) of the Langevin equation~(\bookref{Langevinequation}).
\item Using the solution of equation~(\bookref{Langevinsolution}), express $\ev{\bvec{v}(t) \cdot \bvec{v}(t)}$ in terms of $\ev{\bvec{F}_\mathrm{r}(t_1) \cdot \bvec{F}_\mathrm{r}(t_2)}$.
\item Now use equation~(\ref{randomforcetimecorrelation2}) to show that 
\begin{equation}
\ev{\bvec{v}(t) \cdot \bvec{v}(t)} = \frac{A \tau}{2m^2}.
\end{equation}
\item Finally, invoke the equipartition theorem to argue that $A = 2 n \zeta \kB T$, with $n$ the number of dimensions.
\end{enumerate}

\ifincludesolutions
\Solution
\begin{enumerate}[(a)]
\item The differential equation for $\bvec{v}$ has an inhomogeneous source term $\bvec{F}_\mathrm{r}(t)$. If the source term is set to zero, we are left with a homogeneous equation, which we can separate and integrate:
\begin{align*}
m \dv{\bvec{v}}{t} &= - \zeta \bvec{v} \\
\frac{\dd{\bvec{v}}}{\bvec{v}} &= - \frac{\zeta}{m} \dd{t} \\
\bvec{v}(t) &= \bvec{B} e^{-t/\tau},
\end{align*}
where $\tau = m/\zeta$ and $B$ is an integration constant. We can now use variation of constants to find the solution to the full equation~(\bookref{Langevinequation}). To that end, we make the integration constant $\bvec{B}$ time dependent as $\bvec{B}(t)$, and substitute the resulting function as our Ansatz in the original equation, which gives:
\begin{align*}
\frac{1}{\zeta} \bvec{F}_\mathrm{r}(t) &= \tau \dv{\bvec{v}}{t} + \bvec{v} = \tau \dv{\bvec{B}}{t} e^{-t/\tau} - \bvec{B} e^{-t / \tau} + \bvec{B} e^{-t/\tau} = \tau \dv{\bvec{B}}{t} e^{-t/\tau}
\end{align*}
and thus
\begin{align*}
\bvec{B}(t) &= \frac{1}{m} \int_{-\infty}^t e^{t'/\tau} \bvec{F}_\mathrm{r}(t') \dd{t'}.
\end{align*}
Combining, we find the full solution as given in equation~(\bookref{Langevinsolution})
\begin{align*}
\bvec{v}(t) &= \frac{e^{-t/\tau}}{m} \int_{-\infty}^t e^{t'/\tau} \bvec{F}_\mathrm{r}(t') \dd{t'} = \frac{1}{m} \int_{-\infty}^t e^{-(t-t')/\tau} \bvec{F}_\mathrm{r}(t') \dd{t'}.
\end{align*}
\item We have
\begin{align*}
\ev{\bvec{v}(t) \cdot \bvec{v}(t)} &= \frac{1}{m^2} \ev{\left(\int_{-\infty}^t \dd{t_1}  e^{-(t-t_1)/\tau} \bvec{F}_\mathrm{r}(t_1) \right) \cdot \left(\int_{-\infty}^t \dd{t_2}  e^{-(t-t_2)/\tau} \bvec{F}_\mathrm{r}(t_2) \right)} \\
&= \frac{1}{m^2} \int_{-\infty}^t \dd{t_1} \int_{-\infty}^t \dd{t_2} e^{-(t-t_1)/\tau} e^{-(t-t_2)/\tau} \ev{\bvec{F}_\mathrm{r}(t_1) \cdot \bvec{F}_\mathrm{r}(t_2)}.
\end{align*}
\item This is a simple matter of substitution:
\begin{align*}
\ev{\bvec{v}(t) \cdot \bvec{v}(t)} &= \frac{A}{m^2} \int_{-\infty}^t \dd{t_1} \int_{-\infty}^t \dd{t_2} e^{-(t-t_1)/\tau} e^{-(t-t_2)/\tau} \delta(t_2 - t_1) \\
&= \frac{A}{m^2} \int_{-\infty}^t \dd{t_1} e^{-2(t-t_1)/\tau} = \frac{A \tau}{2 m^2}.
\end{align*}
\item The equipartition theorem tells us that for every quadratic degree of freedom in the energy, we get a contribution of $\frac12 \kB T$ in the ensemble average. The velocity-related terms in the energy are the kinetic energy terms, $\frac12 m \bvec{v} \cdot \bvec{v}$, with one degree of freedom per dimension. We thus get (with $n$ the number of dimensions):
\begin{align*}
n \frac12 \kB T &= \frac12 m \ev{\bvec{v}(t) \cdot \bvec{v}(t)} = \frac{A \tau}{4 m}, \\
A &= \frac{2 n m \kB T}{\tau} = 2 n \zeta \kB T.
\end{align*}
\end{enumerate}
\fi