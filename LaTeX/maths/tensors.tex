\chapter{Tensors}
\label{ch:tensors}

\begin{center}
\parbox{12cm}{Tensors are in some sense the ugly little duck of physics: awful to look at for those not realizing what they really are, or what their potential is. When you get to know them from the point of view of their own species (that is, in the slightly more abstract definition used by mathematicians), you'll quickly discover that they're in fact quite beautiful, and immensely useful to boost. In this chapter, we'll introduce the concept of tensors as multilinear maps, as well as their various representations found in physics.}
\end{center}

\newpage
\section{Linear maps}
\label{sec:tensors_linearmaps}
In this chapter, I'm going to assume you're familiar with notions such as vector spaces, bases, inner products, Hilbert spaces (which are complete vector spaces with an inner product defined on them), and the norm associated with an inner product. If you're rusty on the definitions, check out appendix~\ref{app:basicdefinitions}. I'm going to work with $n$-dimensional vector spaces $V$ over the real numbers $\mathbb{R}$; often we'll simply have $V = \mathbb{R}^n$ or a suitable subset thereof. Much of what we'll do here will extend naturally to (vector spaces over) $\mathbb{C}$, however, you'll need to take some care with how you define the inner product.

The first object we're going to look at is a simple \emph{linear map} from our vector space $V$ to the real numbers $\mathbb{R}$.
\begin{definition}
A map $\phi : V \to \mathbb{R}$ is \emph{linear} if it satisfies $\phi(\bvec{v} + \bvec{w}) = \phi(\bvec{v}) + \phi(\bvec{w})$ and $\phi(a \bvec{v}) = a \phi(\bvec{v})$ for all $a \in \mathbb{R}$ and $\bvec{v}, \bvec{w} \in V$.
\end{definition}
\begin{example} \label{linearmapexample}
For any vector $\bvec{u} \in V$, the map defined by $\phi_{\bvec{u}}(\bvec{v}) = \innerproduct{\bvec{u}}{\bvec{v}}$ is a linear map.
\end{example}

As example~\ref{linearmapexample} shows, given any vector in $V$, we can construct a linear map. A (for mathematicians) natural question to ask is if there are any other linear maps, i.e., linear maps that cannot be written as the inner product of a fixed vector with the argument. The answer is simple: no. The proof is far from trivial, so I'll gladly leave it to a math book, but the result is useful enough to state as a theorem:
\begin{theorem}[Riesz representation theorem] \label{Rieszlemma}
For any linear map $\phi$ from a Hilbert space $H$ to $\mathbb{R}$, there is a unique element $\bvec{u} \in H$ such that $\phi(\cdot)$ equals $\innerproduct{\bvec{u}}{\cdot}$.
\end{theorem}
What theorem~\ref{Rieszlemma} (also known as the Riesz Lemma) says, is that not only is $\phi_{\bvec{u}}(\bvec{v}) = \innerproduct{\bvec{u}}{\bvec{v}}$ a linear map for each $\bvec{u} \in V$, also \emph{all} linear maps are given by the collection $\left\{ \phi_{\bvec{u}}(\bvec{v}) \right\}_{\bvec{u} \in V}$. There is therefore a one-on-one correspondence between the elements of $V$ (i.e., the vectors $\bvec{u} \in V$) and the collection of linear maps $\phi_{\bvec{u}}(\bvec{v})$ from $V$ to $\mathbb{R}$, and we can even go as far as to \emph{identify} each vector with a linear map.

\begin{definition} The \emph{dual} of $V$ is the collection of linear maps from $V$ to $\mathbb{R}$, denoted $V^* = \mathrm{Lin}(V, \mathbb{R})$.
\end{definition}
From the one-on-one identification of vectors with linear maps, it follows immediately that if $V$ is a vector space of dimension $n$, then its dual $V^*$ is likewise a vector space of dimension $n$.

Suppose we have found a standard, orthonormal basis $\left\{ \unitvec{e}_i \right\}_{i=1,\ldots,n}$ for our vector space $V$ (see appendix~\ref{app:basicdefinitions} on what a basis is and how to find one), so we can \emph{decompose} each vector $\bvec{v} \in V$ in the basis vectors as $\bvec{v} = v^i \unitvec{e}_i$ (using the Einstein summation convention). For reasons only known to those who came up with this name, we call the basis defined by the $\unitvec{e}_i$ the \emph{contravariant} basis of $V$, and the components $v^i$ the contravariant components of $\bvec{v}$. It is important to stress that the $v^i$ are a \emph{representation} of $\bvec{v}$, not the abstract object $\bvec{v}$ itself. We could pick a different basis, consisting of $\unitvec{e}'_j$, leading to different components $(v')^j$, while still representing the same vector~$\bvec{v}$.

As we've seen that the dual $V^*$ of $V$ is also a vector space with dimension~$n$, we should also be able to find a $n$-component basis for it. Since the dual consists of linear maps, the basis should likewise consist of linear functions. The canonical (i.e., standard) choice is to pick the $n$ functions that map a vector $\bvec{v} \in V$ onto each of its $n$ components. 
\begin{definition} \label{def:dualbasis}
The canonical basis for the dual space $V^* = \mathrm{Lin}(V, \mathbb{R})$ of linear maps from a vector space $V$ to $\mathbb{R}$ is given by the maps that \emph{project} a vector $\bvec{v} \in V$ onto each of its $n$ coordinates in the standard basis of $V$. These linear maps, denoted $\ddx^i$ are thus given by $\ddx^i(\bvec{v}) = \innerproduct{\unitvec{e}_i}{\bvec{v}} = v^i$.
\end{definition}









\begin{subappendices} 
\section{Basic definitions}
\label{app:basicdefinitions}


\end{subappendices}