\chapter{Mathematical background}
\label{ch:maths}

\section{Basic calculus: functions, derivatives and integrals}
\label{sec:calculus}

\subsection{Functions}
Mathematically, a \emph{function} is an operator that takes as its inputs (or \emph{arguments}) certain parameters and assigns an output value to these. Another way to phrase this is that a function \emph{maps} its arguments (however many) on a given output value (only one). A typical example would be the air temperature, which depends strongly on location, altitude, and the time of day, and maps all these on a single number. To represent this, we can write $T = T(x, y, z, t)$, expressing that the temperature $T$ depends on position (given by $x$, $y$ and $z$ with respect to some reference point) and time $t$ (again with respect to some reference point). If we go a little further, we can also include the range of allowable values for the input and output, and write:
\begin{align}
\label{function}
T : \mathbb{R} \times \mathbb{R} \times \mathbb{R} \times \mathbb{R}_{\geq 0} &\longrightarrow \mathbb{R} \nonumber\\
 (x, y, z, t) &\mapsto T(x, y, z, t).
\end{align}
Note that we didn't include a ``$\geq 0$'' on the right hand side - we could have, of course, but don't need to, as $T$ does map on the real numbers, just not all of them. A function that assigns a unique value to each possible input is called \emph{injective} (another way of saying this is that each possible outcome corresponds to at most one possible input), and conversely a function for which each possible outcome corresponds to at least one possible input is called \emph{surjective}. A one-on-one or \emph{bijective} function is both surjective and injective: there is a unique output for each input, and a unique input for each output. 

\subsection{Derivatives}
Often, we do not only wish to know what the value of a certain function is, but also how quickly it \emph{changes} as we change its arguments. Consider a function $f(x)$. If we plot $f$ as a function of $x$, its rate of change at a certain value of $x$ is the \emph{slope} of $f$ at that point. To determine this slope, or rate of change, we consider the function at $x$ and at a nearby point, then calculate the difference and divide by the distance. Bringing the nearby point closer brings us closer to the actual rate of change, and taking the limit to zero distance gives us that rate, which is known as the \emph{derivative} of the function. For a function $f(x)$ that takes a single argument $x$, we write $\dd f / \dd x$ for the derivative, which can be read as `the infinitesimal change in $f$ ($\dd f$) due to an infinitesimal change in $x$ ($\dd x$)'. The \emph{infinitesimal} change in $x$ here means that we have taken the limit to arbitrarily small (but nonzero) changes in $x$; the infinitesimal change in $f$ is simply the resulting change in the value of $f$. Expressing this in equation form, we have:
\begin{equation}
\label{defderivative}
\frac{\dd f}{\dd x} = \lim_{\eps \to 0} \frac{f(x + \eps) - f(x)}{\eps}.
\end{equation}
A list of common derivatives is given in table~\ref{table:derivatives}. In particular, we note that the derivative of a constant is, of course, zero: a constant doesn't change, so its rate of change must vanish. This simple observation also gives us a hint on how to approach functions of multiple variables: we simply take the derivative with respect to one variable, while treating the others as constants. This technique is known as taking a \emph{partial derivative}. To indicate that we're taking a partial derivative of a function $f(x,y)$ with respect to $x$, we write $\partial f / \partial x$, or even shorter $\partial_x f$. 

\begin{table}
\begin{center}
\begin{tabular}{|l|c|c|}
\hline
&$f(x)$ & $\dd f / \dd x$ \\
\hline
For any number $n \neq 0$: & $x^n$ & $n x^{n-1}$ \\
For any number $a>0$ & $a^x$ & $a^{x} \log(a)$ \\
For any number $a>0$ & $^a\log(x)$ & $1 / (x \log(a))$ \\
& $\sin(x)$ & $\cos(x)$ \\
& $\cos(x)$ & $-\sin(x)$ \\
\hline
\end{tabular}
\end{center}
\caption{Some common derivatives. We use $\log(x)$ for the natural logarithm (base $\e$) of $x$.}
\label{table:derivatives}
\end{table}

Table~\ref{table:derivatives} only lists basic functions - what about more complex ones? First, we note that taking the derivative is linear in the function (we will call the derivative a \emph{linear operator}). Therefore,
\[ \frac{\dd (a f(x))}{\ddx} = a \frac{\dd f}{\ddx} \mbox{ and } \frac{\dd (f(x)+g(x))}{\ddx} = \frac{\dd f}{\ddx} + \frac{\dd g}{\ddx} \] for any functions $f(x)$ and $g(x)$ and number $a$. Fortunately, we have some handy rules for other somewhat more complicated cases.

\begin{theorem}[Chain rule]
If $f(x)$ can be written as $f(g(x))$, with $g(x)$ some other function, then
\begin{equation}
\label{chainrule}
\frac{\dd f}{\dd x} = \frac{\dd f}{\dd g} \frac{\dd g}{\dd x},
\end{equation}
where in the derivative on the left $f$ is taken as a function of $x$, and in the derivative of $f$ on the right, it is taken as a function of $g$.
\end{theorem}

\begin{proof}
We simply apply the definition~(\ref{defderivative}) to the function $f(g(x))$:
\begin{align}
\frac{\dd f}{\dd x} &= \lim_{\eps \to 0} \frac{f(g(x + \eps)) - f(g(x))}{\eps} \nonumber\\
&= \lim_{\eps \to 0} \frac{f(g(x+\eps)) - f(g(x)}{g(x+\eps) - g(x)} \frac{g(x + \eps) - g(x)}{\eps} \nonumber \\
&= \frac{\dd f}{\dd g} \frac{\dd g}{\dd x}.
\end{align}
\end{proof}

\begin{example}
\begin{equation}
\frac{\dd \e^{x^2}}{\ddx} = \frac{\dd \e^g}{\dd g} \frac{\dd g}{\ddx} = \e^{g(x)} \cdot 2x = 2x\e^{x^2},
\end{equation}
where we have taken $g(x) = x^2$.
\end{example}

If a function depends on multiple arguments, the chain rule needs to be applied to each of them - so for instance, if $f(x, y) = f(g(x,y), h(x,y))$, then
\begin{equation}
\label{chainrulemultiple}
\frac{\partial f}{\partial x} = \frac{\partial f}{\partial g} \frac{\partial g}{\partial x} + \frac{\partial f}{\partial h} \frac{\partial h}{\partial x}.
\end{equation}

So far, we haven't seen much difference between ordinary and partial derivatives - but here is one: what if a function has arguments that depend on other arguments? For instance, we could have $f(x(t), t)$ depend on the position and time, while the position itself also varies with time. Then for $\partial_t f$ we still keep $x$ fixed (ignoring its time dependence), but we can also calculate the \emph{total derivative} of $f$ with respect to $t$, using the chain rule:
\begin{equation}
\label{totalderivative}
\frac{\dd f}{\dd t} = \frac{\partial f}{\partial t} + \frac{\partial f}{\partial x} \frac{\partial x}{\partial t}.
\end{equation}

\begin{theorem}[Product rule]
If $f(x) = g(x) h(x)$, then
\begin{equation}
\label{productrule}
\frac{\dd f}{\dd x} = \frac{\dd g}{\dd x} h(x) + g(x) \frac{\dd h}{\dd x}.
\end{equation}
\end{theorem}

\begin{proof}
We simply apply the definition~(\ref{defderivative}) to the function $f(x)$:
\begin{align}
\label{productruleproof}
\frac{\dd f}{\dd x} &= \lim_{\eps \to 0} \frac{g(x + \eps)h(x+\eps) - g(x)h(x)}{\eps} \nonumber\\
&= \lim_{\eps \to 0} \frac{g(x + \eps)h(x+\eps) - g(x + \eps)h(x) + g(x+\eps)h(x)- g(x)h(x)}{\eps} \nonumber \\
&= \lim_{\eps \to 0} g(x) \frac{h(x) - g(x)h(x-\eps)}{\eps} + \lim_{\eps \to 0} \frac{g(x + \eps)- g(x)}{\eps} h(x) \nonumber\\
&= \frac{\dd g}{\dd x} h(x) + g(x) \frac{\dd h}{\dd x}.
\end{align}
Note how in the first term of the third step we shifted our position from $x$ to $x+\eps$. Aren't we cheating? Technically, yes - but fortunately, the piece we are missing is infinitesimally small compared even to the infinitesimal change in $g$ and $h$ (we say its of `second order' in the infinitesimal change). To see that this is true, draw a rectangle representing the size of $f(x)$ - then one side corresponds to $g(x)$, and the other to $h(x)$. An infinitesimal change in either $g(x)$ or $h(x)$ extends their side a little bit, and a change in both also adds a tiny extra rectangle which corresponds to the overlap - and which we're ignoring in the limit\footnote{Another way to express this, is by only writing down the infinitesimal changes in the function values, not in the argument: $\dd f = \dd (g h) = (g + \dd g) (h + \dd h) - gh = g \dd h + h \dd g + \dd g \dd h$, where $\dd g \dd h$ is the negligible, second-order part.}.
\end{proof}

\begin{example}
\begin{equation}
\frac{\dd \sin(x) \cos(x)}{\dd x} = \cos^2(x) - \sin^2(x) = \cos(2x).
\end{equation}
Note that the same result follows from applying the chain rule to $\frac12 \sin(2x)$, which is identical to our original function.
\end{example}

\begin{theorem}[Quotient rule]
If $f(x) = p(x) / q(x)$, then
\begin{equation}
\label{quotientrule}
\frac{\dd f}{\dd x} = \frac{q(x) (\dd p / \dd x) + p(x) (\dd q / \dd x) }{q(x)^2}.
\end{equation}
\end{theorem}
\begin{proof}
Apply the product and chain rule to $f(x) = p(x) \frac{1}{q(x)}$.
\end{proof}

\begin{example}
\begin{equation}
\frac{\dd \tan(x)}{\dd x} = \frac{\dd (\sin(x) / \cos(x))}{\dd x} = \frac{\cos^2(x) + \sin^2(x)}{\cos^2(x)} = \frac{1}{\cos^2(x)}.
\end{equation}
Of course, in the final step we could also have eliminated the cosine, to obtain $1 + \tan^2(x)$.
\end{example}

Naturally, we can look at the rate of change of the rate of change (known as the second derivative), and the rate of change of that, and so on. For ordinary derivatives, we write $\dd / \dd x ( \dd f / \dd x) = \dd^2 f / \dd x^2$ for the second derivative of $f(x)$ to x. For partial derivatives, it matters of course which derivative we look at: for $f(x,y)$ we can take the derivative twice to $x$, twice to $y$, or once to $x$ and once to $y$. For the latter, in principle the order of taking derivatives could matter - but in practice, for any function that you'd encounter in physics, it does not, and $\partial_x \partial_y f = \partial_y \partial_x f$, for which we also use the even shorter notation $\partial^2_{xy} f$.

\subsection{Integrals}
Suppose we have the opposite problem as we did in the previous section - we know the rate of change of a function, and want to determine the function itself. To find it, we have to apply an `inverse derivative', or integral. Graphically, if we plot $g(x) = \dd f / \dd x$ as a function of $x$, the change in $f(x)$ between a point $x=a$ and $x=b$ is the area under the graph of $g(x)$ between the vertical lines corresponding to $x=a$ and $x=b$. To see that this is true, consider a well-known rate of change function: the speed of a car as a function of time. If the speed is constant, the distance traveled is simply the speed times the time taken (the area); if the speed increases, the area becomes larger. However, we can always chop up the interval in small parts, for which the change is small as well, and approximate the total area as the sum of the values of the speed at the small parts times the duration of these parts. Going back to $g(x) = \dd f / \dd x$ and $f(x)$, we have $f(x) \approx \sum_{i=1}^N g(x_i) \Delta x_i$, where $\Delta x_i = \Delta x / N = (b-a)/N$. Like with the derivative, we now take the limit of infinitesimally small parts, which means that the number of them becomes infinite. The resulting sum is known as the Riemann sum, or the \emph{integral} of $g(x)$, and written as $\int_a^b g(x) \dd x$, where the $\int$ symbol indicates the Riemann sum. Fortunately, thanks to a very useful theorem, we do not have to evaluate this sum - the value of the integral only depends of the value of $f(x)$ (known in this context as the \emph{primitive} of $g(x)$) at the boundary:
\begin{theorem}[Fundamental theorem of calculus]
The integral of a continuous function $g(x) = \dd f / \dd x$ over an interval $[a, b]$ is given by
\begin{equation}
\label{integral}
\int_a^b \frac{\dd f}{\dd x} \dd x = f(b) - f(a).
\end{equation}
\end{theorem}
In equation~(\ref{integral}) we indicated the endpoints of the integration interval - it is known as a \emph{definite integral}. However, we may also be interested in the function $f(x)$ itself, which is given by an indefinite integral, and only determined up to a constant:
\begin{equation}
\label{indefintegral}
\int \frac{\dd f}{\dd x} \dd x = f(x) + C.
\end{equation}
The addition of the constant is due to the fact that the derivative of $f(x)$ has to be the function that we integrated over - but the derivative of a constant is zero. You may see it as the price we pay for not specifying a reference point, so we don't know where to start when calculating $f(x)$. If we do provide such a point, we can write:
\begin{equation}
\label{functionintegral}
\int_{x_0}^x \frac{\dd f}{\dd x'} \dd x' = f(x) - f(x_0).
\end{equation}
Note that in~(\ref{functionintegral}) we added a label on the variable $x$ inside the integral, to distinguish it from the value of $x$ at the upper limit of the integration interval. Table~\ref{table:primitives} lists some common primitive functions (sometimes also simply referred to as integrals).

\begin{table}
\begin{center}
\begin{tabular}{|l|c|c|}
\hline
& $\dd f/\dd x$ & $f(x)$\\
\hline
For any number $a$: & $a$ & $a x + C$\\
For any number $n \neq 1$: & $x^n$ & $\frac{1}{n+1} x^{n+1}$ \\
& $1/x$ & $\log(x)$ \\
For any number $a>0$ & $a^x$ & $a^{x} / \log(a)$ \\
For any number $a>0$ & $^a\log(x)$ & $\frac{1}{\log(a)} x (\log(x)-1) + C$ \\
& $\sin(x)$ & $-\cos(x) + C$ \\
& $\cos(x)$ & $\sin(x) + C$ \\
\hline
\end{tabular}
\end{center}
\caption{Some common primitives. Note that each of them contains an undetermined constant $C$.}
\label{table:primitives}
\end{table}

Like taking the derivative, the integral is a linear operator. We also have integral equivalents of the chain and product rule, known as the \emph{substitution rule} and \emph{integration by parts} respectively.

\begin{theorem}[Substitution rule]
Suppose we want to integrate $g(x) = \dd f/\dd x$ over $x$. Then if $g(x)$ can be written as $g(y(x))$ with $y(x)$ some function of $x$, we have:
\begin{equation}
\label{substitutionrule}
\int_{x=a}^{x=b} \frac{\dd f}{\ddx} \ddx = \int_{x=a}^{x=b} \frac{\dd f}{\ddy} \frac{\dd y}{\ddx} \ddx = \int_{y=y(a)}^{y=y(b)} \frac{\dd f}{\ddy} \dd y.
\end{equation}
\end{theorem}
Note that in the last step of (\ref{substitutionrule}) we changed not only the variable to be integrated over, but also the limits of integration.

\begin{proof}
Follows immediately from the chain rule.
\end{proof}

\begin{example}
\begin{equation}
\int_{x=0}^{x=2} x e^{x^2} \dd x = \int_{x=0}^{x=2} e^{x^2} \dd (x^2) = \int_{y=0}^{y=4} e^y \dd y = \big[ e^y \big]_{y=0}^{y=4} = e^4 - 1.
\end{equation}
\end{example}

\begin{theorem}[Integration by parts]
\begin{equation}
\label{integrationbyparts}
\int_{x=a}^{x=b} f(x) \dd g(x) = \big[ f(x) g(x) \big]_a^b - \int_{x=a}^{x=b} g(x) \dd f(x).
\end{equation}
\end{theorem}

\begin{proof}
Note that the product rule can be written as $\dd f g = f \dd g + g \dd f$, so $f \dd g = \dd f g - g \dd f$. Now integrate both sides to obtain~(\ref{integrationbyparts}).
\end{proof}

\begin{example}
\begin{align}
\int_{x=0}^{x=1} x \e^x \dd x &= \int_{x=0}^{x=1} x \dd \e^x \nonumber \\
&= \left[ x \e^{x} \right]_{x=0}^{x=1} - \int_{x=0}^{x=1} \e^x \dd x \nonumber \\
&= \e - (\e - 1) = 1.
\end{align}
\end{example}

\section{Scalars and vectors}
\label{sec:scalarsvectors}

\subsection{Scalars}
A \emph{scalar} is a quantity which only has a \emph{magnitude}. Scalars can simply be (mathematical) numbers, such as 0, 1, $\pi$, or $\e$. When a scalar represents a physical quantity, it also carries a \emph{dimension}, such as time, length or temperature. The dimension tells you something about the physical quantity, but not about its \emph{value}. The value of any quantity that has a dimension must be expressed in terms of a `basis value', better known as a \emph{unit} - the standard units for length being meters, time seconds, and temperature Kelvins. Mathematical quantities on the other hand are dimensionless. Of course, physical quantities can often be combined into \emph{dimensionless numbers} as well - well known examples include the aspect ratio (length/width), the Reynolds number, and the Poisson ratio. You're free to multiply (or inverse-multiply, i.e., divide) any two scalars; the result will again be a scalar. However, you can only add (or subtract) scalars of the same dimension, and if you intend to put a scalar in an exponent or take its logarithm, you better make sure it is dimensionless.

Frequently, the value of a given physical quantity depends on the value of one or more other physical quantities - for instance, the temperature of a sample might depend on the time of measurement. This makes the temperature a \emph{function}: it takes some input parameters (in this case, the scalar time) and maps those on an output parameter (in this case, the scalar temperature). Going one step further, we can also ask what the temperature is as a function of position - making a function with both space and time inputs. A function that maps a spatial coordinate to a value is known as a (scalar) \emph{field}; fields can both be constant and time-dependent. 

\subsection{Vectors}
A \emph{vector} is a quantity which has both a \emph{magnitude} and a \emph{direction}. The magnitude (a scalar!) carries a physical dimension, the direction does not. Physical examples include velocities and forces. A \emph{vector field} assigns a vector value to every point in the relevant space - such as the vectors representing the wind (magnitude and direction) at every point on the surface of the Earth.

The most natural way of assigning values to a vector is by expressing its magnitude as a scalar quantity (so it carries a unit) and its direction with respect to some reference frame - typically by measuring the angle(s) with respect to fixed axes. Of course, both the reference frame and the unit represent choices, but the vector itself doesn't depend on them - just as the actual length of a rod doesn't change if you measure it in inches or centimeters. In two dimensions, one angle suffices; in three dimensions you'll need two, and so on - so in $n$ dimensions you need $n$ numbers: one for the magnitude and $n-1$ for the angles\footnote{Since magnitudes are always positive, this isn't strictly true for one dimension, as there you also need to specify the `direction' (positive or negative). But of course a one-dimensional vector is just a scalar, which can have both positive and negative values.}. There are commonly used \emph{coordinate systems} that employ exactly this scheme: \emph{polar coordinates} in two dimensions, and \emph{spherical coordinates} in three. The reference axes are the $x$-axis in 2D and both the $x$ and $z$-axes in 3D\footnote{Unfortunately, the physics and mathematics literature use different conventions for indicating the angles. In 2D, both $\theta$ and $\phi$ are used, but since there is only one angle, this does not create any problems. In the physics literature, which we'll follow here, in 3D we call the angle with respect to the $z$-axis $\theta$ and have it run from $0$ to $\pi$ (this is the \emph{polar angle}), the angle with respect to the $x$-axis is called $\phi$ and runs from $0$ to $2\pi$ (this is the \emph{azimuthal angle}). In calculus textbooks on the other hand, the polar angle is usually $\phi$ and the azimuthal one $\theta$.}.

Magnitudes and angles may be a natural choice for vectors, but they are not always the easiest to work with - though this may well be due to the way vectors are usually introduced. What we commonly do of course is to write our vectors as a linear combination of basis vectors - in essence, we decompose them in the basis of choice. A \emph{basis} is any set of linearly independent vectors that spans the space - so for an $n$-dimensional space, you need $n$ basis vectors. Usually we take our basis vectors to be \emph{orthonormal}: both mutually perpendicular (orthogonal) and of unit length (normal), where `length' here refers to their magnitude. To express an arbitrary vector $\bvec{v}$ in the basis provided by these basis vectors, we \emph{project} the vector on each of the basis vectors - the resulting number is usually called the component of the vector in the direction of the basis vector.

The simplest and most commonly used set of basis vectors is the Cartesian system, in which space (of any dimension) is divided up into square/cubical/etc. pieces, and the basis vectors point along the edges of those pieces. In 3D, we call the basis vectors $\unitvec{x}$, $\unitvec{y}$, and $\unitvec{z}$, and we decompose our vectors as $\bvec{v} = v_x \unitvec{x} + v_y \unitvec{y} + v_z \unitvec{z}$. Since these unit vectors are unchangeable in time and space, we can specify $\bvec{v}$ simply by giving its components, and write $\bvec{v} = (v_x, v_y, v_z)$.

Although Cartesian coordinates are certainly very handy and useful, it is good to take a step back and reflect on what we've just done - we've taken something which carries a single scalar property (the vector's magnitude) and a directional property (given, in this case, by two dimensionless angles), and expressed it in terms of three scalar quantities (the vector components $v_x$, $v_y$, and $v_z$). Its not hard to see (but easy to forget) that the two ways of considering the vector carry the same information. The magnitude of $\bvec{v}$ can be calculated as $\sqrt{v_x^2 + v_y^2 + v_z^2}$ (courtesy of the Pythagorean theorem and the orthogonality of the Cartesian basis vectors), and the azimuthal and polar angles are given, respectively, by $\phi = \arctan(v_y/v_x)$ and $\theta = \arccos(v_z / \sqrt{v_x^2 + v_y^2 + v_z^2})$. 

\subsection{Vector products}
Like scalars, vectors with the same dimensions can be added. The easiest way to do so is in the context of their (Cartesian) coordinate representation. As each of the components along a basis direction represents a scalar, and the basis directions are orthogonal, we can simply add per direction. For two vectors $\bvec{v}$ and $\bvec{w}$ we can then write $\bvec{v} + \bvec{w} = (v_x + w_x) \unitvec{x} + (v_y+w_y) \unitvec{y} + (v_z+w_z) \unitvec{z}$. Alternatively, we can use the `column representation' of the vectors, which puts the three Cartesian components together in a column, and write:
\begin{equation}
\label{vectoraddition}
\bvec{v} + \bvec{w} = \colvec{v_x}{v_y}{v_z} + \colvec{w_x}{w_y}{w_z} = \colvec{v_x+w_x}{v_y+w_y}{v_z+w_z}.
\end{equation}





