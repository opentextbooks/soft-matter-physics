\section{Tensors}
\label{sec:tensors}

\subsection{Vectors as linear maps}
A \emph{vector} is an object that lives in a \emph{vector space}. Vector spaces can be defined over sets\footnote{Formally, \emph{fields}: a set that is a group under addition, and also a group under multiplication if you leave out the identity under addition (i.e., $0$). If you have no idea what that means: just think of $\mathbb{R}$ and $\mathbb{C}$, we won't be needing any others.} of numbers such as $\mathbb{R}$ or $\mathbb{C}$; vectors can be added and subtracted, and multiplied by scalars from the set their defined over. Vectors can be represented by collections of numbers from the underlying set. \emph{Which} numbers depends on the choice of \emph{basis}. Taking the usual Cartesian coordinate system as our reference point, we can write the three basis vectors of $\mathbb{R}^3$ as
\begin{equation}
\label{Cartesianbasis}
\unitvec{e}_x = \colvec{1}{0}{0}, \qquad \unitvec{e}_y = \colvec{0}{1}{0},\qquad \mbox{and} \quad \unitvec{e}_z = \colvec{0}{0}{1}.
\end{equation}
Any other vector $\bvec{p}$ can now be written as a linear combination of the basis vectors, as
\begin{equation}
\bvec{p} = \colvec{p_x}{p_y}{p_z} = p^{(x)} \unitvec{e}_x + p^{(y)} \unitvec{e}_y + p^{(z)} \unitvec{e}_z = \sum_{i=1}^3 p^i \unitvec{e}_i,
\end{equation}
where $p^1 = p^{(x)}$, $p^2 = p^{(y)}$ and $p^3 = p^{(z)}$. We call the $p^i$ the \emph{components} of $\bvec{p}$ in the basis $\{ \unitvec{e}_x, \unitvec{e}_y, \unitvec{e}_z\}$. Had we chosen a different basis, the (abstract) vector $\bvec{p}$ would still be the same, but the components would change. %We can find each of the components by projecting $\bvec{p}$ on the associated unit vector, $p^i = \bvec{p} \cdot \unitvec{e}_i$. 

It is helpful to pause for a moment to think of how we choose our basis. The choice we made in~(\ref{Cartesianbasis}) is to have a basis vector in each of the directions of increasing value of $x$, $y$ and $z$. We could also have made another choice: we could have chosen our basis vectors for each of the components to be perpendicular\footnote{Vectors are perpendicular if their inner product vanishes)} to the surfaces on which all other components are constant. For Euclidean space, these basis vectors are the same as the ones we already have: $\bvec{e}_x$ is indeed perpendicular to the plane in which both $y$ and $z$ are constant. In general however, the two choices yield two different sets of bases, which are known as contravariant and covariant, respectively. To get a bit of a feel for what the difference is, consider that we could also write the basis vectors as row vectors. The contravariant basis is then the one formed by the column vectors, the covariant basis the one formed by the row vectors. To denote the difference, the position of the indices is swapped: covariant basis vectors are written as $\bvec{e}^j$, and the corresponding components of a vector~$\bvec{p}$ as $p_j$. We will need this notion when discussing membranes as surfaces in space.

Above, we've already tacitly assumed that our vector space has an \emph{inner product}, a function of two vectors that produces a real\footnote{Or complex, for vector fields over $\mathbb{C}$; we'll focus on real vector fields here.} number. The best known inner product is the dot product in $\mathbb{R}^n$, defined as the sum of the products of the components. We typically write a dot product as the product between a row and a column vector:
\begin{equation}
\label{Cartesianinnerproduct}
\bvec{p} \cdot \bvec{q} = \begin{pmatrix} p_1, p_2, p_3 \end{pmatrix} \cdot \colvec{q^1}{q^2}{q^3} = \sum_i p_i q^i.
\end{equation}

The dot product is not the only inner product we can define on $\mathbb{R}^n$; in fact, there are infinitely many options. For a general inner product between vectors, written as $\ev{\bvec{u},\bvec{v}}$, we only have three simple requirements:
\begin{itemize}
\item symmetry: $\ev{\bvec{u},\bvec{v}} = \ev{\bvec{v},\bvec{u}}$,
\item bi-linearity: $\ev{a \bvec{u} + b \bvec{v}, \bvec{w}} = a\ev{\bvec{u},\bvec{w}} + b \ev{\bvec{v},\bvec{w}}$,
\item positive definiteness: $\ev{\bvec{u},\bvec{u}} \geq 0$ and only zero if $\bvec{u} = \bvec{0}$. 
\end{itemize}
You can easily check that the dot product satisfies these requirements. We've used the dot product already to project components of $\bvec{p}$ on the basis $\{\unitvec{e}_i\}$. Note that in equation~(\ref{Cartesianinnerproduct}) we have used the covariant components of $\bvec{p}$, not the contravariant ones. In Cartesian coordinates, these are the same, but in general they are not. More importantly however, they allow us a second interpretation of a vector, apart from the usual `object with a magnitude and a direction': we can interpret vectors as linear functions of vectors. Given an inner product, we can simply define a linear function $\phi_{\bvec{p}}$ as
\begin{equation}
\phi_{\bvec{p}}(\bvec{q}) = \ev{\bvec{p}, \bvec{q}}.
\end{equation}
By definition of the inner product, $\phi_{\bvec{p}}$ is a linear function; we'll state without proof that any linear function can be written in this way (a result known as the Riesz lemma). Because we can therefore identify linear functions one-on-one with vectors, they also form a vector space, known as the \emph{dual} space of the `regular' vectors; these linear functions are often called \emph{one-forms}. Because they form a vector space, we can also define a basis of functions; the `standard' one is the collection of functions that map a vector to one of its components. These basis functions are (somewhat confusingly) written as $\dd x^i$:
\begin{equation}
\dd x^i (\bvec{q}) = q^i.
\end{equation}
%The components of an arbitrary linear function $\phi_{\bvec{p}}$ can now be found by having it act on a basis vector:
%\begin{equation}
%\phi_{\bvec{p}}(\unitvec{e}_j) = \ev{\bvec{p}, \unitvec{e}_j} = 
%\end{equation}
An arbitrary linear function $\phi_{\bvec{p}}$ can now be written as a linear combination of basis functions as
\begin{equation}
\label{oneformcomponents}
\phi_{\bvec{p}}(\bvec{q}) = \ev{\bvec{p}, \bvec{q}} = \sum_i \ev{\bvec{p}, \unitvec{e}_i} q^i = \sum_i \ev{\bvec{p}, \unitvec{e}_i} \dd x^i(\bvec{q}).
\end{equation}
From equation~(\ref{oneformcomponents}), we can read off that the components $\phi_i$ of $\phi$ are given by $\phi_i = \ev{\bvec{p}, \unitvec{e}_i}$. In Cartesian coordinates\footnote{Or more generally, in any coordinate system that is orthonormal, i.e., where the basis vectors are mutually perpendicular and all of unit length.}, these components are of course simply the components of $\bvec{p}$. In an arbitrary coordinate system (where the basis vectors are not mutually orthonormal), the values $\phi_i$ \emph{define} the covariant components of $\bvec{p}$.


\subsection{A basic introduction to tensors}
\label{sec:tensorsintro}
Mathematically, tensors are multi-linear maps from vector spaces to (real or complex) numbers, which is to say, they are functions that take vectors as arguments, give numbers as outputs, and are linear in each of their arguments. If you've studied linear algebra, you'll be familiar with a large class of tensors, that map two vectors in $\mathbb{R}^n$ to numbers. These tensors can be \emph{represented} as matrices, just like vectors in $\mathbb{R}^n$ can be represented as rows or columns of numbers. %\emph{Which} numbers depends on the choice of basis. Taking the usual Cartesian coordinate system as our reference point, we can write the three basis vectors of $\mathbb{R}^3$ as
%\begin{equation}
%\label{Cartesianbasis}
%\unitvec{e}_x = \colvec{1}{0}{0}, \qquad \unitvec{e}_y = \colvec{0}{1}{0},\qquad \mbox{and} \quad \unitvec{e}_z = \colvec{0}{0}{1}.
%\end{equation}
%Any other vector $\bvec{p}$ can now be written as a linear combination of the basis vectors, as
%\begin{equation}
%\bvec{p} = \colvec{p_x}{p_y}{p_z} = p^{(x)} \unitvec{e}_x + p^{(y)} \unitvec{e}_y + p^{(z)} \unitvec{e}_z = \sum_{i=1}^3 p^i \unitvec{e}_i,
%\end{equation}
%where $p^1 = p^{(x)}$, $p^2 = p^{(y)}$ and $p^3 = p^{(z)}$. We call the $p^i$ the \emph{components} of $\bvec{p}$ in the basis $\{ \unitvec{e}_x, \unitvec{e}_y, \unitvec{e}_z\}$. We can find each of the components by projecting $\bvec{p}$ on the associated unit vector, $p^i = \bvec{p} \cdot \unitvec{e}_i$. Had we chosen a different basis, the (abstract) vector $\bvec{p}$ would still be the same, but the components would change.

%It is helpful to pause for a moment to think of how we choose our basis. The choice we made in~(\ref{Cartesianbasis}) is to have a basis vector in each of the directions of increasing value of $x$, $y$ and $z$. We could also have made another choice: we could have chosen our basis vectors for each of the components to be perpendicular\footnote{Vectors are perpendicular if their inner product vanishes)} to the surfaces on which all other components are constant. For Euclidean space, these basis vectors are the same as the ones we already have: $\bvec{e}_x$ is indeed perpendicular to the plane in which both $y$ and $z$ are constant. In general however, the two choices yield two different sets of bases, which are known as contravariant and covariant, respectively. To get a bit of a feel for what the difference is, consider that we could also write the basis vectors as row vectors. The contravariant basis is then the one formed by the column vectors, the covariant basis the one formed by the row vectors. To denote the difference, the position of the indices is swapped: covariant basis vectors are written as $\bvec{e}^j$, and the corresponding components of a vector~$\bvec{p}$ as $p_j$. We will need this notion when discussing membranes as surfaces in space.

A two-tensor is now a map (aka function) that takes two vectors, say $\bvec{p}$ and $\bvec{q}$, and produces a real number. If we write the map as $A(\bvec{p}, \bvec{q})$, we can use our basis to write a representation of $A$ quite easily:
\begin{equation}
\label{tensormatrixrepresentation}
A(\bvec{p}, \bvec{q}) = A\left(\sum_i p^i \unitvec{e}_i, \sum_j q^j \unitvec{e}_j\right) = \sum_i \sum_j p^i q^j A\left( \unitvec{e}_i, \unitvec{e}_j \right) \equiv \sum_i \sum_j p^i q^j A_{ij}.
\end{equation}
In equation~(\ref{tensormatrixrepresentation}), the second equality holds because our map is linear in each of its arguments (if $f(x)$ is linear, we have both $f(a x) = a f(x)$ and $f(x+y) = f(x) + f(y)$). In the last equality, we have defined numbers $A_{ij}$ that \emph{represent} the tensor in the given basis. Naturally, they can be nicely arranged in a square form known as a \emph{matrix}. All two-tensors (the `two' referring to the two arguments, not the dimension of the vectors) can thus be represented by things you know well.

In practice, both in linear algebra and in our applications throughout this book, we use two-tensors not as maps from two vectors to real numbers, but from vectors to vectors. From equation~(\ref{tensormatrixrepresentation}) we can see how this works: if we leave open one of the arguments, we can see what the tensor does when acting on a single vector:
\begin{equation}
A(\bvec{p}, \cdot) = \sum_i p^i A_{ij},
\end{equation}
so we're left with an object with a single free index $j$ (the index $i$ is summed over and doesn't appear in the final answer) that we can define the components of a new vector $q_j$, or rather (because the index on $q$ is at the bottom), the components of the linear map associated with the vector~$\bvec{q}$.

\subsection{The metric tensor}
\label{sec:metrictensor}
To make the concept of section~\ref{sec:tensorsintro} concrete, an example will be useful. Fortunately, we already have one: the inner product, which, after all, takes two vectors and produces a real number. In Cartesian coordinates, the matrix associated with the inner product is the identity matrix, but in general this is not the case. We have:
\begin{equation}
\label{defmetrictensor}
\ev{\bvec{p}, \bvec{q}} = \sum_i \sum_j p^i q^j \ev{\unitvec{e}_i, \unitvec{e}_j} \equiv \sum_i \sum_j p^i q^j g_{ij},
\end{equation}
where the numbers $g_{ij} = \ev{\unitvec{e}_i, \unitvec{e}_j}$ are the components of the \emph{metric tensor}\index{metric tensor}. The metric tensor can be taught of as measuring lengths; after all, the inner product of a vector with itself is the square of its length.

So far, we've expressed the vectors that we put into a two-tensor in terms of their contravariant components, yielding covariant components of the tensors (the generic tensor $A$ and the metric tensor $g$ have both indices below). We can however express our vectors in covariant components, by taking the covariant basis, or, equivalently, thinking of them as components of linear maps. In that case, the representation of the tensor changes. Taking both vectors in covariant form in equation~(\ref{defmetrictensor}) for example, we find
\begin{equation}
\label{metrictensorinverse}
\ev{\bvec{p}, \bvec{q}} = \sum_i \sum_j p_i q_j \ev{\unitvec{e}^i, \unitvec{e}^j} \equiv \sum_i \sum_j p_i q_j g^{ij}.
\end{equation}
Cartesian intuition might suggest that equations~(\ref{defmetrictensor}) and~(\ref{metrictensorinverse}) are one and the same, and for Cartesian coordinates, you'd be correct, as there we have $g_{ij} = \delta_{ij}$ and $g^{ij} = \delta^{ij}$, i.e., in both cases we get the identity. However, in general $g_{ij} \neq g^{ij}$. For the metric, we moreover have the specific property that the two representations are each other's inverse (a result that is not true for general tensors!). To see how this comes about, let's first see what happens if we mix representations: 




\subsection{Tensor invariants}
\label{sec:tensorinvariants}
Representations of tensors depend on the basis they are expressed in. The tensors themselves however are independent of the choice of basis. For one-tensors (vectors), there is a single property that is independent of the choice of basis: the magnitude (or length) of the vector. For two-tensors, there are as many independent invariant properties as there are dimensions. These independent properties may either be the eigenvalues of the tensor, or functions thereof, such as the tensor's trace and determinant. Here we will prove this property; for simplicity, we'll do it in two dimensions, but the argument easily generalizes to higher dimensions.

Let us consider a tensor~$\bvec{A}$ in a matrix representation in some basis. The tensor has a characteristic polynomial
\begin{equation}
\label{charpolynomial}
P(\lambda) = \det(\bvec{A} - \lambda \bvec{I}).
\end{equation}
First we'll prove that this polynomial is independent of the choice of basis. Therefore, let's apply a similarity transform to $\bvec{A}$, which gives $\bvec{A}' = \bvec{C}^{-1} \bvec{A} \bvec{C}$. The characteristic polynomial of the transformed matrix is given by
\begin{align}
\label{charpolynomialinvariance}
P'(\lambda) &= \det(\bvec{A}' - \lambda \bvec{I}) = \det(\bvec{C}^{-1} \bvec{A} \bvec{C} - \lambda \bvec{I}) = \det[\bvec{C}^{-1} ( \bvec{A} - \lambda \bvec{I}) \bvec{C}] \nonumber\\
&= \det(\bvec{C}^{-1}) \det(\bvec{A} - \lambda \bvec{I}) \det(\bvec{C}) = P(\lambda),
\end{align}
where we used that the determinant of a product is the product of the determinants ($\det(\bvec{A}\bvec{B}) = \det(\bvec{A})\det(\bvec{B})$), and that the determinant of the inverse of a matrix is the inverse of the matrix's determinant ($\det(\bvec{C}^{-1}) = 1/\det(\bvec{C})$). Equation~(\ref{charpolynomialinvariance}) shows that the characteristic polynomial is invariant under coordinate transformations. Note that so far we have made no assumptions on the number of dimensions of $\bvec{A}$. If the number of dimensions equals two, we can easily express the characteristic polynomial in terms of the elements of $\bvec{A}$:
\begin{equation}
\label{charpolynomial2D}
P(\lambda) = \det(\bvec{A} - \lambda \bvec{I}) = \lambda^2 - \lambda \Tr(\bvec{A}) + \det(\bvec{A}).
\end{equation}
Now as the characteristic polynomial is invariant for any value of $\lambda$, the coefficients of the various powers of $\lambda$ in equation~(\ref{charpolynomial2D}) must also be invariant, which shows that the trace and determinant of $\bvec{A}$ are invariant. As we can express the eigenvalues of $\bvec{A}$ in terms of the trace and determinant, those must be invariant as well.

In three dimensions, in addition to the trace and determinant of $\bvec{A}$, we find that the sum of the minors of $\bvec{A}$ (the determinants of the matrices you get when crossing out a row and the corresponding column) is also invariant, which again gives us the invariance of the eigenvalues. This sum of minors is not necessarily the easiest invariant to work with, so instead we can pick another quantity: the trace of $\bvec{A}^2$ (i.e., the sum of the squares of all components), which of course can be expressed as a linear combination of the three invariants we already found.